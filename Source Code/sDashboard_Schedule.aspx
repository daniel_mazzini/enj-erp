﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="sDashboard_Schedule.aspx.cs" Inherits="NewERDM.Dashboard_Schedule" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 
    <link href="plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css" media='print' />
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 <section class="content">
          <div class="row">
            <div class="col-md-3">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h4 class="box-title">Draggable Events</h4>
                </div>
                <div class="box-body">
                  <!-- the events -->
                  <div id='external-events'>
                    <div class='external-event bg-green' id="training">Training</div>
                    <div class='external-event bg-light-blue' id="travel">Travel</div>
                    <div class='external-event bg-red' id="cuti">Cuti</div>
                    <div class='external-event bg-maroon' id="working">Working</div>
                    <div class='external-event bg-gray-active' id="OffDay">Off Day</div>
                    <div class='external-event bg-navy-active' id="Other">Other</div>
                    <!--div class="checkbox">
                      <label for='drop-remove'>
                        <input type='checkbox' id='drop-remove' />
                        remove after drop
                      </label>
                    </div-->
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
              <%--div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Create Event</h3>
                </div>
                <div class="box-body">
                  <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                    <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                    <ul class="fc-color-picker" id="color-chooser">
                      <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>																						
                      <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                    </ul>
                  </div><!-- /btn-group -->
                  <div class="input-group">
                    <input id="new-event" type="text" class="form-control" placeholder="Event Title">
                    <div class="input-group-btn">
                      <button id="add-new-event" type="button" class="btn btn-primary btn-flat">Add</button>
                    </div><!-- /btn-group -->
                  </div><!-- /input-group -->
                </div>
              </div>--%>
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-body no-padding">
                  <!-- THE CALENDAR -->
                  <div id="calendar"></div>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

</asp:Content>

    <asp:Content ID="Content3" ContentPlaceHolderID="Script_tambahan" Runat="Server">
    
        <script type="text/javascript" src="<%= ResolveUrl("js/moment.min.js") %> "></script>
        <script type="text/javascript" src="<%= ResolveUrl("plugins/fullcalendar/fullcalendar.min.js") %> "></script>
        <script type="text/javascript">
             $(function () {

                 /* initialize the external events
                  -----------------------------------------------------------------*/
                 function ini_events(ele) {
                     ele.each(function () {

                         // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                         // it doesn't need to have a start or end
                         var eventObject = {
                             title: $.trim($(this).text()) // use the element's text as the event title
                         };

                         // store the Event Object in the DOM element so we can get to it later
                         $(this).data('eventObject', eventObject);

                         // make the event draggable using jQuery UI
                         $(this).draggable({
                             zIndex: 1070,
                             revert: true, // will cause the event to go back to its
                             revertDuration: 0  //  original position after the drag
                         });

                     });
                 }
                 ini_events($('#external-events div.external-event'));

                 /* initialize the calendar
                  -----------------------------------------------------------------*/
                 //Date for the calendar events (dummy data)
                 var date = new Date();
                 var d = date.getDate(),
                         m = date.getMonth(),
                         y = date.getFullYear();
                 $('#calendar').fullCalendar({
                     header: {
                         left: 'prev,next today',
                         center: 'title',
                         right: 'month'
                     },
                     buttonText: {
                         month: 'month'
                     },
                     eventClick: function(event, jsEvent, view) {                       
                         loadWind();
                     },
                     //events: function(start, end, callback) 
                     //{

                     //},
                     events: [
                        {
                        title: 'Cuti',
                        start: new Date(y, m, 1),
                        end: new Date(y, m, 11),
                        allDay: true,
                        backgroundColor: "#f56954", //red
                        borderColor: "#f56954" //red
                        }
                     ],
                     //Random default events
                     editable: true,
                     droppable: true, // this allows things to be dropped onto the calendar !!!
                     drop: function (date, allDay) {// this function is called when something is dropped

                         // retrieve the dropped element's stored Event Object
                         var originalEventObject = $(this).data('eventObject');

                         // we need to copy it, so that multiple events don't have a reference to the same object
                         var copiedEventObject = $.extend({}, originalEventObject);

                         // assign it the date that was reported
                         copiedEventObject.start = date;
                         copiedEventObject.allDay = allDay;
                         copiedEventObject.backgroundColor = $(this).css("background-color");
                         copiedEventObject.borderColor = $(this).css("border-color");

                         // render the event on the calendar
                         // the last `true` argument determines if the event "sticks (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                         $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                         $('#calendar').fullCalendar.getEvents();

                         // is the "remove after drop" checkbox checked?
                         if ($('#drop-remove').is(':checked')) {
                             // if so, remove the element from the "Draggable Events" list
                             $(this).remove();
                         }
                     
            

                     }
                 });


                 /* ADDING EVENTS */
                 var currColor = "#3c8dbc"; //Red by default
                 //Color chooser button
                 var colorChooser = $("#color-chooser-btn");
                 $("#color-chooser > li > a").click(function (e) {
                     e.preventDefault();
                     //Save color
                     currColor = $(this).css("color");
                     //Add color effect to button
                     $('#add-new-event').css({ "background-color": currColor, "border-color": currColor });
                 });
                 $("#add-new-event").click(function (e) {
                     e.preventDefault();
                     //Get value and make sure it is not null
                     var val = $("#new-event").val();
                     if (val.length == 0) {
                         return;
                     }

                     //Create events
                     var event = $("<div />");
                     event.css({ "background-color": currColor, "border-color": currColor, "color": "#fff" }).addClass("external-event");
                     event.html(val);
                     $('#external-events').prepend(event);

                     //Add draggable funtionality
                     ini_events(event);

                     //Remove event from text input
                     $("#new-event").val("");
                 });
             });
        </script>
           
        <%--<%=s_script %>--%>
         <ext:XScript ID="XScript1" runat="server">
        <script>
            function loadWind() {
                #{WindowSchedule}.show();
            }
      </script>
  </ext:XScript>
        <ext:Window runat="server" ID="WindowSchedule" Hidden="true" ToFrontOnShow="true" Modal="true" FocusOnToFront="true" Height="273" Width="412" 
                        Resizable="false" Title="Schedule" >
            <Items>
                <ext:FormPanel ID="panelReLogin" runat="server" Width="400" Height="240" Frame="true" Border="true" BodyPadding="13" 
                                    DefaultAnchor="100%" UI="Info">
                    <Items>
                        <ext:TextField ID="EmployeeID" runat="server" Hidden="true" />
                        <ext:DateField ID="dDateFrom" runat="server" FieldLabel="Date From " Format="ddd, dd-MMM-yyyy" Width="80" />
                        <ext:DateField ID="dDateTo" runat="server" FieldLabel="Date To " Format="ddd, dd-MMM-yyyy" Width="80" />
                        <ext:ComboBox ID="typeSchedule" runat="server" FieldLabel="Type ">
                            <Items>
                                <ext:ListItem Text="Training" Value="Training" />
                                <ext:ListItem Text="Travel" Value="Travel" />
                                <ext:ListItem Text="Cuti" Value="Cuti" />
                                <ext:ListItem Text="Working" Value="Working" />
                                <ext:ListItem Text="Off Day" Value="Off Day" />
                                <ext:ListItem Text="Other" Value="Other" />
                            </Items>
                        </ext:ComboBox>
                        <ext:TextArea ID="desc" runat="server" FieldLabel="Description" />
                    </Items>
                    <Buttons>
                            <ext:Button runat="server" ID="btn_save_job" Height="30" UI="Success" Html="<b>SAVE DATA</b>" >
                              <DirectEvents>
                                    <Click OnEvent="btn_save_click">
                                        <EventMask ShowMask="true" Msg="Saving Data Please Wait..." />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="btn_close_job" Height="30" UI="Danger"  Html="<b>CLOSE</b>" >
                                   <DirectEvents>
                                       <Click OnEvent="btnClose_click" >
                                           <EventMask ShowMask="true" Msg="Loading..." />
                                       </Click>
                                   </DirectEvents>
                            </ext:Button> 
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>
   </asp:Content>