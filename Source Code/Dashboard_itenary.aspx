﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard_itenary.aspx.cs" Inherits="NewERDM.Dashboard_itenary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <ext:XScript ID="XScript1" runat="server">
            <script>
                var addmaster = function () {
                    var grid = #{grdDetail};
                    grid.editingPlugin.cancelEdit();

                    // Create a record instance through the ModelManager
                    var r = Ext.ModelManager.create({
                        f_dFligt       : '', 
                        f_routefrom    : '',
                        f_routeTo      : '',
                        f_flightdetail : '',
                        f_airline      : '',
                        f_timelimit    : '',
                        f_CancelCode   : '',
                        f_paymentStat  : '',
                        f_costcode     : '',
                        f_status       : ''
                    }, 'data');
                    grid.store.insert(0, r);
                    grid.editingPlugin.startEdit(0, 0);
                };


         var saveDataWaiting = function ()
             {
                App.Hidden1.setValue(Ext.encode(App.grdWaitingList.getRowsValues({ selectedOnly: false })));
            };
   </script>
    </ext:XScript>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <ext:Hidden ID="Hidden1" runat="server" />
     <div class='row'> 
           <section class="col-lg-4 connectedSortable">
              <div class="box box-warning">
                <div class="box-header with-border">
                   <div class="box-tools pull-right">
                    <h3 class="box-title"><b class="text-yellow">Waiting List Flight&nbsp;&nbsp;</b></h3>
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                          <ext:GridPanel id="grdWaitingList" runat="server" Frame="false" Height="360px" Header="false" Border="false" Cls="x-grid-custom" HideHeaders="true" >
                                    <Store>
				                        <ext:Store ID="Store_waiting_list" RemoteSort="true" runat="server" PageSize="9" OnReadData="Store_waiting_list_RefreshData">
						                    <Model>
							                    <ext:Model ID="Model3" runat="server" IDProperty="BookingCode"  >
								                    <Fields>
                                                        <ext:ModelField Name="Nama" />
                                                        <ext:ModelField Name="BookingCode" />
                                                        <ext:ModelField Name="DateOfFlight" SortDir="DESC"/>
										                <ext:ModelField Name="RouteFrom"  />
										                <ext:ModelField Name="RouteTo"  />
                                                        <ext:ModelField Name="ETD"  />
                                                        <ext:ModelField Name="Status"  />
                                                    </Fields>
							                    </ext:Model>
						                    </Model>
 <%--                                           <Sorters>
                                                <ext:DataSorter Property="DateOfFlight" Direction="DESC" />
                                            </Sorters> --%>
				                        </ext:Store>
                                    </Store>                 
                                    <ColumnModel  ID="ColumnModel2"  runat="server" >
				                        <Columns>
                                            <ext:DateColumn runat="server" ID="DescFilter" DataIndex="DateOfFlight" Visible="false" Format="yyyyMMdd" ></ext:DateColumn>
                                         <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="30"   >
	                                         <Template runat="server">
		                                         <Html>
                                                     <img src="images/seat.png" />
			                                      </Html>
	                                         </Template>
                                         </ext:TemplateColumn>
                                        <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Flex="1" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
				                                         {Nama}<br />
				                                         <font color="blue"><b>{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br />

			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>
                                           <ext:TemplateColumn runat="server" MenuDisabled="true" Header="Template">
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
				                                         {RouteFrom}-{RouteTo}<br />
				                                         <font color="red"><b>{DateOfFlight}</b></font><br />

			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>  
                                        </Columns>
			                        </ColumnModel>
                                    <DirectEvents>
                                        <CellDblClick OnEvent="btn_edit_doubleclick" >
                                            <EventMask ShowMask="true" />
                                            <ExtraParams>
	                                            <ext:Parameter Name="Values" Value="Ext.encode(#{grdWaitingList}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
                                            </ExtraParams>
                                        </CellDblClick>
                                    </DirectEvents>
                                    <BottomBar >
                                        <ext:PagingToolbar runat="server" ID="paging1"  DisplayInfo="false" DisplayMsg="false" >
                                           <Items>
                                                <ext:Button runat="server"  ID="btnExportWaitingList"  Html="<b>EXPORT</b>" Cls="btn btn-warning btn-flat" ToolTipType="Qtip" ToolTip="<b>EXPORT TO EXCEL (Waiting List)" >
                                                     <DirectEvents>
                                                         <Click OnEvent="ToExcelWaiting" IsUpload="true">

                                                         </Click>
                                                     </DirectEvents>
                                                </ext:Button> 
                                           </Items>
                                        </ext:PagingToolbar>
                                    </BottomBar>
				                </ext:GridPanel>
                </div><!-- /.box-body -->
               </div> 
              
            </section>
           <section class="col-lg-8 connectedSortable">
            <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Date range"><i class="fa fa-calendar"></i></button>
                    <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->

                  <i class="fa fa-map-marker"></i>
                  <h3 class="box-title">
                    Itenary
                  </h3>
                </div>
                <div class="box-body">
                  <div id="world-map" style="height: 250px; width: 100%;"></div>
                </div><!-- /.box-body-->
                <div class="box-footer no-border">
                  <div class="row">
                    <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                      <div id="sparkline-1"></div>
                      <div class="knob-label">Visitors</div>
                    </div><!-- ./col -->
                    <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                      <div id="sparkline-2"></div>
                      <div class="knob-label">Online</div>
                    </div><!-- ./col -->
                    <div class="col-xs-4 text-center">
                      <div id="sparkline-3"></div>
                      <div class="knob-label">Exists</div>
                    </div><!-- ./col -->
                  </div><!-- /.row -->
                </div>
              </div>
              <!-- /.box -->
</section>
     </div>
     <div class='row'>         
           <div class="col-md-4 connectedSortable">
              <div class="box box-primary">
                <div class="box-header with-border">
                  
                  <div class="box-tools pull-right">
                      <b><ext:Label runat="server" ID="txtDateNow" Width="140px" /></b>
                    <h3 class="box-title"><b class="text-red">Today Flight&nbsp;&nbsp;</b></h3>
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                       <ext:GridPanel id="grdToday" runat="server" Frame="false" Height="240px"   Header="false"  Border="false" Cls="x-grid-custom" HideHeaders="true"    >
                                    <Store>
				                          <ext:Store ID="Store_Notif" runat="server" PageSize="3" OnReadData="Store_Notif_RefreshData" >
							                    <Model>
								                    <ext:Model ID="Model1" runat="server"  IDProperty="BookingCode" >
									                    <Fields>
                                                            <ext:ModelField Name="Nama" />
                                                            <ext:ModelField Name="BookingCode" />
                                                            <ext:ModelField Name="DateOfFlight" />
										                    <ext:ModelField Name="RouteFrom" />
										                    <ext:ModelField Name="RouteTo" />
                                                            <ext:ModelField Name="ETD" />
                                                            <ext:ModelField Name="Status" />
                                                        </Fields>
								                    </ext:Model>
							                    </Model>
				                            </ext:Store>
                                    </Store>                 
                                    <ColumnModel ID="ColumnModel1" runat="server" >
				                        <Columns>
                                          <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template"   Flex="1" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
                                                         <b>{Nama}</b>
                                                         <br /><br />
                                                         <img src="images/flown.png" />
				                                         <font color="blue"><b>&nbsp;&nbsp;{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br /><br />
			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>
                                         <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for="."><br />
				                                         {RouteFrom}-{RouteTo}<br />
				                                         <font color="blue"><b>{DateOfFlight}</b></font><br />
			                                         </tpl>
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>  
                                        <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="30px" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
				                                         <br />
				                                         <font color="green"><b>{Status}</b></font><br />
			                                         </tpl>
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>
                                            <ext:CommandColumn ID="DownloadPdf" runat="server" Width="40">
                                                    <Commands>
                                                        <ext:GridCommand Icon="Attach" CommandName="download"> 
                                                            <ToolTip Text="download" />
                                                        </ext:GridCommand>
                                                    </Commands>
                                                    <DirectEvents>
                                                        <Command OnEvent="Download_Click" IsUpload="true">
                                                            <ExtraParams>
                                                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
									                        </ExtraParams>
                                                        </Command>
                                                    </DirectEvents>
                                                </ext:CommandColumn>  
                                        </Columns>
			                        </ColumnModel>
                                        <DirectEvents>
                                            <CellDblClick OnEvent="btn_edit_doubleclick" >
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
                                                    <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
                                                </ExtraParams>
                                            </CellDblClick>
                                        </DirectEvents>                                                    
                                    <BottomBar >
                                        <ext:PagingToolbar runat="server" ID="PagingToolbar1"  DisplayInfo="false" DisplayMsg="false" >
                                           <Items>
                                                <ext:TextField runat="server" ID="upload" Hidden="true" />
                                           </Items>
                                        </ext:PagingToolbar>
                                    </BottomBar>
				                </ext:GridPanel>
                   <br /> 
                       <ext:Panel runat="server"  Header="false" Frame="false" FrameHeader="false" Border="false" >
                           <Buttons>
                                  <ext:Button runat="server" ID="btnNew" Cls="btn btn-success"  Html="<b>Add New</b>" >
                                      <DirectEvents >
                                            <Click OnEvent="btnNew_click" >
                                                 <EventMask ShowMask="true"  Msg="Loading ..." />
                                            </Click>
                                      </DirectEvents>
                                    </ext:Button>
                                  <ext:Button runat="server" ID="btnExport2" Cls="btn btn btn-danger"  Html="<b>Export&nbsp;&nbsp;&nbsp;&nbsp;</b>" >
                                                     <DirectEvents>
                                                         <Click OnEvent="ToExcelToday" IsUpload="true">

                                                         </Click>
                                                     </DirectEvents>
                                  </ext:Button> 
                           </Buttons>
                       </ext:Panel>
                      </div><!-- /.box-body -->
                
              </div><!-- /.box -->
            </div>
           <div class="col-md-4 connectedSortable">
              <div class="box box-success">
                <div class="box-header with-border">
                  <div class="box-tools pull-right">
                    <h3 class="box-title"><b class="text-red">Payment In Progress</b>&nbsp;&nbsp;</h3>
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <ext:GridPanel id="grdPayment" runat="server" Frame="false"   Height="290px"   Header="false"  Border="false" Cls="x-grid-custom" HideHeaders="true">
                                   <Store>
				                            <ext:Store ID="Store_paid_process_rev" runat="server" PageSize="5" OnReadData="Store_paid_process_RefreshData">
                                                <Model>
								                    <ext:Model ID="Model2" runat="server" IDProperty="DateOfFlight" >
									                    <Fields>
                                                            <ext:ModelField Name="Nama" />
                                                            <ext:ModelField Name="BookingCode" />
                                                            <ext:ModelField Name="DateOfFlight" />
										                    <ext:ModelField Name="RouteFrom"  />
										                    <ext:ModelField Name="RouteTo"  />
                                                            <ext:ModelField Name="PaymentStatus"  />
                                                            <ext:ModelField Name="ETD"  />
                                                        </Fields>
								                    </ext:Model>
							                    </Model>
				                            </ext:Store>
                                        </Store>    
                                    <ColumnModel  ID="ColumnModel3"  runat="server" >
				                        <Columns>
                                       <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template"  Width="30" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl >
                                                        <img src="images/payment.png" />
			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>
                                          <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template"   Flex="1" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
                                                         <b>{Nama}</b><br />
                                                         <font color="blue"><b>&nbsp;&nbsp;{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br /><br />
			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>
                                         <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
                                                         {DateOfFlight}
				                                         <br />
				                                         <font color="blue"><b>{RouteFrom}-{RouteTo}</b></font><br />

			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>  
                                        <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template"   >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for="."><br>
				                                        <font color="green"><b>{PaymentStatus}</b></font><br />

			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>  
                                        </Columns>
			                        </ColumnModel>
                    
                                    <DirectEvents>
                                            <CellDblClick OnEvent="btn_edit_doubleclick" >
                                                <EventMask ShowMask="true" />
                                                <ExtraParams>
	                                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdPayment}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
                                                </ExtraParams>
                                            </CellDblClick>
                                    </DirectEvents>
                                   
                                     <BottomBar >
                                        <ext:PagingToolbar runat="server" ID="PagingToolbar2" DisplayInfo="false" DisplayMsg="false" >
                                           <Items>
                                              <ext:Button runat="server" ID="Button1" Html="<b>EXPORT</b>" Cls="btn btn-danger" >
                                               <DirectEvents>
                                                         <Click OnEvent="ToExcelPayment" IsUpload="true">

                                                         </Click>
                                                     </DirectEvents>
                                              </ext:Button> 
                                           </Items>
                                        </ext:PagingToolbar>
                                               
                                    </BottomBar>
                                    
				                </ext:GridPanel>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
     </div>
  

</asp:Content>
