﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="mail.aspx.cs" Inherits="NewERDM.mail.mail" %>
  <asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <script type="text/javascript">
        function getStatusIcon(status){
            switch(status){
                case 1:
                    return "icon-email";
                case 2:
                    return "icon-emailopen";
            }
            
            return "";
        }
    </script>  
    
  </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
         <section class="content">
          <div class="row">
            <div class="col-md-3">
              <a href="<%= ResolveUrl("compose_mail.aspx") %>" class="btn btn-primary btn-block margin-bottom">Compose</a>
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Folders</h3>
                </div>
                <div class="box-body no-padding">
                  <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="#"><i class="fa fa-inbox"></i> Inbox <span class="label label-primary pull-right"><ext:Label runat="server"  ID="tInbox"/></span></a></li>
                    <li><a href="#"><i class="fa fa-envelope-o"></i> Sent</a></li>
                    <li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li>
                  </ul>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
             

            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Inbox</h3>
                  <div class="box-tools pull-right">
                    <div class="has-feedback">
                      <input type="text" class="form-control input-sm" placeholder="Search Mail"/>
                      <span class="glyphicon glyphicon-search form-control-feedback"></span>
                    </div>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="mailbox-controls">
                    <!-- Check all button -->
                    <button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                    <div class="btn-group">
                      <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                      <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                      <button class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                    </div><!-- /.btn-group -->
                    <button class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                    
                  </div>
                  <div class="table-responsive mailbox-messages">
                   <ext:XTemplate ID="emailtpl" runat="server">
                        <Html>
                            {Body}
                        </Html>
                    </ext:XTemplate>
                  <ext:Panel 
                    runat="server"
                    Frame="true"
                    Height="450"
                    Layout="BorderLayout">
                    <Items>
                       <ext:GridPanel  id="gvEmails"  runat="server"  Flex="1"  RowLines="false" Frame="false" HideHeaders="true"
                  Padding="2" Header="false"   Cls="table table-hover table-striped"   Region="North"   Split="true"    Border="false"     >
                                     <Store>
									     <ext:Store runat="server" ID="store_email" PageSize="22"  OnReadData="store_email_RefreshData" >
										       <Model>
											       <ext:Model ID="Model4" runat="server"   >
												    <Fields>
                                                        <ext:ModelField Name="From" Type="String" />
							                            <ext:ModelField Name="Subject" />
                                                        <ext:ModelField Name="DateSent"   />
                                                        <ext:ModelField Name="Body" />
                                                        <ext:ModelField Name="Sender" />
													</Fields>
											    </ext:Model>
										       </Model>
									     </ext:Store>
							     </Store>
                         
						 	      <ColumnModel ID="ColumnModel1"  runat="server"> 
							           <Columns >       
                                           <%--<ext:CheckColumn runat="server" ID="chk" Width="30"/>--%>
                                           <ext:Column runat="server" ID="cfrom" DataIndex="Sender" Flex="1"  />
                                           <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Flex="1"  Height="30"  >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
				                                         <font color="black"><b>{Subject}</b></font>&nbsp;&nbsp;
			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>
                                         <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="180"  >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
				                                         <font color="black"><b>{DateSent}</b></font>
			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>
                                       </Columns>
							     </ColumnModel>
                                  <Listeners>
                                   <SelectionChange Handler="selected.length && #{emailtpl}.overwrite(#{DetailPanel}.body, selected[0].data);" />
                                 </Listeners>
                         	      <BottomBar>
									     <ext:PagingToolbar ID="PagingToolbar4" runat="server"  DisplayInfo="false" StoreID="store_employee"    />
							     </BottomBar>
                                 <SelectionModel>
                                    <ext:CheckboxSelectionModel runat="server" Mode="Multi" />
                                 </SelectionModel> 
						      </ext:GridPanel>
                         <ext:Panel 
                            ID="DetailPanel" 
                            runat="server"
                            Region="Center"
                            BodyPadding="2"
                            BodyStyle="background: #ffffff;" AutoScroll="true"   
                             Title="Message : ">                
                        </ext:Panel>
                       </Items>
                    </ext:Panel>
         
                  </div><!-- /.mail-box-messages -->
                </div><!-- /.box-body -->
            
              </div><!-- /. box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
</asp:Content>