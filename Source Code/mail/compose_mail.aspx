﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="compose_mail.aspx.cs" Inherits="NewERDM.mail.compose_mail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script type="text/javascript">
        function getStatusIcon(status){
            switch(status){
                case 1:
                    return "icon-email";
                case 2:
                    return "icon-emailopen";
            }
            
            return "";
        }
    </script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ext:FormPanel runat="server" ID="frmverified" Title =""  BodyPadding="12" Header="false"  Frame="false"  Border="false" DisabledCls="true">
                <Items>
                    <ext:TextField ID="emailPengirim" runat="server" Hidden="true" />
                    <ext:TextField ID="emailPenerima" runat="server" FieldLabel="To " Width="500" />
                    <ext:TextField ID="emailSubject" runat="server" FieldLabel="Subject " Width="500" />
                    <ext:TextArea ID="emailMessage" runat="server" FieldLabel="Message " Width="500" Height="200" />
                    <ext:TextField ID="emailAttach" runat="server" FieldLabel="Attach " Width="500" ReadOnly="true" />
                </Items>
            </ext:FormPanel>
            <div class="mailbox-controls">
                    <!-- Check all button -->
                    <button class="btn btn-default btn-sm"><i class="fa fa-send-o"></i> Send</button>
                    <div class="btn-group">
                      <button class="btn btn-default btn-sm"><i class="ion-android-attach"></i></button>
                    </div><!-- /.btn-group -->                 
            </div>
        </div>
    </div>
</asp:Content>

