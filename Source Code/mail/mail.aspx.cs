﻿using OpenPop.Common.Logging;
using OpenPop.Mime;
using OpenPop.Mime.Header;
using OpenPop.Pop3;
using OpenPop.Pop3.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using NewERDM.lib.BusinessClass;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using SpreadsheetLight;


namespace NewERDM.mail
{
    public partial class mail : System.Web.UI.Page
    {

        UserApplication userApp;
        UserApplicationBL usrApplicationBL = new UserApplicationBL();

        protected void Page_Load(object sender, EventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];
          // if (!IsPostBack)
           //{
               Read_Emails();
           //}

         }

        void Read_Emails()
        {
            Pop3Client pop3Client;
            if (Session["Pop3Client"] == null)
            {
                string emailAddress;
                string emailPass;
                pop3Client = new Pop3Client();

                DataTable dtEmployee = new DataTable();
                var objGetEmp = new clsDashboardUser();
                dtEmployee = objGetEmp.get_EmployeeEmail(userApp.EmployeeID);

                if (dtEmployee.Rows.Count > 0)
                {
                    emailAddress = dtEmployee.Rows[0]["Email_Address"].ToString();
                    emailPass = dtEmployee.Rows[0]["Email_Password"].ToString();

                    
                    pop3Client.Connect("mail.enj.co.id", 110, false);
                    pop3Client.Authenticate(emailAddress, emailPass);
                    Session["Pop3Client"] = pop3Client;
                }
                //else
                //{
                //    X.MessageBox.Show(new MessageBoxConfig
                //    {
                //        Title = "WARNING",
                //        Message = "EMPLOYEE NAME CAN'T BE EMPTY !",
                //        Buttons = MessageBox.Button.OK,
                //        Icon = MessageBox.Icon.WARNING
                //    });

                //    X.Redirect(CommonUtility.ResolveUrl("Dashboard_itenary.aspx"), "Loading ... ");
                //}
            }
            else
            {
                pop3Client = (Pop3Client)Session["Pop3Client"];
            }

            
            int messageCount = pop3Client.GetMessageCount();
            this.tInbox.Html= string.Format("<b>{0}</b>", messageCount);
            this.Emails = new List<Email>();
            int counter = 0;
            for (int i = messageCount; i >= 1; i--)
            {
                Message message = pop3Client.GetMessage(i);
                Email email = new Email()
                {
                    MessageNumber = i,
                    Subject = message.Headers.Subject,
                    DateSent = message.Headers.DateSent,
                    From = string.Format("<a href = {1}'>{0}</a>", message.Headers.From.DisplayName, message.Headers.From.Address),
                    Sender = message.Headers.From.DisplayName,
                };
                MessagePart body = message.FindFirstHtmlVersion();
                if (body != null)
                {
                    email.Body = body.GetBodyAsText();
                }
                else
                {
                    body = message.FindFirstPlainTextVersion();
                    if (body != null)
                    {
                        email.Body = body.GetBodyAsText();
                    }
                }
                List<MessagePart> attachments = message.FindAllAttachments();

                foreach (MessagePart attachment in attachments)
                {
                    email.Attachments.Add(new Attachment
                    {
                        FileName = attachment.FileName,
                        ContentType = attachment.ContentType.MediaType,
                        Content = attachment.Body
                    });
                }
                this.Emails.Add(email);
                counter++;
                if (counter > 2)
                {
                    break;
                }
            }

            store_email.DataSource = this.Emails;
            store_email.DataBind();

        }

        protected void store_email_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            Read_Emails();
        }

        protected List<Email> Emails
        {
            get { return (List<Email>)ViewState["Emails"]; }
            set { ViewState["Emails"] = value; }
        }

        [Serializable]
        public class Email
        {
            public Email()
            {
                this.Attachments = new List<Attachment>();
            }
            public int MessageNumber { get; set; }
            public string From { get; set; }
            public string Sender{ get; set; }
            public string Subject { get; set; }
            public string Body { get; set; }
            public DateTime DateSent { get; set; }
            public List<Attachment> Attachments { get; set; }
        }

        [Serializable]
        public class Attachment
        {
            public string FileName { get; set; }
            public string ContentType { get; set; }
            public byte[] Content { get; set; }
        }

    }
}