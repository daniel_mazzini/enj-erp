﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using NewERDM.lib.BusinessClass;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using SpreadsheetLight;

namespace NewERDM
{
    public partial class Dashboard_Schedule : System.Web.UI.Page
    {
        #region Field

        UserApplication userApp;
        UserApplicationBL usrApplicationBL = new UserApplicationBL();
        
        //string s_script = "//aku di tulis disiini";

        #endregion Field

        #region Properties

        #endregion Properties

        #region Method

        #endregion Method

        protected void Page_Load(object sender, EventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];
            if (userApp == null)
            {

            }
            else
            {
                this.EmployeeID.Text = userApp.EmployeeID;
            }
            //Response.Write("<b>test</b>");
        }

        protected void btn_save_click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(this.EmployeeID.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Time Out Login, Please Logout and Login !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else if (this.dDateFrom.Text == null)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Please Insert Date From !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else if (this.dDateTo.Text == null)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Please Insert Date To !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else if (string.IsNullOrEmpty(this.typeSchedule.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Please Insert Type Schedule !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else if (string.IsNullOrEmpty(this.desc.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Please Insert Description !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                var objInsertSchedule = new clsSchedule();
                objInsertSchedule.InsertSchedule(this.EmployeeID.Text, Convert.ToDateTime(this.dDateFrom.Text), Convert.ToDateTime(this.dDateTo.Text),
                                                  this.typeSchedule.Text, this.desc.Text);

                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Information",
                    Message = "Save Data Success",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO
                });
            }
        }
        protected void btnClose_click(object sender, DirectEventArgs e)
        {
            this.WindowSchedule.Hide();
        }
    }
}