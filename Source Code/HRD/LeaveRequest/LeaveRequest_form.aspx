﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LeaveRequest_form.aspx.cs" Inherits="NewERDM.HRD.LeaveRequest.LeaveRequest_form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <ext:XScript ID="XScript1" runat="server">
	    <script>
	        var addmaster = function () {
	            var grid = #{grdDetail};
	            grid.editingPlugin.cancelEdit();

	            // Create a record instance through the ModelManager
	            var r = Ext.ModelManager.create({
	                workingDay       : '',
	                dayExcluded      : '',
	                dayCobus         : '',
	                dayDeducted      : '',
	                projectedAccrual : '',
	                projectedBalance : ''
	            }, 'data');
	            grid.store.insert(0,r)
                grd.editingPlugin.startEdit(0,0)
	        };
	    </script>
    </ext:XScript>
    <style>
    	.x-column-header-inner .x-column-header-text {
    		white-space: normal;
    	}
        .x-grid-row-over .x-grid-cell-inner {
    		font-weight : bold;
    	}
    	.ux-my-class {
    		white-space: normal;
            align-content: center/* Here your CSS */
    	}
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ext:FormPanel runat="server" ID="frmverified" Title ="" BodyPadding="12" Header="false" Frame="false" Border="false" DisabledCls="true">
                <Items>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:FormPanel runat="server" ID="FormPanel1" Title ="User Information" BodyPadding="12" Width="1080" Frame="false" Border="false" DisabledCls="true">
                                <Items>
                                    <ext:Label runat="server" Text="You Are " />
                                    <ext:Label runat="server" Text="" Width="5" />
                                    <ext:Label runat="server" ID="lblUserid" />
                                    <ext:Label runat="server" Text="." />
                                    <ext:Label runat="server" Text="" Width="5" />
                                    <ext:Label runat="server" Text="You Request leave for " />
                                    <ext:Label runat="server" Text="" Width="5" />
                                    <ext:Label runat="server" ID="lblUserid1" />
                                    <ext:Label runat="server" Text="." />
                                    <ext:TextField runat="server" ID="lblUserEmployee" Hidden="true" />
                                    <ext:DateField runat="server" ID="dateHired" />
                                    <ext:DateField runat="server" ID="dateNow" />
                                </Items>
                            </ext:FormPanel>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:FormPanel runat="server" ID="FormPanel2" Title ="STEP 1 - Enter Leave Plan" BodyPadding="12" Width="1080" Frame="false" Border="false" DisabledCls="true">
                                <Items>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:RadioGroup runat="server" FieldLabel="Are you shift employee? " ColumnsNumber="1" LabelWidth="150" AutomaticGrouping="false">
                                                <Items>
                                                    <ext:Radio runat="server" Name="shift" InputValue="0" BoxLabel="No" /> 
                                                    <ext:Radio runat="server" Name="shift" InputValue="1" BoxLabel="Yes" /> 
                                                </Items>
                                            </ext:RadioGroup>
                                            <ext:Label runat="server" Text="" Width="350" />
                                            <ext:TextField runat="server" ID="BalanceLeave" Text="12" FieldLabel="Your Available Leave Balance is " LabelWidth="180" Width="210" Disabled="true" />
                                            <ext:TextField runat="server" ID="BalanceLeaveTemp" Text="12" Hidden="true" />
                                            <ext:Label runat="server" Text="" Width="10" />
                                            <ext:Label runat="server" Text="working days." Disabled="true" />
                                        </Items>
                                    </ext:FieldContainer>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:DateField runat="server" ID="startDate" FieldLabel="Leave Start Date " LabelWidth="150" Format="ddd, dd-MMM-yyyy">
                                                <DirectEvents>
                                                    <Change OnEvent="hitungTanggal_Click" />
                                                </DirectEvents>
                                            </ext:DateField>
                                            <ext:Label runat="server" Text="" Width="82" />
                                            <ext:DateField runat="server" ID="endDate" FieldLabel="Leave End Date " LabelWidth="180" Format="ddd, dd-MMM-yyyy" >
                                                <DirectEvents>
                                                    <Change OnEvent="hitungTanggal_Click" />
                                                </DirectEvents>
                                            </ext:DateField>
                                            <ext:Label runat="server" Text="" Width="10" />
                                            <ext:Label runat="server" ID="lblJumlahLeave" />
                                            <ext:TextField runat="server" ID="jumlahLeaveTemp" Hidden="true" />
                                        </Items>
                                    </ext:FieldContainer>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:RadioGroup runat="server" FieldLabel="Purpose of Leave " ColumnsNumber="1" LabelWidth="150" AutomaticGrouping="false">
                                                <Items>
                                                    <ext:Radio runat="server" Name="purpose" InputValue="0" BoxLabel="Offsite Leave" /> 
                                                    <ext:Radio runat="server" Name="purpose" InputValue="1" BoxLabel="Onsite or Local Leave" /> 
                                                </Items>
                                            </ext:RadioGroup>
                                            <ext:Label runat="server" Text="" Width="255" />
                                            <ext:TextField runat="server" ID="ContactNo" FieldLabel="Contact No. During Leave " LabelWidth="182" />
                                        </Items>
                                    </ext:FieldContainer>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:TextArea runat="server" ID="Description" FieldLabel="Purpose Description" LabelWidth="150" Width="500" />
                                        </Items>
                                    </ext:FieldContainer>
                                </Items>
                            </ext:FormPanel>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:FormPanel runat="server" ID="FormPanel3" Title ="STEP 2 - For Special Leave not deducted from your leave balance(ie. public holiday, medical, etc.)" BodyPadding="12" Width="1080" Frame="false" Border="false" DisabledCls="true">
                                <Items>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:Checkbox runat="server" ID="chkPublicHoliday" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Public Holiday" >
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="123" />
                                            <ext:TextField runat="server" ID="publicHoliday" Disabled="true" Width="30" />
                                            <ext:Label runat="server" Text="" Width="100" />
                                            <ext:Checkbox runat="server" ID="chkDayOff" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Day off during leave" >
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="17" />
                                            <ext:TextField runat="server" ID="dayOff" Disabled="true" Width="30"/>
                                            </Items>
                                    </ext:FieldContainer>
                                     <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:Checkbox runat="server" ID="chkMedivac" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Medivac" >
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="158" />
                                            <ext:TextField runat="server" ID="medivac" Disabled="true" Width="30"/>
                                            <ext:Label runat="server" Text="" Width="100" />
                                            <ext:Checkbox runat="server" ID="chkLeaveWithoutPay" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Leave Without Pay" >
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="25" />
                                            <ext:TextField runat="server" ID="leaveWithoutPay" Disabled="true" Width="30" />
                                        </Items>
                                    </ext:FieldContainer>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:Checkbox runat="server" ID="chkPilgrimage" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Pilgrimage" >
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="146" />
                                            <ext:TextField runat="server" ID="pilgrimage" Disabled="true" Width="30" />
                                            <ext:Label runat="server" Text="" Width="100" />
                                            <ext:Checkbox runat="server" ID="chkMarriageChild" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Marriage of Child">
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="34" />
                                            <ext:TextField runat="server" ID="marriageChild" Disabled="true" Width="30" />
                                        </Items>
                                    </ext:FieldContainer>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:Checkbox runat="server" ID="chkTraining" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Training" >
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="158" />
                                            <ext:TextField runat="server" ID="training" Disabled="true" Width="30" />
                                            <ext:Label runat="server" Text="" Width="100" />
                                            <ext:Checkbox runat="server" ID="chkFamilyFatching" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Family Fetching" >
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="41" />
                                            <ext:TextField runat="server" ID="familyFatching" Disabled="true" Width="30" />
                                        </Items>
                                    </ext:FieldContainer>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:Checkbox runat="server" ID="chkBirthOfChild" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Birth of Child/Circumcision/Baptis">
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="20" />
                                            <ext:TextField runat="server" ID="birthOfChild" Disabled="true" Width="30" />
                                            <ext:Label runat="server" Text="" Width="100" />
                                            <ext:Checkbox runat="server" ID="chkLongSickness" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Long Sickness">
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="50" />
                                            <ext:TextField runat="server" ID="longSickness" Disabled="true" Width="30"/>
                                        </Items>
                                    </ext:FieldContainer>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:Checkbox runat="server" ID="chkMaternity" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Maternity">
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="152" />
                                            <ext:TextField runat="server" ID="maternity" Disabled="true" Width="30" />
                                            <ext:Label runat="server" Text="" Width="100" />
                                            <ext:Checkbox runat="server" ID="chkMarriageEmployee" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Marriage of Employee">
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="10" />
                                            <ext:TextField runat="server" ID="marriageEmployee" Disabled="true" Width="30"/>
                                        </Items>
                                    </ext:FieldContainer>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:Checkbox runat="server" ID="chkMedicalCheckUp" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Medical Check Up">
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="105" />
                                            <ext:TextField runat="server" ID="medicalCheckup" Disabled="true" Width="30" />
                                            <ext:Label runat="server" Text="" Width="100" />
                                            <ext:Checkbox runat="server" ID="chkPaternity" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Paternity">
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="80" />
                                            <ext:TextField runat="server" ID="paternity" Disabled="true" Width="30"/>
                                        </Items>
                                    </ext:FieldContainer>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:Checkbox runat="server" ID="chkContractCompletion" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Contract Completion">
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="90" />
                                            <ext:TextField runat="server" ID="contractCompletion" Disabled="true" Width="30" />
                                            <ext:Label runat="server" Text="" Width="100" />
                                        </Items>
                                    </ext:FieldContainer>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:Checkbox runat="server" ID="chkVisa" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Visa Renewal">
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="132" />
                                            <ext:TextField runat="server" ID="visaRenewal" Disabled="true" Width="30" />
                                            <ext:Label runat="server" Text="" Width="100" />
                                        </Items>
                                    </ext:FieldContainer>
                                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                        <Items>
                                            <ext:Checkbox runat="server" ID="chkParliament" Checked="false" StyleSpec="margin-bottom:10px;" BoxLabel="Parliament">
                                                <DirectEvents>
                                                   <Change OnEvent="Disable_Click" />
                                                </DirectEvents>
                                            </ext:Checkbox>
                                            <ext:Label runat="server" Text="" Width="146" />
                                            <ext:TextField runat="server" ID="parliament" Disabled="true" Width="30" />
                                            <ext:Label runat="server" Text="" Width="100" />
                                        </Items>
                                    </ext:FieldContainer>
                                </Items>
                            </ext:FormPanel>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:FormPanel runat="server" ID="FormPanel4" Title ="STEP 3 - for CUBUS Leave, click here" BodyPadding="12" Width="1080" Frame="false" Border="false" DisabledCls="true">
                                <Items>

                                </Items>
                            </ext:FormPanel>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:FormPanel runat="server" ID="FormPanel5" Title="Leave Balance Calculation" BodyPadding="12" Width="1070" Frame="false" Border="false" DisabledCls="true">
                                <Items>
                                    <ext:GridPanel ID="grdDetail" runat="server"   TabIndex="7" region="Center" Layout="FitLayout" Height="150" Padding="8" EnableColumnHide="false" SortableColumns="false">
                                        <Store>
                                             <ext:Store runat="server" ID="Store_Detail" PageSize="20" OnReadData="Store_Detail_refreshData">
                                                <Model>
                                                    <ext:Model ID="Model1" runat="server" IDProperty="employeeID" Name="data">
                                                        <Fields>
                                                            <ext:ModelField Name="pid" />
                                                            <ext:ModelField Name="employeeID" />
                                                            <ext:ModelField Name="TotalWorking" />
                                                            <ext:ModelField Name="DayExcluded" />
                                                            <ext:ModelField Name="Cobus" />
                                                            <ext:ModelField Name="DayDeducted" />
                                                            <ext:ModelField Name="ProjectAccrual" />
                                                            <ext:ModelField Name="ProjectLeaveBalance" />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                        <ColumnModel ID="ColumnModel1" runat="server">        
                                            <Columns>
                                                <ext:Column ID="Column7" runat="server" Text="pid" DataIndex="pid" Hidden="true" Width="150"  >
                                                    <Editor>
                                                        <ext:TextField runat="server" ID="pid" />
                                                    </Editor>
                                                </ext:Column>
                                                <ext:Column ID="Column8" runat="server" Text="pid" DataIndex="employeeID" Hidden="true" Width="150"  >
                                                    <Editor>
                                                        <ext:TextField runat="server" ID="employeeID" />
                                                    </Editor>
                                                </ext:Column>
                                                <ext:Column ID="Column1" runat="server" Text="Total Working days during leave(Mon - Fri)" DataIndex="TotalWorking" Width="150" Align="Center" >
                                                    <Editor>
                                                        <ext:TextField runat="server" ID="TotalWorking" />
                                                    </Editor>
                                                </ext:Column>
                                                <ext:Column ID="Column2" runat="server" Text="Day Excluded due to Special Leave" DataIndex="DayExcluded" Width="180" Align="Center">
                                                    <Editor>
                                                        <ext:TextField runat="server" ID="DayExcluded" />
                                                    </Editor>
                                                </ext:Column>
                                                <ext:Column ID="Column3" runat="server" Text="Day Excluded due to Cobus" DataIndex="Cobus" Width="150" Align="Center">
                                                    <Editor>
                                                        <ext:TextField runat="server" ID="Cobus" />
                                                    </Editor>
                                                </ext:Column>
                                                <ext:Column ID="Column4" runat="server" Text="Days to be deducted from your Leave Balance" DataIndex="DayDeducted" Width="240" Align="Center">
                                                    <Editor>
                                                        <ext:TextField runat="server" ID="DayDeducted" />
                                                    </Editor>
                                                </ext:Column>
                                                <ext:Column ID="Column5" runat="server" Text="Projected Accrual" DataIndex="ProjectAccrual" Width="100" Align="Center">
                                                    <Editor>
                                                        <ext:TextField runat="server" ID="ProjectAccrual" />
                                                    </Editor>
                                                </ext:Column>
                                                <ext:Column ID="Column6" runat="server" Text="Projected Leave Balance (after leave taken)" DataIndex="ProjectLeaveBalance" Width="208" Align="Center">
                                                    <Editor>
                                                        <ext:TextField runat="server" ID="ProjectLeaveBalance" />
                                                    </Editor>
                                                </ext:Column>
                                            </Columns>
                                        </ColumnModel>
                                        <Plugins>
                                            <ext:CellEditing runat="server" ClicksToEdit="1" />
                                        </Plugins>
                                    </ext:GridPanel>
                                </Items>
                                <Buttons>
                                    <ext:Button runat="server" ID="btnCalculate" Text="Recalculate Leave Balance" UI="Primary">
                                        <DirectEvents>
                                            <Click OnEvent="btnCalculate_Click">
                                                <EventMask ShowMask="true" Msg="Loading Calculate" />
                                                 <ExtraParams>
									                <ext:Parameter Name="Value" Value="Ext.encode(#{grdDetail}.getRowsValues({selectedOnly : false}))" Mode="Raw">
									                </ext:Parameter>
                                                </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                 </Buttons>
                            </ext:FormPanel>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:FormPanel runat="server" ID="FormPanel6" Title ="STEP 4 - Leave Request Approval" BodyPadding="12" Width="1070" Frame="false" Border="false" DisabledCls="true">
                                <Items>
                                    <ext:TextField runat="server" ID="Approval" FieldLabel="Approver Name " />
                                </Items>
                                <Buttons>
                                    <ext:Button runat="server" ID="btnSubmit" Text="SUBMIT REQUEST" UI="Primary" >
                                        <DirectEvents>
                                            <Click OnEvent="btnSubmit_Click" >
                                                <EventMask ShowMask="true" Msg="Loading process..." />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Button runat="server" ID="btnClear" Text="CLEAR" ui="Danger" >
                                        <DirectEvents>
                                            <Click OnEvent="btnClear_Click" />
                                        </DirectEvents>
                                    </ext:Button>
                                </Buttons>
                            </ext:FormPanel>
                        </Items>
                    </ext:FieldContainer>
                </Items>
            </ext:FormPanel>
        </div>
    </div>
</asp:Content>