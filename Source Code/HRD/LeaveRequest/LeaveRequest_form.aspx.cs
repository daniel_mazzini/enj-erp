﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using NewERDM.lib.BusinessClass;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using SpreadsheetLight;
using System.Globalization;

namespace NewERDM.HRD.LeaveRequest
{
    public partial class LeaveRequest_form : System.Web.UI.Page
    {
        #region Field

        UserApplication userApp;
        UserApplicationBL usrApplicationBL = new UserApplicationBL();
        
        int jumlahLeave;
        
        #endregion Field

        #region Properties
        
        protected void Store_Detail_refreshData(object sender, StoreReadDataEventArgs e)
        {
            //getLeaveCalculate();
        }
        #endregion Properties

        #region Methods

        protected void displayData(string strEmployeeID)
        {
            DataTable dt = new DataTable();
            var objGet = new clsLeaveRequest();
            dt = objGet.getLeaveCalculate(strEmployeeID);

            if (dt.Rows.Count > 0)
            {
                this.pid.Text = dt.Rows[0]["pid"].ToString();
                this.TotalWorking.Text = dt.Rows[0]["TotalWorking"].ToString();
                this.DayExcluded.Text = dt.Rows[0]["DayExcluded"].ToString();
                this.Cobus.Text = dt.Rows[0]["Cobus"].ToString();
                this.DayDeducted.Text = dt.Rows[0]["DayDeducted"].ToString();
                this.ProjectAccrual.Text = dt.Rows[0]["ProjectAccrual"].ToString();
                this.ProjectLeaveBalance.Text = dt.Rows[0]["ProjectLeaveBalance"].ToString();
            }
        }
        protected void getLeaveCalculate()
        {
            DataTable dt = null;
            var objget = new clsLeaveRequest();
            dt = objget.get_displayCalculate(this.lblUserEmployee.Text);
            Store_Detail.DataSource = dt;
            Store_Detail.DataBind();
        }
        protected void getDateHired(string strEmployeeID)
        {
             DataTable dt = new DataTable();
            var objGet = new clsLeaveRequest();
            dt = objGet.getHiredEmployee(strEmployeeID);

            if (dt.Rows.Count > 0)
            {

            }
        }
        #endregion Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                userApp = (UserApplication)Session["ses_user"];

                startDate.Value = DateTime.Today;

                if (userApp == null)
                {

                }
                else
                {
                    this.dateNow.Value = DateTime.Today;
                    this.lblUserid.Text = String.Format("{0}", userApp.UserName);
                    this.lblUserid1.Text = String.Format("{0}", userApp.UserName);
                    this.lblUserEmployee.Text = String.Format("{0}", userApp.EmployeeID);
                    getDateHired(this.lblUserEmployee.Text);
                }
            }
        }
        protected void Disable_Click(object sender, DirectEventArgs e)
        {
            if(chkPublicHoliday.Checked == false)
            {
                publicHoliday.Text = "";
                publicHoliday.Disabled = true;
            }
            else
            {
                publicHoliday.Disabled = false;
                publicHoliday.Focus();
            }

            if (chkMedivac.Checked == false)
            {
                medivac.Text = "";
                medivac.Disabled = true;
            }
            else
            {
                medivac.Disabled = false;
                medivac.Focus();
            }

            if(chkPilgrimage.Checked == false)
            {
                pilgrimage.Text = "";
                pilgrimage.Disabled = true;
            }
            else
            {
                pilgrimage.Disabled = false;
                pilgrimage.Focus();
            }

            if (chkTraining.Checked == false)
            {
                training.Text = "";
                training.Disabled = true;
            }
            else
            {
                training.Disabled = false;
                training.Focus();
            }

            if (chkBirthOfChild.Checked == false)
            {
                birthOfChild.Text = "";
                birthOfChild.Disabled = true;
            }
            else
            {
                birthOfChild.Disabled = false;
                birthOfChild.Focus();
            }

            if (chkMaternity.Checked == false)
            {
                maternity.Text = "";
                maternity.Disabled = true;
            }
            else
            {
                maternity.Disabled = false;
                maternity.Focus();
            }

            if (chkMedicalCheckUp.Checked == false)
            {
                medicalCheckup.Text = "";
                medicalCheckup.Disabled = true;
            }
            else
            {
                medicalCheckup.Disabled = false;
                medicalCheckup.Focus();
            }

            if (chkContractCompletion.Checked == false)
            {
                contractCompletion.Text = "";
                contractCompletion.Disabled = true;
            }
            else
            {
                contractCompletion.Disabled = false;
                contractCompletion.Focus();
            }

            if (chkVisa.Checked == false)
            {
                visaRenewal.Text = "";
                visaRenewal.Disabled = true;
            }
            else
            {
                visaRenewal.Disabled = false;
                visaRenewal.Focus();
            }

            if (chkParliament.Checked == false)
            {
                parliament.Text = "";
                parliament.Disabled = true;
            }
            else
            {
                parliament.Disabled = false;
                parliament.Focus();
            }

            if (chkDayOff.Checked == false)
            {
                dayOff.Text = "";
                dayOff.Disabled = true;
            }
            else
            {
                dayOff.Disabled = false;
                dayOff.Focus();
            }

            if (chkLeaveWithoutPay.Checked == false)
            {
                leaveWithoutPay.Text = "";
                leaveWithoutPay.Disabled = true;
            }
            else
            {
                leaveWithoutPay.Disabled = false;
                leaveWithoutPay.Focus();
            }

            if (chkMarriageChild.Checked == false)
            {
                marriageChild.Text = "";
                marriageChild.Disabled = true;
            }
            else
            {
                marriageChild.Disabled = false;
                marriageChild.Focus();
            }

            if (chkFamilyFatching.Checked == false)
            {
                familyFatching.Text = "";
                familyFatching.Disabled = true;
            }
            else
            {
                familyFatching.Disabled = false;
                familyFatching.Focus();
            }

            if (chkLongSickness.Checked == false)
            {
                longSickness.Text = "";
                longSickness.Disabled = true;
            }
            else
            {
                longSickness.Disabled = false;
                longSickness.Focus();
            }

            if (chkMarriageEmployee.Checked == false)
            {
                marriageEmployee.Text = "";
                marriageEmployee.Disabled = true;
            }
            else
            {
                marriageEmployee.Disabled = false;
                marriageEmployee.Focus();
            }

            if (chkPaternity.Checked == false)
            {
                paternity.Text = "";
                paternity.Disabled = true;
            }
            else
            {
                paternity.Disabled = false;
                paternity.Focus();
            }
        }
        protected void hitungTanggal_Click(object sender, DirectEventArgs e)
        {
            if (endDate.Text == "1/1/0001 12:00:00 AM" || startDate.Text == "1/1/0001 12:00:00 AM")
            {
                this.jumlahLeaveTemp.Text = "";
                this.lblJumlahLeave.Text = "";
            }
            else
            { 
                DateTime tglAwal = Convert.ToDateTime(startDate.Text);
                string formatTglAwal = tglAwal.ToString("yyyyMMdd");

                DateTime tglAkhir = Convert.ToDateTime(endDate.Text);
                string formatTglAkhir = tglAkhir.ToString("yyyyMMdd");

                TimeSpan totalHari = tglAkhir - tglAwal;
                int calculate = Convert.ToInt32(totalHari.TotalDays) + 1;

                if (calculate < 1)
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Warning",
                        Message = "End date should not be smaller than the start date !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    lblJumlahLeave.Text = "";
                    return;
                }
                else
                { 
                    lblJumlahLeave.Text = calculate.ToString("#");
                    jumlahLeaveTemp.Text = calculate.ToString("#");
                }
            }
        }
        protected void btnCalculate_Click(object sender, DirectEventArgs e)
        {
            if (this.jumlahLeaveTemp.Text == "")
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Start date or end date must be value !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }

            int publicHolidayValue;
            int medivacValue;
            int pilgrimageValue;
            int trainingValue;
            int birthOfChildValue;
            int maternityValue;
            int medicalCheckUpValue;
            int contractCompletionValue;
            int visaRenewalValue;
            int parliamentValue;
            int dayOffValue;
            int leaveWithoutPayValue;
            int marriageChildValue;
            int familyFatchingValue;
            int longSicknessValue;
            int marriageEmployeeValue;
            int paternityValue;

            jumlahLeave = Convert.ToInt32(this.jumlahLeaveTemp.Text);

            if (chkPublicHoliday.Checked == false)
            {
                publicHolidayValue = 0;
            }
            else
            {
                publicHolidayValue = Convert.ToInt16(publicHoliday.Text);
            }

            if (chkMedivac.Checked == false)
            {
                medivacValue = 0;
            }
            else
            {
                medivacValue = Convert.ToInt16(medivac.Text);
            }

            if (chkPilgrimage.Checked == false)
            {
                pilgrimageValue = 0;
            }
            else
            {
                pilgrimageValue = Convert.ToInt16(pilgrimage.Text);
            }

            if (chkTraining.Checked == false)
            {
                trainingValue = 0;
            }
            else
            {
                trainingValue = Convert.ToInt16(training.Text);
            }

            if (chkBirthOfChild.Checked == false)
            {
                birthOfChildValue = 0;
            }
            else
            {
                birthOfChildValue = Convert.ToInt16(birthOfChild.Text);
            }

            if (chkMaternity.Checked == false)
            {
                maternityValue = 0;
            }
            else
            {
                maternityValue = Convert.ToInt16(maternity.Text);
            }

            if (chkMedicalCheckUp.Checked == false)
            {
                medicalCheckUpValue = 0;
            }
            else
            {
                medicalCheckUpValue = Convert.ToInt16(medicalCheckup.Text);
            }

            if (chkContractCompletion.Checked == false)
            {
                contractCompletionValue = 0;
            }
            else
            {
                contractCompletionValue = Convert.ToInt16(contractCompletion.Text);
            }

            if (chkVisa.Checked == false)
            {
                visaRenewalValue = 0;
            }
            else
            {
                visaRenewalValue = Convert.ToInt16(visaRenewal.Text);
            }

            if (chkParliament.Checked == false)
            {
                parliamentValue = 0;
            }
            else
            {
                parliamentValue = Convert.ToInt16(parliament.Text);
            }

            if (chkDayOff.Checked == false)
            {
                dayOffValue = 0;
            }
            else
            {
                dayOffValue = Convert.ToInt16(dayOff.Text);
            }

            if (chkLeaveWithoutPay.Checked == false)
            {
                leaveWithoutPayValue = 0;
            }
            else
            {
                leaveWithoutPayValue = Convert.ToInt16(leaveWithoutPay.Text);
            }

            if (chkMarriageChild.Checked == false)
            {
                marriageChildValue = 0;
            }
            else
            {
                marriageChildValue = Convert.ToInt16(marriageChild.Text);
            }

            if (chkFamilyFatching.Checked == false)
            {
                familyFatchingValue = 0;
            }
            else
            {
                familyFatchingValue = Convert.ToInt16(familyFatching.Text);
            }

            if (chkLongSickness.Checked == false)
            {
                longSicknessValue = 0;
            }
            else
            {
                longSicknessValue = Convert.ToInt16(longSickness.Text);
            }

            if (chkMarriageEmployee.Checked == false)
            {
                marriageEmployeeValue = 0;
            }
            else
            {
                marriageEmployeeValue = Convert.ToInt16(marriageEmployee.Text);
            }

            if (chkPaternity.Checked == false)
            {
                paternityValue = 0;
            }
            else
            {
                paternityValue = Convert.ToInt16(paternity.Text);
            }

            int workingDayValue = jumlahLeave - publicHolidayValue;
            int dayExcludedValue = medivacValue + pilgrimageValue + trainingValue + birthOfChildValue + maternityValue +
                                medicalCheckUpValue + contractCompletionValue + visaRenewalValue + parliamentValue + dayOffValue +
                                leaveWithoutPayValue + marriageChildValue + familyFatchingValue + longSicknessValue + marriageEmployeeValue + paternityValue;
            int cobusFormValue = 0;
            int dayDeductedValue = workingDayValue - dayExcludedValue;
            int projectedAccrualValue = 0;
            int projectedBalanceValue = Convert.ToInt32(BalanceLeaveTemp.Text) - dayDeductedValue;

            DataTable getLeaveTemp = new DataTable();
            var objGetLeaveTemp = new clsLeaveRequest();
            getLeaveTemp = objGetLeaveTemp.get_cekLeaveTempTb(this.lblUserEmployee.Text);

            if (getLeaveTemp.Rows.Count == 0)
            {
                var objInsertTbLeaveTemp = new clsLeaveRequest();
                objInsertTbLeaveTemp.InsertTbLeaveTemp(this.lblUserEmployee.Text, workingDayValue, dayExcludedValue, cobusFormValue, dayDeductedValue,
                                                        projectedAccrualValue, projectedBalanceValue);
            }
            else
            {
                //string jsonValues = e.ExtraParams["Value"];
                //List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);

                //foreach (var record in records)
                //{
                var objUpdateTbLeaveTemp = new clsLeaveRequest();
                objUpdateTbLeaveTemp.UpdateTbLeaveTemp(this.lblUserEmployee.Text, workingDayValue, dayExcludedValue, cobusFormValue, dayDeductedValue,
                                                        projectedAccrualValue, projectedBalanceValue);
                //}
            }

            displayData(this.lblUserEmployee.Text);
            getLeaveCalculate();
        }
        protected void btnSubmit_Click(object sender, DirectEventArgs e)
        {
            if (this.jumlahLeaveTemp.Text == "")
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Start date or end date must be value !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }

            if (this.Description.Text == "")
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Purpose Description must be value !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }

            var objInsertDataLeave= new clsLeaveRequest();
            objInsertDataLeave.InsertDataLeaveRequest();


        }
        protected void btnClear_Click(object sender, DirectEventArgs e)
        {
            var objDelete = new clsLeaveRequest();
            objDelete.Delete_tbLeaveTemp(this.lblUserEmployee.Text);

            X.Redirect(CommonUtility.ResolveUrl("Dashboard_itenary.aspx"), "Loading ... ");
        }
        
    }
}