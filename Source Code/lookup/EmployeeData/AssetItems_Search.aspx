﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AssetItems_Search.aspx.cs" Inherits="NewERDM.lookup.EmployeeData.AssetItems_Search" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ext:FormPanel runat="server" ID="frmverified" Title ="" BodyPadding="12" Header="false" Frame="false" Border="false" DisabledCls="true">
                <Items>
                <ext:GridPanel ID="grdAssetItem" runat="server" Border="false" Layout="FitLayout" Height="520" Region="Center" Margins="0 5 5 5"  >
                    <Store>
                        <ext:Store ID="Store_AssetItems" PageSize="21" runat="server" OnReadData="Store_AssetItems_RefreshData" GroupField="Nama" >
                            <Model>
                                <ext:Model ID="Model13" runat="server" IDProperty="FICNumber">
                                    <Fields>
                                        <ext:ModelField Name="FICNumber" />
                                        <ext:ModelField Name="FICName" />
                                        <ext:ModelField Name="OLDName" />
                                        <ext:ModelField Name="ServiceTag" />
                                        <ext:ModelField Name="SerialNo" />
                                        <ext:ModelField Name="AssetType" />
                                        <ext:ModelField Name="BrandModel" />
                                        <ext:ModelField Name="AssetDetail" />
                                        <ext:ModelField Name="DeliveryNo" />
                                        <ext:ModelField Name="DeliveryDate" />
                                        <ext:ModelField Name="ItemCode" />
                                        <ext:ModelField Name="AccountingCode" />
                                        <ext:ModelField Name="CompanyAsset" />
                                        <ext:ModelField Name="JoinDomainFMI" />
                                        <ext:ModelField Name="JoinDomainTicket" />
                                        <ext:ModelField Name="JoinDomainDate" />
                                        <ext:ModelField Name="JoinDomainBy" />
                                        <ext:ModelField Name="AssetStatus" />
                                        <ext:ModelField Name="DisposedTicket" />
                                        <ext:ModelField Name="DisposedDate" />
                                        <ext:ModelField Name="DisposedLetter" />
                                        <ext:ModelField Name="DisposedBy" />
                                        <ext:ModelField Name="AssetRemarks" />
                                        <ext:ModelField Name="iUsed" />
                                        <ext:ModelField Name="EmployeeName" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>

                    <ColumnModel runat="server">
                        <Columns>
                            <ext:Column ID="Column1" runat="server" Width="80" Text="FIC No." Filterable="false" DataIndex="FICNumber" />
                            <ext:Column ID="Column2" runat="server" Width="200" Text="FIC Name" Filterable="false" DataIndex="FICName" />
                            <ext:Column ID="Column3" runat="server" Width="200" Text="OLD Name" Filterable="false" DataIndex="OLDName"  />
                            <ext:Column ID="Column4" runat="server" Width="100" Text="Service Tag" Filterable="false" DataIndex="ServiceTag"/>
                            <ext:Column ID="Column5" runat="server" Width="100" Text="Serial No." Filterable="false" DataIndex="SerialNo"/>
                            <ext:Column ID="Column6" runat="server" Width="80" Text="Asset Type" Filterable="false" DataIndex="AssetType" />
                            <ext:Column ID="Column7" runat="server" Width="200" Text="Brand Model" Filterable="false" DataIndex="BrandModel" />
                            <ext:Column ID="Column8" runat="server" Width="200" Text="Asset Detail" Filterable="false" DataIndex="AssetDetail"  />
                            <ext:Column ID="Column9" runat="server" Width="80" Text="Delivery No" Filterable="false" DataIndex="DeliveryNo"/>
                            <ext:Column ID="Column10" runat="server" Width="100" Text="Delivery Date" Filterable="false" DataIndex="DeliveryDate"/>
                            <ext:Column ID="Column11" runat="server" Width="80" Text="Item Code" Filterable="false" DataIndex="ItemCode" />
                            <ext:Column ID="Column12" runat="server" Width="80" Text="Acc. Code" Filterable="false" DataIndex="AccountingCode" />
                            <ext:Column ID="Column13" runat="server" Width="80" Text="Company" Filterable="false" DataIndex="CompanyAsset"  />
                            <ext:Column ID="Column14" runat="server" Width="90" Text="Join Domain FMI" Filterable="false" DataIndex="JoinDomainFMI"/>
                            <ext:Column ID="Column15" runat="server" Width="100" Text="Join Domain Ticket" Filterable="false" DataIndex="JoinDomainTicket"/>
                            <ext:Column ID="Column16" runat="server" Width="100" Text="Join Domain Date" Filterable="false" DataIndex="JoinDomainDate" />
                            <ext:Column ID="Column17" runat="server" Width="100" Text="Join Domain By" Filterable="false" DataIndex="JoinDomainBy" />
                            <ext:Column ID="Column18" runat="server" Width="80" Text="Asset Status" Filterable="false" DataIndex="AssetStatus"  />
                            <ext:Column ID="Column19" runat="server" Width="100" Text="Disposed Ticket" Filterable="false" DataIndex="DisposedTicket"/>
                            <ext:Column ID="Column20" runat="server" Width="100" Text="Disposed Date" Filterable="false" DataIndex="DisposedDate"/>
                            <ext:Column ID="Column21" runat="server" Width="200" Text="Disposed Letter" Filterable="false" DataIndex="DisposedLetter" />
                            <ext:Column ID="Column22" runat="server" Width="100" Text="Disposed By" Filterable="false" DataIndex="DisposedBy"  />
                            <ext:Column ID="Column23" runat="server" Width="200" Text="Remarks" Filterable="false" DataIndex="AssetRemarks"/>
                            <ext:Column ID="Column24" runat="server" Width="80" Text="Used" Filterable="false" DataIndex="iUsed"/>
                            <ext:Column ID="Column25" runat="server" Width="150" Text="Employee Name" Filterable="false" DataIndex="EmployeeName"/>
                        </Columns>
                    </ColumnModel>
                    <DirectEvents>
                        <CellDblClick OnEvent="btn_edit_doubleclick" >
                             <EventMask ShowMask="true" />
                                <ExtraParams>
	                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdAssetItem}.getRowsValues({selectedOnly : true}))"
			                                Mode="Raw">
	                                </ext:Parameter>
                                </ExtraParams>
                        </CellDblClick>
                    </DirectEvents>
                    <TopBar>
                        <ext:Toolbar ID="Toolbar2" runat="server" Layout="ColumnLayout">
                            <Items>
                                <ext:TextField runat="server" FieldLabel="Filter" ID="f_search" EmptyText="Please type FIC Number "  Width="310" LabelWidth="50"     />
                                <ext:Label ID="Label7" Text="&nbsp;" Width="5" runat="Server" />

                                 <ext:Panel runat="server" Frame="false" Border="false" Cls="my-panel">
                                 <Items>
                                    <ext:Button ID="cmd_Search" runat="server" Text="Search"  UI="Success" ToolTipType="Qtip" ToolTip="<b>Search</b><br>Search New Travel Itinerary Items">
                                        <DirectEvents>
                                            <Click OnEvent="btn_search_click" >
                                                <EventMask ShowMask="true" Msg="Searching.... Please wait.." />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Label ID="Label1" Text="&nbsp;&nbsp;" Width="2" runat="Server" />
                                    <ext:Button ID="cmd_New" runat="server" Text="New Asset Items" UI="Success" ToolTipType="Qtip" ToolTip="<b>New</b><br>Input New Asset Items">
                                    <DirectEvents>
                                            <Click OnEvent="cmd_New_Click" >
                                                <EventMask ShowMask="true" Msg="Searching.... Please wait.." />
                                            </Click>
                                    </DirectEvents>
                                    </ext:Button>
                                     <ext:Label ID="Label8" Text="&nbsp;&nbsp;" Width="2" runat="Server" />
                                     <ext:Button ID="Button1" runat ="server" Text="Delete" UI="Warning" ToolTipType="Qtip" ToolTip="<b>DELETE</b><br>Delete MailBag Tracking</br>">
                                        <DirectEvents>
                                            <Click OnEvent="btn_delete_click">
                                                <Confirmation ConfirmRequest="true" Message="Are you sure to delete this record ?" Title="Delete Records" />
                                                <EventMask ShowMask="true" Msg="Delete Data Please wait..." />
                                                <ExtraParams>
									               <ext:Parameter Name="Values" Value="Ext.encode(#{grdAssetItem}.getRowsValues({selectedOnly : true}))"
											               Mode="Raw">
									               </ext:Parameter>
								               </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Label ID="Label9" Text="&nbsp;&nbsp;" Width="2" runat="Server" />
                                    <ext:Button ID="Button2" runat="server" Text="Export To Excel" UI="Primary" ToolTipType="Qtip" ToolTip="<b>Export To Excel</b>">
                                        <DirectEvents>
                                            <Click OnEvent="btn_export_click" IsUpload="true">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Label ID="Label10" Text="&nbsp;&nbsp;" Width="2" runat="Server" />
                                    <ext:Button ID="btn_Close" runat="server" Text="Close"  UI="Danger" ToolTipType="Qtip" ToolTip="<b>Close</b><br>Close This Module" Handler="this.up('window').close()" >
                                        <DirectEvents>
                                            <Click OnEvent="btn_close_click" >
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click StopEvent="true"/>
                                        </Listeners>
                                    </ext:Button>
                                 </Items>                                     
                                 </ext:Panel>
                                <ext:TextField ID="IDMailBag" runat="server" Hidden ="true" />
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
<%--                    <View>
                        <ext:GridView runat="server" StripeRows="true" MarkDirty="false" />
                    </View>
                    <Features>
                        <ext:GroupingSummary ID="GroupingSummary" runat="server" GroupHeaderTplString="{MailBagID}" HideGroupedHeader="true" 
                            EnableGroupingMenu="false"  StartCollapsed="false" />
                    </Features>--%>
                  <BottomBar>
				<ext:PagingToolbar ID="PagingToolbar3" runat="server" >
                   <Items>
                       <ext:Button runat="server" Text="<b>Clear Filters</b>" Icon="Erase" Handler="this.up('grid').filters.clearFilters();" />
                   </Items>
                </ext:PagingToolbar>
			</BottomBar>
                </ext:GridPanel>
                </Items>
            </ext:FormPanel>
            <ext:Window ID="wndExport" runat="server" Hidden="true" Icon="PageExcel" Title="Export To Excel" Height="180" Width="350" Modal="true" BodyPadding="2" Closable="false"   >
                <Items>
                    <ext:FormPanel runat="server" ID="FormPanel1" Title =""  BodyPadding="12"  Height="180" Header="false"  Frame="false"  Border="false"  >
                        <LayoutConfig >
                            <ext:VBoxLayoutConfig  />
                        </LayoutConfig>  
                        <Items>
                            <ext:TextField runat="server" ID="FICNumber" FieldLabel="FIC Number" />
                            <%--<ext:DateField runat="server" ID="dFrom" FieldLabel="From" Format="dd/MM/yyyy" />
                            <ext:DateField runat="server" ID="dTo" FieldLabel="To" Format="dd/MM/yyyy"  />--%>
                        </Items>
                    </ext:FormPanel>
                </Items>
                <Buttons>
                <ext:Button runat="server" ID="btn_exportAsset" Text="To Excel" Click="ToExcel" UI="Primary">
                    <DirectEvents>
                        <Click OnEvent="ToExcel" IsUpload="true" >
                        </Click>
                    </DirectEvents>
                </ext:Button>
                        
				<ext:Button runat="server" ID="Button3" UI="Warning"  Text="<b>Close</b>"  >
                    <DirectEvents>
                          <Click OnEvent="btn_view_close_click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
				</ext:Button>
			    </Buttons>
            </ext:Window>
        </div>
    </div>
</asp:Content>