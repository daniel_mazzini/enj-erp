﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using NewERDM.lib.BusinessClass;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using SpreadsheetLight;

namespace NewERDM.lookup.EmployeeData
{
    public partial class EmployeeClass : System.Web.UI.Page
    {
        #region field

        UserApplication userApp;
        UserApplicationBL usrApplicationBL = new UserApplicationBL();
        #endregion

        #region constructor

        protected void Store_View_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            get_EmployeeClass();
        }
        #endregion constructor

        #region method

        protected void get_EmployeeClass()
        {
            var objGet = new clsLookup();
            this.Store_View.DataSource = objGet.getEmployeeClass();
            this.Store_View.DataBind();
        }
        protected void getDataEmployeeClass(string strPid)
        {
            string category;

            category = "EmployeeClass";

            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getDataLookup(category, strPid);

            if (dt.Rows.Count > 0)
            {
                this.pid.Text = dt.Rows[0]["pid"].ToString();
                this.Category.Text = dt.Rows[0]["Category"].ToString();
                this.CategoryTemp.Text = dt.Rows[0]["Category"].ToString();
                this.Code.Text = dt.Rows[0]["Code"].ToString();
                this.Code.ReadOnly = true;
                this.Name.Text = dt.Rows[0]["Name"].ToString();
            }
        }

        #endregion method

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                get_EmployeeClass();
            }
        }
        protected void btn_save_click(object sender, DirectEventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];
            if (userApp == null)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Session timeout, please relogin !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                this.txt_Userid.Text = userApp.UserID;
                //mandatory
                if (string.IsNullOrEmpty(this.CategoryTemp.Text))
                {
                    CommonUtility.PrintMessageInTaskBar("Category can't be empty !", "WARNING");
                    return;
                }
                if (string.IsNullOrEmpty(this.Code.Text))
                {
                    CommonUtility.PrintMessageInTaskBar("Lookup can't be empty !", "WARNING");
                    return;
                }
                if (string.IsNullOrEmpty(this.Name.Text))
                {
                    CommonUtility.PrintMessageInTaskBar("Description can't be empty !", "WARNING");
                    return;
                }

                //save  edit            
                if (string.IsNullOrEmpty(this.pid.Text))
                {
                    try
                    {
                        var objSave = new clsLookup();
                        objSave.fSave(1, 0, this.CategoryTemp.Text, this.Code.Text, this.Name.Text, userApp.UserID);

                        CommonUtility.PrintMessageInTaskBar("DATA SAVED", "INFORMATION");
                    }
                    catch
                    {
                        CommonUtility.PrintMessageInTaskBar("CAN'T SAVED", "WARNING");
                    }
                }
                else //EDIT
                {
                    try
                    {
                        var objUpdate = new clsLookup();
                        objUpdate.fUpdate(2, Convert.ToInt16(this.pid.Text), this.CategoryTemp.Text, this.Code.Text, this.Name.Text, userApp.UserID);

                        CommonUtility.PrintMessageInTaskBar("DATA UPDATED", "INFORMATION");
                    }
                    catch
                    {

                        CommonUtility.PrintMessageInTaskBar("CAN'T UPDATED", "WARNING");
                    }
                }

                this.Store_View.Reload();
                this.Code.Text = "";
                this.Name.Text = "";
            }
        }
        protected void cmd_edit_click(object sender, DirectEventArgs e)
        {
            string jsonValues = e.ExtraParams["Values"];
            List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);

            //Validasi, jika user tidak melakukan click baris
            if (records.Count == 0)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Please selected row !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }

            String jsonSelectedRows = e.ExtraParams["Values"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        getDataEmployeeClass(row["pid"]);
                    }
                    catch
                    {

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "ERROR",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.ERROR
                        });
                    }
                }
            }
        }
        protected void cmd_Delete_click(object sender, DirectEventArgs args)
        {
            //X.MessageBox.Confirm("Test", "Are you sure?", new MessageBoxButtonsConfig
            //{
            //    Yes = new MessageBoxButtonConfig
            //    {
            //        Text = "Yes"
            //    },
            //    No = new MessageBoxButtonConfig
            //    {
            //    }
            //}).Show();

            string jsonValues = args.ExtraParams["Values"];
            List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);

            //Validasi, jika user tidak melakukan click baris
            if (records.Count == 0)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Please selected row !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }


            String jsonSelectedRows = args.ExtraParams["Values"]; // <DirectEvents> <ext:Parameter Name="Values" referensi :  <Click OnEvent="btnDelete_Click">
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);
            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        var objDelete = new clsLookup();
                        objDelete.fDelete(Convert.ToInt16(row["pid"]));

                        CommonUtility.PrintMessageInTaskBar("RECORD DELETED", "INFORMATION");

                        get_EmployeeClass();
                    }
                    catch (Exception ex)
                    {
                        CommonUtility.PrintMessageInTaskBar("CAN'T DELETED", "WARNING");
                    }
                    //   X.Msg.Alert("info", row["Code"].ToString()).Show();
                }
            }
            this.Store_View.Reload();
        }
        protected void btn_Cancel_Click(object sender, DirectEventArgs args)
        {
            this.Category.Text = "Employee Class";
            this.CategoryTemp.Text = "EmployeeClass";
            this.Code.Text = "";
            this.Code.ReadOnly = false;
            this.Name.Text = "";

            //X.Redirect("Dashboard_itenary.aspx", "Loading ... ");
        }
    }
}