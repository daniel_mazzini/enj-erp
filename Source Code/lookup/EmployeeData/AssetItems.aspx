﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="AssetItems.aspx.cs" Inherits="NewERDM.lookup.EmployeeData.AssetItems" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="col-md-12">
            <ext:FormPanel runat="server" ID="frmverified" Title =""  BodyPadding="12" Header="false"  Frame="false"  Border="false" DisabledCls="true" >
                <Items>
                    <ext:FieldContainer runat="server" AnchorVertical="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="FICNumber" FieldLabel="FIC Number " LabelWidth="120" />
                            <ext:Label runat="server"  Text="" Width="20" />
                            <ext:TextField runat="server" ID="FICName" FieldLabel="FIC Name " LabelWidth="80" Width="375" />
                            <ext:Label runat="server" Text="" Width="23" />
                            <ext:TextField runat="server" ID="OldName" FieldLabel="Old Name " LabelWidth="98" Width="350" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="ServiceTag" FieldLabel="Service Tag " LabelWidth="120" />
                            <ext:Label runat="server"  Text="" Width="20" />
                            <ext:ComboBox runat="server" ID="AssetType" FieldLabel="Asset Type " DisplayField="Name"
                                  ValueField="Code" Editable="false" LabelWidth="80" >
                                <Store>
                                    <ext:Store runat="server" ID="Store_AssetType" AutoLoad="true" OnReadData="Store_AssetType_RefreshData" >
                                        <Model>
                                            <ext:Model ID="Model3" runat="server"  >
                                                <Fields>
                                                    <ext:ModelField Name="Code" />
                                                    <ext:ModelField Name="Name" />
                                                </Fields>
                                            </ext:Model>
                                        </Model>
                                    </ext:Store>
                                </Store>
                            </ext:ComboBox>
                            <ext:Label runat="server"  Text="" Width="160" />
                            <ext:TextField runat="server" ID="SerialNo" FieldLabel="Serial Number " LabelWidth="98" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="AccCode" FieldLabel="Accounting Code " LabelWidth="120" />
                            <ext:Label runat="server"  Text="" Width="20" />
                            <ext:TextField runat="server" ID="BrandModel" FieldLabel="Brand Model " LabelWidth="80" Width="300" />
                            <ext:Label runat="server"  Text="" Width="95" />
                            <ext:TextField runat="server" ID="AssetDetail" FieldLabel="Asset Detail " LabelWidth="100" Width="350" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="DeliveryNo" FieldLabel="Delivery No. " LabelWidth="120" />
                            <ext:Label runat="server"  Text="" Width="20" />
                            <ext:DateField runat="server" ID="DeliveryDate" FieldLabel="Delivery Date " LabelWidth="80" Format="ddd, dd-MMM-yyyy" />
                            <ext:Label runat="server"  Text="" Width="160" />
                            <ext:TextField runat="server" ID="ItemCode" FieldLabel="Item Code " LabelWidth="98" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                             <ext:ComboBox runat="server" ID="CompanyAsset" FieldLabel="Company Asset " LabelWidth="120" >
                                <Items>
                                    <ext:ListItem Text="ENJ ASSET" Value="ENJ ASSET" />
                                    <ext:ListItem Text="ENJ" Value="ENJ" />
                                    <ext:ListItem Text="PTFI ASSET" Value="PTFI ASSET" />
                                </Items>
                            </ext:ComboBox>
                            <ext:Label runat="server"  Text="" Width="20" />
                            <ext:FileUploadField runat="server" ID="f_UploadPhoto" FieldLabel="Photo " LabelWidth="80" Icon="Attach" />
                            <ext:TextField runat="server" ID="LinkUploadPhoto" FieldLabel="Photo " LabelWidth="80" />
                            <ext:Button runat="server" ID="btn_UploadPhoto" Text="Attach..." Icon="Attach" Hidden="true" Margins="0 0 0 5" >
                                <DirectEvents>
                                    <Click OnEvent="btn_UploadPhoto_click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Label runat="server"  Text="" Width="85" />
                            <ext:TextField runat="server" ID="AssetRemark" FieldLabel="Remarks " LabelWidth="98" Width="350" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:ComboBox runat="server" ID="JoinDomainFMI" FieldLabel="Join Domain FMI " LabelWidth="120" >
                                <Items>
                                    <ext:ListItem Text="YES" Value="YES" />
                                    <ext:ListItem Text="NO" Value="NO" />
                                </Items>
                            </ext:ComboBox>
                            <ext:Label runat="server"  Text="" Width="20" />
                            <ext:FileUploadField runat="server" ID="f_UploadPO" FieldLabel="PO Number " LabelWidth="80" Icon="Attach" />
                            <ext:TextField runat="server" ID="LinkUploadPO" FieldLabel="PO Number " LabelWidth="80" />                                          
                            <ext:Button runat="server" ID="btn_UploadPO" Text="Attach..." Icon="Attach" Hidden="true" Margins="0 0 0 5" >
                                <DirectEvents>
                                    <Click OnEvent="btn_UploadPO_click">
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldContainer>
                    <ext:TextField runat="server" ID="JoinDomainTicket" FieldLabel="Join Domain Ticket " LabelWidth="120" />
                    <ext:DateField runat="server" ID="JoinDomainDate" FieldLabel="Join Domain Date " LabelWidth="120" Format="ddd, dd-MMM-yyyy" />
                    <ext:TextField runat="server" ID="JoinDomainBy" FieldLabel="Join Domain By " LabelWidth="120" />
                    <ext:ComboBox runat="server" ID="AssetStatus" FieldLabel="Asset Status " LabelWidth="120" >
                        <Items>
                            <ext:ListItem Text="ACTIVE" Value="ACTIVE" />
                            <ext:ListItem Text="DISPOSED" Value="DISPOSED" />
                        </Items>
                    </ext:ComboBox>
                    <ext:TextField runat="server" ID="DisposedTicket" FieldLabel="Disposed Ticket " LabelWidth="120" />
                    <ext:DateField runat="server" ID="DisposedDate" FieldLabel="Disposed Date " LabelWidth="120" Format="ddd, dd-MMM-yyyy" />
                    <ext:TextField runat="server" ID="DisposedLetter" FieldLabel="Disposed Letter " LabelWidth="120" />
                    <ext:TextField runat="server" ID="DisposedBy" FieldLabel="Disposed By " LabelWidth="120" />
<%--                    <ext:TextField runat="server" ID="Photo" FieldLabel="Photo " LabelWidth="120" />--%>
<%--                    <ext:TextField runat="server" ID="PONo" FieldLabel="PO Number " LabelWidth="120" />--%>
                    <%--<ext:Checkbox runat="server" ID="chkCategory" LabelWidth="120" />--%>
                </Items>
                <Buttons>
                <ext:Button runat="server" ID="btn_Save" UI="Primary" Height="30" Text="<b>Save</b>">
                    <DirectEvents>
                        <Click OnEvent="btn_Save_click" >
                            <EventMask ShowMask="true" Msg="Save data please wait..." />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button runat="server" ID="btn_close"  UI="Danger" Height="30" Text="<b>Close</b>">
                    <DirectEvents>
                        <Click OnEvent="btn_close_click" >
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </div>
    </div>
</asp:Content>