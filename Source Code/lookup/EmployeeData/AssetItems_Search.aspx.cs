﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using NewERDM.lib.BusinessClass;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using SpreadsheetLight;

namespace NewERDM.lookup.EmployeeData
{
    public partial class AssetItems_Search : System.Web.UI.Page
    {
        #region Field
        
        #endregion Field

        #region Properties
        protected void Store_AssetItems_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getDataAssetItems();
        }

        #endregion Properties

        #region Methods

        protected void getDataAssetItems()
        {
            DataTable dt = new DataTable();
            var objGet = new clsEmployee();
            dt = objGet.get_AssetItems_Search(this.f_search.Text.Trim());
            Store_AssetItems.DataSource = dt;

            if (dt.Rows.Count > 0)
            {
                return;
            }
            else
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Information",
                    Message = "FIC Number Not Found !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO
                });
            }
        }
        public void ExportToExcelSearch(DataTable dt)
        {
            SLDocument sl = new SLDocument();

            var objget = new clsEmployee();
            dt = objget.getAssetItems_ExportToExcel(this.FICNumber.Text.Trim());
            Store_AssetItems.DataSource = dt;

            if (dt.Rows.Count > 0)
            {
                //Header
                sl.SetCellValue(1, 1, "FIC Number");
                sl.SetCellValue(1, 2, "FIC Name");
                sl.SetCellValue(1, 3, "Old Name");
                sl.SetCellValue(1, 4, "Service Tag");
                sl.SetCellValue(1, 5, "Serial No.");
                sl.SetCellValue(1, 6, "Asset Type");
                sl.SetCellValue(1, 7, "Brand Model");
                sl.SetCellValue(1, 8, "Asset Detail");
                sl.SetCellValue(1, 9, "Delivery No");
                sl.SetCellValue(1, 10, "Delivery Date");
                sl.SetCellValue(1, 11, "Item Code");
                sl.SetCellValue(1, 12, "Acc. Code");
                sl.SetCellValue(1, 13, "Company Asset");
                sl.SetCellValue(1, 14, "Join Domain FMI");
                sl.SetCellValue(1, 15, "Join Domain Ticket");
                sl.SetCellValue(1, 16, "Join Domain Date");
                sl.SetCellValue(1, 17, "Join Domain By");
                sl.SetCellValue(1, 18, "Asset Status");
                sl.SetCellValue(1, 19, "Disposed Ticket");
                sl.SetCellValue(1, 20, "Disposed Date");
                sl.SetCellValue(1, 21, "Disposed Letter");
                sl.SetCellValue(1, 22, "Disposed By");
                sl.SetCellValue(1, 23, "Remark");
                sl.SetCellValue(1, 24, "Used");
                sl.SetCellValue(1, 25, "Employee Name");

                //Color Header
                sl.ApplyNamedCellStyle(1, 1, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 2, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 3, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 4, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 5, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 6, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 7, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 8, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 9, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 10, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 11, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 12, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 13, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 14, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 15, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 16, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 17, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 18, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 19, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 20, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 21, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 22, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 23, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 24, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 25, SLNamedCellStyleValues.Accent1);


                //Counter
                int iRows = 0;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    iRows = 0 + i;

                    sl.SetCellValue(2 + iRows, 1, dt.Rows[i][0].ToString());
                    sl.SetCellValue(2 + iRows, 2, dt.Rows[i][1].ToString());
                    sl.SetCellValue(2 + iRows, 3, dt.Rows[i][2].ToString());
                    sl.SetCellValue(2 + iRows, 4, dt.Rows[i][3].ToString());
                    sl.SetCellValue(2 + iRows, 5, dt.Rows[i][4].ToString());
                    sl.SetCellValue(2 + iRows, 6, dt.Rows[i][5].ToString());
                    sl.SetCellValue(2 + iRows, 7, dt.Rows[i][6].ToString());
                    sl.SetCellValue(2 + iRows, 8, dt.Rows[i][7].ToString());
                    sl.SetCellValue(2 + iRows, 9, dt.Rows[i][8].ToString());
                    sl.SetCellValue(2 + iRows, 10, dt.Rows[i][9].ToString());
                    sl.SetCellValue(2 + iRows, 11, dt.Rows[i][10].ToString());
                    sl.SetCellValue(2 + iRows, 12, dt.Rows[i][11].ToString());
                    sl.SetCellValue(2 + iRows, 13, dt.Rows[i][12].ToString());
                    sl.SetCellValue(2 + iRows, 14, dt.Rows[i][13].ToString());
                    sl.SetCellValue(2 + iRows, 15, dt.Rows[i][14].ToString());
                    sl.SetCellValue(2 + iRows, 16, dt.Rows[i][15].ToString());
                    sl.SetCellValue(2 + iRows, 17, dt.Rows[i][16].ToString());
                    sl.SetCellValue(2 + iRows, 18, dt.Rows[i][17].ToString());
                    sl.SetCellValue(2 + iRows, 19, dt.Rows[i][18].ToString());
                    sl.SetCellValue(2 + iRows, 20, dt.Rows[i][19].ToString());
                    sl.SetCellValue(2 + iRows, 21, dt.Rows[i][20].ToString());
                    sl.SetCellValue(2 + iRows, 22, dt.Rows[i][21].ToString());
                    sl.SetCellValue(2 + iRows, 23, dt.Rows[i][22].ToString());
                    sl.SetCellValue(2 + iRows, 24, dt.Rows[i][23].ToString());
                    sl.SetCellValue(2 + iRows, 25, dt.Rows[i][24].ToString());
                }


                //Auto Fit Column
                sl.AutoFitColumn(1);
                sl.AutoFitColumn(2);
                sl.AutoFitColumn(3);
                sl.AutoFitColumn(4);
                sl.AutoFitColumn(5);
                sl.AutoFitColumn(6);
                sl.AutoFitColumn(7);
                sl.AutoFitColumn(8);
                sl.AutoFitColumn(9);
                sl.AutoFitColumn(10);
                sl.AutoFitColumn(11);
                sl.AutoFitColumn(12);
                sl.AutoFitColumn(13);
                sl.AutoFitColumn(14);
                sl.AutoFitColumn(15);
                sl.AutoFitColumn(16);
                sl.AutoFitColumn(17);
                sl.AutoFitColumn(18);
                sl.AutoFitColumn(19);
                sl.AutoFitColumn(20);
                sl.AutoFitColumn(21);
                sl.AutoFitColumn(22);
                sl.AutoFitColumn(23);
                sl.AutoFitColumn(24);
                sl.AutoFitColumn(25);


                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=AssetItemsExcel.xlsx");
                sl.SaveAs(Response.OutputStream);
                Response.End();
            }
        }
        #endregion Methods
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void cmd_New_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("AssetItems.aspx", "Loading Page ...");
        }
        protected void btn_search_click(object sender, DirectEventArgs e)
        {
            getDataAssetItems();

            Store_AssetItems.Reload();
        }
        protected void btn_edit_doubleclick(object sender, DirectEventArgs e)
        {
            String jsonSelectedRows = e.ExtraParams["Values"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);
            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        if (row["iUsed"].ToString() == "Yes")
                        {
                            X.MessageBox.Show(new MessageBoxConfig
                            {
                                Title = "Information",
                                Message = "Sorry, FIC already Used, Can't edit !",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            });
                            return;
                        }
                        else
                        {
                            X.Redirect("~/lookup/EmployeeData/AssetItems.aspx?FICNumber=" + row["FICNumber"]);
                        }
                    }
                    catch
                    {

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "Warning",
                            Message = "Error",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.ERROR
                        });
                    }

                }
            }
        }
        protected void btn_export_click(object sender, DirectEventArgs e)
        {
            this.wndExport.Show();
        }
        protected void ToExcel(object sender, EventArgs e)
        {
            ExportToExcelSearch((DataTable)ViewState["grdAssetItem"]);
        }
        protected void btn_delete_click(object sender, DirectEventArgs e)
        {
            String jsonSelectedRows = e.ExtraParams["Values"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        if (row["iUsed"].ToString() == "Yes")
                        {
                            X.MessageBox.Show(new MessageBoxConfig
                            {
                                Title = "Information",
                                Message = "Sorry, FIC already Used, Can't edit !",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            });
                            return;
                        }
                        else
                        {
                            var objDelete = new clsEmployee();
                            objDelete.DeleteAssetItems(row["FICNumber"]);
                            f_search.Text = string.Empty;
                        }
                    }
                    catch
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "CAN'T DELETE",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                    }

                }
            }
            this.Store_AssetItems.Reload();
        }
        protected void btn_close_click(object sender, DirectEventArgs e)
        {
            X.Redirect(CommonUtility.ResolveUrl("Dashboard_itenary.aspx"), "Loading ... ");
        }
        protected void btn_view_close_click(object sender, DirectEventArgs e)
        {
            this.wndExport.Close();
        }

    }
}