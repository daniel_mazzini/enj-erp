﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="IndividualGrade.aspx.cs" Inherits="NewERDM.lookup.EmployeeData.IndividualGrade" %>
<asp:Content ID="Header" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>

<asp:Content ID="Detail" ContentPlaceHolderID="MainContent" Runat="Server">
   <div class="row">
        <div class="col-md-12">
            <ext:FormPanel runat="server" ID="frmverified" Title ="" BodyPadding="12" Header="false" Frame="false" Border="false" DisabledCls="true">
                <Items>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField TabIndex="0" runat="server" ID="pid" Hidden="true" />
                            <ext:TextField tabIndex="1" runat="server" ID="Category" FieldLabel="Category" Text="Individual Grade" Disabled="true" Width="350"/>
                            <ext:TextField runat="server" ID="CategoryTemp" Hidden="true" Text="IndividualGrade"/>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField TabIndex="2" runat="server" ID="Code" FieldLabel="Code" Width="350" />
                            <ext:TextField runat="server" ID="txt_Userid" Hidden="true" />
                            <ext:Label runat="server" Text="" Width="20" />
                            <ext:Button runat="server" ID="btn_save" Text="Save">
                                <DirectEvents>
                                    <Click OnEvent="btn_save_click" >
                                        <EventMask ShowMask="true" Msg="Save Data..." />
                                    </Click>
                                    
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField TabIndex="3" runat="server" ID="Name" FieldLabel="Name" Width="350"/>
                            <ext:Label runat="server" Text="" Width="20" />
                            <ext:Button runat="server" ID="btn_Cancel" Text="Cancel">
                                <DirectEvents>
                                    <Click OnEvent="btn_Cancel_Click">
                                            <EventMask ShowMask="true" Msg="Loading..." />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Items>
                    </ext:FieldContainer>
                    <ext:Label runat="server" Text="" Height="20" />
                    <ext:GridPanel id="grdView" runat="server" Border="false" region="Center" Margins="0 5 5 5">
                        <Store>
                            <ext:Store ID="Store_View" PageSize="21" runat="server" OnReadData="Store_View_RefreshData">
                                <Model>
				                    <ext:Model ID="Model1" runat="server" IDProperty="pid" >
					                    <Fields>
                                            <ext:ModelField Name="pid" />
						                    <ext:ModelField Name="Category" />
						                    <ext:ModelField Name="Code" />
                                            <ext:ModelField Name="Name" />
					                    </Fields>
				                    </ext:Model>
			                    </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel runat="server">
				            <Columns>
					            <ext:Column ID="Column1" runat="server" width="100" Text="PID" Visible="false"   DataIndex="pid" Filterable="true" />
                                <ext:Column ID="Column2" runat="server" Width="150"  Text ="CATEGORY" Visible="false" DataIndex="Category" Filterable="true" />
                                <ext:Column ID="Column3" runat="server" Width="150"  Text ="INDIVIDUAL GRADE ID" DataIndex="Code" Filterable="true" />
                                <ext:Column ID="Column4" runat="server" width="200" Text="INDIVIDUAL GRADE NAME" DataIndex="Name" Filterable="true"   />
                                <ext:CommandColumn ID="cmd_edit" runat="server" Width="50" Align="Center" Text="Edit">
                                    <Commands>
                                        <ext:GridCommand Icon="NoteEdit" CommandName="Edit">
                                            <ToolTip Text="Edit" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="cmd_edit_click">
                                            <EventMask ShowMask="true" Msg="Update data Please Wait..." />
                                            <ExtraParams>
								                <ext:Parameter Name="Values" Value="Ext.encode(#{grdView}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
							                </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
                                <ext:CommandColumn ID="cmd_Delete" runat="server" Width="50" Align="Center" Text="Delete">
                                    <Commands>
                                        <ext:GridCommand Icon="Delete" CommandName="Delete">
                                            <ToolTip Text="Edit" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="cmd_Delete_click">
                                            <EventMask ShowMask="true" Msg="Delete data Please Wait..." />
                                            <ExtraParams>
								                <ext:Parameter Name="Values" Value="Ext.encode(#{grdView}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
							                </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>
				            </Columns>
			            </ColumnModel>
                    </ext:GridPanel>
                </Items>
            </ext:FormPanel>
        </div>
    </div>
</asp:Content>