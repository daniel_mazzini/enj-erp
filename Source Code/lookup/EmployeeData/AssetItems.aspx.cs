﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using NewERDM.lib.BusinessClass;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using SpreadsheetLight;

namespace NewERDM.lookup.EmployeeData
{
    public partial class AssetItems : System.Web.UI.Page
    {
        #region Field

        UserApplication userApp;
        UserApplicationBL usrApplicationBL = new UserApplicationBL();

        #endregion Field

        #region Properties
        
        protected void Store_AssetType_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getAssetType();
        }
        #endregion Properties

        #region Method

        private static string pathWeb()
        {
            string strPathWeb = HttpContext.Current.Server.MapPath("~");
            return strPathWeb;
        }
        protected void getAssetType()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupAssetType();
            Store_AssetType.DataSource = dt;
            Store_AssetType.DataBind();
        }
        protected void getDataAsset_Update(string strFICNumber)
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.get_Asset_Detail_Update(strFICNumber);

            if (dt.Rows.Count > 0)
            {
                this.FICNumber.Text = dt.Rows[0]["FICNumber"].ToString();
                this.FICName.Text = dt.Rows[0]["FICName"].ToString();
                this.OldName.Text = dt.Rows[0]["OLDName"].ToString();
                this.ServiceTag.Text = dt.Rows[0]["ServiceTag"].ToString();
                this.SerialNo.Text = dt.Rows[0]["SerialNumber"].ToString();
                
                if (string.IsNullOrEmpty(dt.Rows[0]["PONumber"].ToString()))
                {
                    this.LinkUploadPO.Text = "No Attachement";
                    
                    this.f_UploadPO.Hidden = true;
                    this.LinkUploadPO.Hidden = false;
                    this.btn_UploadPO.Hidden = false;
                }
                else
                {
                    this.LinkUploadPO.Text = dt.Rows[0]["PONumber"].ToString();
                    this.f_UploadPO.Hidden = true;
                    this.LinkUploadPO.Hidden = false;
                    this.btn_UploadPO.Hidden = false;
                }

                this.AssetType.Text = dt.Rows[0]["AssetType"].ToString();
                this.BrandModel.Text = dt.Rows[0]["BrandModel"].ToString();
                this.AssetDetail.Text = dt.Rows[0]["AssetDetail"].ToString();
                this.DeliveryDate.Text = dt.Rows[0]["DeliveryDate"].ToString();
                this.DeliveryNo.Text = dt.Rows[0]["DeliveryNo"].ToString();
                this.ItemCode.Text = dt.Rows[0]["ItemCode"].ToString();
                this.AccCode.Text = dt.Rows[0]["AccountingCode"].ToString();
                this.CompanyAsset.Text = dt.Rows[0]["CompanyAsset"].ToString();
                this.JoinDomainFMI.Text = dt.Rows[0]["JoinDomainFMI"].ToString();
                this.JoinDomainTicket.Text = dt.Rows[0]["JoinDomainTicket"].ToString();
                this.JoinDomainBy.Text = dt.Rows[0]["JoinDomainBy"].ToString();
                this.AssetStatus.Text = dt.Rows[0]["AssetStatus"].ToString();
                this.DisposedTicket.Text = dt.Rows[0]["DisposedTicket"].ToString();
                this.DisposedDate.Text = dt.Rows[0]["DisposedDate"].ToString();
                this.DisposedLetter.Text = dt.Rows[0]["DisposedLetter"].ToString();
                this.DisposedBy.Text = dt.Rows[0]["DisposedBy"].ToString();

                if (string.IsNullOrEmpty(dt.Rows[0]["Photo"].ToString()))
                {
                    this.LinkUploadPhoto.Text = "No Attachement";

                    this.f_UploadPhoto.Hidden = true;
                    this.LinkUploadPhoto.Hidden = false;
                    this.btn_UploadPhoto.Hidden = false;
                }
                else
                {
                    this.LinkUploadPhoto.Text = dt.Rows[0]["Photo"].ToString();
                    this.f_UploadPhoto.Hidden = true;
                    this.LinkUploadPhoto.Hidden = false;
                    this.btn_UploadPhoto.Hidden = false;
                }

                this.AssetRemark.Text = dt.Rows[0]["AssetRemark"].ToString();
            }
        }
        //protected void getDetailBooking()
        //{
        //    DataTable dt = null;
        //    var objget = new clsItenary();
        //    dt = objget.get_booking_detail(this.f_bookingcode.Text.Trim());
        //    store_Detail.DataSource = dt;
        //    store_Detail.DataBind();

        //}

        #endregion Method


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                userApp = (UserApplication)Session["ses_user"];

                getAssetType();

                this.f_UploadPhoto.Hidden = false;
                this.LinkUploadPhoto.Hidden = true;
                this.btn_UploadPhoto.Hidden = true;

                this.f_UploadPO.Hidden = false;
                this.LinkUploadPO.Hidden = true;
                this.btn_UploadPO.Hidden = true;

                string queryString_pid = string.Empty;
                queryString_pid = Request.QueryString["FICNumber"];
                if (!string.IsNullOrEmpty(queryString_pid))
                {
                    this.FICNumber.ReadOnly = true;
                    getDataAsset_Update(queryString_pid);
                    //getDataAsset();
                }
 
            }
        }
        protected void btn_UploadPhoto_click(object sender, DirectEventArgs e)
        {
            this.f_UploadPhoto.Hidden = false;
            this.LinkUploadPhoto.Hidden = true;
            this.btn_UploadPhoto.Hidden = true;
        }
        protected void btn_UploadPO_click(object sender, DirectEventArgs e)
        {
            this.f_UploadPO.Hidden = false;
            this.LinkUploadPO.Hidden = true;
            this.btn_UploadPO.Hidden = true;
        }
        protected void btn_Save_click(object sender, DirectEventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];

            if (userApp == null)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Error",
                    Message = "Sorry, your must be relogin !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                if (string.IsNullOrEmpty(this.FICNumber.Text))
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Warning",
                        Message = "FIC Number Can't be Empty !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    return;
                }

                if (string.IsNullOrEmpty(this.AssetType.Text))
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Warning",
                        Message = "Asset Type Can't be Empty !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    return;
                }

                //check double booking code
                DataTable dt = new DataTable();
                var objGet = new clsEmployee();
                dt = objGet.get_CheckFICNumber(this.FICNumber.Text.Trim());

                string sPathWeb = pathWeb();

                if (dt.Rows.Count == 0)
                {
                    if (this.f_UploadPhoto.HasFile)
                    {
                        string namePhoto = FICNumber.Text + "_Photo.PDF";
                        this.f_UploadPhoto.PostedFile.SaveAs(String.Format("{0}\\UploadFile\\AssetItems\\Photo\\{1}", sPathWeb, namePhoto));
                        string fileTargetPathPhoto = Server.MapPath("~\\UploadFile\\AssetItems\\Photo\\");

                        if (this.f_UploadPO.HasFile)
                        {
                            string namePO = FICNumber.Text + "_PO.PDF";
                            this.f_UploadPO.PostedFile.SaveAs(String.Format("{0}\\UploadFile\\AssetItems\\PO\\{1}", sPathWeb, namePO));
                            string fileTargetPathPO = Server.MapPath("~\\UploadFile\\AssetItems\\PO\\");

                            if (this.AssetType.Text == "Software")
                            {
                                var InsertDataAsset = new clsEmployee();
                                InsertDataAsset.InsertItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 1, 0);
                            }
                            else
                            {
                                var InsertDataAsset = new clsEmployee();
                                InsertDataAsset.InsertItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 0, 0);
                            }
                        }
                        else
                        {
                            string namePO = "";
                            string fileTargetPathPO = "";

                            if (this.AssetType.Text == "Software")
                            {
                                var InsertDataAsset = new clsEmployee();
                                InsertDataAsset.InsertItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 1, 0);
                            }
                            else
                            {
                                var InsertDataAsset = new clsEmployee();
                                InsertDataAsset.InsertItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 0, 0);
                            }
                        }
                    }
                    else
                    {
                        string namePhoto = "";
                        string fileTargetPathPhoto = "";

                        if (this.f_UploadPO.HasFile)
                        {
                            string namePO = FICNumber.Text + "_PO.PDF";
                            this.f_UploadPO.PostedFile.SaveAs(String.Format("{0}\\UploadFile\\AssetItems\\PO\\{1}", sPathWeb, namePO));
                            string fileTargetPathPO = Server.MapPath("~\\UploadFile\\AssetItems\\PO\\");

                            if (this.AssetType.Text == "Software")
                            {
                                var InsertDataAsset = new clsEmployee();


                                InsertDataAsset.InsertItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 1, 0);
                            }
                            else
                            {
                                var InsertDataAsset = new clsEmployee();
                                InsertDataAsset.InsertItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 0, 0);
                            }
                        }
                        else
                        {
                            string namePO = "";
                            string fileTargetPathPO = "";

                            if (this.AssetType.Text == "Software")
                            {
                                var InsertDataAsset = new clsEmployee();
                                InsertDataAsset.InsertItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 1, 0);
                            }
                            else
                            {
                                var InsertDataAsset = new clsEmployee();
                                InsertDataAsset.InsertItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 0, 0);
                            }
                        }
                    }

                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Information",
                        Message = "Save Data Success",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO
                    });

                    //X.Redirect(CommonUtility.ResolveUrl("lookup/EmployeeData/AssetItems_Search.aspx"), "Loading ... ");
                }
                else
                {
                    if (this.f_UploadPhoto.HasFile)
                    {
                        string namePhoto = FICNumber.Text + "_Photo.PDF";
                        this.f_UploadPhoto.PostedFile.SaveAs(String.Format("{0}\\UploadFile\\AssetItems\\Photo\\{1}", sPathWeb, namePhoto));
                        string fileTargetPathPhoto = Server.MapPath("~\\UploadFile\\AssetItems\\Photo\\");

                        if (this.f_UploadPO.HasFile)
                        {
                            string namePO = FICNumber.Text + "_PO.PDF";
                            this.f_UploadPO.PostedFile.SaveAs(String.Format("{0}\\UploadFile\\AssetItems\\PO\\{1}", sPathWeb, namePO));
                            string fileTargetPathPO = Server.MapPath("~\\UploadFile\\AssetItems\\PO\\");

                            if(this.AssetType.Text == "Software")
                            {
                                var UpdateDataAsset = new clsEmployee();
                                UpdateDataAsset.UpdateItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 1, 0);
                            }
                            else
                            {
                                var UpdateDataAsset = new clsEmployee();
                                UpdateDataAsset.UpdateItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 0, 0);
                            }
                        }
                        else
                        {
                            string namePO = "";
                            string fileTargetPathPO = "";
                            
                            if (this.AssetType.Text == "Software")
                            {
                                var UpdateDataAsset = new clsEmployee();
                                UpdateDataAsset.UpdateItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 1, 0);
                            }
                            else
                            {
                                var UpdateDataAsset = new clsEmployee();
                                UpdateDataAsset.UpdateItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 0, 0);
                            }

                        }
                    }
                    else
                    {
                        string namePhoto = "";
                        string fileTargetPathPhoto = "";

                        if (this.f_UploadPO.HasFile)
                        {
                            string namePO = FICNumber.Text + "_PO.PDF";
                            this.f_UploadPO.PostedFile.SaveAs(String.Format("{0}\\UploadFile\\AssetItems\\PO\\{1}", sPathWeb, namePO));
                            string fileTargetPathPO = Server.MapPath("~\\UploadFile\\AssetItems\\PO\\");

                            if (this.AssetType.Text == "Software")
                            {
                                var UpdateDataAsset = new clsEmployee();
                                UpdateDataAsset.UpdateItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 1, 0);
                            }
                            else
                            {
                                var UpdateDataAsset = new clsEmployee();
                                UpdateDataAsset.UpdateItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 0, 0);
                            }
                        }
                        else
                        {
                            string namePO = "";
                            string fileTargetPathPO = "";

                            if (this.AssetType.Text == "Software")
                            {
                                var UpdateDataAsset = new clsEmployee();
                                UpdateDataAsset.UpdateItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 1, 0);
                            }
                            else
                            {
                                var UpdateDataAsset = new clsEmployee();
                                UpdateDataAsset.UpdateItemsAset(this.FICNumber.Text, this.FICName.Text, this.OldName.Text, this.ServiceTag.Text, this.SerialNo.Text,
                                    namePO, fileTargetPathPO, this.AssetType.Text, this.BrandModel.Text, this.AssetDetail.Text, Convert.ToDateTime(this.DeliveryDate.Text),
                                    this.DeliveryNo.Text, this.ItemCode.Text, this.AccCode.Text, this.CompanyAsset.Text, this.JoinDomainFMI.Text, this.JoinDomainTicket.Text,
                                    Convert.ToDateTime(this.JoinDomainDate.Text), this.JoinDomainBy.Text, this.AssetStatus.Text, this.DisposedTicket.Text,
                                    Convert.ToDateTime(this.DisposedDate.Text), this.DisposedLetter.Text, this.DisposedBy.Text, namePhoto, fileTargetPathPhoto,
                                    this.AssetRemark.Text, userApp.UserID, 0, 0);
                            }
                        }
                    }

                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Information",
                        Message = "Update Data Success",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO
                    });
                }
            }
        }
        protected void btn_close_click(object sender, DirectEventArgs e)
        {
            X.Redirect(CommonUtility.ResolveUrl("lookup/EmployeeData/AssetItems_Search.aspx"), "Loading ... ");
        }
    }
}