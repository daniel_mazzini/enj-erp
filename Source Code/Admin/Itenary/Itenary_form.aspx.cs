﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using NewERDM.lib.BusinessClass;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using SpreadsheetLight;


namespace NewERDM.Admin.Itenary
{
    public partial class Itenary_form : System.Web.UI.Page
    {
        #region field

        UserApplication userApp;
        UserApplicationBL usrApplicationBL = new UserApplicationBL();
        #endregion

        private static string pathWeb()
        {
            string strPathWeb = HttpContext.Current.Server.MapPath("~");
            return strPathWeb;
        }

        #region constructor
        protected void store_airline_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getAirline();
        }
        protected void store_justification_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getjustification();
        }
        protected void store_Accomodation_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getaccomodation();
        }
        protected void store_transport_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            gettransportation();
        }
        protected void Store_EmployeeName_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployee();
        }
        protected void store_RouteFrom_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCityFrom();
        }
        protected void store_RouteTO_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCityTo();
        }
        protected void store_FlightDetail_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getFlightDetail();
        }
        protected void store_status_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getstatus();
        }
        protected void store_payment_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getstatuspayment();
        }
        protected void store_store_Detail_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getDetailBooking();
        }
        protected void store_cat_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCostCodeCat();
        }
        protected void store_act_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCostCodeAct(this.f_Category.Text);
        }
        protected void store_cl_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCostClient();
        }
        protected void store_cc_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCostCodecc();
        }

        #endregion Construction

        #region method
        protected void getBookingDetail_Update(string strBookingCode)
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.get_Booking_Detail_Update(strBookingCode);

            if (dt.Rows.Count > 0)
            {
                this.ddlemployee.Text = dt.Rows[0]["Nama"].ToString();
                this.cmd_Accomodation.Text = dt.Rows[0]["Accomodation"].ToString();
                this.cmd_justification.Text = dt.Rows[0]["Justification"].ToString();
                this.cmd_transport.Text = dt.Rows[0]["Transportation"].ToString();
                this.Remark.Text = dt.Rows[0]["Remark"].ToString();
                this.cmb_airline.Text = dt.Rows[0]["f_alirlinecode"].ToString();
                this.f_bookingcode.Text = dt.Rows[0]["BookingCode"].ToString();
                this.f_dFligt.Text = dt.Rows[0]["f_dFligt"].ToString();
                this.cmb_cityform.Text = dt.Rows[0]["f_routefrom"].ToString();
                this.cmb_cityTo.Text = dt.Rows[0]["f_routeTo"].ToString();
                this.f_flightdetail.Text = dt.Rows[0]["f_flightdetail"].ToString();
                this.f_timelimit.Text = dt.Rows[0]["f_timelimit"].ToString();
                this.f_CancelCode.Text = dt.Rows[0]["f_CancelCode"].ToString();
                this.f_costcode.Text = dt.Rows[0]["f_costcode"].ToString();
                this.cmb_payment.Text = dt.Rows[0]["f_paymentStat"].ToString();
                this.cmb_status.Text = dt.Rows[0]["f_status"].ToString();
                //this.pathUpload.Text = dt.Rows[0]["f_UploadPdf"].ToString();

                //this.f_UploadPdf.Value = dt.Rows[0]["f_UploadPdf"].ToString();

                if (string.IsNullOrEmpty(dt.Rows[0]["f_UploadPdf"].ToString()))
                {
                    this.LinkUploadPdf.Text = "No Attachement";
                    this.f_UploadPdf.Hidden = true;
                    this.LinkUploadPdf.Hidden = false;
                    this.btn_Upload.Hidden = false;
                }
                else
                {
                    this.LinkUploadPdf.Text = dt.Rows[0]["f_UploadPdf"].ToString();
                    this.f_UploadPdf.Hidden = true;
                    this.LinkUploadPdf.Hidden = false;
                    this.btn_Upload.Hidden = false;
                }

                //this.f_bookingcode.ReadOnly = true;

                //this.wndEntri.Show();
            }
        }
        protected void getAirline()
        {
            DataTable dt = new DataTable();
            var objGet = new clsItenary();
            dt = objGet.getLookupAirline();
            store_airline.DataSource = dt;
        }
        protected void getstatus()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getLookupstatus();
            store_status.DataSource = dt;
            store_status.DataBind();
        }
        protected void getstatuspayment()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getLookuppayment();
            store_payment.DataSource = dt;
            store_payment.DataBind();
        }
        protected void getjustification()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getLookupJustification();
            store_justification.DataSource = dt;
            store_justification.DataBind();
        }
        protected void getaccomodation()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getLookupaccomodation();
            store_Accomodation.DataSource = dt;
            store_Accomodation.DataBind();
        }
        protected void gettransportation()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getLookuptransport();
            store_transport.DataSource = dt;
            store_transport.DataBind();
        }
        protected void getEmployee()
        {
            DataTable dt = new DataTable();
            var objget = new clsItenary();
            dt = objget.getLookupEmployee();
            Store_EmployeeName.DataSource = dt;
        }
        protected void getCityFrom()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getPOH();
            store_city1.DataSource = dt;
            store_city1.DataBind();
        }
        protected void getCityTo()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getPOH();
            store_city2.DataSource = dt;
            store_city2.DataBind();
        }
        protected void getFlightDetail()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getFlightDetail();
            store_FlightDetail.DataSource = dt;
            store_FlightDetail.DataBind();
        }
        protected void getDetailBooking()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.get_booking_detail(this.f_bookingcode.Text.Trim());
            store_Detail.DataSource = dt;
            store_Detail.DataBind();

        }
        protected void getCostCodeCat()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.get_costcode_category();
            store_cat.DataSource = dt;
            store_cat.DataBind();
        }
        protected void getCostCodeAct(string s_category)
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.get_costcode_activity(s_category);
            store_act.DataSource = dt;
            store_act.DataBind();
        }
        protected void getCostClient()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.get_costcode_client(this.f_Category.Text, f_Activity.Text);
            store_client.DataSource = dt;
            store_client.DataBind();
        }
        protected void getCostCodecc()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.get_costcode_costcode(this.f_Category.Text, this.f_Activity.Text, this.f_cli.Text);
            storecc.DataSource = dt;
            storecc.DataBind();
        }
        public void ExportToExcelDetail(DataTable dt)
        {
            SLDocument sl = new SLDocument();

            var objget = new clsItenary();
            dt = objget.getItenary_ExportToExcelDetail(this.f_bookingcode.Text);
            store_Detail.DataSource = dt;

            if (dt.Rows.Count > 0)
            {
                //Header
                sl.SetCellValue(1, 1, "Nama");
                sl.SetCellValue(1, 2, "Date Of Flight");
                sl.SetCellValue(1, 3, "Flight Detail");
                sl.SetCellValue(1, 4, "Booking Code");
                sl.SetCellValue(1, 5, "Route From");
                sl.SetCellValue(1, 6, "Route To");
                sl.SetCellValue(1, 7, "Status");
                sl.SetCellValue(1, 8, "Time Limit");

                //Color Header
                sl.ApplyNamedCellStyle(1, 1, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 2, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 3, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 4, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 5, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 6, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 7, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 8, SLNamedCellStyleValues.Accent1);


                //Counter
                int iRows = 0;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    iRows = 0 + i;

                    sl.SetCellValue(2 + iRows, 1, dt.Rows[i][0].ToString());
                    sl.SetCellValue(2 + iRows, 2, dt.Rows[i][1].ToString());
                    sl.SetCellValue(2 + iRows, 3, dt.Rows[i][2].ToString());
                    sl.SetCellValue(2 + iRows, 4, dt.Rows[i][3].ToString());
                    sl.SetCellValue(2 + iRows, 5, dt.Rows[i][4].ToString());
                    sl.SetCellValue(2 + iRows, 6, dt.Rows[i][5].ToString());
                    sl.SetCellValue(2 + iRows, 7, dt.Rows[i][6].ToString());
                    sl.SetCellValue(2 + iRows, 8, dt.Rows[i][7].ToString());
                }


                //Auto Fit Column
                sl.AutoFitColumn(1);
                sl.AutoFitColumn(2);
                sl.AutoFitColumn(3);
                sl.AutoFitColumn(4);
                sl.AutoFitColumn(5);
                sl.AutoFitColumn(6);
                sl.AutoFitColumn(7);
                sl.AutoFitColumn(8);


                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=TravelItineraryExcel.xlsx");
                sl.SaveAs(Response.OutputStream);
                Response.End();
            }
        }

        #endregion Method

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                userApp = (UserApplication)Session["ses_user"];

                getjustification();
                getaccomodation();
                gettransportation();
                getEmployee();
                getCityFrom();
                getCityTo();
                getFlightDetail();
                getstatus();
                getstatuspayment();
                getAirline();

                this.f_UploadPdf.Hidden = false;
                this.LinkUploadPdf.Hidden = true;
                this.btn_Upload.Hidden = true;
                this.TAGADD.Text = "";


                string queryString_pid = string.Empty;
                queryString_pid = Request.QueryString["bookingCode"];
                if (!string.IsNullOrEmpty(queryString_pid))
                {
                    getBookingDetail_Update(queryString_pid);
                    getDetailBooking();
                }
             }

           
        }
        protected void btn_save_header_click(object sender, DirectEventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];

            if (userApp == null)
            {
                this.txtUsername.Text = txt_UserID.Text;
                chkGrid.ClearSelection();
                this.WindowLogin.Show();
            }
            else
            {
                this.txt_UserID.Text = userApp.UserID;
                if (string.IsNullOrEmpty(this.ddlemployee.Text))
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "WARNING",
                        Message = "EMPLOYEE NAME CAN'T BE EMPTY !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    chkGrid.ClearSelection();
                    return;
                }
                if (string.IsNullOrEmpty(this.f_bookingcode.Text))
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "WARNING",
                        Message = "BOOKING CODE CAN'T BE EMPTY !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    chkGrid.ClearSelection();
                    return;
                }

                if (string.IsNullOrEmpty(this.cmd_Accomodation.Text))
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "WARNING",
                        Message = "ACCOMODATION CAN'T BE EMPTY !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    chkGrid.ClearSelection();
                    return;
                }
                if (string.IsNullOrEmpty(this.cmd_justification.Text))
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "WARNING",
                        Message = "JUSTIFICATION CAN'T BE EMPTY !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    chkGrid.ClearSelection();
                    return;
                }
                if (string.IsNullOrEmpty(this.cmd_transport.Text))
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "WARNING",
                        Message = "TRANSPORTATION CAN'T BE EMPTY !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    chkGrid.ClearSelection();
                    return;
                }
                //18-02-2015 (Request baru dari Pak Rustam)
                //1.Simpan data booking header jika user klik tanggal date of flight
                //2.Simpan data booking detail ditambahkan tombol save

                //insert table Employee Temp
                DataTable dtEmpTmp = new DataTable();
                var objGetEmpTmp = new clsItenary();
                dtEmpTmp = objGetEmpTmp.get_CheckEmployeeName(this.ddlemployee.Text);

                if (dtEmpTmp.Rows.Count == 0)
                {
                    var objInsEmpTmp = new clsItenary();
                    objInsEmpTmp.InsertEmployeeTemp(this.ddlemployee.Text, this.f_bookingcode.Text, userApp.UserID);
                }

                //check double booking code
                DataTable dt = new DataTable();
                var objGet = new clsItenary();
                dt = objGet.get_CheckBookingCode(this.f_bookingcode.Text.Trim());

                string sPathWeb = pathWeb();

                if (dt.Rows.Count == 0)
                {
                    if (this.f_UploadPdf.HasFile)
                    {
                        string nameEdit = f_bookingcode.Text + "_Itinerary.PDF";
                        this.f_UploadPdf.PostedFile.SaveAs(String.Format("{0}\\UploadFile\\Itinerary\\{1}", sPathWeb, nameEdit));
                        string fileTargetPath = Server.MapPath("~\\UploadFile\\Itinerary\\");

                        var objInsHeader = new clsItenary();
                        objInsHeader.InsertBooking(this.f_bookingcode.Text.Trim(), this.ddlemployee.Text.Trim(), this.cmd_justification.Text.Trim(),
                                                    this.cmd_Accomodation.Text.Trim(), this.cmd_transport.Text.Trim(), userApp.UserID, this.Remark.Text, nameEdit, fileTargetPath);//this.txt_UserID.Text,, //"Masih ERROR", "Masih ERROR");
                    }
                    else
                    {
                        string nameEdit = "";
                        string fileTargetPath = "";

                        var objInsHeader = new clsItenary();
                        objInsHeader.InsertBooking(this.f_bookingcode.Text.Trim(), this.ddlemployee.Text.Trim(), this.cmd_justification.Text.Trim(),
                                                    this.cmd_Accomodation.Text.Trim(), this.cmd_transport.Text.Trim(), userApp.UserID, this.Remark.Text, nameEdit, fileTargetPath);// this.txt_UserID.Text, //"Masih ERROR", "Masih ERROR"); 
                    }

                }
                else
                {
                    if (this.f_UploadPdf.HasFile)
                    {
                        string nameEdit = f_bookingcode.Text + "_Itinerary.PDF";
                        this.f_UploadPdf.PostedFile.SaveAs(String.Format("{0}\\UploadFile\\Itinerary\\{1}", sPathWeb, nameEdit));
                        string fileTargetPath = Server.MapPath("~\\UploadFile\\Itinerary\\");
                        //hanya melakukan Update header
                        var objUpdHeader = new clsItenary();
                        objUpdHeader.UpdateBooking(this.f_bookingcode.Text.Trim(), this.ddlemployee.Text.Trim(), this.cmd_justification.SelectedItem.Value,
                                                this.cmd_Accomodation.SelectedItem.Value, this.cmd_transport.SelectedItem.Value, userApp.UserID, this.Remark.Text, nameEdit, fileTargetPath);//this.txt_UserID.Text, //"Masih ERROR", "Masih ERROR");



                        //Update Employee Temporary
                        //objUpdHeader.UpdateEmployeeTemp(this.f_bookingcode.Text, this.ddlemployee.Text, userApp.UserID);
                    }
                    else
                    {
                        string nameEdit = "";
                        string fileTargetPath = "";

                        //hanya melakukan Update header
                        var objUpdHeader = new clsItenary();
                        objUpdHeader.UpdateBooking(this.f_bookingcode.Text.Trim(), this.ddlemployee.Text.Trim(), this.cmd_justification.SelectedItem.Value,
                                                this.cmd_Accomodation.SelectedItem.Value, this.cmd_transport.SelectedItem.Value, userApp.UserID, this.Remark.Text, nameEdit, fileTargetPath);// this.txt_UserID.Text, //"Masih ERROR", "Masih ERROR");

                        //Update Employee Temporary
                        //objUpdHeader.UpdateEmployeeTemp(this.f_bookingcode.Text, this.ddlemployee.Text, userApp.UserID);
                    }
                }
                chkGrid.CheckOnly = true;
            }
        }
        protected void btn_save_detail_row_click(object sender, DirectEventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];

            if (userApp == null)
            {
                this.txtUsername.Text = txt_UserID.Text;
                chkGrid.ClearSelection();
                this.WindowLogin.Show();
            }
            else
            {
                this.txt_UserID.Text = userApp.UserID;

                string jsonValues = e.ExtraParams["Values"];
                List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);

                //Validasi, jika user tidak melakukan click baris
                if (records.Count == 0)
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "WARNING",
                        Message = "Please selected row !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    return;
                }
                else
                {
                    foreach (var record in records)
                    {
                        if (string.IsNullOrEmpty(record["f_dFligt"].ToString()))
                        {
                            X.MessageBox.Show(new MessageBoxConfig
                            {
                                Title = "WARNING",
                                Message = "FLIGHT DATE CAN'T BE EMPTY !",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            });
                            return;
                        }
                        if (record["f_dFligt"].ToString() == null)
                        {
                            X.MessageBox.Show(new MessageBoxConfig
                            {
                                Title = "WARNING",
                                Message = "FLIGHT DATE CAN'T BE EMPTY !",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            });
                            return;
                        }

                        if (string.IsNullOrEmpty(record["f_routefrom"].ToString()))
                        {
                            X.MessageBox.Show(new MessageBoxConfig
                            {
                                Title = "WARNING",
                                Message = "ROUTE FROM CAN'T BE EMPTY !",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            });
                            return;
                        }

                        if (string.IsNullOrEmpty(record["f_routeTo"].ToString()))
                        {
                            X.MessageBox.Show(new MessageBoxConfig
                            {
                                Title = "WARNING",
                                Message = "ROUTE TO CAN'T BE EMPTY !",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            });
                            return;
                        }

                        if (string.IsNullOrEmpty(record["f_flightdetail"].ToString()))
                        {
                            X.MessageBox.Show(new MessageBoxConfig
                            {
                                Title = "WARNING",
                                Message = "FLIGHT DETAIL CAN'T BE EMPTY !",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            });
                            return;
                        }

                        if (string.IsNullOrEmpty(record["f_airline"].ToString()))
                        {
                            X.MessageBox.Show(new MessageBoxConfig
                            {
                                Title = "WARNING",
                                Message = "AIRLINE CODE CAN'T BE EMPTY !",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            });
                            return;
                        }

                        if (string.IsNullOrEmpty(record["f_status"].ToString()))
                        {
                            X.MessageBox.Show(new MessageBoxConfig
                            {
                                Title = "WARNING",
                                Message = "STATUS CAN'T BE EMPTY !",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            });
                            return;
                        }

                        if (string.IsNullOrEmpty(record["f_timelimit"].ToString()))
                        {
                            X.MessageBox.Show(new MessageBoxConfig
                            {
                                Title = "WARNING",
                                Message = "TIME LIMIT CAN'T BE EMPTY !",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            });
                            return;
                        }

                        if (string.IsNullOrEmpty(record["f_paymentStat"].ToString()))
                        {
                            X.MessageBox.Show(new MessageBoxConfig
                            {
                                Title = "WARNING",
                                Message = "PAYMENT STATUS CAN'T BE EMPTY !",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            });
                            return;
                        }

                        if (this.TAGADD.Text == "Add Detail" && string.IsNullOrEmpty(record["f_costcode"].ToString()))
                        {
                            var objInsDetail = new clsItenary();
                            objInsDetail.InsertBookingDetail(this.f_bookingcode.Text.Trim(), Convert.ToDateTime(record["f_dFligt"]), record["f_routefrom"], record["f_routeTo"], record["f_flightdetail"], record["f_airline"], record["f_status"], Convert.ToDateTime(record["f_timelimit"]), record["f_CancelCode"], record["f_costcode"], record["f_paymentStat"], userApp.UserID);//this.txt_UserID.Text); 
                        }
                        else if (this.TAGADD.Text == "Add Detail" && !string.IsNullOrEmpty(record["f_costcode"].ToString()))
                        {
                            store_Detail.Reload();
                            this.grdDetail.Refresh();
                        }
                        else
                        {
                            X.MessageBox.Show(new MessageBoxConfig
                            {
                                Title = "WARNING",
                                Message = "ALREADY EXSIST DATA, PLEASE CLICK BUTTON EDIT !",
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.WARNING
                            });
                            return;
                        }
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "INFORMATION",
                            Message = "DATA SAVED",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO
                        });
                        btn_new.Tag = "";
                        this.TAGADD.Text = "";
                    }
                }

                store_Detail.Reload();
                this.grdDetail.Refresh();
            }
        }
        protected void btn_edit_detail_click(object sender, DirectEventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];
            //if (this.TAGADD.Text == "Add Detail")
            //{
            //    X.MessageBox.Show(new MessageBoxConfig
            //    {
            //        Title = "WARNING",
            //        Message = "UPDATE FAILED",
            //        Buttons = MessageBox.Button.OK,
            //        Icon = MessageBox.Icon.WARNING
            //    });
            //    return;
            //}

            if (userApp == null)
            {
                this.txtUsername.Text = txt_UserID.Text;
                chkGrid.ClearSelection();
                this.WindowLogin.Show();
                //X.MessageBox.Show(new MessageBoxConfig
                //{
                //    Title = "WARNING",
                //    Message = "Session timeout, please relogin !",
                //    Buttons = MessageBox.Button.OK,
                //    Icon = MessageBox.Icon.WARNING
                //});
                //return;
            }
            else
            {
                this.txt_UserID.Text = userApp.UserID;

                string jsonValues = e.ExtraParams["Values"];
                List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);

                if (records.Count == 0)
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "WARNING",
                        Message = "Please selected row !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    return;
                }
                else
                {
                    foreach (var record in records)
                    {
                        if (string.IsNullOrEmpty(record["f_costcode"]) && string.IsNullOrEmpty(this.txt_costCode.Text))
                        {
                            string dateFlight = Convert.ToDateTime(record["f_dFligt"].ToString()).ToShortDateString();
                            string dateTimeLine = Convert.ToDateTime(record["f_timelimit"].ToString()).ToShortDateString();

                            var objUpdate = new clsItenary();
                            objUpdate.UpdateBookingDetail(Convert.ToInt16(record["pid"]), Convert.ToDateTime(dateFlight), record["f_routefrom"], record["f_routeTo"], record["f_flightdetail"], record["f_airline"], record["f_status"], Convert.ToDateTime(dateTimeLine), record["f_CancelCode"], record["f_costcode"], record["f_paymentStat"], userApp.UserID);//this.txt_UserID.Text); 
                        }
                        else if (!string.IsNullOrEmpty(record["f_costcode"]))
                        {
                            string dateFlight = Convert.ToDateTime(record["f_dFligt"].ToString()).ToShortDateString();
                            string dateTimeLine = Convert.ToDateTime(record["f_timelimit"].ToString()).ToShortDateString();

                            var objUpdate = new clsItenary();
                            objUpdate.UpdateBookingDetail(Convert.ToInt16(record["pid"]), Convert.ToDateTime(dateFlight), record["f_routefrom"], record["f_routeTo"], record["f_flightdetail"], record["f_airline"], record["f_status"], Convert.ToDateTime(dateTimeLine), record["f_CancelCode"], record["f_costcode"], record["f_paymentStat"], userApp.UserID);//this.txt_UserID.Text); 
                        }
                        else if (string.IsNullOrEmpty(record["f_costcode"]) && !string.IsNullOrEmpty(this.txt_costCode.Text))
                        {
                            string dateFlight = Convert.ToDateTime(record["f_dFligt"].ToString()).ToShortDateString();
                            string dateTimeLine = Convert.ToDateTime(record["f_timelimit"].ToString()).ToShortDateString();

                            var objUpdate = new clsItenary();
                            objUpdate.UpdateBookingDetail(Convert.ToInt16(record["pid"]), Convert.ToDateTime(dateFlight), record["f_routefrom"], record["f_routeTo"], record["f_flightdetail"], record["f_airline"], record["f_status"], Convert.ToDateTime(dateTimeLine), record["f_CancelCode"], record["f_costcode"], record["f_paymentStat"], userApp.UserID);//this.txt_UserID.Text); 
                        }

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "INFORMATION",
                            Message = "DATA UPDATED",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO
                        });
                    }
                }
                chkGrid.ClearSelection();
                this.grdDetail.ClearListeners();
                //this.grdDetail.ClearContent();
                this.grdDetail.Refresh();
                store_Detail.Reload();
            }
        }
        protected void btn_NewTag_Click(object sender, DirectEventArgs e)
        {
            this.btn_new.Tag = "Add Detail";
            this.TAGADD.Text = "Add Detail";
        }
        protected void btn_Upload_click(object sender, DirectEventArgs e)
        {
            this.f_UploadPdf.Hidden = false;
            this.LinkUploadPdf.Hidden = true;
            this.btn_Upload.Hidden = true;
        }
        protected void btn_showUp_Click(Object sender, DirectEventArgs e)
        {
            txt_costCode.Text = string.Empty;
            getCostCodeCat();
            wndcostcode.Show();
        }
        protected void btn_pick_click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(txt_costCode.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Please Select Cost Code",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }

            this.grdCostCode.ClearContent();
            this.grdClient.ClearContent();
            this.grdCatAct.ClearContent();
            this.wndcostcode.Close();
            store_Detail.Reload();
            this.grdDetail.Refresh();
        }
        protected void Cell_Click(object sender, DirectEventArgs e)
        {
            CellSelectionModel sm = this.grdCostCode_Cat.GetSelectionModel() as CellSelectionModel;
            this.f_Category.Text = sm.SelectedCell.Value;
            getCostCodeAct(this.f_Category.Text);

        }
        protected void Cell_ACT_Click(object sender, DirectEventArgs e)
        {
            CellSelectionModel sm = this.grdCatAct.GetSelectionModel() as CellSelectionModel;
            f_Activity.Text = sm.SelectedCell.Value;
            getCostClient();
        }
        protected void Cell_Cli_Click(object sender, DirectEventArgs e)
        {
            CellSelectionModel sm = this.grdClient.GetSelectionModel() as CellSelectionModel;
            f_cli.Text = sm.SelectedCell.Value;
            getCostCodecc();
        }
        protected void Cell_cost_Click(object sender, DirectEventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];

            CellSelectionModel sm = this.grdCostCode.GetSelectionModel() as CellSelectionModel;
            txt_costCode.Text = sm.SelectedCell.Value;

            string jsonValues = e.ExtraParams["Values"];
            List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);

            foreach (var record in records)
            {
                if (this.TAGADD.Text == "Add Detail")
                {
                    var objInsDetail = new clsItenary();
                    objInsDetail.InsertBookingDetail(this.f_bookingcode.Text.Trim(), Convert.ToDateTime(record["f_dFligt"]), record["f_routefrom"], record["f_routeTo"], record["f_flightdetail"], record["f_airline"], record["f_status"], Convert.ToDateTime(record["f_timelimit"]), record["f_CancelCode"], this.txt_costCode.Text, record["f_paymentStat"], userApp.UserID);
                }
                else
                {
                    //DataTable dt = new DataTable();
                    //var objGet = new clsItenary();
                    //dt = objGet.get_CheckCostCode(record["f_costcode"], this.f_bookingcode.Text.Trim());

                    //if (dt.Rows.Count == 0)
                    //{

                    //}
                    //else
                    //{
                    string dateFlight = Convert.ToDateTime(record["f_dFligt"].ToString()).ToShortDateString();
                    string dateTimeLine = Convert.ToDateTime(record["f_timelimit"].ToString()).ToShortDateString();

                    var objUpdate = new clsItenary();
                    objUpdate.UpdateBookingDetail(Convert.ToInt16(record["pid"]), Convert.ToDateTime(dateFlight), record["f_routefrom"], record["f_routeTo"], record["f_flightdetail"], record["f_airline"], record["f_status"], Convert.ToDateTime(dateTimeLine), record["f_CancelCode"], txt_costCode.Text, record["f_paymentStat"], userApp.UserID);//this.txt_UserID.Text); 
                    //}
                }
            }
        }
        protected void btn_delete_detail(object sender, DirectEventArgs args)
        {
            String jsonSelectedRows = args.ExtraParams["Value"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            if (selectedRowData.Length == 0)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Please selected row !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        var objDelete = new clsItenary();
                        objDelete.DeleteBooking_Detail(Convert.ToInt16(row["pid"]));
                    }
                    catch
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "CAN'T DELETE",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                    }
                }
            }
            store_Detail.Reload();
            this.grdDetail.Refresh();
        }
        protected void btn_close_click(object sender, DirectEventArgs e)
        {
            this.grdDetail.ClearContent();
            this.chkGrid.ClearSelection();

            X.Redirect(CommonUtility.ResolveUrl("Dashboard_itenary.aspx"), "Loading ... ");
           
        }
        protected void ToExcelDetail(object sender, EventArgs e)
        {
            ExportToExcelDetail((DataTable)ViewState["grdDetail"]);
        }
        protected void btn_New_Input_Click(object sender, DirectEventArgs e)
        {
            this.grdDetail.ClearContent();
            this.chkGrid.ClearSelection();

            X.Redirect(CommonUtility.ResolveUrl("Admin/Itenary/Itenary_form.aspx"), "Loading ... ");
        }
        protected void btn_login_Click(Object sender, DirectEventArgs e)
        {
            try
            {
                // string  strPassword = CommonUtility.encryptText(this.txtPassword.Text);
                string strPassword = this.txtPassword.Text;
                userApp = usrApplicationBL.getUserApplicationByUserNamePassword(this.txtUsername.Text.Trim(), strPassword);
                if (userApp != null)
                {
                    Session["ses_user"] = userApp;
                    this.WindowLogin.Close();
                    //X.Redirect("app/mMain.aspx", "Loading ... ");
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Peringatan",
                        Message = "User Name Atau Password Salah !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        AnimEl = this.btn_login.ClientID

                    });

                }

            }
            catch (Exception ex)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Title = "Peringatan",
                    Message = "Kesalahan Koneksi !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    AnimEl = this.btn_login.ClientID

                });
            }
        }

    }
}