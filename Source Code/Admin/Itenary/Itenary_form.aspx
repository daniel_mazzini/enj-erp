﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Itenary_form.aspx.cs" Inherits="NewERDM.Admin.Itenary.Itenary_form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <ext:XScript ID="XScript1" runat="server">
    <script>
        var addmaster = function () {
            var grid = #{grdDetail};
            grid.editingPlugin.cancelEdit();

            // Create a record instance through the ModelManager
            var r = Ext.ModelManager.create({
                f_dFligt       : '',
                f_routefrom    : '',
                f_routeTo      : '',
                f_flightdetail : '',
                f_airline      : '',
                f_timelimit    : '',
                f_CancelCode   : '',
                f_costcode     : '',
                f_paymentStat  : '',
                f_status       : ''
            }, 'data');
            //grid.store.insert(0, r);
            //grid.editingPlugin.startEdit(0, 0);
            grid.store.insert(HTMLTableRowElement+1,r);
            grid.editingPlugin.startEdit(HTMLTableRowElement+1,HTMLTableRowElement+1);
        };
       
        //var saveData = function () {
        //    App.Hidden1.setValue(Ext.encode(App.grdTravel.getRowsValues({selectedOnly : false})));
        //};
        var exportDetailInput = function ()
        {
            App.Hidden1.setValue(Ext.encode(App.grdDetail.getRowsValues({ selectedOnly: false })));
        }
  
    </script>
</ext:XScript>
    <style>
	  .my-scale.x-btn-default-Small-icon-text-left .x-btn-icon {
			width: 16px;
		}
	   .add32 {
			background-image : url(img/Word-icon.png) !important;
		}   
         .kanan{text-align:right;}
        .Fkanan
         {
            text-align:right;
            border:none;
            background-image: none;
        }
        .my-panel .x-panel-header {
			background-color: #dfe8f6 !important;
			 
		}
 
		.my-panel .x-panel-body {
			border: none;
			background-color: #dfe8f6 !important;
		}

         
         .search-item {
            font          : normal 11px tahoma, arial, helvetica, sans-serif;
            padding       : 3px 10px 3px 10px;
            border        : 1px solid #fff;
            border-bottom : 1px solid #eeeeee;
            white-space   : normal;
            color         : #555;
        }
        
        .search-item h3 {
            display     : block;
            font        : inherit;
            font-weight : bold;
            color       : #222;
            margin      :0px;
        }

        .search-item h3 span {
            float       : right;
            font-weight : normal;
            margin      : 0 0 5px 5px;
            width       : 100px;
            display     : block;
            clear       : none;
        } 
        
        p { width: 650px; }
        
        .ext-ie .x-form-text { position : static !important; }
 	</style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 <div class="row">
     <div class="col-md-12">
        <ext:FormPanel runat="server" ID="frmverified" Title =""  BodyPadding="12" Header="false"  Frame="false"  Border="false" DisabledCls="true"  >
            <Items>
                <ext:FieldContainer 
                                        runat="server"
                                        AnchorHorizontal="100%"
                                        Layout="HBoxLayout">
                                <Items>
                                       <ext:TextField ID="TAGADD" runat="server" Hidden="true" />
                                          <ext:ComboBox ID="ddlemployee" runat="server" TypeAhead="false" ForceSelection="false" FieldLabel="Employee Name"  Width="320"  HideBaseTrigger="true"   MinChars="0" TriggerAction="Query" PageSize="10"  QueryMode="Local" DisplayField="Name" ValueField="Name" EmptyText="Please Type Employee Name ..">
                                                                   <Store>
				                                                       <ext:Store 
							                                                ID="Store_EmployeeName" 
							                                                runat="server" 
							                                                    >
							                                                <Model>
								                                                <ext:Model ID="Model16" runat="server"      >
									                                                <Fields>
										                                                <ext:ModelField Name="ID"  />
										                                                <ext:ModelField Name="Name"  />
									                                                </Fields>
								                                                </ext:Model>
							                                                </Model>
				                                                  </ext:Store>
                                                                 </Store>   
                                                  <ListConfig  LoadingText="Searching...">
                                                    <ItemTpl runat="server">
                                                        <Html>
                                                            <div class="search-item">
							                                    {ID}</br>
							                                     <h3>{Name}</h3>
						                                    </div>
                                                        </Html>
                                                    </ItemTpl>
                                                </ListConfig>
                                                </ext:ComboBox>
                        
                                <ext:TextField runat="server" ID="f_EmployeeID" Hidden="true" />
                                <ext:ComboBox TabIndex="1" runat="server" ID="cmd_justification" FieldLabel="Justification" Margins="0 0 0 20" DisplayField="Name" ValueField="Code" Editable="false" >
                                    <Store>
                                        <ext:Store runat="server" ID="store_justification" AutoLoad="true" OnReadData="store_justification_RefreshData" >
                                            <Model>
                                                <ext:Model ID="Model2" runat="server"  >
                                                    <Fields>
                                                        <ext:ModelField Name="Code"   />
                                                        <ext:ModelField Name="Name"  />
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                    </ext:Store>
                                    </Store>
                                </ext:ComboBox>
                                </Items>
                                </ext:FieldContainer>
                               <ext:FieldContainer 
                                        runat="server"
                                        AnchorHorizontal="100%"
                                        Layout="HBoxLayout">
                                <Items>
                                    <ext:TextField TabIndex="2" runat="server" ID="f_bookingcode" FieldLabel="Booking Code"  Width="320"/>
                                    <ext:ComboBox  TabIndex="3" runat="server" ID="cmd_Accomodation" FieldLabel="Accomodation" Margins="0 0 0 20" DisplayField="Name" ValueField="Code" Editable="false"  >
                                            <Store>
                                                <ext:Store runat="server" ID="store_Accomodation" AutoLoad="true" OnReadData="store_Accomodation_RefreshData" >
                                                    <Model>
                                                        <ext:Model ID="Model3" runat="server"  >
                                                            <Fields>
                                                                <ext:ModelField Name="Code"   />
                                                                <ext:ModelField Name="Name"  />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                            </ext:Store>
                                            </Store>
                                    </ext:ComboBox>                                   
                                </Items>
                                </ext:FieldContainer>
                          
                                <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                                    <Items>
                                        <ext:ComboBox TabIndex="4" runat="server" ID="cmd_transport" FieldLabel="Transportation"    DisplayField="Name" ValueField="Code"   Editable="false"   >
                                            <Store>
                                                <ext:Store runat="server" ID="store_transport" AutoLoad="true" OnReadData="store_transport_RefreshData" >
                                                    <Model>
                                                        <ext:Model ID="Model5" runat="server"  >
                                                            <Fields>
                                                                <ext:ModelField Name="Code"   />
                                                                <ext:ModelField Name="Name"  />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                            </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                        <ext:FileUploadField runat="server" ID="f_UploadPdf" TabIndex="5" FieldLabel="Upload E-Ticket " Width="380" Margins="0 0 0 85" Icon="Attach" />
                                    
                                        <ext:TextField runat="server" ID="LinkUploadPdf" FieldLabel="Upload E-Ticket" Width="300" Margins="0 0 0 85"></ext:TextField>                                  
                                    
                                        <ext:Button runat="server" ID="btn_Upload" Text="Attach..." Icon="Attach" Hidden="true" Margins="0 0 0 10"  > 
                                            <DirectEvents>
                                                <Click OnEvent="btn_Upload_click">
                                                    <EventMask ShowMask="true" />
                                                </Click>
                                            </DirectEvents>
                                        </ext:Button>                                       
                                        <ext:TextField runat="server" id="Remark" TabIndex="6" FieldLabel="Remarks " Width="320" MarginSpec="0 0 0 20" />
                                        <ext:TextField runat="server" ID="txt_UserID" Hidden="true" />
                                    </Items>
                                 </ext:FieldContainer>
					            <ext:GridPanel  id="grdDetail"  runat="server" region="Center" Layout="FitLayout" Height="280" Padding="8" EnableColumnHide="false" >
							     <Store>
									     <ext:Store runat="server" ID="store_Detail" PageSize="10"  OnReadData="store_store_Detail_RefreshData" >
										       <Model>
											       <ext:Model ID="Model4" runat="server" IDProperty="pid" Name="data"  >
												    <Fields>
                                                        <ext:ModelField Name ="pid" />
							                            <ext:ModelField Name="f_dFligt" UseNull="true" DateFormat="ddMMyyyy"/>
													    <ext:ModelField Name="f_routefrom" />
													    <ext:ModelField Name="f_routeTo" />
													    <ext:ModelField Name="f_flightdetail" />
                                                        <ext:ModelField Name ="f_airline" />
													    <ext:ModelField Name="f_timelimit" DateFormat="ddMMyyyy" />
                                                        <ext:ModelField Name="f_CancelCode" />
                                                        <ext:ModelField Name="f_paymentStat" />
                                                        <ext:ModelField Name="f_costcode" />
                                                        <ext:ModelField Name="f_status" />                                                    
												    </Fields>
											    </ext:Model>
										       </Model>
									     </ext:Store>
							     </Store>
                                  <View>
					                     <ext:GridView ID="grdDetail_view" runat="server" LoadMask="false" />
			                      </View>
							      <ColumnModel ID="ColumnModel1"  runat="server"> 
							        <Columns>       
                                <ext:Column ID="Column21" runat="server"  Text="Seq.No" Hidden="true" width="40" DataIndex="pid">
                                                <Editor>
                                                    <ext:TextField runat="server" ID="f_pid" />
                                                </Editor>
                                            </ext:Column>
										    <ext:DateColumn runat="server" width="100" Text="Date Of FLight"  DataIndex="f_dFligt" Format="ddd, dd-MMM-yyyy" >
                                            <Editor>
                                                <ext:DateField ID="f_dFligt" runat="server" Name="dFlight" Format="dd-MMM-yyyy" />
                                              </Editor>
                                            </ext:DateColumn>

										    <ext:Column ID="Column1" runat="server" width="100" Text="Route From"  DataIndex="f_routefrom" AutoDataBind="true" >
                                             <Editor>
                                                     <ext:ComboBox ID="cmb_cityform" AutoShow="false" runat="server" TypeAhead="false" ForceSelection="false"
                                                          Width="400" MinChars="0" TriggerAction="Query" 
                                                          QueryMode="Local" DisplayField="Name" ValueField="Code" AutoDataBind="true" >
                                                    <Store>
				                                        <ext:Store ID="store_city1" runat="server" OnReadData="store_RouteFrom_RefreshData" >
                                                            <Model>
								                                <ext:Model ID="Model8" runat="server" >
									                                <Fields>
										                                <%--<ext:ModelField Name="ID"  />--%>
                                                                        <ext:ModelField Name="Code"   />
										                                <ext:ModelField Name="Name"  />
									                                </Fields>
								                                </ext:Model>
							                                </Model>
				                                        </ext:Store>
                                                    </Store>
                                                     </ext:ComboBox>
                                              </Editor>
                                            </ext:Column>

										    <ext:Column ID="Column23" runat="server" width="100" Text="Route To"  DataIndex="f_routeTo" AutoDataBind="true" >
                                             <Editor>
                                                     <ext:ComboBox ID="cmb_cityTo" AutoShow="false" runat="server" TypeAhead="false" ForceSelection="false"
                                                          Width="400" MinChars="0" TriggerAction="Query" 
                                                          QueryMode="Local" DisplayField="Name" ValueField="Code" AutoDataBind="true" >
                                                       <Store>
                                                            <ext:Store runat="server" ID="store_city2" AutoLoad="true" OnReadData="store_RouteTO_RefreshData" >
                                                                <Model>
                                                                    <ext:Model ID="Model9" runat="server"  >
                                                                        <Fields>
                                                                            <ext:ModelField Name="Code"   />
                                                                            <ext:ModelField Name="Name"  />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                        </ext:Store>
                                                        </Store>
                                                    </ext:ComboBox>
                                              </Editor>
                                            </ext:Column>

                                            <ext:Column ID="Column24" runat="server" width="80" Text="Air Line"  DataIndex="f_airline" >
                                             <Editor>
                                                     <ext:ComboBox  runat="server" ID="cmb_airline"  DisplayField="Name" ValueField="Code" ForceSelection="false" TypeAhead="false" TriggerAction="Query" QueryMode="Local" >
                                                       <Store>
                                                            <ext:Store runat="server" ID="store_airline" AutoLoad="true" OnReadData="store_airline_RefreshData" >
                                                                <Model>
                                                                    <ext:Model ID="Model10" runat="server"  >
                                                                        <Fields>
                                                                            <ext:ModelField Name="Code" />
                                                                            <ext:ModelField Name="Name"  />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                        </ext:Store>
                                                        </Store>
                                                    </ext:ComboBox>
                                              </Editor>
                                            </ext:Column>

										    <ext:Column ID="Column25" runat="server" width="100" Text="Flight Detail" DataIndex="f_flightdetail" >
                                             <Editor>
                                                   <ext:ComboBox ID="f_flightdetail" AutoShow="true" runat="server" TypeAhead="false" ForceSelection="false" Width="400"  HideBaseTrigger="True"  MinChars="0" TriggerAction="Query"  QueryMode="Local" DisplayField="Name" ValueField="Name" >
                                                    <Store>
				                                        <ext:Store ID="store_FlightDetail" runat="server" >
                                                            <Model>
								                                <ext:Model ID="Model19" runat="server"      >
									                                <Fields>
										                                <ext:ModelField Name="Name"  />
									                                </Fields>
								                                </ext:Model>
							                                </Model>
				                                        </ext:Store>
                                                    </Store>   
                                                </ext:ComboBox>
                                              </Editor>
                                            </ext:Column>

                                             <ext:Column ID="Column26" runat="server"  Text="Status" width="80" DataIndex="f_status">
                                             <Editor>
                                                    <ext:ComboBox  runat="server" ID="cmb_status" ForceSelection="false" TypeAhead="false" DisplayField="Name" ValueField="Code" TriggerAction="Query" QueryMode="Local" >
                                                       <Store>
                                                            <ext:Store runat="server" ID="store_status" AutoLoad="true" OnReadData="store_status_RefreshData" >
                                                                <Model>
                                                                    <ext:Model ID="Model11" runat="server"  >
                                                                        <Fields>
                                                                            <ext:ModelField Name="Code"   />
                                                                            <ext:ModelField Name="Name"  />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                        </ext:Store>
                                                        </Store>
                                                    </ext:ComboBox>                                             
                                              </Editor>
                                            </ext:Column>

								    	     <ext:DateColumn runat="server"   width="100" Text="Time Limit"  DataIndex="f_timelimit" Format="ddd, dd-MMM-yyyy" >
                                             <Editor>
                                                     <ext:DateField runat="server" ID="f_timelimit"  Name="dLimit" Format="dd-MMM-yyyy" />
                                              </Editor> 
                                             </ext:DateColumn>

								    	    <ext:Column ID="Column27" runat="server" width="80" Text="Cancel Code"  DataIndex="f_CancelCode" >
                                               <Editor>
                                                    <ext:TextField runat="server" ID="f_CancelCode" />
                                              </Editor>
                                            </ext:Column>
                                        
                                            <ext:Column ID="cmb_payment_1" runat="server" width="100" Text="Payment Status"  DataIndex="f_paymentStat" >
                                              <Editor>
                                                    <ext:ComboBox  runat="server" ID="cmb_payment" ForceSelection="false"  TypeAhead="false" DisplayField="Name" ValueField="Code" TriggerAction="Query" QueryMode="Local" >
                                                       <Store>
                                                            <ext:Store runat="server" ID="store_payment" AutoLoad="true" OnReadData="store_payment_RefreshData" >
                                                                <Model>
                                                                    <ext:Model ID="Model12" runat="server"  >
                                                                        <Fields>
                                                                            <ext:ModelField Name="Code"   />
                                                                            <ext:ModelField Name="Name"  />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                        </ext:Store>
                                                        </Store>
                                                    </ext:ComboBox>
                                              </Editor> 
                                            </ext:Column>

                                    	    <ext:Column ID="c_costCode" runat="server" width="100" Text="Cost Code" DataIndex="f_costcode">
                                                <Editor>
                                                    <ext:TriggerField runat="server" ID="f_costcode" DataIndex="f_costcode" >
                                                        <Triggers>
                                                            <ext:FieldTrigger Icon="Clear" Qtip="Click to clear field" HideTrigger="true" />
                                                            <ext:FieldTrigger Icon="Ellipsis" Qtip="Click to choose value" />
                                                        </Triggers>
                                                        <DirectEvents>
                                                            <TriggerClick OnEvent="btn_showUp_Click" />
                                                        </DirectEvents>
                                                    </ext:TriggerField>
                                                </Editor>
                                            </ext:Column>
                                            <ext:CommandColumn ID="cmd_save" runat="server" Width="40" Align="Center" Text="" TagHiddenName="Add Detail">
                                                <Commands>
                                                    <ext:GridCommand Icon="Accept" CommandName="Save"> 
                                                        <ToolTip Text="Save" />
                                                    </ext:GridCommand>
                                                </Commands>
                                                <DirectEvents>
                                                    <Command OnEvent="btn_save_detail_row_click">
                                                        <EventMask ShowMask="true" msg="Saving Data Please Wait..." />
                                                        <ExtraParams>
									                        <ext:Parameter Name="Values" Value="Ext.encode(#{grdDetail}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
									                    </ExtraParams>
                                                    </Command>
                                                </DirectEvents>
                                            </ext:CommandColumn>

                                            <ext:CommandColumn ID="cmd_edit" runat="server" Width="40" Align="Center" Text="" TagHiddenName="Edit Detail">
                                                <Commands>
                                                    <ext:GridCommand Icon="NoteEdit" CommandName="Edit" > 
                                                            <ToolTip Text="Save Edit" />
                                                    </ext:GridCommand>
                                                </Commands>
                                                <DirectEvents>
                                                <Command OnEvent="btn_edit_detail_click">
                                                    <EventMask ShowMask="true" Msg="Update data Please Wait..." />
                                                    <ExtraParams>
									                    <ext:Parameter Name="Values" Value="Ext.encode(#{grdDetail}.getRowsValues({selectedOnly : true}))"
											                    Mode="Raw">
									                    </ext:Parameter>
								                    </ExtraParams>
                                                </Command>                                
                                                </DirectEvents>
                                            </ext:CommandColumn>

                                            <ext:CommandColumn ID="cmd_delete" runat="server" Width="40"  Align="Center" Text="">
                                                  <Commands>
                                                            <ext:GridCommand Icon="Delete" CommandName="Delete"> 
                                                                   <ToolTip Text="Delete" />
                                                            </ext:GridCommand>
                                                  </Commands>                                              
                                                <DirectEvents>
                                                    <Command OnEvent="btn_delete_detail">
                                                        <Confirmation ConfirmRequest="true" Message="Are you sure delete records ?"  Title="Delete Records"  />
                                                        <EventMask ShowMask="true" Msg="Delete Data Please Wait..." />
                                                        <ExtraParams>
									                        <ext:Parameter Name="Value" Value="Ext.encode(#{grdDetail}.getRowsValues({selectedOnly : true}))"  Mode="Raw">
									                        </ext:Parameter>
                                                       
								                        </ExtraParams>
                                                    </Command>
                                
                                                 </DirectEvents>
                                            </ext:CommandColumn>
				                    </Columns>
							     </ColumnModel>
                                  <SelectionModel>
                                      <ext:CheckboxSelectionModel ID="chkGrid" runat="server" Mode="Single" >
                                            <DirectEvents>
                                                <Select OnEvent="btn_save_header_click">
                                                        <EventMask ShowMask="true" />
                                                    <ExtraParams>
                                                    <ext:Parameter 
                                                        Name="values" 
                                                        Value="Ext.encode(#{grdDetail}.getRowsValues({selectedOnly : true}))" Mode="Raw" Encode="true" />
                                                </ExtraParams>
                                                </Select>
                                            </DirectEvents>
                                      </ext:CheckboxSelectionModel>
                                  </SelectionModel>
                            
                        
                                <Plugins>
                                <ext:CellEditing runat="server" ClicksToEdit="1"    >
                                               
                                </ext:CellEditing>
                                    </Plugins>   
							      <BottomBar>
									     <ext:PagingToolbar ID="PagingToolbar4" runat="server"   StoreID="store_cari"    />
							     </BottomBar>
						      </ext:GridPanel>
            </Items>
            <Buttons>
                <ext:Button runat="server" ID="btn_New_Input" UI="Primary" Height="30" Text="NEW INPUT" ToolTipType="Qtip" ToolTip="<b>NEW INPUT" >
                    <DirectEvents>
                       <Click OnEvent="btn_New_Input_Click" />
                    </DirectEvents>
                </ext:Button>
                <ext:Button runat="server" ID="btn_exportDet" UI="Primary" Height="30" Text="EXPORT TO EXCEL" ToolTipType="Qtip" ToolTip="<b>EXPORT TO EXCEL" >
                    <DirectEvents>
                        <Click OnEvent="ToExcelDetail" IsUpload="true">
                        </Click>
                    </DirectEvents>
                </ext:Button>
			    <ext:Button runat="server" ID="btn_new"  UI="Success" Height="30"  Text="<b>ADD NEW DETAIL</b>"     >
                    <DirectEvents>
                        <Click OnEvent="btn_NewTag_Click" />
                    </DirectEvents>
                    <Listeners>
                        <Click Fn="addmaster" />
                    </Listeners>
                </ext:Button>
		        <ext:Button runat="server" ID="btn_close"   UI="Danger" Height="30" Text="<b>CLOSE</b>">
                    <DirectEvents>
                        <Click OnEvent="btn_close_click" >
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Buttons>   
        </ext:FormPanel>
                  <ext:Window ID="wndcostcode" runat="server" Hidden="true" Icon="ApplicationForm" Title="Cost Code Entry" Height="450" Width="1000" Modal="true" BodyPadding="2" Closable="false"   >
		      <Items>
                  <ext:Container ID="Container1" runat="server" Region="Center" Layout="VBoxLayout" Border="false">
					    <LayoutConfig>
						    <ext:VBoxLayoutConfig Align="Stretch" />
					    </LayoutConfig>
					    <Items>
						     <ext:Container ID="Container2" runat="server" Layout="HBoxLayout" Flex="1"  Frame="true"   Border="false" Padding="5" >
							    <LayoutConfig>
								    <ext:HBoxLayoutConfig Align="Stretch" />
							    </LayoutConfig>
							    <Items>
                                          <ext:Panel runat="server" Title="CATEGORY" ID="pnl1"  flex="1"  Padding="5" Height="380">
                                               <Items>
                                                    <ext:GridPanel id="grdCostCode_Cat" runat="server" Border="false"  >
                                                        <Store>
                                                            <ext:Store runat="server" ID="store_cat" AutoLoad="true" PageSize="15" OnReadData="store_cat_RefreshData" >
                                                                <Model>
                                                                    <ext:Model ID="Model14" runat="server"  >
                                                                        <Fields>
                                                                            <ext:ModelField Name="CATEGORY"   />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                        </ext:Store>
                                                        </Store>
                                                        <ColumnModel  ID="ColumnModel4"  runat="server">
				                                            <Columns>
					                                            <ext:Column ID="Column41" runat="server"  Text=""  DataIndex="CATEGORY"  flex="1" HideTitleEl="true"   />
					                                        </Columns>
			                                            </ColumnModel>
                                                            <BottomBar>
				                                                 <ext:PagingToolbar ID="PagingToolbar5" runat="server"  StoreID="store_cat"  >
                                                            </ext:PagingToolbar>
			                                            </BottomBar>
                                                       <SelectionModel>
                                                        <ext:CellSelectionModel runat="server">
                                                            <DirectEvents>
                                                                <Select OnEvent="Cell_Click" >
                                                                    <EventMask ShowMask="true" Msg="Loading..." />
                                                                </Select>                        
                                                            </DirectEvents>
                                                        </ext:CellSelectionModel>
                                                    </SelectionModel>          
                                                    </ext:GridPanel>                      
                                               </Items>
                                          </ext:Panel>
                                          <ext:Panel runat="server" ID="Panel2" Flex="1" Padding="5" Title="ACTIVITY">
                                               <Items>
                                                     <ext:GridPanel id="grdCatAct" runat="server" Border="false"    >
                                                        <Store>
                                                            <ext:Store runat="server" ID="store_act" AutoLoad="true" PageSize="15" OnReadData="store_act_RefreshData" >
                                                                <Model>
                                                                    <ext:Model ID="Model15" runat="server"  >
                                                                        <Fields>
                                                                            <ext:ModelField Name="ACTIVITY"   />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                        </ext:Store>
                                                        </Store>
                                                        <ColumnModel  ID="ColumnModel5"  runat="server">
				                                            <Columns>
					                                            <ext:Column ID="Column42" runat="server"  Text=""  DataIndex="ACTIVITY"  flex="1"  HideTitleEl="true"   />
					                                        </Columns>
			                                            </ColumnModel>
                                                            <BottomBar>
				                                                 <ext:PagingToolbar ID="PagingToolbar6" runat="server"  StoreID="store_act"  >
                                                            </ext:PagingToolbar>
			                                            </BottomBar>
                                                          <SelectionModel>
                                                                <ext:CellSelectionModel runat="server">
                                                                    <DirectEvents>
                                                                        <Select OnEvent="Cell_ACT_Click" >
                                                                            <EventMask ShowMask="true" Msg="Loading..." />
                                                                        </Select>                        
                                                                    </DirectEvents>
                                                                </ext:CellSelectionModel>
                                                          </SelectionModel>         
                                                    </ext:GridPanel>
                                               </Items>
                                          </ext:Panel>
                                          <ext:Panel runat="server" Title="CLIENT" ID="Panel4" Flex="1" Padding="5"  >
                                               <Items>
                                                     <ext:GridPanel id="grdClient" runat="server" Border="false"  Header="false" >
                                                        <Store>
                                                            <ext:Store runat="server" ID="store_client" AutoLoad="true" PageSize="14" OnReadData="store_cl_RefreshData" >
                                                                <Model>
                                                                    <ext:Model ID="Model17" runat="server"  >
                                                                        <Fields>
                                                                            <ext:ModelField Name="CLIENT"   />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                        </ext:Store>
                                                        </Store>
                                                        <ColumnModel  ID="ColumnModel6"  runat="server">
				                                            <Columns>
					                                            <ext:Column ID="Column43" runat="server"  Text=""  DataIndex="CLIENT"  flex="1" HideTitleEl="true"   />
					                                        </Columns>
			                                            </ColumnModel>
                                                            <SelectionModel>
                                                                <ext:CellSelectionModel runat="server">
                                                                    <DirectEvents>
                                                                        <Select OnEvent="Cell_Cli_Click" >
                                                                            <EventMask ShowMask="true" Msg="Loading..." />
                                                                        </Select>                        
                                                                    </DirectEvents>
                                                                </ext:CellSelectionModel>
                                                          </SelectionModel>         
                                                    </ext:GridPanel>
                                               </Items>
                                          </ext:Panel>
                                            <ext:Panel runat="server" Title="COSTCODE" ID="Panel3" Flex="1" Padding="5"  Height="380" >
                                               <Items>
                                                     <ext:GridPanel id="grdCostCode" runat="server" Border="false"  Header="false" >
                                                        <Store>
                                                            <ext:Store runat="server" ID="storecc" AutoLoad="true" PageSize="15" OnReadData="store_cc_RefreshData" >
                                                                <Model>
                                                                    <ext:Model ID="Model18" runat="server"  >
                                                                        <Fields>
                                                                            <ext:ModelField Name="COSTCODE"   />
                                                                        </Fields>
                                                                    </ext:Model>
                                                                </Model>
                                                        </ext:Store>
                                                        </Store>
                                                        <ColumnModel  ID="ColumnModel7"  runat="server">
				                                            <Columns>
					                                            <ext:Column ID="fl_costcode" runat="server"  Text=""  DataIndex="COSTCODE"  flex="1"  HideTitleEl="true"  />
					                                        </Columns>
			                                            </ColumnModel>
                                                            <BottomBar>
				                                                 <ext:PagingToolbar ID="PagingToolbar7" runat="server"  StoreID="storecc"  >
                                                            </ext:PagingToolbar>
			                                            </BottomBar>
                                                          <SelectionModel>
                                                                <ext:CellSelectionModel runat="server">
                                                                    <DirectEvents>
                                                                        <Select OnEvent="Cell_cost_Click" >
                                                                            <EventMask ShowMask="true" Msg="Loading..." />
                                                                            <ExtraParams>
									                                            <ext:Parameter Name="Values" Value="Ext.encode(#{grdDetail}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
									                                        </ExtraParams>
                                                                        </Select>                        
                                                                    </DirectEvents>
                                                                </ext:CellSelectionModel>
                                                          </SelectionModel>       
                                                    </ext:GridPanel>
                                               </Items>
                                          </ext:Panel>
                                          <ext:TextField runat="server"   ID="f_Category" Hidden="true" />
                                          <ext:TextField runat="server"   ID="f_Activity" Hidden="true" />
                                          <ext:TextField runat="server"   ID="f_cli" Hidden="true" /> 
                                          <ext:TextField runat="server"   ID="txt_costCode" Hidden="true" />     
                                </Items>
                             </ext:Container>
                       </Items>
                 </ext:Container>
              </Items>
                <Buttons>
			        <ext:Button runat="server" ID="btn_pick" UI="Primary"  Text="<b>OK</b>"    >
                        <DirectEvents>
                            <Click OnEvent="btn_pick_click">
                            <EventMask ShowMask="true" Msg="Loading..."/>
                            <ExtraParams>
						        <ext:Parameter Name="Values" Value="Ext.encode(#{grdDetail}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
						    </ExtraParams>
                            </Click>
                    </DirectEvents>  
                </ext:Button>
		        <ext:Button runat="server" ID="Button4" UI="Danger" Text="<b>CANCEL</b>" Handler="this.up('window').close()" />
	        </Buttons>
        </ext:Window>
        <ext:Window ID="WindowLogin" runat="server" Hidden="true" ToFrontOnShow="true" Modal="true"  FocusOnToFront="true" Header="false" Frame="false" FrameHeader="false" Border="false"  Icon="ApplicationFormMagnify" Title="ReLogin"
                 Height="152" Width="412"  BodyPadding="0" Closable="false" Resizable="false">
                <Items>
                    <ext:FormPanel ID="panelReLogin" runat="server" Title="Session Time Out Please Relogin" Width="400" Height="140" Frame="true" Border="true" BodyPadding="13" 
                                    DefaultAnchor="100%" UI="Info">
                        <Items>
                            <ext:TextField ID="txtUsername" runat="server" ReadOnly="false" FieldLabel="User ID" AllowBlank="false" BlankText="Your username is required." 
                                Text="" SelectOnFocus="true" />
                            <ext:TextField ID="txtPassword" runat="server" ReadOnly="false" InputType="Password" FieldLabel="Password" AllowBlank="false" 
                                BlankText="Your password is required." Text="" SelectOnFocus="true" />
                      </Items>
                    <Buttons>
                        <ext:Button ID="btn_login" runat="server" Text="<b>LOGIN</b>"  Cls="btn btn btn-danger" Disabled="true" >
                              <DirectEvents>
                                     <Click OnEvent="btn_login_Click">
                                            <EventMask ShowMask="true" /> 
                                     </Click>
                              </DirectEvents>
                        </ext:Button>
                    </Buttons>
                    <Listeners >
                        <ValidityChange Handler="#{btn_login}.setDisabled(!valid);" />
                    </Listeners>
                    </ext:FormPanel>
                </Items>
        </ext:Window>
       </div>
</div>
   
</asp:Content>