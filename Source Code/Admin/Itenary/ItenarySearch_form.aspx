﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ItenarySearch_form.aspx.cs" Inherits="NewERDM.Admin.Itenary.ItenarySearch_form" %>
<asp:Content ID="Header" ContentPlaceHolderID="head" Runat="Server">

<ext:XScript ID="XScript1" runat="server">
    <script>
        var addmaster = function () {
            var grid = #{grdDetail};
            grid.editingPlugin.cancelEdit();

            // Create a record instance through the ModelManager
            var r = Ext.ModelManager.create({
                f_dFligt       : '',
                f_routefrom    : '',
                f_routeTo      : '',
                f_flightdetail : '',
                f_airline      : '',
                f_timelimit    : '',
                f_CancelCode   : '',
                f_costcode     : '',
                f_paymentStat  : '',
                f_status       : ''
            }, 'data');
            grid.store.insert(0, r);
            grid.editingPlugin.startEdit(0, 0);
        };
       
        var saveData = function () {
            App.Hidden1.setValue(Ext.encode(App.grdTravel.getRowsValues({selectedOnly : false})));
        };  
    </script>
</ext:XScript>
<style>
	  .my-scale.x-btn-default-Small-icon-text-left .x-btn-icon {
			width: 16px;
		}
	   .add32 {
			background-image : url(img/Word-icon.png) !important;
		}   
         .kanan{text-align:right;}
        .Fkanan
         {
            text-align:right;
            border:none;
            background-image: none;
        }
        .my-panel .x-panel-header {
			background-color: #dfe8f6 !important;
			 
		}
 
		.my-panel .x-panel-body {
			border: none;
			background-color: #dfe8f6 !important;
		}

         
         .search-item {
            font          : normal 11px tahoma, arial, helvetica, sans-serif;
            padding       : 3px 10px 3px 10px;
            border        : 1px solid #fff;
            border-bottom : 1px solid #eeeeee;
            white-space   : normal;
            color         : #555;
        }
        
        .search-item h3 {
            display     : block;
            font        : inherit;
            font-weight : bold;
            color       : #222;
            margin      :0px;
        }

        .search-item h3 span {
            float       : right;
            font-weight : normal;
            margin      : 0 0 5px 5px;
            width       : 100px;
            display     : block;
            clear       : none;
        } 
        
        p { width: 650px; }
        
        .ext-ie .x-form-text { position : static !important; }
</style>

</asp:Content>

<asp:Content ID="Detail" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <ext:FormPanel runat="server" ID="frmverified" Title ="" BodyPadding="12" Header="false" Frame="false" Border="false" DisabledCls="true">
                <Items>
                <ext:GridPanel ID="grdTravel" runat="server" Border="false" Layout="FitLayout" Height="520" Region="Center" Margins="0 5 5 5" EnableColumnHide="false" >
                    <Store>
                        <ext:Store ID="store_travel" PageSize="21" runat="server" OnReadData="store_travel_RefreshData" GroupField="Nama" >
                            <Model>
                                <ext:Model ID="Model13" runat="server" IDProperty="BookingCode">
                                    <Fields>
                                        <ext:ModelField Name="BookingCode"  />
                                        <ext:ModelField Name="dateflight" />
                                        <ext:ModelField Name="Nama" />
                                        <ext:ModelField Name="justification" />
                                        <ext:ModelField Name="accomodation" />
                                        <ext:ModelField Name="transportation" />
                                        <ext:ModelField Name="RouteFrom" />
                                        <ext:ModelField Name="RouteTo" />
                                        <ext:ModelField Name="Airline" />
                                        <ext:ModelField Name="TimeLimit" />
                                        <ext:ModelField Name="FlightDetail" />
                                        <ext:ModelField Name="PaymentStatus" />
                                        <ext:ModelField Name ="Status" />
                                        <ext:ModelField Name ="UserModified" />
                                        <ext:ModelField Name="Remark" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>

                    <ColumnModel runat="server">
                        <Columns>
                            <ext:Column ID="Column15" runat="server" Width="80" Text="BookingCode"  Filterable="false" DataIndex="BookingCode" />
                            <ext:Column ID="Column28" runat="server" Width="80" Text="Date Flight"    Filterable="false" DataIndex="dateflight"/>
                            <ext:Column ID="Column29" runat="server" Width="100" Text="Employee Name"    Filterable="false"   DataIndex="Nama"  SummaryType="Count" Sortable="true" />
                            <ext:Column ID="Column30" runat="server" Width="80" Text="Justification"   Filterable="false" DataIndex="justification"  />
                            <ext:Column ID="Column31" runat="server" Width="80" Text="Accomodation"    Filterable="false" DataIndex="accomodation"/>
                            <ext:Column ID="Column32" runat="server" Width="80" Text="Transportation"    Filterable="false" DataIndex="transportation"/>
                            <ext:Column ID="Column33" runat="server" Width="80" Text="RouteFrom"    Filterable="false" DataIndex="RouteFrom"/>
                            <ext:Column ID="Column34" runat="server" Width="80" Text="RouteTo"    Filterable="false" DataIndex="RouteTo"/>
                            <ext:Column ID="Column35" runat="server" Width="80" Text="Air Line"    Filterable="false" DataIndex="Airline"/>
                            <ext:Column ID="Column36" runat="server" Width="80" Text="Time Limit"    Filterable="false" DataIndex="TimeLimit"/>
                            <ext:Column ID="Column37" runat="server" Width="100" Text="Payment Status"    Filterable="false" DataIndex="PaymentStatus"/>
                            <ext:Column ID="Column38" runat="server" Width="80" Text="Status"    Filterable="false" DataIndex="Status"/>
                            <ext:Column ID="Column39" runat="server" Width="80" Text="Modified By"    Filterable="false" DataIndex="UserModified"/>
                            <ext:Column ID="Column40" runat="server" Width="80" Text="Remark"  Hidden="true"   Filterable="false" DataIndex="Remark"/>
                        </Columns>
                    </ColumnModel>
                    <DirectEvents>
                        <CellDblClick OnEvent="btn_edit_doubleclick" >
                             <EventMask ShowMask="true" />
                                <ExtraParams>
	                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdTravel}.getRowsValues({selectedOnly : true}))"
			                                Mode="Raw">
	                                </ext:Parameter>
                                </ExtraParams>
                        </CellDblClick>
                    </DirectEvents>
                    <TopBar>
                        <ext:Toolbar ID="Toolbar2" runat="server" Layout="ColumnLayout">
                            <Items>
                                <ext:Label ID="Label6" Text="&nbsp;" Width="2" runat="Server" />
                                <ext:DateField ID="dtDateFrom" runat="server" FieldLabel="Date From "
                                    LabelWidth="70" Width="180" Format="dd/MM/yyyy" AllowBlank="true" MsgTarget="Side"
                                    IndicatorTip="Required Field" SelectOnFocus="true"> 
                                </ext:DateField>
                                <ext:DateField ID="dtDateTo" runat="server" FieldLabel="&nbsp;Date To "
                                    LabelWidth="70" Width="180" Format="dd/MM/yyyy" AllowBlank="true" MsgTarget="Side"
                                    IndicatorTip="Required Field" SelectOnFocus="true"
                                    EnableKeyEvents="true">
                                </ext:DateField>
                                <ext:TextField runat="server" FieldLabel="Filter" ID="f_search" EmptyText="Please type employee name or Booking Code "  Width="310" LabelWidth="50"     />
                                <ext:Label ID="Label7" Text="&nbsp;" Width="5" runat="Server" />

                                 <ext:Panel runat="server" Frame="false" Border="false" Cls="my-panel">
                                 <Items>
                                    <ext:Button ID="cmd_Search" runat="server" Text="SEARCH"  UI="Success" ToolTipType="Qtip" ToolTip="<b>Search</b><br>Search New Travel Itinerary Items">
                                    <DirectEvents>
                                            <Click OnEvent="btn_search_click" >
                                                <EventMask ShowMask="true" Msg="Searching.... Please wait.." />
                                            </Click>
                                    </DirectEvents>
                                    </ext:Button>
                                     <ext:Label ID="Label8" Text="&nbsp;&nbsp;" Width="2" runat="Server" />
                                     <ext:Button ID="Button1" runat ="server" Text="DELETE" UI="Warning" ToolTipType="Qtip" ToolTip="<b>DELETE</b><br>Delete Travel Itinerary Items</br>">
                                            <DirectEvents>
                                            <Click OnEvent="btn_delete_click">
                                                <Confirmation ConfirmRequest="true" Message="Are you sure to delete this record ?" Title="Delete Records" />
                                                <EventMask ShowMask="true" Msg="Delete Data Please wait..." />
                                                <ExtraParams>
									               <ext:Parameter Name="Values" Value="Ext.encode(#{grdTravel}.getRowsValues({selectedOnly : true}))"
											               Mode="Raw">
									               </ext:Parameter>
								               </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Label ID="Label9" Text="&nbsp;&nbsp;" Width="2" runat="Server" />
                                    <ext:Button ID="Button2" runat="server" Text="EXPORT TO EXCEL" UI="Primary" ToolTipType="Qtip" ToolTip="<b>EXPORT TO EXCEL</b>">
                                        <DirectEvents>
                                            <Click OnEvent="btn_export_click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Label ID="Label10" Text="&nbsp;&nbsp;" Width="2" runat="Server" />
                                    <ext:Button ID="btn_Close" runat="server" Text="CLOSE"  UI="Danger" ToolTipType="Qtip" ToolTip="<b>EXIT</b><br>Exit This Module" Handler="this.up('window').close()" >
                                        <DirectEvents>
                                            <Click OnEvent="btn_close_click" >
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click StopEvent="true"/>
                                        </Listeners>
                                    </ext:Button>
                                 </Items>                                     
                                 </ext:Panel>
                                <ext:TextField ID="BookingCode" runat="server" Hidden ="true" />
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                      <View>
                          <ext:GridView runat="server" StripeRows="true" MarkDirty="false" />
                      </View>
                      <Features>
                        <ext:GroupingSummary ID="GroupingSummary1" runat="server" GroupHeaderTplString="{name}" HideGroupedHeader="true" 
                            EnableGroupingMenu="false"  StartCollapsed="false" />
                     </Features>
                  <BottomBar>
				<ext:PagingToolbar ID="PagingToolbar3" runat="server" >
                   <Items>
                       <ext:Button runat="server" Text="<b>Clear Filters</b>" Icon="Erase" Handler="this.up('grid').filters.clearFilters();" />
                   </Items>
                </ext:PagingToolbar>
			</BottomBar>
                </ext:GridPanel>
                </Items>
            </ext:FormPanel>
                    <ext:Window ID="wndExport" runat="server" Hidden="true" Icon="PageExcel" Title="Export To Excel" Height="180" Width="350" Modal="true" BodyPadding="2" Closable="false"   >
           <Items>
                <ext:FormPanel runat="server" ID="FormPanel1" Title =""  BodyPadding="12"  Height="180" Header="false"  Frame="false"  Border="false"  >
                    <LayoutConfig >
                        <ext:VBoxLayoutConfig  />
                    </LayoutConfig>  
                     <Items>
                        <ext:TextField runat="server" ID="Booking_Code" FieldLabel="Booking Code" />
                        <ext:DateField runat="server" ID="dFrom" FieldLabel="From" Format="dd/MM/yyyy" />
                        <ext:DateField runat="server" ID="dTo" FieldLabel="To" Format="dd/MM/yyyy"  />
                       
                    </Items>
                </ext:FormPanel>
            </Items>
              <Buttons>
                <ext:Button runat="server" ID="btn_exportItenerary" Text="TO EXCEL" Click="ToExcel" UI="Primary">
                    <DirectEvents>
                        <Click OnEvent="ToExcel" IsUpload="true" >
                        </Click>
                    </DirectEvents>
                </ext:Button>
                        
				<ext:Button runat="server" ID="Button3" UI="Warning"  Text="<b>CLOSE</b>"  >
                    <DirectEvents>
                          <Click OnEvent="btn_view_close_click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
				</ext:Button>
			</Buttons>
        </ext:Window>
        </div>
    </div>
</asp:Content>