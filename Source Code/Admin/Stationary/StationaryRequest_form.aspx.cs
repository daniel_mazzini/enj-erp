﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using NewERDM.lib.BusinessClass;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using SpreadsheetLight;

namespace NewERDM.Admin.Stationary
{
    public partial class StationaryRequest_form : System.Web.UI.Page
    {
        #region Field
        
        UserApplication userApp;
        UserApplicationBL usrApplicationBL = new UserApplicationBL();
        
        #endregion Field

        #region Properties

        protected void store_Detail_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            
        }
        protected void Store_Items_RefreshData(object sender, StoreReadDataEventArgs e)
        {

        }

        #endregion Properties

        #region Method

        #endregion Method

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                userApp = (UserApplication)Session["ses_user"];
                
                if (userApp == null)
                {

                }
                else
                {
                    this.DateStationary.Value = DateTime.Now;
                    this.EmployeeID.Text = String.Format("{0}", userApp.EmployeeID);
                    this.EmployeeName.Text = String.Format("{0}", userApp.UserName);
                }
            }
        }
        protected void btn_close_click(object sender, DirectEventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];

            if (userApp.GroupID == "ADMIN")
            {
                X.Redirect(CommonUtility.ResolveUrl("Dashboard_itenary.aspx"), "Loading ... ");
            }
            else if (userApp.GroupID == "USER")
            {
                X.Redirect(CommonUtility.ResolveUrl("Dashboard_User.aspx"), "Loading ... ");
            }
            else if (userApp.GroupID == "SUPERADMIN" || userApp.GroupID == "DATABASE")
            {
                //Masih Contoh untuk yang groupid SUPERADMIN dan DATABASE
                X.Redirect(CommonUtility.ResolveUrl("Dashboard_itenary.aspx"), "Loading ... ");
            }
            //X.Redirect(CommonUtility.ResolveUrl("Dashboard_itenary.aspx"), "Loading ... ");
        }
        protected void btn_submit_click(object sender, DirectEventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];
            if (userApp == null)
            {

            }
            else
            {
                this.Userid.Text = String.Format("{0}", userApp.UserID); ;

                if (string.IsNullOrEmpty(this.stationaryID.Text))
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Warning",
                        Message = "Stationary ID can't be empty !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    return;
                }
                if(this.DateStationary.Text == "1/1/0001 12:00:00 AM")
                {
                    X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "Warning",
                            Message = "Stationary Date can't be empty !",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                    return;
                }
            }
        }
    }
}