﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StationaryRequest_form.aspx.cs" Inherits="NewERDM.Admin.Stationary.StationaryRequest_form" %>
<asp:Content ID="Header" runat="server" ContentPlaceHolderID="head">
     <ext:XScript ID="XScript1" runat="server">
	    <script>
	        var addmaster = function () {
	            var grid = #{grdDetail};
	            grid.editingPlugin.cancelEdit();

	            // Create a record instance through the ModelManager
	            var r = Ext.ModelManager.create({
                    pid             : '',
                    ItemCode        : '',
                    ItemName        : '',
                    QtyStockTemp    : '',
                    QtyStock        : '',
	                QtyReq          : '',
	                Remark          : ''
	            }, 'data');
	            grid.store.insert(0,r)
	            grd.editingPlugin.startEdit(0,0)
	            //grid.store.insert(0, r);
	            //grid.editingPlugin.startEdit(0, 0);
	        };
	    </script>
    </ext:XScript>
</asp:Content>
<asp:Content ID="Detail" runat="server" ContentPlaceHolderID="MainContent">
    <div class="row">
        <div class="col-md-12">
            <ext:FormPanel runat="server" ID="frmverified" Title =""  BodyPadding="12" Header="false" Frame="false" Border="false" DisabledCls="true" >
                <Items>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="stationaryID" FieldLabel="Stationary ID " Width="230" />
                            <ext:Label runat="server" Text="" Width="150" />
                            <ext:DateField runat="server" ID="DateStationary" FieldLabel="Date " LabelWidth="80" Format="ddd, dd-MMM-yyyy" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="EmployeeID" Hidden="true" />
                            <ext:TextField runat="server" ID="Userid" Hidden="true" />
                            <ext:TextField runat="server" ID="EmployeeName" FieldLabel="Employee Name " Disabled="true" Width="350" />
                            <ext:Label runat="server" Text="" Width="30" />
                            <ext:TextField runat="server" ID="Description" FieldLabel="Description " LabelWidth="80" Width="400" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:GridPanel id="grdDetail" runat="server" region="Center" Layout="FitLayout" Height="280" Width="800" Padding="8" EnableColumnHide="false" >
                        <Store>
                            <ext:Store runat="server" ID="store_Detail" PageSize="10"  OnReadData="store_Detail_RefreshData">
                                <Model>
                                    <ext:Model ID="Model4" runat="server" IDProperty="pid" Name="data">
                                        <Fields>
                                            <ext:ModelField Name="pid" />
                                            <ext:ModelField Name="ItemCode" />
                                            <ext:ModelField Name="ItemName" />
                                            <ext:ModelField Name="QtyTemp" />
                                            <ext:ModelField Name="QtyStock" />
                                            <ext:ModelField Name="QtyReq" />
                                            <ext:ModelField Name="Remark" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel ID="ColumnModel1" runat="server">
                            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="pid" Hidden="true" width="80" DataIndex="pid">
                                    <Editor>
                                        <ext:TextField runat="server" ID="pid" />
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column2" runat="server" Text="Item Name" Width="250" DataIndex="ItemCode">
                                    <Editor>
                                        <ext:ComboBox  runat="server" ID="cmb_status" ForceSelection="false" TypeAhead="false" DisplayField="Name" ValueField="Code" TriggerAction="Query" QueryMode="Local" >
                                            <Store>
                                                <ext:Store runat="server" ID="Store_Items" AutoLoad="true" OnReadData="Store_Items_RefreshData" >
                                                    <Model>
                                                        <ext:Model ID="Model11" runat="server"  >
                                                            <Fields>
                                                                <ext:ModelField Name="Code"   />
                                                                <ext:ModelField Name="Name"  />
                                                            </Fields>
                                                        </ext:Model>
                                                    </Model>
                                                </ext:Store>
                                            </Store>
                                        </ext:ComboBox>
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column3" runat="server" Text="Item Name" Width="350" DataIndex="ItemName" Hidden="true">
                                    <Editor>
                                        <ext:TextField runat="server" ID="ItemName" />
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column7" runat="server" Text="Qty Stock" Width="70" DataIndex="QtyTemp" Hidden="true">
                                    <Editor>
                                        <ext:TextField runat="server" ID="QtyStockTemp" Hidden="true" />
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column4" runat="server" Text="Qty Stock" Width="70" DataIndex="QtyStock" Disabled="true">
                                    <Editor>
                                        <ext:TextField runat="server" ID="QtyStock" Disabled="true" />
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column5" runat="server" Text="Qty Req." Width="70" DataIndex="QtyReq">
                                    <Editor>
                                        <ext:TextField runat="server" ID="QtyReq" />
                                    </Editor>
                                </ext:Column>
                                <ext:Column ID="Column6" runat="server" Text="Remark" Width="380" DataIndex="Remark">
                                    <Editor>
                                        <ext:TextField runat="server" ID="Remark" />
                                    </Editor>
                                </ext:Column>
                            </Columns>
                        </ColumnModel>
                        <Plugins>
                            <ext:CellEditing runat="server" ClicksToEdit="1" />
                        </Plugins>
                        <Buttons>
                            <ext:Button ID="btn_Detail" runat="server" UI="Success" Text="Add Detail" Height="30">
                                <Listeners>
                                    <Click Fn="addmaster" />
                                </Listeners>
                            </ext:Button>
                            <ext:Button runat="server" ID="btn_submit" UI="Primary" Height="30" Text="<b>Submit</b>">
                                <DirectEvents>
                                    <Click OnEvent="btn_submit_click" >
                                        <EventMask ShowMask="true" Msg="Save data please wait..." />
                                        <ExtraParams>
						                    <ext:Parameter Name="Values" Value="Ext.encode(#{grdDetail}.getRowsValues({selectedOnly : false}))" Mode="Raw" />
						                </ExtraParams>
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button runat="server" ID="btn_close"  UI="Danger" Height="30" Text="<b>Close</b>">
                                <DirectEvents>
                                    <Click OnEvent="btn_close_click" >
                                        <EventMask ShowMask="true" />
                                    </Click>
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:GridPanel>
                </Items>
            </ext:FormPanel>
        </div>
    </div>
</asp:Content>