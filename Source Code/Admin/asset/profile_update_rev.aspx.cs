﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using NewERDM.lib.BusinessClass;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using SpreadsheetLight;

namespace NewERDM.Admin.asset
{
    public partial class profile_update_rev : System.Web.UI.Page
    {
        #region Field
        
        UserApplication userApp;

        #endregion Field

        #region Properties
        protected void store_employee_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getemployee();
        }
        protected void store_ethnicGroup_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEthnicGroup();
        }
        protected void store_city_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCityLookup();
        }
        protected void store_state_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getStateLookup();
        }
        protected void store_country_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCountryLookup();
        }
        protected void store_employeetype_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeTypeLookup();
        }
        protected void store_employeeclass_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeClassLookup();
        }
        protected void store_individualgrade_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getIndividualGradeLookup();
        }
        protected void store_orggroup_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getOrgGroupLookup();
        }
        protected void store_section_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getSectionLookup();
        }
        protected void store_costcenter_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCostCenterLookup();
        }
        protected void store_paygroup_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getPayGroupLookup();
        }
        protected void Store_AssetName_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getAssetName();
        }

        protected void store_AssetsOther_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeAssetOther();
        }
        protected void store_AssetsSoftware_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeAssetSoftware();
        }
        protected void store_dependent_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeDependents();
        }
        protected void store_Training_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeTraining();
        }
        
        #endregion Properties

        #region Methods
        private static string pathWeb()
        {
            string strPathWeb = HttpContext.Current.Server.MapPath("~");
            return strPathWeb;
        }
        protected void getemployee()
        {
            DataTable dt = new DataTable();
            var objGet = new clsEmployee();
            dt = objGet.getEmployees();
            store_employee.DataSource = dt;
        }
        protected void viewdata(object sender, DirectEventArgs args)
        {
            String jsonSelectedRows = args.ExtraParams["ID"];
            StringBuilder sBuilderupdate;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            sBuilderupdate = new StringBuilder();
            foreach (Dictionary<string, string> row in selectedRowData)
            {
                try
                {
                    getemployeeprofile(row["employeeid"]);
                    getEmployeeAssetOther_Update(row["employeeid"]);
                    this.EmployeeIDAssetWindTemp.Text = row["employeeid"];
                    this.EmployeeIDAssetWind.Text = row["employeeid"];
                    getEmployeeAssetSoftware_Update(row["employeeid"]);
                    getEmployeeDepentdents_Update(row["employeeid"]);
                    getEmployeeTraining_Update(row["employeeid"]);
                    this.EmployeeIDTrainingWind.Text = row["employeeid"];
                    this.EmployeeIDDependentsWind.Text = row["employeeid"]; 
                    getEmployeeList(row["employeeid"]);

                    Store_Training.Reload();
                    store_depentdent.Reload();
                    Store_AssetsOther.Reload();
                    Store_AssetsSoftware.Reload();
                }
                catch
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "WARNING",
                        Message = "WARNING 1",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                }
            }

            //this.store_Detail.Reload();
            //this.store_depentdent.Reload();
        }
        protected void getemployeeprofile(string strID)
        {
            DataTable dt = new DataTable();
            var objGet = new clsEmployee();
            dt = objGet.getEmployeeID_Profile(strID);

            if (dt.Rows.Count > 0)
            {
                this.f_employeeid.Text = dt.Rows[0]["employeeid"].ToString();
                this.f_jabatan.Text = dt.Rows[0]["f_jabatan"].ToString();
                this.f_homephone.Text = dt.Rows[0]["f_homephone"].ToString();
                this.f_mobile.Text = dt.Rows[0]["f_mobile"].ToString();
                this.f_email.Text = dt.Rows[0]["f_email"].ToString();
                this.f_emailPass.Text = dt.Rows[0]["f_emailPass"].ToString();
                this.f_address.Text = dt.Rows[0]["f_address"].ToString();
            }
        }
        protected void getEmployeeList(string strEmployeeID)
        {
            DataTable dt = new DataTable();
            var objGet = new clsEmployee();
            dt = objGet.getEmployeesList(strEmployeeID.Trim());

            if (dt.Rows.Count > 0)
            {

                this.NetworkID.Text = dt.Rows[0]["NetworkID"].ToString();
                this.CompanyID.Text = dt.Rows[0]["CompanyID"].ToString();


                this.EmployeeID.Text = dt.Rows[0]["ID"].ToString();
                this.First_Name.Text = dt.Rows[0]["First_Name"].ToString();
                this.Middle_Name.Text = dt.Rows[0]["Middle_Name"].ToString();
                this.Last_Name.Text = dt.Rows[0]["Last_Name"].ToString();
                this.Gender.Text = dt.Rows[0]["Gender"].ToString();
                this.DateOfBirth.Text = dt.Rows[0]["DateOfBirth"].ToString();
                this.PlaceOfBirth.Text = dt.Rows[0]["PlaceOfBirth"].ToString();
                this.MaritalStatus.Text = dt.Rows[0]["MaritalStatus"].ToString();
                this.Religion.Text = dt.Rows[0]["Religion"].ToString();
                this.Nationality.Text = dt.Rows[0]["Nationality"].ToString();
                this.EthnicGroup.Text = dt.Rows[0]["EthnicGroup"].ToString();
                this.Address.Text = dt.Rows[0]["Address"].ToString();
                this.City.Text = dt.Rows[0]["City"].ToString();
                this.State.Text = dt.Rows[0]["State_Province"].ToString();
                this.Country.Text = dt.Rows[0]["Country_Region"].ToString();
                this.Zip.Text = dt.Rows[0]["ZIPPostal_Code"].ToString();
                this.HomePhone.Text = dt.Rows[0]["Home_Phone"].ToString();
                this.WorkPhone.Text = dt.Rows[0]["WorkPhone"].ToString();
                this.MobilePhone.Text = dt.Rows[0]["Mobile_Phone"].ToString();
                this.Fax.Text = dt.Rows[0]["Fax_Number"].ToString();
                this.Page.Text = dt.Rows[0]["Web_Page"].ToString();
                this.PhonetoRadion.Text = dt.Rows[0]["PhonetoRadion"].ToString();
                this.Email1.Text = dt.Rows[0]["Email1"].ToString();
                this.Email2.Text = dt.Rows[0]["Email2"].ToString();
                this.KTP.Text = dt.Rows[0]["KTP"].ToString();
                this.BPJS.Text = dt.Rows[0]["BPJS"].ToString();
                this.NPWP.Text = dt.Rows[0]["NPWP"].ToString();

                this.EmployeeType.Text = dt.Rows[0]["Type"].ToString();
                this.EmployeeClass.Text = dt.Rows[0]["Class"].ToString();
                this.IndividualGrade.Text = dt.Rows[0]["Grade"].ToString();
                this.OriginalHireDate.Text = dt.Rows[0]["OrginalHireDate"].ToString();
                this.RehireDate.Text = dt.Rows[0]["RehireDate"].ToString();
                this.ServiceDate.Text = dt.Rows[0]["ServiceDate"].ToString();
                this.LastPromotion.Text = dt.Rows[0]["LastPromotion"].ToString();

                this.Vendor.Text = dt.Rows[0]["Vendor"].ToString();
                this.WorkAt.Text = dt.Rows[0]["WorkAt"].ToString();
                this.FunctionalDepartment.Text = dt.Rows[0]["FunctionalDepartment"].ToString();
                this.OrgGroup.Text = dt.Rows[0]["OrgGroup"].ToString();
                this.Section.Text = dt.Rows[0]["Section"].ToString();
                this.Location.Text = dt.Rows[0]["Location"].ToString();
                this.Office.Text = dt.Rows[0]["Office"].ToString();

                this.JobTittle.Text = dt.Rows[0]["Job_Title"].ToString();
                this.PositionTitle.Text = dt.Rows[0]["PositionTitle"].ToString();
                this.Supervisor.Text = dt.Rows[0]["Supervisior"].ToString();
                this.CostCenter.Text = dt.Rows[0]["CostCenter"].ToString();

                this.PayGroup.Text = dt.Rows[0]["PayGroup"].ToString();
                this.BenefitCode.Text = dt.Rows[0]["BenefitCode"].ToString();
                this.UnionCode.Text = dt.Rows[0]["UnionCode"].ToString();
                this.TaxCode.Text = dt.Rows[0]["TaxCode"].ToString();

                this.NameOfContact.Text = dt.Rows[0]["NameOfContact"].ToString();
                this.Relationship.Text = dt.Rows[0]["Relationship"].ToString();
                this.PhoneNumber.Text = dt.Rows[0]["PhoneNumber"].ToString();
                this.MobileNumber.Text = dt.Rows[0]["MobileNumber"].ToString();
                this.Email.Text = dt.Rows[0]["Email"].ToString();

                this.PassportNo.Text = dt.Rows[0]["PassportNo"].ToString();
                this.PassportDateofIssue.Text = dt.Rows[0]["PassportDateofIssue"].ToString();
                this.PassportExpirationDate.Text = dt.Rows[0]["PassportExpirationIssue"].ToString();
                this.PassportIssuingOffice.Text = dt.Rows[0]["PassportIssuingOffice"].ToString();
                this.PassportRegNo.Text = dt.Rows[0]["PassportRegNo"].ToString();
                this.VisaNo.Text = dt.Rows[0]["VisaNo"].ToString();
                this.VisaType.Text = dt.Rows[0]["VisaType"].ToString();
                this.VisaIssueDate.Text = dt.Rows[0]["VisaIssueDate"].ToString();
                this.VisaExpirationDate.Text = dt.Rows[0]["VisaExpirationDate"].ToString();
                this.VisaIssuingOffice.Text = dt.Rows[0]["VisaIssuingOffice"].ToString();

            }
        }
        protected void getEthnicGroup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupEthnicGroup();
            store_EthnicGroup.DataSource = dt;
            store_EthnicGroup.DataBind();
        }
        protected void getCityLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupCity();
            store_City.DataSource = dt;
            store_City.DataBind();
        }
        protected void getStateLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupState();
            store_State.DataSource = dt;
            store_State.DataBind();
        }
        protected void getCountryLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupCountry();
            store_Country.DataSource = dt;
            store_Country.DataBind();
        }
        protected void getEmployeeTypeLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupEmployeeType();
            store_EmployeeType.DataSource = dt;
            store_EmployeeType.DataBind();
        }
        protected void getEmployeeClassLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupEmployeeClass();
            store_EmployeeClass.DataSource = dt;
            store_EmployeeClass.DataBind();
        }
        protected void getIndividualGradeLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupIndividualGrade();
            store_IndividualGrade.DataSource = dt;
            store_IndividualGrade.DataBind();
        }
        protected void getOrgGroupLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupOrgGroup();
            store_OrgGroup.DataSource = dt;
            store_OrgGroup.DataBind();
        }
        protected void getSectionLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupSection();
            store_Section.DataSource = dt;
            store_Section.DataBind();
        }
        protected void getCostCenterLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupCostCenter();
            store_CostCenter.DataSource = dt;
            store_CostCenter.DataBind();
        }
        protected void getPayGroupLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupPayGroup();
            store_PayGroup.DataSource = dt;
            store_PayGroup.DataBind();
        }
        protected void getAssetName()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupAssetName();
            Store_AssetName.DataSource = dt;
            Store_AssetName.DataBind();
        }

        protected void getEmployeeAssetOther()
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeAssetOther_Detail(this.f_employeeid.Text);

            Store_AssetsOther.DataSource = dt;
            Store_AssetsOther.DataBind();

        }
        protected void getEmployeeAssetOther_Update(string strEmployeeID)
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeAssetOther_Update(strEmployeeID);

            if (dt.Rows.Count > 0)
            {
                this.f_pid.Text = dt.Rows[0]["f_pid"].ToString();
                this.f_employeid.Text = dt.Rows[0]["employeeid"].ToString();
                this.f_assetid.Text = dt.Rows[0]["f_assetid"].ToString();
                this.f_assetname.Text = dt.Rows[0]["f_assetname"].ToString();
                this.f_serialnumber.Text = dt.Rows[0]["f_serialnumber"].ToString();
                //this.f_areacode.Text = dt.Rows[0]["f_areacode"].ToString();
                this.f_site.Text = dt.Rows[0]["f_site"].ToString();
                this.f_location.Text = dt.Rows[0]["f_location"].ToString();
                this.f_ponumber.Text = dt.Rows[0]["f_ponumber"].ToString();
                this.f_Responsibility.Text = dt.Rows[0]["f_Responsibility"].ToString();
                this.f_description.Text = dt.Rows[0]["f_description"].ToString();

                getEmployeeAssetOther();
            }

        }
        protected void getEmployeeAssetSoftware()
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeAssetSoftware_Detail(this.f_employeeid.Text);

            Store_AssetsSoftware.DataSource = dt;
            Store_AssetsSoftware.DataBind();

        }
        protected void getEmployeeAssetSoftware_Update(string strEmployeeID)
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeAssetSoftware_Update(strEmployeeID);

            if (dt.Rows.Count > 0)
            {
                this.f_pidSoftware.Text = dt.Rows[0]["f_pidSoftware"].ToString();
                this.employeeidSoftware.Text = dt.Rows[0]["employeeidSoftware"].ToString();
                this.f_assetidSoftware.Text = dt.Rows[0]["f_assetidSoftware"].ToString();
                this.f_assetnameSoftware.Text = dt.Rows[0]["f_assetnameSoftware"].ToString();
                this.f_serialnumberSoftware.Text = dt.Rows[0]["f_serialnumberSoftware"].ToString();
                //this.f_areacodeSoftware.Text = dt.Rows[0]["f_areacodeSoftware"].ToString();
                this.f_siteSoftware.Text = dt.Rows[0]["f_siteSoftware"].ToString();
                this.f_locationSoftware.Text = dt.Rows[0]["f_locationSoftware"].ToString();
                this.f_ponumberSoftware.Text = dt.Rows[0]["f_ponumberSoftware"].ToString();
                this.f_ResponsibilitySoftware.Text = dt.Rows[0]["f_ResponsibilitySoftware"].ToString();
                this.f_descriptionSoftware.Text = dt.Rows[0]["f_descriptionSoftware"].ToString();

                getEmployeeAssetSoftware();
            }

        }
        protected void getEmployeeDependents()
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeDependents_Detail(this.f_employeeid.Text);

            store_depentdent.DataSource = dt;
            store_depentdent.DataBind();

        }
        protected void getEmployeeDepentdents_Update(string strEmployeeID)
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeDependents_Update(strEmployeeID);

            if (dt.Rows.Count > 0)
            {
                this.f_pid_dep.Text = dt.Rows[0]["f_pid_dep"].ToString();
                this.f_employeeid_dep.Text = dt.Rows[0]["employeeid"].ToString();
                this.f_dependentid_dep.Text = dt.Rows[0]["f_dependentid"].ToString();
                this.f_dependentname_dep.Text = dt.Rows[0]["f_dependentname"].ToString();
                this.f_relationship_dep.Text = dt.Rows[0]["f_relationship"].ToString();
                this.f_birthdate_dep.Text = dt.Rows[0]["f_birthdate"].ToString();
                this.f_education_dep.Text = dt.Rows[0]["f_education"].ToString();
                this.f_sex_dep.Text = dt.Rows[0]["f_sex"].ToString();
                this.f_eligibleclass_dep.Text = dt.Rows[0]["f_eligibleclass"].ToString();
                this.f_remark_dep.Text = dt.Rows[0]["f_remark"].ToString();

                getEmployeeDependents();
            }

        }
        protected void getEmployeeTraining()
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeTraining_Detail(this.f_employeeid.Text);

            Store_Training.DataSource = dt;
            Store_Training.DataBind();

        }
        protected void getEmployeeTraining_Update(string strEmployeeID)
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeTraining_Update(strEmployeeID);

            if (dt.Rows.Count > 0)
            {
                this.pidTraining.Text = dt.Rows[0]["pidTraining"].ToString();
                this.EmployeeIDTraining.Text = dt.Rows[0]["EmployeeIDTraining"].ToString();
                this.TrainingType.Text = dt.Rows[0]["TrainingType"].ToString();
                this.TrainingName.Text = dt.Rows[0]["TrainingName"].ToString();
                this.TrainingMandatory.Text = dt.Rows[0]["TrainingMandatory"].ToString();
                this.TrainingLocation.Text = dt.Rows[0]["TrainingLocation"].ToString();
                this.TrainingStartDate.Text = dt.Rows[0]["TrainingStartDate"].ToString();
                this.TrainingEndDate.Text = dt.Rows[0]["TrainingEndDate"].ToString();
                this.TrainingCertificateNo.Text = dt.Rows[0]["TrainingCertificateNo"].ToString();
                this.TrainingProvider.Text = dt.Rows[0]["TrainingProvider"].ToString();

                getEmployeeTraining();
            }

        }

        #endregion Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];
            if (userApp == null)
            {

            }
            else
            {
                getemployee();

                getEthnicGroup();
                getCityLookup();
                getStateLookup();
                getCountryLookup();
                getEmployeeTypeLookup();
                getEmployeeClassLookup();
                getIndividualGradeLookup();
                getOrgGroupLookup();
                getSectionLookup();
                getCostCenterLookup();
                getPayGroupLookup();
            }
        }
        protected void btnNew_click(object sender, DirectEventArgs e)
        {

            //save as photo
            string sPathWeb = pathWeb();
            string sID = userApp.EmployeeID + ".jpg";

            if (this.f_SmallPhoto.HasFile)
            {
                this.f_SmallPhoto.PostedFile.SaveAs(String.Format("{0}\\photo\\{1}", sPathWeb, sID));

            }
            else
            {
                this.f_SmallPhoto.Text = "";
            }

            if (this.f_ProfilePhoto.HasFile)
            {
                this.f_ProfilePhoto.PostedFile.SaveAs(String.Format("{0}\\photo\\profile\\{1}", sPathWeb, sID));
            }
            else
            {

                this.f_ProfilePhoto.Text = "";
            }

            //Update Employee
            var objUpd = new clsEmployee();
            objUpd.UpdEmployeeProfile(this.f_jabatan.Text, this.f_homephone.Text, this.f_mobile.Text, this.f_email.Text, this.f_emailPass.Text, this.f_address.Text, this.f_employeeid.Text);

            X.MessageBox.Show(new MessageBoxConfig
            {
                Title = "INFORMATION",
                Message = "DATA SAVED",
                Buttons = MessageBox.Button.OK,
                Icon = MessageBox.Icon.INFO
            });

        }
        protected void btn_save_personal_click(object sender, DirectEventArgs e)
        {
            if(string.IsNullOrEmpty(this.EmployeeID.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Employee ID can't be empty !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                //Check double ID
                DataTable dt = new DataTable();
                var objGet = new clsEmployee();
                dt = objGet.getCheckEmployeeID(this.EmployeeID.Text.Trim());

                if (dt.Rows.Count == 0)
                {
                    var objInsPersonal = new clsEmployee();
                    objInsPersonal.InsPersonalEmployee(this.EmployeeID.Text, this.CompanyID.Text, this.NetworkID.Text, this.First_Name.Text, this.Middle_Name.Text,
                                                       this.Last_Name.Text, this.Gender.Text, Convert.ToDateTime(this.DateOfBirth.Text), this.PlaceOfBirth.Text,
                                                       this.MaritalStatus.Text, this.Religion.Text, this.Nationality.Text, this.EthnicGroup.Text, this.Address.Text,
                                                       this.City.Text, this.State.Text, this.Country.Text, this.Zip.Text, this.HomePhone.Text, this.WorkPhone.Text,
                                                       this.MobilePhone.Text, this.Fax.Text, this.Page.Text, this.PhonetoRadion.Text, this.Email1.Text, this.Email2.Text,
                                                       this.KTP.Text, this.BPJS.Text, this.NPWP.Text);

                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Information",
                        Message = "Save Data Success",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO
                    });


                }
                else
                {
                    var objUpdPersonal = new clsEmployee();
                    objUpdPersonal.UpdPersonalEmployee(this.EmployeeID.Text, this.CompanyID.Text, this.NetworkID.Text, this.First_Name.Text, this.Middle_Name.Text,
                                                       this.Last_Name.Text, this.Gender.Text, Convert.ToDateTime(this.DateOfBirth.Text), this.PlaceOfBirth.Text,
                                                       this.MaritalStatus.Text, this.Religion.Text, this.Nationality.Text, this.EthnicGroup.Text, this.Address.Text,
                                                       this.City.Text, this.State.Text, this.Country.Text, this.Zip.Text, this.HomePhone.Text, this.WorkPhone.Text,
                                                       this.MobilePhone.Text, this.Fax.Text, this.Page.Text, this.PhonetoRadion.Text, this.Email1.Text, this.Email2.Text,
                                                       this.KTP.Text, this.BPJS.Text, this.NPWP.Text);

                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Information",
                        Message = "Update Data Success",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO
                    });

                }
            }
        }
        protected void btn_save_job_click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(this.EmployeeID.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Please Choose Employee ID !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                if (this.OriginalHireDate.Text == "1/1/0001 12:00:00 AM")
                {
                    X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Orginal Hire Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                    return;
                }
                if (this.RehireDate.Text == "1/1/0001 12:00:00 AM")
                {
                    X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "ReHire Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                    return;
                }
                if (this.ServiceDate.Text == "1/1/0001 12:00:00 AM")
                {
                    X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Service Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                    return;
                }
                if (this.LastPromotion.Text == "1/1/0001 12:00:00 AM")
                {
                    X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Last Promotion can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                    return;
                }

                var objUpdJobInformation = new clsEmployee();
                objUpdJobInformation.UpdJobInformationEmployee(this.EmployeeID.Text, this.EmployeeType.Text, Convert.ToDateTime(this.OriginalHireDate.Text), Convert.ToDateTime(this.RehireDate.Text), Convert.ToDateTime(this.ServiceDate.Text),
                                Convert.ToDateTime(this.LastPromotion.Text), this.Vendor.Text, this.WorkAt.Text, this.FunctionalDepartment.Text, this.OrgGroup.Text, this.Section.Text, this.Location.Text,
                                this.Office.Text, this.JobTittle.Text, this.PositionTitle.Text, this.Supervisor.Text, this.CostCenter.Text, this.PayGroup.Text, this.BenefitCode.Text, this.UnionCode.Text, this.TaxCode.Text);


                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Information",
                    Message = "Save Data Success",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO
                });
            }
        }
        protected void btn_save_emergency_click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(this.EmployeeID.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Please Choose Employee ID !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                var objUpdEmergency = new clsEmployee();
                objUpdEmergency.UpdEmergencyEmployee(this.EmployeeID.Text, this.NameOfContact.Text, this.Relationship.Text, this.PhoneNumber.Text, this.MobileNumber.Text, this.Email.Text);

                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Information",
                    Message = "Save Data Success",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO
                });
            }
        }
        protected void btn_save_passport_click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(this.EmployeeIDAssetWindTemp.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Please Choose Employee ID !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                if (string.IsNullOrEmpty(this.PassportNo.Text))
                {
                    X.MessageBox.Show(new MessageBoxConfig { Title = "Warning", Message = "Passport Number can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                    return;
                }
                if (this.PassportDateofIssue.Text == "1/1/0001 12:00:00 AM")
                {
                    X.MessageBox.Show(new MessageBoxConfig { Title = "Warning", Message = "Passport Date Issue can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                    return;
                }
                if (this.PassportExpirationDate.Text == "1/1/0001 12:00:00 AM")
                {
                    X.MessageBox.Show(new MessageBoxConfig { Title = "Warning", Message = "Passport Expiration Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                    return;
                }
                if (this.PassportIssuingOffice.Text == "1/1/0001 12:00:00 AM")
                {
                    X.MessageBox.Show(new MessageBoxConfig { Title = "Warning", Message = "Passport Issuing Office can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                    return;
                }
                if (this.VisaIssueDate.Text == "1/1/0001 12:00:00 AM")
                {
                    X.MessageBox.Show(new MessageBoxConfig { Title = "Warning", Message = "Vissa Issue Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                    return;
                }
                if (this.VisaExpirationDate.Text == "1/1/0001 12:00:00 AM")
                {
                    X.MessageBox.Show(new MessageBoxConfig { Title = "Warning", Message = "VIssa Expiration Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                    return;
                }
                if (this.VisaIssuingOffice.Text == "1/1/0001 12:00:00 AM")
                {
                    X.MessageBox.Show(new MessageBoxConfig { Title = "Warning", Message = "VIssa Issuing Office can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                    return;
                }

                var objUpdPassportVisa = new clsEmployee();
                objUpdPassportVisa.UpdPassportVisaEmployee(this.EmployeeIDAssetWindTemp.Text, this.PassportNo.Text, Convert.ToDateTime(this.PassportDateofIssue.Text),
                                    Convert.ToDateTime(this.PassportExpirationDate.Text), Convert.ToDateTime(this.PassportIssuingOffice.Text), this.PassportRegNo.Text,
                                    this.VisaNo.Text, this.VisaType.Text, Convert.ToDateTime(this.VisaIssueDate.Text),Convert.ToDateTime(this.VisaExpirationDate.Text),
                                    Convert.ToDateTime(this.VisaIssuingOffice.Text));

                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Information",
                    Message = "Save Data Success",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO
                });
            }
        }
        protected void btn_save_click(object sender, DirectEventArgs e)
        {
            //Mandatory
            if (this.DateOfBirth.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Date Of Birth can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.OriginalHireDate.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Orginal Hire Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.RehireDate.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "ReHire Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.ServiceDate.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Service Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.LastPromotion.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Last Promotion can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.PassportDateofIssue.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Passport Date Issue can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.PassportExpirationDate.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Passport Expiration Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.PassportIssuingOffice.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Passport Issuing Office can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.VisaIssueDate.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Vissa Issue Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.VisaExpirationDate.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "VIssa Expiration Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.VisaIssuingOffice.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "VIssa Issuing Office can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }

            //Check double ID
            DataTable dt = new DataTable();
            var objGet = new clsEmployee();
            dt = objGet.getCheckEmployeeID(this.EmployeeID.Text.Trim());

            if (dt.Rows.Count == 0)
            {

                //Insert Data
                var objIns = new clsEmployee();
                objIns.InsEmployee(this.EmployeeID.Text, this.CompanyID.Text, this.NetworkID.Text, this.Last_Name.Text, this.First_Name.Text, this.Middle_Name.Text, this.Gender.Text,
                                this.f_email.Text, this.JobTittle.Text, "BussinesPhone", this.HomePhone.Text, this.MobilePhone.Text, this.Fax.Text, this.Address.Text,
                                this.City.Text, this.State.Text, this.Zip.Text, this.Country.Text, this.Page.Text, "POH", "Status", this.EmployeeType.Text, "Notes",
                                this.IndividualGrade.Text, Convert.ToDateTime(this.DateOfBirth.Text), this.PlaceOfBirth.Text, this.MaritalStatus.Text, this.Religion.Text, this.Nationality.Text,
                                this.EthnicGroup.Text, this.WorkPhone.Text, PhonetoRadion.Text, this.Email1.Text, this.Email2.Text, this.KTP.Text, this.BPJS.Text, this.NPWP.Text,
                                this.EmployeeClass.Text, Convert.ToDateTime(this.OriginalHireDate.Text), Convert.ToDateTime(this.RehireDate.Text), Convert.ToDateTime(this.ServiceDate.Text),
                                Convert.ToDateTime(this.LastPromotion.Text), this.Vendor.Text, this.WorkAt.Text, this.FunctionalDepartment.Text, this.OrgGroup.Text, this.Section.Text, this.Location.Text,
                                this.Office.Text, this.PositionTitle.Text, this.Supervisor.Text, this.CostCenter.Text, this.PayGroup.Text, this.BenefitCode.Text, this.UnionCode.Text, this.TaxCode.Text,
                                this.NameOfContact.Text, this.Relationship.Text, this.PhoneNumber.Text, this.MobileNumber.Text, this.Email.Text, this.PassportNo.Text, Convert.ToDateTime(this.PassportDateofIssue.Text),
                                Convert.ToDateTime(this.PassportExpirationDate.Text), Convert.ToDateTime(this.PassportIssuingOffice.Text), this.PassportRegNo.Text, this.VisaNo.Text, this.VisaType.Text,
                                Convert.ToDateTime(this.VisaIssueDate.Text), Convert.ToDateTime(this.VisaExpirationDate.Text), Convert.ToDateTime(this.VisaIssuingOffice.Text));

                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "INFORMATION",
                    Message = "DATA SAVED",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO
                });
            }
            else
            {
                //Update Data

                var objUpd = new clsEmployee();
                objUpd.UpdEmployee(this.EmployeeID.Text, this.CompanyID.Text, this.NetworkID.Text, this.Last_Name.Text, this.First_Name.Text, this.Middle_Name.Text, this.Gender.Text,
                                this.f_email.Text, this.JobTittle.Text, this.WorkPhone.Text, this.HomePhone.Text, this.MobilePhone.Text, this.Fax.Text, this.Address.Text,
                                this.City.Text, this.State.Text, this.Zip.Text, this.Country.Text, this.Page.Text, this.City.Text, "A", this.EmployeeType.Text, "Notes",
                                this.IndividualGrade.Text, Convert.ToDateTime(this.DateOfBirth.Text), this.PlaceOfBirth.Text, this.MaritalStatus.Text, this.Religion.Text, this.Nationality.Text,
                                this.EthnicGroup.Text, this.WorkPhone.Text, PhonetoRadion.Text, this.Email1.Text, this.Email2.Text, this.KTP.Text, this.BPJS.Text, this.NPWP.Text,
                                this.EmployeeClass.Text, Convert.ToDateTime(this.OriginalHireDate.Text), Convert.ToDateTime(this.RehireDate.Text), Convert.ToDateTime(this.ServiceDate.Text),
                                Convert.ToDateTime(this.LastPromotion.Text), this.Vendor.Text, this.WorkAt.Text, this.FunctionalDepartment.Text, this.OrgGroup.Text, this.Section.Text, this.Location.Text,
                                this.Office.Text, this.PositionTitle.Text, this.Supervisor.Text, this.CostCenter.Text, this.PayGroup.Text, this.BenefitCode.Text, this.UnionCode.Text, this.TaxCode.Text,
                                this.NameOfContact.Text, this.Relationship.Text, this.PhoneNumber.Text, this.MobileNumber.Text, this.Email.Text, this.PassportNo.Text, Convert.ToDateTime(this.PassportDateofIssue.Text),
                                Convert.ToDateTime(this.PassportExpirationDate.Text), Convert.ToDateTime(this.PassportIssuingOffice.Text), this.PassportRegNo.Text, this.VisaNo.Text, this.VisaType.Text,
                                Convert.ToDateTime(this.VisaIssueDate.Text), Convert.ToDateTime(this.VisaExpirationDate.Text), Convert.ToDateTime(this.VisaIssuingOffice.Text));

                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "INFORMATION",
                    Message = "DATA UPDATED",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO
                });
            }
        }
        protected void btn_AddAssets_click(object sender, DirectEventArgs e)
        {
            this.getAssetName();
            this.WindowAssets.Show();
        }
        protected void getDataAssetType_click(object sender, DirectEventArgs e)
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getAssetType(fFicNumberWind.Text);

            if (dt.Rows.Count > 0)
            {
                AssetTypeWindTemp.Text = dt.Rows[0]["AssetType"].ToString(); ;
                AssetTypeWind.Text = dt.Rows[0]["AssetType"].ToString();
            }
        }
        protected void btn_addDependents_click(object sender, DirectEventArgs e)
        {
            this.DependentsIDWind.Text = this.EmployeeIDDependentsWind.Text;
            this.WindowDependents.Show();
        }
        protected void btn_addTraining_click(object sender, DirectEventArgs e)
        {
            this.WindowTraining.Show();
        }
        protected void btn_Save_Asset_click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(this.EmployeeIDAssetWindTemp.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Please choose employee !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            if (string.IsNullOrEmpty(this.pidAssetWind.Text))
            {
                var objUpdateAssetItem = new clsEmployee();
                objUpdateAssetItem.UpdAssetItem(this.fFicNumberWind.Text, this.AssetTypeWindTemp.Text, Convert.ToInt16("1"));

                var objInsertAsset = new clsEmployee();
                objInsertAsset.InsAsset(this.EmployeeIDAssetWindTemp.Text, this.fFicNumberWind.Text, this.AssetSiteWind.Text, this.AssetLocationWind.Text,
                                        this.AssetResponsibilityWind.Text, this.DescriptionWind.Text);
            }
            else
            {
                var objUpdateAsset = new clsEmployee();
                //objUpdateAsset.UpdAsset();
            }

            X.MessageBox.Show(new MessageBoxConfig
            {
                Title = "Information",
                Message = "Save Data Success",
                Buttons = MessageBox.Button.OK,
                Icon = MessageBox.Icon.INFO
            });
            this.WindowAssets.Hide();
            Store_AssetsOther.Reload();
            Store_AssetsSoftware.Reload();

        }
        protected void btn_Save_Dependents_click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(this.EmployeeIDDependentsWind.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Please choose employee !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }

            if (string.IsNullOrEmpty(this.pidDependentsWind.Text))
            {
                var objInsDependents = new clsEmployee();
                objInsDependents.InsEmployeeDependents(this.EmployeeIDDependentsWind.Text, this.DependentsIDWind.Text, this.DependentsNameWind.Text,
                                                        this.DependentsRelationshipWind.Text, Convert.ToDateTime(this.DependentsBirthdayWind.Text),
                                                        this.DependentsEducationWind.Text, this.DependentsGenderWind.Text, this.DependentsEligibleClassWind.Text,
                                                        this.DependentsRemarksWind.Text);
            }
            else
            {
                var objUpdateDependents = new clsEmployee();
                objUpdateDependents.UpdEmployeeDependents(Convert.ToInt16(this.pidDependentsWind.Text), this.EmployeeIDDependentsWind.Text, this.DependentsIDWind.Text,
                                                        this.DependentsNameWind.Text, this.DependentsRelationshipWind.Text, Convert.ToDateTime(this.DependentsBirthdayWind.Text),
                                                        this.DependentsEducationWind.Text, this.DependentsGenderWind.Text, this.DependentsEligibleClassWind.Text,
                                                        this.DependentsRemarksWind.Text);
            }

            X.MessageBox.Show(new MessageBoxConfig
            {
                Title = "Information",
                Message = "Save Data Success",
                Buttons = MessageBox.Button.OK,
                Icon = MessageBox.Icon.INFO
            });
            this.EmployeeIDDependentsWind.Text = string.Empty;
            this.DependentsIDWind.Text = string.Empty;
            this.DependentsNameWind.Text = string.Empty;
            this.DependentsRelationshipWind.Text = string.Empty;
            this.DependentsBirthdayWind.Text = null;
            this.DependentsEducationWind.Text = string.Empty;
            this.DependentsGenderWind.Text = string.Empty;
            this.DependentsEligibleClassWind.Text = string.Empty;
            this.DependentsRemarksWind.Text = string.Empty;
            this.WindowDependents.Hide();
            store_depentdent.Reload();

        }
        protected void btn_Save_Training_click(object sender, DirectEventArgs e)
        {
            if (string.IsNullOrEmpty(this.EmployeeIDTrainingWind.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Please choose employee !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }

            if (string.IsNullOrEmpty(this.pidTrainingWind.Text))
            {
                var objInsTraining = new clsEmployee();
                objInsTraining.InsTraining(this.EmployeeIDTrainingWind.Text, this.TrainingTypeWind.Text, this.TrainingNameWind.Text, this.TrainingMandatoryWind.Text,
                                            this.TrainingLocationWind.Text, Convert.ToDateTime(this.TrainingStartDateWind.Text),
                                            Convert.ToDateTime(this.TrainingEndDateWind.Text), this.TrainingCertificateWind.Text, this.TrainingProviderWind.Text,
                                            Convert.ToDateTime(this.ExpiredDateWind.Text));
            }
            else
            {
                var objUpdateTraining = new clsEmployee();
                objUpdateTraining.UpdTraining(Convert.ToInt16(this.pidTrainingWind), this.EmployeeIDTrainingWind.Text, this.TrainingTypeWind.Text, this.TrainingNameWind.Text, this.TrainingMandatoryWind.Text,
                                            this.TrainingLocationWind.Text, Convert.ToDateTime(this.TrainingStartDateWind.Text),
                                            Convert.ToDateTime(this.TrainingEndDateWind.Text), this.TrainingCertificateWind.Text, this.TrainingProviderWind.Text,
                                            Convert.ToDateTime(this.ExpiredDateWind.Text));
            }

            X.MessageBox.Show(new MessageBoxConfig
            {
                Title = "Information",
                Message = "Save Data Success",
                Buttons = MessageBox.Button.OK,
                Icon = MessageBox.Icon.INFO
            });
            this.TrainingTypeWind.Text = string.Empty;
            this.TrainingNameWind.Text = string.Empty;
            this.TrainingMandatoryWind.Text = string.Empty;
            this.TrainingLocationWind.Text = string.Empty;
            this.TrainingStartDateWind.Text = null;
            this.TrainingEndDateWind.Text = null;
            this.TrainingCertificateWind.Text = string.Empty;
            this.TrainingProviderWind.Text = string.Empty;
            this.ExpiredDateWind.Text = null;
            this.WindowTraining.Hide();
            Store_Training.Reload();
        }
        protected void btnClose_click(object sender, DirectEventArgs e)
        {
            X.Redirect(CommonUtility.ResolveUrl("Dashboard_itenary.aspx"), "Loading ... ");
        }
        protected void btn_Close_Windows_click(object sender, DirectEventArgs e)
        {
            this.WindowAssets.Hide();
            this.WindowDependents.Hide();
            this.WindowTraining.Hide();
        }
        protected void btn_edit_assetOther_doubleclick(object sender, DirectEventArgs e)
        {
            String jsonSelectedRows = e.ExtraParams["Values"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);
            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        this.pidAssetWind.Text = row["f_pid"];
                        this.EmployeeIDAssetWindTemp.Text = row["employeeid"];
                        this.EmployeeIDAssetWind.Text = row["employeeid"];
                        this.fFicNumberWind.Text = row["f_assetname"];
                        this.AssetTypeWindTemp.Text = row["f_category"];
                        this.AssetTypeWind.Text = row["f_category"];
                        this.AssetSiteWind.Text = row["f_site"];
                        this.AssetLocationWind.Text = row["f_location"];
                        this.AssetResponsibilityWind.Text = row["f_Responsibility"];
                        this.DescriptionWind.Text = row["f_description"];

                        this.btn_Save_Asset.Visible = false;
                        this.WindowAssets.Show();

                        //X.Redirect("~/Admin/mailbag/mailbagtracking_form.aspx?MailBagID=" + row["MailBagID"]);
                    }
                    catch
                    {

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "Warning",
                            Message = "Error !",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.ERROR
                        });
                    }
                }
            }
        }
        protected void btn_edit_assetSoftware_doubleclick(object sender, DirectEventArgs e)
        {
            String jsonSelectedRows = e.ExtraParams["Values"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);
            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        this.pidAssetWind.Text = row["f_pidSoftware"];
                        this.EmployeeIDAssetWindTemp.Text = row["employeeidSoftware"];
                        this.EmployeeIDAssetWind.Text = row["employeeidSoftware"];
                        this.fFicNumberWind.Text = row["f_assetnameSoftware"];
                        this.AssetTypeWindTemp.Text = row["f_categorySoftware"];
                        this.AssetTypeWind.Text = row["f_categorySoftware"];
                        this.AssetSiteWind.Text = row["f_siteSoftware"];
                        this.AssetLocationWind.Text = row["f_locationSoftware"];
                        this.AssetResponsibilityWind.Text = row["f_ResponsibilitySoftware"];
                        this.DescriptionWind.Text = row["f_descriptionSoftware"];

                        this.btn_Save_Asset.Visible = false;
                        this.WindowAssets.Show();

                        //X.Redirect("~/Admin/mailbag/mailbagtracking_form.aspx?MailBagID=" + row["MailBagID"]);
                    }
                    catch
                    {

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "Warning",
                            Message = "Error !",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.ERROR
                        });
                    }

                }
            }
        }
        protected void btn_edit_dependen_doubleclick(object sender, DirectEventArgs e)
        {
            String jsonSelectedRows = e.ExtraParams["Values"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);
            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        this.pidDependentsWind.Text = row["f_pid_dep"];
                        this.EmployeeIDDependentsWind.Text = row["employeeid"];
                        this.DependentsIDWind.Text = row["f_dependentid"];
                        this.DependentsNameWind.Text = row["f_dependentname"];
                        this.DependentsRelationshipWind.Text = row["f_relationship"];
                        this.DependentsBirthdayWind.Text = row["f_birthdate"];
                        this.DependentsEducationWind.Text = row["f_education"];
                        this.DependentsGenderWind.Text = row["f_sex"];
                        this.DependentsEligibleClassWind.Text = row["f_eligibleclass"];
                        this.DependentsRemarksWind.Text = row["f_remark"];

                        this.WindowDependents.Show();

                        //X.Redirect("~/Admin/mailbag/mailbagtracking_form.aspx?MailBagID=" + row["MailBagID"]);

                    }
                    catch
                    {

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "Warning",
                            Message = "Error !",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.ERROR
                        });
                    }

                }
            }
        }
        protected void btn_edit_training_doubleclick(object sender, DirectEventArgs e)
        {
            String jsonSelectedRows = e.ExtraParams["Values"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);
            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        this.pidTrainingWind.Text = row["pidTraining"];
                        this.EmployeeIDTrainingWind.Text = row["EmployeeIDTraining"];
                        this.TrainingTypeWind.Text = row["TrainingType"];
                        this.TrainingNameWind.Text = row["TrainingName"];
                        this.TrainingMandatoryWind.Text = row["TrainingMandatory"];
                        this.TrainingLocationWind.Text = row["TrainingLocation"];
                        this.TrainingStartDateWind.Text = row["TrainingStartDate"];
                        this.TrainingEndDateWind.Text = row["TrainingEndDate"];
                        this.TrainingCertificateWind.Text = row["TrainingCertificateNo"];
                        this.TrainingProviderWind.Text = row["TrainingProvider"];
                        this.ExpiredDateWind.Text = row["ExpiredDate"];

                        this.WindowTraining.Show();

                        //X.Redirect("~/Admin/mailbag/mailbagtracking_form.aspx?MailBagID=" + row["MailBagID"]);
                    }
                    catch
                    {

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "Warning",
                            Message = "Error !",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.ERROR
                        });
                    }

                }
            }
        }
        protected void btn_delete_AssetOther(object sender, DirectEventArgs e)
        {
            String jsonSelectedRows = e.ExtraParams["Value"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            if (selectedRowData.Length == 0)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Please selected row !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        var updAssetItem = new clsEmployee();
                        updAssetItem.UpdateAssetItem(row["f_assetid"]);

                        var objDelete = new clsEmployee();
                        objDelete.Delete_Assets(Convert.ToInt16(row["f_pid"]));
                    }
                    catch
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "CAN'T DELETE",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                    }
                }
            }
            Store_AssetsOther.Reload();
            this.grdAssetOther.Refresh();
        }
        protected void btn_delete_AssetSoftware(object sender, DirectEventArgs e)
        {
            String jsonSelectedRows = e.ExtraParams["Value"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            if (selectedRowData.Length == 0)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Please selected row !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        var updAssetItem = new clsEmployee();
                        updAssetItem.UpdateAssetItem(row["f_assetidSoftware"]);

                        var objDelete = new clsEmployee();
                        objDelete.Delete_Assets(Convert.ToInt16(row["f_pidSoftware"]));
                    }
                    catch
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "CAN'T DELETE",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                    }
                }
            }
            Store_AssetsSoftware.Reload();
            this.grdAssetSoftware.Refresh();
        }
        protected void btn_delete_Dependents(object sender, DirectEventArgs args)
        {
            String jsonSelectedRows = args.ExtraParams["Value"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            if (selectedRowData.Length == 0)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Please selected row !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        var objDelete = new clsEmployee();
                        objDelete.Delete_Dependents(Convert.ToInt16(row["f_pid_dep"]));
                    }
                    catch
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "CAN'T DELETE",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                    }
                }
            }
            store_depentdent.Reload();
            this.grdDepentdent.Refresh();
        }
        protected void btn_delete_Training(object sender, DirectEventArgs args)
        {
            String jsonSelectedRows = args.ExtraParams["Value"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            if (selectedRowData.Length == 0)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Please selected row !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        var objDelete = new clsEmployee();
                        objDelete.Delete_Training(Convert.ToInt16(row["pidTraining"]));
                    }
                    catch
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "CAN'T DELETE",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                    }
                }
            }
            Store_Training.Reload();
            this.grdTraining.Refresh();
        }
    }
}