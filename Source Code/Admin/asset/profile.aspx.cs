﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SmartLibraries.DataAccessLayer;
using NewERDM.lib.BusinessClass;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
 

namespace NewERDM.Admin.asset
{
    public partial class profile : System.Web.UI.Page
    {  
        
        
        #region field

        UserApplication userApp;
        string imageuser = "";

        #endregion field



        #region Construction

        //protected void store_employee_RefreshData(object sender, StoreReadDataEventArgs e)
        //{
            //getemployeeProfile();
        //}
        protected void store_AssetsOther_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeAssetOther();
        }
        protected void store_AssetsSoftware_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeAssetSoftware();
        }
        protected void store_dependent_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeDependentsProfile();
        }
        protected void store_Training_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeTraining();
        }

        protected void store_ethnicGroup_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEthnicGroup();
        }
        protected void store_city_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCityLookup();
        }
        protected void store_state_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getStateLookup();
        }
        protected void store_country_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCountryLookup();
        }
        protected void store_employeetype_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeTypeLookup();
        }
        protected void store_employeeclass_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeClassLookup();
        }
        protected void store_individualgrade_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getIndividualGradeLookup();
        }
        protected void store_orggroup_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getOrgGroupLookup();
        }
        protected void store_section_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getSectionLookup();
        }
        protected void store_costcenter_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCostCenterLookup();
        }
        protected void store_paygroup_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getPayGroupLookup();
        }
        
        #endregion Construction


        #region Method
        protected void getEmployee(string strEmployee)
        {
            DataTable dt;
            var objget = new clsEmployee();
            dt = objget.getEmployeeID(strEmployee.Trim());
            if (dt.Rows.Count > 0 ) 
            {
                
               lEmployeeID.Text = CommonUtility.strNull(dt.Rows[0]["ID"].ToString());
               lPosition.Text = CommonUtility.strNull(dt.Rows[0]["Job_title"].ToString());
               lWorkPhone.Text = CommonUtility.strNull(dt.Rows[0]["WorkPhone"].ToString());
               lFax.Text = CommonUtility.strNull(dt.Rows[0]["Fax_Number"].ToString());
               lphone.Text = CommonUtility.strNull(dt.Rows[0]["Home_Phone"].ToString());
               lmobile.Text = CommonUtility.strNull(dt.Rows[0]["Mobile_Phone"].ToString());
               lemail.Text = CommonUtility.strNull(dt.Rows[0]["Email_Address"].ToString());
               laddress.Text = CommonUtility.strNull(dt.Rows[0]["address"].ToString());
               getEmployeeAssetSoftware();
               getEmployeeAssetOther();
               getemployeeprofile(userApp.EmployeeID);
               getEmployeeDependentsProfile();
               getEmployeeTraining();
               getEmployeeList(userApp.EmployeeID);

            }

            lname.Html = string.Format("<b>{0}</b>", userApp.UserName);  
        }
        //protected void getemployeeaset()
        //{
        //    DataTable dt = new DataTable();
        //    var objGet = new clsEmployee();
        //    dt = objGet.getEmployeeAsset(userApp.EmployeeID);
        //    store_Detail.DataSource = dt;
        //}
        //protected void getemployee()
        //{
        //    DataTable dt = new DataTable();
        //    var objGet = new clsEmployee();
        //    dt = objGet.getEmployees();
        //    store_employee.DataSource = dt;
        //}
        protected void getEmployeeAssetOther()
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeAssetOther_Detail(userApp.EmployeeID);

            Store_AssetsOther.DataSource = dt;
            Store_AssetsOther.DataBind();

        }
        protected void getEmployeeAssetSoftware()
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeAssetSoftware_Detail(userApp.EmployeeID);

            Store_AssetsSoftware.DataSource = dt;
            Store_AssetsSoftware.DataBind();

        }
        protected void getemployeeprofile(string strID)
        {
            DataTable dt = new DataTable();
            var objGet = new clsEmployee();
            dt = objGet.getEmployeeID_Profile(strID);

            if (dt.Rows.Count > 0)
            {
                this.f_employeeid.Text = dt.Rows[0]["employeeid"].ToString();
                this.f_jabatan.Text = dt.Rows[0]["f_jabatan"].ToString();
                this.f_homephone.Text = dt.Rows[0]["f_homephone"].ToString();
                this.f_mobile.Text = dt.Rows[0]["f_mobile"].ToString();
                this.f_email.Text = dt.Rows[0]["f_email"].ToString();
                this.f_address.Text = dt.Rows[0]["f_address"].ToString();
            }
        }
        protected void getEmployeeDependentsProfile()
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeDependents_Detail(userApp.EmployeeID);

            store_depentdent.DataSource = dt;
            store_depentdent.DataBind();

        }
        protected void getEmployeeTraining()
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeTraining_Detail(userApp.EmployeeID);

            Store_Training.DataSource = dt;
            Store_Training.DataBind();

        }
        protected void getEmployeeList(string strEmployeeID)
        {
            DataTable dt = new DataTable();
            var objGet = new clsEmployee();
            dt = objGet.getEmployeesList(strEmployeeID.Trim());

            if (dt.Rows.Count > 0)
            {

                this.NetworkID.Text = dt.Rows[0]["NetworkID"].ToString();
                this.CompanyID.Text = dt.Rows[0]["CompanyID"].ToString();


                this.EmployeeID.Text = dt.Rows[0]["ID"].ToString();
                this.First_Name.Text = dt.Rows[0]["First_Name"].ToString();
                this.Middle_Name.Text = dt.Rows[0]["Middle_Name"].ToString();
                this.Last_Name.Text = dt.Rows[0]["Last_Name"].ToString();
                this.Gender.Text = dt.Rows[0]["Gender"].ToString();
                this.DateOfBirth.Text = dt.Rows[0]["DateOfBirth"].ToString();
                this.PlaceOfBirth.Text = dt.Rows[0]["PlaceOfBirth"].ToString();
                this.MaritalStatus.Text = dt.Rows[0]["MaritalStatus"].ToString();
                this.Religion.Text = dt.Rows[0]["Religion"].ToString();
                this.Nationality.Text = dt.Rows[0]["Nationality"].ToString();
                this.EthnicGroup.Text = dt.Rows[0]["EthnicGroup"].ToString();
                this.Address.Text = dt.Rows[0]["Address"].ToString();
                this.City.Text = dt.Rows[0]["City"].ToString();
                this.State.Text = dt.Rows[0]["State_Province"].ToString();
                this.Country.Text = dt.Rows[0]["Country_Region"].ToString();
                this.Zip.Text = dt.Rows[0]["ZIPPostal_Code"].ToString();
                this.HomePhone.Text = dt.Rows[0]["Home_Phone"].ToString();
                this.WorkPhone.Text = dt.Rows[0]["WorkPhone"].ToString();
                this.MobilePhone.Text = dt.Rows[0]["Mobile_Phone"].ToString();
                this.Fax.Text = dt.Rows[0]["Fax_Number"].ToString();
                this.Page.Text = dt.Rows[0]["Web_Page"].ToString();
                this.PhonetoRadion.Text = dt.Rows[0]["PhonetoRadion"].ToString();
                this.Email1.Text = dt.Rows[0]["Email1"].ToString();
                this.Email2.Text = dt.Rows[0]["Email2"].ToString();
                this.KTP.Text = dt.Rows[0]["KTP"].ToString();
                this.BPJS.Text = dt.Rows[0]["BPJS"].ToString();
                this.NPWP.Text = dt.Rows[0]["NPWP"].ToString();

                this.EmployeeType.Text = dt.Rows[0]["Type"].ToString();
                this.EmployeeClass.Text = dt.Rows[0]["Class"].ToString();
                this.IndividualGrade.Text = dt.Rows[0]["Grade"].ToString();
                this.OriginalHireDate.Text = dt.Rows[0]["OrginalHireDate"].ToString();
                this.RehireDate.Text = dt.Rows[0]["RehireDate"].ToString();
                this.ServiceDate.Text = dt.Rows[0]["ServiceDate"].ToString();
                this.LastPromotion.Text = dt.Rows[0]["LastPromotion"].ToString();

                this.Vendor.Text = dt.Rows[0]["Vendor"].ToString();
                this.WorkAt.Text = dt.Rows[0]["WorkAt"].ToString();
                this.FunctionalDepartment.Text = dt.Rows[0]["FunctionalDepartment"].ToString();
                this.OrgGroup.Text = dt.Rows[0]["OrgGroup"].ToString();
                this.Section.Text = dt.Rows[0]["Section"].ToString();
                this.Location.Text = dt.Rows[0]["Location"].ToString();
                this.Office.Text = dt.Rows[0]["Office"].ToString();

                this.JobTittle.Text = dt.Rows[0]["Job_Title"].ToString();
                this.PositionTitle.Text = dt.Rows[0]["PositionTitle"].ToString();
                this.Supervisor.Text = dt.Rows[0]["Supervisior"].ToString();
                this.CostCenter.Text = dt.Rows[0]["CostCenter"].ToString();

                this.PayGroup.Text = dt.Rows[0]["PayGroup"].ToString();
                this.BenefitCode.Text = dt.Rows[0]["BenefitCode"].ToString();
                this.UnionCode.Text = dt.Rows[0]["UnionCode"].ToString();
                this.TaxCode.Text = dt.Rows[0]["TaxCode"].ToString();

                this.NameOfContact.Text = dt.Rows[0]["NameOfContact"].ToString();
                this.Relationship.Text = dt.Rows[0]["Relationship"].ToString();
                this.PhoneNumber.Text = dt.Rows[0]["PhoneNumber"].ToString();
                this.MobileNumber.Text = dt.Rows[0]["MobileNumber"].ToString();
                this.Email.Text = dt.Rows[0]["Email"].ToString();

                this.PassportNo.Text = dt.Rows[0]["PassportNo"].ToString();
                this.PassportDateofIssue.Text = dt.Rows[0]["PassportDateofIssue"].ToString();
                this.PassportExpirationDate.Text = dt.Rows[0]["PassportExpirationIssue"].ToString();
                this.PassportIssuingOffice.Text = dt.Rows[0]["PassportIssuingOffice"].ToString();
                this.PassportRegNo.Text = dt.Rows[0]["PassportRegNo"].ToString();
                this.VisaNo.Text = dt.Rows[0]["VisaNo"].ToString();
                this.VisaType.Text = dt.Rows[0]["VisaType"].ToString();
                this.VisaIssueDate.Text = dt.Rows[0]["VisaIssueDate"].ToString();
                this.VisaExpirationDate.Text = dt.Rows[0]["VisaExpirationDate"].ToString();
                this.VisaIssuingOffice.Text = dt.Rows[0]["VisaIssuingOffice"].ToString();

            }
        }


        protected void getEthnicGroup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupEthnicGroup();
            store_EthnicGroup.DataSource = dt;
            store_EthnicGroup.DataBind();
        }
        protected void getCityLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupCity();
            store_City.DataSource = dt;
            store_City.DataBind();
        }
        protected void getStateLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupState();
            store_State.DataSource = dt;
            store_State.DataBind();
        }
        protected void getCountryLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupCountry();
            store_Country.DataSource = dt;
            store_Country.DataBind();
        }
        protected void getEmployeeTypeLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupEmployeeType();
            store_EmployeeType.DataSource = dt;
            store_EmployeeType.DataBind();
        }
        protected void getEmployeeClassLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupEmployeeClass();
            store_EmployeeClass.DataSource = dt;
            store_EmployeeClass.DataBind();
        }
        protected void getIndividualGradeLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupIndividualGrade();
            store_IndividualGrade.DataSource = dt;
            store_IndividualGrade.DataBind();
        }
        protected void getOrgGroupLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupOrgGroup();
            store_OrgGroup.DataSource = dt;
            store_OrgGroup.DataBind();
        }
        protected void getSectionLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupSection();
            store_Section.DataSource = dt;
            store_Section.DataBind();
        }
        protected void getCostCenterLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupCostCenter();
            store_CostCenter.DataSource = dt;
            store_CostCenter.DataBind();
        }
        protected void getPayGroupLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupPayGroup();
            store_PayGroup.DataSource = dt;
            store_PayGroup.DataBind();
        }

        #endregion Method

        //protected void store_Detail_RefreshData(object sender, StoreReadDataEventArgs e)
        //{
        //    getemployeeaset();
        //}
        
        protected void Page_Load(object sender, EventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];




            if (userApp == null)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                imageuser = string.Format(CommonUtility.ResolveUrl("~/photo/profile/{0}.jpg"), userApp.EmployeeID);
                photo_profile.Src = imageuser;
                photo_profile.Src = imageuser;
                getEmployee(userApp.EmployeeID);
                //viewdata();
                // Response.Write(userApp.EmployeeID);
                getEthnicGroup();
                getCityLookup();
                getStateLookup();
                getCountryLookup();
                getEmployeeTypeLookup();
                getEmployeeClassLookup();
                getIndividualGradeLookup();
                getOrgGroupLookup();
                getSectionLookup();
                getCostCenterLookup();
                getPayGroupLookup();
            }
        }
    }
}