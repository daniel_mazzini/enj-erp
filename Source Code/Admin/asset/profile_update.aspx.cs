﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SmartLibraries.DataAccessLayer;
using NewERDM.lib.BusinessClass;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using System.Text;


namespace NewERDM.Admin.asset
{
    public partial class profile_update : System.Web.UI.Page
    {
        #region field

        UserApplication userApp;

        #endregion

        private static string pathWeb()
        {
            string strPathWeb = HttpContext.Current.Server.MapPath("~");
            return strPathWeb;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Write("<b>test</b>");

            userApp = (UserApplication)Session["ses_user"];
            if (userApp == null)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                getemployee();

                getEthnicGroup();
                getCityLookup();
                getStateLookup();
                getCountryLookup();
                getEmployeeTypeLookup();
                getEmployeeClassLookup();
                getIndividualGradeLookup();
                getOrgGroupLookup();
                getSectionLookup();
                getCostCenterLookup();
                getPayGroupLookup();
            }

           
        }
        protected void store_ethnicGroup_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEthnicGroup();
        }
        protected void store_city_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCityLookup();
        }
        protected void store_state_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getStateLookup();
        }
        protected void store_country_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCountryLookup();
        }
        protected void store_employeetype_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeTypeLookup();
        }
        protected void store_employeeclass_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeClassLookup();
        }
        protected void store_individualgrade_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getIndividualGradeLookup();
        }
        protected void store_orggroup_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getOrgGroupLookup();
        }
        protected void store_section_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            
            getSectionLookup();
        }
        protected void store_costcenter_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getCostCenterLookup();
        }
        protected void store_paygroup_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getPayGroupLookup();
        }

        protected void store_employee_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getemployee();
        }
        protected void store_Detail_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeAsset();
        }
        protected void store_dependent_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeDependents();
        }

        protected void getEthnicGroup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupEthnicGroup();
            store_EthnicGroup.DataSource = dt;
            store_EthnicGroup.DataBind();
        }
        protected void getCityLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupCity();
            store_City.DataSource = dt;
            store_City.DataBind();
        }
        protected void getStateLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupState();
            store_State.DataSource = dt;
            store_State.DataBind();
        }
        protected void getCountryLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupCountry();
            store_Country.DataSource = dt;
            store_Country.DataBind();
        }
        protected void getEmployeeTypeLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupEmployeeType();
            store_EmployeeType.DataSource = dt;
            store_EmployeeType.DataBind();
        }
        protected void getEmployeeClassLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupEmployeeClass();
            store_EmployeeClass.DataSource = dt;
            store_EmployeeClass.DataBind();
        }
        protected void getIndividualGradeLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupIndividualGrade();
            store_IndividualGrade.DataSource = dt;
            store_IndividualGrade.DataBind();
        }
        protected void getOrgGroupLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupOrgGroup();
            store_OrgGroup.DataSource = dt;
            store_OrgGroup.DataBind();
        }
        protected void getSectionLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupSection();
            store_Section.DataSource = dt;
            store_Section.DataBind();
        }
        protected void getCostCenterLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupCostCenter();
            store_CostCenter.DataSource = dt;
            store_CostCenter.DataBind();
        }
        protected void getPayGroupLookup()
        {
            DataTable dt = null;
            var objget = new clsEmployee();
            dt = objget.getLookupPayGroup();
            store_PayGroup.DataSource = dt;
            store_PayGroup.DataBind();
        }
        protected void getemployee()
        {
            DataTable dt = new DataTable();
            var objGet = new clsEmployee();
            dt = objGet.getEmployees();
            store_employee.DataSource = dt;
        }
        protected void getemployeeprofile(string strID)
        {
            DataTable dt = new DataTable();
            var objGet = new clsEmployee();
            dt = objGet.getEmployeeID_Profile(strID);
            
            if(dt.Rows.Count > 0)
            {
                this.f_employeeid.Text = dt.Rows[0]["employeeid"].ToString();
                this.f_jabatan.Text = dt.Rows[0]["f_jabatan"].ToString();
                this.f_homephone.Text = dt.Rows[0]["f_homephone"].ToString();
                this.f_mobile.Text = dt.Rows[0]["f_mobile"].ToString();
                this.f_email.Text = dt.Rows[0]["f_email"].ToString();
                this.f_address.Text = dt.Rows[0]["f_address"].ToString();
            }
        }
        protected void getEmployeeAsset()
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt= objget.getEmployeeAssetOther_Detail(this.f_employeeid.Text);

            store_Detail.DataSource = dt;
            store_Detail.DataBind();

        }      
        protected void getEmployeeAsset_Update(string strEmployeeID)
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeAssetOther_Update(strEmployeeID);

            if (dt.Rows.Count > 0)
            {
                this.f_pid.Text = dt.Rows[0]["f_pid"].ToString();
                this.f_employeid.Text = dt.Rows[0]["employeeid"].ToString();
                this.f_assetid.Text = dt.Rows[0]["f_assetid"].ToString();
                this.f_assetname.Text = dt.Rows[0]["f_assetname"].ToString();
                this.f_serialnumber.Text = dt.Rows[0]["f_serialnumber"].ToString();
                this.f_areacode.Text = dt.Rows[0]["f_areacode"].ToString();
                this.f_site.Text = dt.Rows[0]["f_site"].ToString();
                this.f_location.Text = dt.Rows[0]["f_location"].ToString();
                this.f_ponumber.Text = dt.Rows[0]["f_ponumber"].ToString();
                this.f_description.Text = dt.Rows[0]["f_description"].ToString();

                getEmployeeAsset();
            }
         
        }
        protected void getEmployeeDependents()
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeDependents_Detail(this.f_employeeid.Text);

            store_depentdent.DataSource = dt;
            store_depentdent.DataBind();

        }
        protected void getEmployeeList(string strEmployeeID)
        {
            DataTable dt = new DataTable();
            var objGet = new clsEmployee();
            dt = objGet.getEmployeesList(strEmployeeID.Trim());

            if(dt.Rows.Count > 0)
            {

                this.NetworkID.Text = dt.Rows[0]["NetworkID"].ToString();
                this.CompanyID.Text = dt.Rows[0]["CompanyID"].ToString();


                this.EmployeeID.Text = dt.Rows[0]["ID"].ToString();
                this.First_Name.Text = dt.Rows[0]["First_Name"].ToString();
                this.Middle_Name.Text = dt.Rows[0]["Middle_Name"].ToString();
                this.Last_Name.Text = dt.Rows[0]["Last_Name"].ToString();
                this.Gender.Text = dt.Rows[0]["Gender"].ToString();
                this.DateOfBirth.Text = dt.Rows[0]["DateOfBirth"].ToString();
                this.PlaceOfBirth.Text = dt.Rows[0]["PlaceOfBirth"].ToString();
                this.MaritalStatus.Text = dt.Rows[0]["MaritalStatus"].ToString();
                this.Religion.Text = dt.Rows[0]["Religion"].ToString();
                this.Nationality.Text = dt.Rows[0]["Nationality"].ToString();
                this.EthnicGroup.Text = dt.Rows[0]["EthnicGroup"].ToString();
                this.Address.Text = dt.Rows[0]["Address"].ToString();
                this.City.Text = dt.Rows[0]["City"].ToString();
                this.State.Text = dt.Rows[0]["State_Province"].ToString();
                this.Country.Text = dt.Rows[0]["Country_Region"].ToString();
                this.Zip.Text = dt.Rows[0]["ZIPPostal_Code"].ToString();
                this.HomePhone.Text = dt.Rows[0]["Home_Phone"].ToString();
                this.WorkPhone.Text = dt.Rows[0]["WorkPhone"].ToString();
                this.MobilePhone.Text = dt.Rows[0]["Mobile_Phone"].ToString();
                this.Fax.Text = dt.Rows[0]["Fax_Number"].ToString();
                this.Page.Text = dt.Rows[0]["Web_Page"].ToString();
                this.PhonetoRadion.Text = dt.Rows[0]["PhonetoRadion"].ToString();
                this.Email1.Text = dt.Rows[0]["Email1"].ToString();
                this.Email2.Text = dt.Rows[0]["Email2"].ToString();
                this.KTP.Text = dt.Rows[0]["KTP"].ToString();
                this.BPJS.Text = dt.Rows[0]["BPJS"].ToString();
                this.NPWP.Text = dt.Rows[0]["NPWP"].ToString();

                this.EmployeeType.Text = dt.Rows[0]["Type"].ToString();
                this.EmployeeClass.Text = dt.Rows[0]["Class"].ToString();
                this.IndividualGrade.Text = dt.Rows[0]["Grade"].ToString();
                this.OriginalHireDate.Text = dt.Rows[0]["OrginalHireDate"].ToString();
                this.RehireDate.Text = dt.Rows[0]["RehireDate"].ToString();
                this.ServiceDate.Text = dt.Rows[0]["ServiceDate"].ToString();
                this.LastPromotion.Text = dt.Rows[0]["LastPromotion"].ToString();

                this.Vendor.Text = dt.Rows[0]["Vendor"].ToString();
                this.WorkAt.Text = dt.Rows[0]["WorkAt"].ToString();
                this.FunctionalDepartment.Text = dt.Rows[0]["FunctionalDepartment"].ToString();
                this.OrgGroup.Text = dt.Rows[0]["OrgGroup"].ToString();
                this.Section.Text = dt.Rows[0]["Section"].ToString();
                this.Location.Text = dt.Rows[0]["Location"].ToString();
                this.Office.Text = dt.Rows[0]["Office"].ToString();

                this.JobTittle.Text = dt.Rows[0]["Job_Title"].ToString();
                this.PositionTitle.Text = dt.Rows[0]["PositionTitle"].ToString();
                this.Supervisor.Text = dt.Rows[0]["Supervisior"].ToString();
                this.CostCenter.Text = dt.Rows[0]["CostCenter"].ToString();

                this.PayGroup.Text = dt.Rows[0]["PayGroup"].ToString();
                this.BenefitCode.Text = dt.Rows[0]["BenefitCode"].ToString();
                this.UnionCode.Text = dt.Rows[0]["UnionCode"].ToString();
                this.TaxCode.Text = dt.Rows[0]["TaxCode"].ToString();

                this.NameOfContact.Text = dt.Rows[0]["NameOfContact"].ToString();
                this.Relationship.Text = dt.Rows[0]["Relationship"].ToString();
                this.PhoneNumber.Text = dt.Rows[0]["PhoneNumber"].ToString();
                this.MobileNumber.Text = dt.Rows[0]["MobileNumber"].ToString();
                this.Email.Text = dt.Rows[0]["Email"].ToString();

                this.PassportNo.Text = dt.Rows[0]["PassportNo"].ToString();
                this.PassportDateofIssue.Text = dt.Rows[0]["PassportDateofIssue"].ToString();
                this.PassportExpirationDate.Text = dt.Rows[0]["PassportExpirationIssue"].ToString();
                this.PassportIssuingOffice.Text = dt.Rows[0]["PassportIssuingOffice"].ToString();
                this.PassportRegNo.Text = dt.Rows[0]["PassportRegNo"].ToString();
                this.VisaNo.Text = dt.Rows[0]["VisaNo"].ToString();
                this.VisaType.Text = dt.Rows[0]["VisaType"].ToString();
                this.VisaIssueDate.Text = dt.Rows[0]["VisaIssueDate"].ToString();
                this.VisaExpirationDate.Text = dt.Rows[0]["VisaExpirationDate"].ToString();
                this.VisaIssuingOffice.Text = dt.Rows[0]["VisaIssuingOffice"].ToString();

            }
        }
        protected void getEmployeeDepentdents_Update(string strEmployeeID)
        {
            DataTable dt = new DataTable();
            var objget = new clsEmployee();
            dt = objget.getEmployeeDependents_Update(strEmployeeID);

            if(dt.Rows.Count > 0)
            {
                this.f_pid_dep.Text = dt.Rows[0]["f_pid"].ToString();
                this.f_employeeid_dep.Text = dt.Rows[0]["employeeid"].ToString();
                this.f_dependentid_dep.Text = dt.Rows[0]["f_dependentid"].ToString();
                this.f_dependentname_dep.Text = dt.Rows[0]["f_dependentname"].ToString();
                this.f_relationship_dep.Text = dt.Rows[0]["f_relationship"].ToString();
                this.f_birthdate_dep.Text = dt.Rows[0]["f_birthdate"].ToString();
                this.f_education_dep.Text = dt.Rows[0]["f_education"].ToString();
                this.f_sex_dep.Text = dt.Rows[0]["f_sex"].ToString();
                this.f_eligibleclass_dep.Text = dt.Rows[0]["f_eligibleclass"].ToString();
                this.f_remark_dep.Text = dt.Rows[0]["f_remark"].ToString();

                getEmployeeDependents();
            }

        }
        protected void viewdata(object sender, DirectEventArgs args)
        {
            String jsonSelectedRows = args.ExtraParams["ID"];
            StringBuilder sBuilderupdate;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            sBuilderupdate = new StringBuilder();
            foreach (Dictionary<string, string> row in selectedRowData)
            {
                try
                {
                   getemployeeprofile(row["employeeid"]);
                   getEmployeeAsset_Update(row["employeeid"]);
                   getEmployeeDepentdents_Update(row["employeeid"]);
                   getEmployeeList(row["employeeid"]);
                                      
                }
                catch
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "WARNING",
                        Message = "WARNING 1",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                }
            }

            this.store_Detail.Reload();
            this.store_depentdent.Reload();

        }
        protected void btnNew_click(object sender, DirectEventArgs e)
        {
            
            //save as photo
            string sPathWeb = pathWeb();
            string sID = userApp.EmployeeID + ".jpg";

            if(this.f_SmallPhoto.HasFile)
            {                
                this.f_SmallPhoto.PostedFile.SaveAs(String.Format("{0}\\photo\\{1}", sPathWeb, sID));
               
            }
            else
            {
                this.f_SmallPhoto.Text = "";
            }

            if(this.f_ProfilePhoto.HasFile)
            {
                this.f_ProfilePhoto.PostedFile.SaveAs(String.Format("{0}\\photo\\profile\\{1}", sPathWeb, sID));
            }
            else
            {
               
                this.f_ProfilePhoto.Text = "";
            }

            //Update Employee
            var objUpd = new clsEmployee();
            objUpd.UpdEmployeeProfile(this.f_jabatan.Text, this.f_homephone.Text, this.f_mobile.Text, this.f_email.Text, this.f_emailPass.Text, this.f_address.Text, this.f_employeeid.Text);

            X.MessageBox.Show(new MessageBoxConfig
            {
                Title = "INFORMATION",
                Message = "DATA SAVED",
                Buttons = MessageBox.Button.OK,
                Icon = MessageBox.Icon.INFO
            });

        }
        protected void btnClose_click(object sender, DirectEventArgs e)
        {
            X.Redirect(CommonUtility.ResolveUrl("Dashboard_itenary.aspx"), "Loading ... ");
        }
        protected void btn_save_asset_click(object sender, DirectEventArgs e)
        {                     
            string jsonValues = e.ExtraParams["ID"];
            List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);

            foreach (var record in records)
            {
                if (string.IsNullOrEmpty(record["f_pid"].ToString()))
                {
                    var objIns = new clsEmployee();
                    objIns.InsEmployeeAsset(record["employeeid"], record["f_assetid"], record["f_category"], record["f_assetname"], "", record["f_serialnumber"],
                                            record["f_areacode"], record["f_site"], record["f_location"], record["f_ponumber"], record["f_description"]);

                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "INFORMATION",
                        Message = "DATA SAVED",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO
                    });
                }
                else
                {

                    DataTable dt = new DataTable();
                    var objGet = new clsEmployee();
                    dt = objGet.getCheckDoublePid(Convert.ToInt16(record["f_pid"]));

                    if (dt.Rows.Count > 0)
                    {
                        var objUpd = new clsEmployee();
                        objUpd.UpdEmployeeAsset(Convert.ToInt16(record["f_pid"]), record["employeeid"], record["f_assetid"], record["f_category"], record["f_assetname"], record["f_assetname"],
                                            record["f_serialnumber"], record["f_areacode"], record["f_site"], record["f_location"], record["f_ponumber"], record["f_description"]);

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "INFORMATION",
                            Message = "DATA UPDATED",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO
                        });
                    }                   
                }

                store_Detail.Reload();
                this.grdAsset.Refresh();

            }
        
        }     
        protected void btn_save_dep_click(object sender, DirectEventArgs e)
        {
            string jsonValues = e.ExtraParams["ID"];
            List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);

            foreach (var record in records)
            {
                if (string.IsNullOrEmpty(record["f_pid"].ToString()))
                {
                    var objIns = new clsEmployee();
                    objIns.InsEmployeeDependents(record["employeeid"], record["f_dependentid"], record["f_dependentname"], record["f_relationship"], Convert.ToDateTime(record["f_birthdate"]),
                                            record["f_education"], record["f_sex"], record["f_eligibleclass"], record["f_remark"]);

                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "INFORMATION",
                        Message = "DATA SAVED",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO
                    });
                }
                else
                {

                    DataTable dt = new DataTable();
                    var objGet = new clsEmployee();
                    dt = objGet.getCheckDoublePid_Dependents(Convert.ToInt16(record["f_pid"]));

                    if (dt.Rows.Count > 0)
                    {
                        var objUpd = new clsEmployee();
                        objUpd.UpdEmployeeDependents(Convert.ToInt16(record["f_pid"]), record["employeeid"], record["f_dependentid"], record["f_dependentname"], record["f_relationship"], Convert.ToDateTime(record["f_birthdate"]),
                                            record["f_education"], record["f_sex"], record["f_eligibleclass"], record["f_remark"]);

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "INFORMATION",
                            Message = "DATA UPDATED",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO
                        });
                    }
                }

                store_depentdent.Reload();
                this.grdDepentdent.Refresh();

            }
        }
        protected void btn_save_click(object sender, DirectEventArgs e)
        {
            //Mandatory
            if(this.DateOfBirth.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig   {  Title="WARNING",  Message="Date Of Birth can't be empty !",  Buttons= MessageBox.Button.OK,  Icon= MessageBox.Icon.WARNING    });
                return;
            }
            if (this.OriginalHireDate.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Orginal Hire Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.RehireDate.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "ReHire Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.ServiceDate.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Service Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.LastPromotion.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Last Promotion can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.PassportDateofIssue.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Passport Date Issue can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.PassportExpirationDate.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Passport Expiration Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.PassportIssuingOffice.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Passport Issuing Office can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.VisaIssueDate.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "Vissa Issue Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.VisaExpirationDate.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "VIssa Expiration Date can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }
            if (this.VisaIssuingOffice.Text == "1/1/0001 12:00:00 AM")
            {
                X.MessageBox.Show(new MessageBoxConfig { Title = "WARNING", Message = "VIssa Issuing Office can't be empty !", Buttons = MessageBox.Button.OK, Icon = MessageBox.Icon.WARNING });
                return;
            }

            //Check double ID
            DataTable dt = new DataTable();
            var objGet = new clsEmployee();
            dt = objGet.getCheckEmployeeID(this.EmployeeID.Text.Trim());

            if(dt.Rows.Count == 0)
            {
                //Insert Data

                var objIns = new clsEmployee();
                objIns.InsEmployee(this.EmployeeID.Text, this.CompanyID.Text, this.NetworkID.Text, this.Last_Name.Text, this.First_Name.Text, this.Middle_Name.Text, this.Gender.Text,
                                this.f_email.Text, this.JobTittle.Text, "BussinesPhone", this.HomePhone.Text, this.MobilePhone.Text, this.Fax.Text, this.Address.Text,
                                this.City.Text, this.State.Text, this.Zip.Text, this.Country.Text, this.Page.Text, "POH", "Status", this.EmployeeType.Text, "Notes",
                                this.IndividualGrade.Text, Convert.ToDateTime(this.DateOfBirth.Value), this.PlaceOfBirth.Text, this.MaritalStatus.Text, this.Religion.Text, this.Nationality.Text,
                                this.EthnicGroup.Text, this.WorkPhone.Text, PhonetoRadion.Text, this.Email1.Text, this.Email2.Text, this.KTP.Text, this.BPJS.Text, this.NPWP.Text,
                                this.EmployeeClass.Text, Convert.ToDateTime(this.OriginalHireDate.Value), Convert.ToDateTime(this.RehireDate.Value), Convert.ToDateTime(this.ServiceDate.Value), 
                                Convert.ToDateTime(this.LastPromotion.Value), this.Vendor.Text, this.WorkAt.Text, this.FunctionalDepartment.Text, this.OrgGroup.Text, this.Section.Text, this.Location.Text, 
                                this.Office.Text, this.PositionTitle.Text, this.Supervisor.Text, this.CostCenter.Text, this.PayGroup.Text, this.BenefitCode.Text, this.UnionCode.Text, this.TaxCode.Text, 
                                this.NameOfContact.Text, this.Relationship.Text, this.PhoneNumber.Text, this.MobileNumber.Text, this.Email.Text, this.PassportNo.Text, Convert.ToDateTime(this.PassportDateofIssue.Value), 
                                Convert.ToDateTime(this.PassportExpirationDate.Value),  Convert.ToDateTime(this.PassportIssuingOffice.Value), this.PassportRegNo.Text, this.VisaNo.Text, this.VisaType.Text, 
                                Convert.ToDateTime(this.VisaIssueDate.Value), Convert.ToDateTime(this.VisaExpirationDate.Value), Convert.ToDateTime(this.VisaIssuingOffice.Value));

                X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "INFORMATION",
                        Message="DATA SAVED",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO
                    });
            }
            else
            {
                //Update Data

                var objUpd = new clsEmployee();
                objUpd.UpdEmployee(this.EmployeeID.Text, this.CompanyID.Text, this.NetworkID.Text, this.Last_Name.Text, this.First_Name.Text, this.Middle_Name.Text, this.Gender.Text,
                                this.f_email.Text, this.JobTittle.Text, this.WorkPhone.Text, this.HomePhone.Text, this.MobilePhone.Text, this.Fax.Text, this.Address.Text,
                                this.City.Text, this.State.Text, this.Zip.Text, this.Country.Text, this.Page.Text, this.City.Text, "A", this.EmployeeType.Text, "Notes",
                                this.IndividualGrade.Text, Convert.ToDateTime(this.DateOfBirth.Value), this.PlaceOfBirth.Text, this.MaritalStatus.Text, this.Religion.Text, this.Nationality.Text,
                                this.EthnicGroup.Text, this.WorkPhone.Text, PhonetoRadion.Text, this.Email1.Text, this.Email2.Text, this.KTP.Text, this.BPJS.Text, this.NPWP.Text,
                                this.EmployeeClass.Text, Convert.ToDateTime(this.OriginalHireDate.Value), Convert.ToDateTime(this.RehireDate.Value), Convert.ToDateTime(this.ServiceDate.Value),
                                Convert.ToDateTime(this.LastPromotion.Value), this.Vendor.Text, this.WorkAt.Text, this.FunctionalDepartment.Text, this.OrgGroup.Text, this.Section.Text, this.Location.Text,
                                this.Office.Text, this.PositionTitle.Text, this.Supervisor.Text, this.CostCenter.Text, this.PayGroup.Text, this.BenefitCode.Text, this.UnionCode.Text, this.TaxCode.Text,
                                this.NameOfContact.Text, this.Relationship.Text, this.PhoneNumber.Text, this.MobileNumber.Text, this.Email.Text, this.PassportNo.Text, Convert.ToDateTime(this.PassportDateofIssue.Value),
                                Convert.ToDateTime(this.PassportExpirationDate.Value), Convert.ToDateTime(this.PassportIssuingOffice.Value), this.PassportRegNo.Text, this.VisaNo.Text, this.VisaType.Text,
                                Convert.ToDateTime(this.VisaIssueDate.Value), Convert.ToDateTime(this.VisaExpirationDate.Value), Convert.ToDateTime(this.VisaIssuingOffice.Value));

                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "INFORMATION",
                    Message = "DATA UPDATED",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO
                });
            }
        }
        protected void btn_delete_Asset(object sender, DirectEventArgs args)
        {
            String jsonSelectedRows = args.ExtraParams["Value"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            if (selectedRowData.Length == 0)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Please selected row !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        var objDelete = new clsEmployee();
                        objDelete.Delete_Assets(Convert.ToInt16(row["f_pid"]));
                    }
                    catch
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "CAN'T DELETE",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                    }
                }
            }
            store_Detail.Reload();
            this.grdAsset.Refresh();
        }
        protected void btn_delete_Dependents(object sender, DirectEventArgs args)
        {
            String jsonSelectedRows = args.ExtraParams["Value"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            if (selectedRowData.Length == 0)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Please selected row !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        var objDelete = new clsEmployee();
                        objDelete.Delete_Dependents(Convert.ToInt16(row["f_pid"]));
                    }
                    catch
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "CAN'T DELETE",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                    }
                }
            }
            store_depentdent.Reload();
            this.grdDepentdent.Refresh();
        }
        
    }
}