﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="profile_update_rev.aspx.cs" Inherits="NewERDM.Admin.asset.profile_update_rev" %>
<asp:Content ID="Header" runat="server" ContentPlaceHolderID="head">
    <ext:XScript ID="XScript1" runat="server">
        <script>
            var addmaster = function () {
            var grid = #{grdAsset};
            grid.editingPlugin.cancelEdit();

            // Create a record instance through the ModelManager
            var r = Ext.ModelManager.create({
                f_pid       : '',
                f_employeid    : '',
                f_employeename      : '',
                f_assetid : '',
                f_category      : '',
                f_assetname    : '',
                f_serialnumber   : '',
                f_areacode     : '',
                f_site  : '',
                f_location       : '',
                f_ponumber : '',
                f_description : ''
            }, 'data');
          
            grid.store.insert(HTMLTableRowElement+1,r);
            grid.editingPlugin.startEdit(HTMLTableRowElement+1,HTMLTableRowElement+1);
            };

            var addDependent = function () {
            var grid = #{grdDepentdent};
            grid.editingPlugin.cancelEdit();

            // Create a record instance through the ModelManager
            var r = Ext.ModelManager.create({
                f_pid       : '',
                f_employeid    : '',
                f_dependentid      : '',
                f_dependentname : '',
                f_relationship      : '',
                f_birthdate    : '',
                f_education   : '',
                f_sex     : '',
                f_eligibleclass  : '',
                f_remark       : ''
            }, 'datadep');
          
            grid.store.insert(HTMLTableRowElement+1,r);
            grid.editingPlugin.startEdit(HTMLTableRowElement+1,HTMLTableRowElement+1);
            };
        </script>
    </ext:XScript>
</asp:Content>

<asp:Content ID="Detail" runat="server" ContentPlaceHolderID="MainContent">
    <section class="content">
        <div class="row">
            <div class="col-md-4">
              <div class="box box-solid">
                <div class="box-body">
                    <ext:GridPanel  id="grdListEmployee" runat="server" Flex="1" Padding="2" Height="522"  >
						<Store>
								<ext:Store runat="server" ID="store_employee" PageSize="22"  OnReadData="store_employee_RefreshData" >
									<Model>
										<ext:Model ID="Model4" runat="server" IDProperty="employeeid"   >
										<Fields>
                                            <ext:ModelField Name="employeeid" />
							                <ext:ModelField Name="employeename" />
										</Fields>
									</ext:Model>
									</Model>
								</ext:Store>
						</Store>
                        <View>
					            <ext:GridView ID="grdDetail_view" runat="server" LoadMask="false" />
			            </View>
						<ColumnModel ID="ColumnModel1"  runat="server"> 
						<Columns>       
                                <ext:Column ID="Column21" runat="server" Text="Employee ID"   width="80" DataIndex="employeeid"/>
                                <ext:Column ID="Column1" runat="server"  Text="Employee Name"  Flex="1"  DataIndex="employeename" Filterable="true"/>
                            </Columns>
						</ColumnModel>
                        <DirectEvents>
                            <CellClick OnEvent="viewdata">
                                <EventMask ShowMask="true" Msg="Loading..." />
                                    <ExtraParams>
                                        <ext:Parameter Name="ID" Value="Ext.encode(#{grdListEmployee}.getRowsValues({selectedOnly : true}))" Mode="Raw"/>
                                    </ExtraParams>
                            </CellClick> 
                        </DirectEvents>
                        <BottomBar>
								<ext:PagingToolbar ID="PagingToolbar4" runat="server"  DisplayInfo="false" StoreID="store_employee"    />
						</BottomBar>
                              
					</ext:GridPanel>

                </div>
              </div>
            </div>

            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body ">
                        <ext:Panel runat="server" ID="pnlcontainer" Border="false">
                            <Items>
                                <ext:TabPanel ID="TabPanel1" runat="server" ActiveTabIndex="0" TabAlign="Left" BodyPadding="10" >
                                    <Items>
                                        <%--Tab Profile --%>
                                        <ext:Panel ID="Tab1" runat="server" Title="Profile" BodyPadding="6" AutoScroll="true"  Border="false" >
                                            <Items>
                                                <ext:FormPanel runat="server" ID="frmProfile"  BodyPadding="8" Border="false">
                                                    <Items>
                                                        <ext:TextField runat="server" ID="f_employeeid"  LabelWidth="120"   FieldLabel="ID"   Width="400" Hidden="true"   /> 
                                                        <ext:TextField runat="server" ID="f_jabatan"  LabelWidth="120"   FieldLabel="Position"   Width="400"   /> 
                                                        <ext:TextField runat="server" ID="f_dept"      LabelWidth="120"  FieldLabel="Dept" Width="300"   /> 
                                                        <ext:TextField runat="server" ID="f_homephone" LabelWidth="120"   FieldLabel="Home Phone"  /> 
                                                        <ext:TextField runat="server" ID="f_mobile"   LabelWidth="120"   FieldLabel="Mobile Phone"  /> 
                                                        <ext:TextField runat="server" ID="f_email"    LabelWidth="120"   FieldLabel="E-Mail"  Width="400"  />
                                                        <ext:TextField runat="server" ID="f_emailPass" Hidden="true" />
                                                        <ext:TextArea  runat="server" ID="f_address"  LabelWidth="120"   FieldLabel="Address" Width="400" />  
                                                        <ext:FileUploadField runat="server" ID="f_SmallPhoto" TabIndex="5" LabelWidth="120" FieldLabel="Upload Photo (small)"
                                                             Width="500" Margins="0 0 0 85" Icon="Attach" />
                                                        <ext:FileUploadField runat="server" ID="f_ProfilePhoto" TabIndex="5" LabelWidth="120" FieldLabel="Upload Photo Profile"
                                                             Width="500" Margins="0 0 0 85" Icon="Attach" />
                                                    </Items>
                                                    <Buttons>
                                                        <ext:Button runat="server" ID="btnNew" Height="30" UI="Success" Html="<b>SAVE DATA</b>">
                                                            <DirectEvents>
                                                                <Click OnEvent="btnNew_click">
                                                                    <EventMask ShowMask="true" Msg="Loading..." />
                                                                </Click>
                                                            </DirectEvents>
                                                        </ext:Button>
                                                        <ext:Button runat="server" ID="btnClose" Height="30" UI="Danger"  Html="<b>CLOSE</b>">
                                                            <DirectEvents>
                                                                <Click OnEvent="btnClose_click">
                                                                    <EventMask ShowMask="true" Msg="Loading.." />
                                                                </Click>
                                                            </DirectEvents>
                                                        </ext:Button>
                                                    </Buttons>
                                                </ext:FormPanel>
                                            </Items>
                                        </ext:Panel>

                                        <%-- Personal Data --%>
                                        <ext:Panel ID="Tab2" runat="server" Title="Personal" BodyPadding="6"  Border="false">
                                            <Items>
                                                <ext:FormPanel runat="server" ID="frmPersonal" BodyPadding="8" Border="false">
                                                    <Items>
                                                        <ext:FormPanel runat="server" ID="frmPersonalData" Title="Personal Data" BodyPadding="8" Border="false">
                                                            <Items>
                                                                <ext:TextField runat="server" ID="NetworkID" LabelWidth="120" FieldLabel="NetworkID" Hidden="true" Width="400" /> 
                                                                <ext:TextField runat="server" ID="CompanyID"  LabelWidth="120" FieldLabel="CompanyID" Hidden="true" Width="400" /> 
                                                                <ext:TextField runat="server" ID="EmployeeID"  LabelWidth="120" FieldLabel="ID"   Width="400" /> 
                                                                <ext:TextField runat="server" ID="First_Name"  LabelWidth="120" FieldLabel="First Name" Width="400" /> 
                                                                <ext:TextField runat="server" ID="Middle_Name"  LabelWidth="120" FieldLabel="Middle Name" Width="400" /> 
                                                                <ext:TextField runat="server" ID="Last_Name"  LabelWidth="120" FieldLabel="Last Name" Width="400" /> 
                                                                <ext:ComboBox runat="server" ID="Gender" LabelWidth="120" Text="Male" FieldLabel="Gender" >
                                                                    <Items>
                                                                        <ext:ListItem Text="Male" Value="M" />
                                                                        <ext:ListItem Text="Female" Value="F" />
                                                                    </Items>
                                                                </ext:ComboBox>
                                                                <ext:DateField runat="server" ID="DateOfBirth" LabelWidth="120" FieldLabel="Date Of Birth" Format="ddd, dd-MMM-yyyy" />
                                                                <ext:TextField runat="server" ID="PlaceOfBirth"  LabelWidth="120"   FieldLabel="Place Of Birth"   Width="400"   /> 
                                                                <ext:ComboBox runat="server" ID="MaritalStatus" LabelWidth="120" Text="Single" FieldLabel="Marital Status" >
                                                                    <Items>
                                                                        <ext:ListItem Text="Single" Value="Single" />
                                                                        <ext:ListItem Text ="Merried" Value="Merried" />
                                                                    </Items>
                                                                </ext:ComboBox>
                                                                <ext:ComboBox runat="server" ID="Religion" LabelWidth="120" FieldLabel="Religion" Text="Islam" >
                                                                    <Items>
                                                                        <ext:ListItem Text="Islam" Value="Islam" />
                                                                        <ext:ListItem Text="Katolik" Value="Katolik" />
                                                                        <ext:ListItem Text="Kristen" Value="Kristen" />
                                                                        <ext:ListItem Text="Protestan" Value="Protestan" />
                                                                        <ext:ListItem Text="Budha" Value="Budha" />
                                                                        <ext:ListItem Text="Hindu" Value="Hindu" />
                                                                    </Items>
                                                                </ext:ComboBox>
                                                                <ext:ComboBox runat="server" ID="Nationality" LabelWidth="120" FieldLabel="Nationality" Text="WNI" >
                                                                    <Items>
                                                                        <ext:ListItem Text="WNI" Value="WNI" />
                                                                        <ext:ListItem Text="WNA" Value="WNA" />
                                                                    </Items>
                                                                </ext:ComboBox>
                                                                <ext:ComboBox runat="server" ID="EthnicGroup" LabelWidth="120" FieldLabel="Ethnic Group" DisplayField="Name"
                                                                     ValueField="Code" Editable="false">
                                                                    <Store>
                                                                        <ext:Store runat="server" ID="store_EthnicGroup" AutoLoad="true" OnReadData="store_ethnicGroup_RefreshData" >
                                                                            <Model>
                                                                                <ext:Model ID="Model3" runat="server"  >
                                                                                    <Fields>
                                                                                        <ext:ModelField Name="Code"   />
                                                                                        <ext:ModelField Name="Name"  />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:FormPanel>
                                                        <ext:FormPanel runat="server" ID="frmAddressDetail" Title="Address Detail" BodyPadding="8"   Border="false">
                                                            <Items>
                                                                <ext:TextArea runat="server" ID="Address"  LabelWidth="120"   FieldLabel="Address" Width="400" /> 
                                                                <ext:ComboBox runat="server" ID="City" LabelWidth="120" FieldLabel="City" DisplayField="Name" ValueField="Code" Editable="false">
                                                                    <Store>
                                                                        <ext:Store runat="server" ID="store_City" AutoLoad="true" OnReadData="store_city_RefreshData" >
                                                                            <Model>
                                                                                <ext:Model ID="Model5" runat="server"  >
                                                                                    <Fields>
                                                                                    <ext:ModelField Name="Code"   />
                                                                                    <ext:ModelField Name="Name"  />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                </ext:ComboBox>
                                                                <ext:ComboBox runat="server" ID="State" LabelWidth="120" FieldLabel="State" DisplayField="Name" ValueField="Code" Editable="false">
                                                                    <Store>
                                                                        <ext:Store runat="server" ID="store_State" AutoLoad="true" OnReadData="store_state_RefreshData" >
                                                                            <Model>
                                                                                <ext:Model ID="Model6" runat="server"  >
                                                                                    <Fields>
                                                                                    <ext:ModelField Name="Code"   />
                                                                                    <ext:ModelField Name="Name"  />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                </ext:ComboBox>
                                                                <ext:ComboBox runat="server" ID="Country" LabelWidth="120" FieldLabel="Country" DisplayField="Name" ValueField="Code" Editable="false">
                                                                    <Store>
                                                                        <ext:Store runat="server" ID="store_Country" AutoLoad="true" OnReadData="store_country_RefreshData" >
                                                                            <Model>
                                                                                <ext:Model ID="Model7" runat="server"  >
                                                                                    <Fields>
                                                                                    <ext:ModelField Name="Code"   />
                                                                                    <ext:ModelField Name="Name"  />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                </ext:ComboBox>
                                                                <ext:TextField runat="server" ID="Zip"  LabelWidth="120"   FieldLabel="Zip"   Width="400"   /> 
                                                                <ext:TextField runat="server" ID="HomePhone"  LabelWidth="120"   FieldLabel="Home Phone"   Width="400"   /> 
                                                            </Items>
                                                        </ext:FormPanel>
                                                        <ext:FormPanel runat="server" ID="frmContactDetail" Title="Contact Detail" BodyPadding="8"   Border="false">
                                                            <Items>
                                                                <ext:TextField runat="server" ID="WorkPhone" LabelWidth="120" FieldLabel="Work Phone" Width="400" /> 
                                                                <ext:TextField runat="server" ID="MobilePhone" LabelWidth="120" FieldLabel="Mobile" Width="400" /> 
                                                                <ext:TextField runat="server" ID="Fax" LabelWidth="120" FieldLabel="Fax" Width="400" /> 
                                                                <ext:TextField runat="server" ID="Page" LabelWidth="120" FieldLabel="Page" Width="400" /> 
                                                                <ext:TextField runat="server" ID="PhonetoRadion" LabelWidth="120" FieldLabel="Phone to Radion" Width="400" /> 
                                                                <ext:TextField runat="server" ID="Email1" LabelWidth="120" FieldLabel="Email 1" Width="400" /> 
                                                                <ext:TextField runat="server" ID="Email2" LabelWidth="120" FieldLabel="Email 2" Width="400" /> 
                                                            </Items>
                                                        </ext:FormPanel>
                                                        <ext:FormPanel runat="server" ID="frmOtherDetail" Title="Other Detail" BodyPadding="8"   Border="false">
                                                            <Items>
                                                                <ext:TextField runat="server" ID="KTP" LabelWidth="120" FieldLabel="KTP" Width="400" /> 
                                                                <ext:TextField runat="server" ID="BPJS" LabelWidth="120" FieldLabel="BPJS" Width="400" />
                                                                <ext:TextField runat="server" ID="NPWP" LabelWidth="120" FieldLabel="NPWP" Width="400" />
                                                            </Items>
                                                        </ext:FormPanel>
                                                    </Items>
                                                    <Buttons>
                                                        <ext:Button runat="server" ID="btn_save_personal" Height="30" UI="Success" Html="<b>SAVE DATA</b>" >
                                                            <DirectEvents>
                                                                <Click OnEvent="btn_save_personal_click" >
                                                                    <EventMask ShowMask="true" Msg="Saving Data Please Wait..." />
                                                                </Click>
                                                            </DirectEvents>
                                                        </ext:Button>
                                                        <ext:Button runat="server" ID="btn_close_personal" Height="30" UI="Danger"  Html="<b>CLOSE</b>" >
                                                           <DirectEvents>
                                                               <Click OnEvent="btnClose_click">
                                                                   <EventMask ShowMask="true" Msg="Loading..." />
                                                               </Click>
                                                           </DirectEvents>
                                                        </ext:Button> 
                                                    </Buttons>
                                                </ext:FormPanel>
                                            </Items>
                                        </ext:Panel>

                                        <%-- Job Information --%>
                                        <ext:Panel ID="Tab3"  runat="server" Title="Job Information" BodyPadding="6"  Border="false">
                                            <Items>
                                                <ext:FormPanel runat="server" ID="frmInformation"  BodyPadding="8" Border="false">
                                                    <Items>
                                                        <ext:FormPanel runat="server" ID="frmJobInformation" Title="Job Information" BodyPadding="8"   Border="false">
                                                            <Items>
                                                                <ext:ComboBox runat="server" ID="EmployeeType" LabelWidth="120" FieldLabel="Employee Type" DisplayField="Name" ValueField="Code" Editable="false">
                                                                    <Store>
                                                                        <ext:Store runat="server" ID="store_EmployeeType" AutoLoad="true" OnReadData="store_employeetype_RefreshData" >
                                                                            <Model>
                                                                                <ext:Model ID="Model8" runat="server"  >
                                                                                    <Fields>
                                                                                    <ext:ModelField Name="Code"   />
                                                                                    <ext:ModelField Name="Name"  />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                </ext:ComboBox>
                                                                <ext:ComboBox runat="server" ID="EmployeeClass" LabelWidth="120" FieldLabel="Employee Class" DisplayField="Name" ValueField="Code" Editable="false">
                                                                    <Store>
                                                                        <ext:Store runat="server" ID="store_EmployeeClass" AutoLoad="true" OnReadData="store_employeeclass_RefreshData" >
                                                                            <Model>
                                                                                <ext:Model ID="Model9" runat="server"  >
                                                                                    <Fields>
                                                                                    <ext:ModelField Name="Code"   />
                                                                                    <ext:ModelField Name="Name"  />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                </ext:ComboBox>
                                                                <ext:ComboBox runat="server" ID="IndividualGrade" LabelWidth="120" FieldLabel="Individual Grade" DisplayField="Name" ValueField="Code" Editable="false">
                                                                    <Store>
                                                                        <ext:Store runat="server" ID="store_IndividualGrade" AutoLoad="true" OnReadData="store_individualgrade_RefreshData" >
                                                                            <Model>
                                                                                <ext:Model ID="Model10" runat="server"  >
                                                                                    <Fields>
                                                                                    <ext:ModelField Name="Code"   />
                                                                                    <ext:ModelField Name="Name"  />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                </ext:ComboBox>
                                                                <ext:DateField runat="server" ID="OriginalHireDate" LabelWidth="120" FieldLabel="Original Hire Date" Format="ddd, dd-MMM-yyyy" />
                                                                <ext:DateField runat="server" ID="RehireDate" LabelWidth="120" FieldLabel="RehireDate" Format="ddd, dd-MMM-yyyy" />
                                                                <ext:DateField runat="server" ID="ServiceDate" LabelWidth="120" FieldLabel="Service Date" Format="ddd, dd-MMM-yyyy" />
                                                                <ext:DateField runat="server" ID="LastPromotion" LabelWidth="120" FieldLabel="Last Promotion" Format="ddd, dd-MMM-yyyy" />
                                                            </Items>
                                                        </ext:FormPanel>
                                                        <ext:FormPanel runat="server" ID="frmCompanyDetails" Title="Company Details" BodyPadding="8"   Border="false">
                                                            <Items>
                                                                <ext:TextField runat="server" ID="Vendor"  LabelWidth="120"   FieldLabel="Vendor"   Width="400"   /> 
                                                                <ext:TextField runat="server" ID="WorkAt"  LabelWidth="120"   FieldLabel="Work At"   Width="400"   /> 
                                                                <ext:TextField runat="server" ID="FunctionalDepartment"  LabelWidth="120"   FieldLabel="Functional Department"   Width="400"   /> 
                                                                <ext:ComboBox runat="server" ID="OrgGroup" LabelWidth="120" FieldLabel="Org Group" DisplayField="Name" ValueField="Code" Editable="false">
                                                                    <Store>
                                                                        <ext:Store runat="server" ID="store_OrgGroup" AutoLoad="true" OnReadData="store_orggroup_RefreshData" >
                                                                            <Model>
                                                                                <ext:Model ID="Model11" runat="server"  >
                                                                                    <Fields>
                                                                                    <ext:ModelField Name="Code"   />
                                                                                    <ext:ModelField Name="Name"  />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                </ext:ComboBox>
                                                                <ext:ComboBox runat="server" ID="Section" LabelWidth="120" FieldLabel="Section" DisplayField="Name" ValueField="Code" Editable="false">
                                                                    <Store>
                                                                        <ext:Store runat="server" ID="store_Section" AutoLoad="true" OnReadData="store_section_RefreshData" >
                                                                            <Model>
                                                                                <ext:Model ID="Model12" runat="server"  >
                                                                                    <Fields>
                                                                                    <ext:ModelField Name="Code"   />
                                                                                    <ext:ModelField Name="Name"  />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                </ext:ComboBox>
                                                                <ext:TextField runat="server" ID="Location"  LabelWidth="120"   FieldLabel="Location"   Width="400"   /> 
                                                                <ext:TextField runat="server" ID="Office"  LabelWidth="120"   FieldLabel="Office"   Width="400"   /> 
                                                            </Items>
                                                        </ext:FormPanel>
                                                        <ext:FormPanel runat="server" ID="frmJobGrade" Title="Job Grade" BodyPadding="8"   Border="false">
                                                            <Items>
                                                                <ext:TextField runat="server" ID="JobTittle"  LabelWidth="120"   FieldLabel="Job Tittle"   Width="400"   /> 
                                                                <ext:TextField runat="server" ID="PositionTitle"  LabelWidth="120"   FieldLabel="Position Title"   Width="400"   /> 
                                                                <ext:TextField runat="server" ID="Supervisor"  LabelWidth="120"   FieldLabel="Supervisor"   Width="400"   /> 
                                                                <ext:ComboBox runat="server" ID="CostCenter" LabelWidth="120" FieldLabel="Cost Center" DisplayField="Name" ValueField="Code" Editable="false">
                                                                    <Store>
                                                                        <ext:Store runat="server" ID="store_CostCenter" AutoLoad="true" OnReadData="store_costcenter_RefreshData" >
                                                                            <Model>
                                                                                <ext:Model ID="Model13" runat="server"  >
                                                                                    <Fields>
                                                                                    <ext:ModelField Name="Code"   />
                                                                                    <ext:ModelField Name="Name"  />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                </ext:ComboBox>
                                                            </Items>
                                                        </ext:FormPanel>
                                                        <ext:FormPanel runat="server" ID="frmOtherDetailInformation" Title="Other Detail" BodyPadding="8"   Border="false">
                                                            <Items>
                                                                <ext:ComboBox runat="server" ID="PayGroup" LabelWidth="120" FieldLabel="Pay Group" DisplayField="Name" ValueField="Code" Editable="false">
                                                                    <Store>
                                                                        <ext:Store runat="server" ID="store_PayGroup" AutoLoad="true" OnReadData="store_paygroup_RefreshData" >
                                                                            <Model>
                                                                                <ext:Model ID="Model14" runat="server"  >
                                                                                    <Fields>
                                                                                    <ext:ModelField Name="Code"   />
                                                                                    <ext:ModelField Name="Name"  />
                                                                                    </Fields>
                                                                                </ext:Model>
                                                                            </Model>
                                                                        </ext:Store>
                                                                    </Store>
                                                                </ext:ComboBox>
                                                                <ext:TextField runat="server" ID="BenefitCode"  LabelWidth="120"   FieldLabel="Benefit Code"   Width="400"   /> 
                                                                <ext:TextField runat="server" ID="UnionCode"  LabelWidth="120"   FieldLabel="Union Code"   Width="400"   />
                                                                <ext:TextField runat="server" ID="TaxCode"  LabelWidth="120"   FieldLabel="Tax Code"   Width="400"   />
                                                            </Items>
                                                        </ext:FormPanel>
                                                    </Items>
                                                    <Buttons>
                                                        <ext:Button runat="server" ID="btn_save_job" Height="30" UI="Success" Html="<b>SAVE DATA</b>" >
                                                            <DirectEvents>
                                                                <Click OnEvent="btn_save_job_click">
                                                                    <EventMask ShowMask="true" Msg="Saving Data Please Wait..." />
                                                                </Click>
                                                            </DirectEvents>
                                                        </ext:Button>
                                                        <ext:Button runat="server" ID="btn_close_job" Height="30" UI="Danger"  Html="<b>CLOSE</b>" >
                                                            <DirectEvents>
                                                                <Click OnEvent="btnClose_click" >
                                                                    <EventMask ShowMask="true" Msg="Loading..." />
                                                                </Click>
                                                            </DirectEvents>
                                                        </ext:Button> 
                                                    </Buttons>
                                                </ext:FormPanel>       
                                            </Items>
                                        </ext:Panel>

                                        <%-- Asset  --%>
                                        <ext:Panel ID="Tab4" runat="server" Title="Office / My Asset" BodyPadding="6" Border="false" >
                                            <Items>
                                                <ext:FormPanel runat="server" ID="frmAsset" BodyPadding="2" Border="false">
                                                    <Items>
                                                        <ext:FormPanel ID="AssetOther" runat="server" Title="Other Asset" BodyPadding="8" Border="false">
                                                            <Items>
                                                                <ext:GridPanel runat="server" ID="grdAssetOther" region="Center" Layout="FitLayout" Height="280" EnableColumnHide="false">
                                                                    <Store>
									                                    <ext:Store runat="server" ID="Store_AssetsOther" PageSize="10"  OnReadData="store_AssetsOther_RefreshData" >
								                                            <Model>
									                                            <ext:Model ID="Model15" runat="server" IDProperty="f_pid" Name="data"  >
									                                                <Fields>
                                                                                        <ext:ModelField Name ="f_pid" />
                                                                                        <ext:ModelField Name="employeeid" />
							                                                            <ext:ModelField Name="f_employeename" />
										                                                <ext:ModelField Name="f_assetid" />
										                                                <ext:ModelField Name="f_category" />
                                                                                        <ext:ModelField Name="f_assetname" />
                                                                                        <ext:ModelField Name="f_serialnumber" />
                                                                                        <ext:ModelField Name="f_site" />
                                                                                        <ext:ModelField Name="f_location" />
                                                                                        <ext:ModelField Name="f_ponumber" />
                                                                                        <ext:ModelField Name="f_Responsibility" />
                                                                                        <ext:ModelField Name="f_description" />                                                    
									                                                </Fields>
								                                                </ext:Model>
								                                            </Model>
							                                            </ext:Store>
							                                        </Store>

                                                                    <ColumnModel ID="ColumnModel4"  runat="server"> 
						                                                <Columns>
                                                                            <ext:Column ID="Column13" runat="server"  Text="pid" Hidden="true"   width="40" DataIndex="f_pid">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_pid" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column17" runat="server" Hidden="true" Text="Employee ID" width="100" DataIndex="employeeid">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_employeid" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column22" runat="server"  Text="Employee Name" Hidden="true" width="230" DataIndex="f_employeename">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_employeename" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column26" runat="server"  Text="Asset ID" width="120" DataIndex="f_assetid">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_assetid" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column27" runat="server"  Text="Asset Name" width="200" DataIndex="f_assetname">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_assetname" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column28" runat="server"  Text="Serial Numer" width="120" DataIndex="f_serialnumber">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_serialnumber" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column29" runat="server"  Text="Asset Category" width="120" DataIndex="f_category">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_category" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column30" runat="server"  Text="PO Number" width="120" DataIndex="f_ponumber">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_ponumber" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column32" runat="server"  Text="Site" width="100" DataIndex="f_site">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_site" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column33" runat="server"  Text="Location" width="100" DataIndex="f_location">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_location" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column43" runat="server"  Text="Responsibility" Hidden="true"   width="40" DataIndex="f_Responsibility">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_Responsibility" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column34" runat="server"  Text="Description" width="200" DataIndex="f_description">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_description" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:CommandColumn ID="cmd_delete_AssetOther" runat="server" Width="40" Align="Center" Text="Delete">
                                                                                <Commands>
                                                                                    <ext:GridCommand Icon="Delete" CommandName="Delete"> 
                                                                                        <ToolTip Text="Delete" />
                                                                                    </ext:GridCommand>
                                                                                </Commands>
                                                                                <DirectEvents>
                                                                                    <Command OnEvent="btn_delete_AssetOther">
                                                                                        <Confirmation ConfirmRequest="true" Message="Are you sure delete records ?" Title="Delete Records"  />
                                                                                        <EventMask ShowMask="true" Msg="Delete Data Please Wait..." />
                                                                                        <ExtraParams>
									                                                        <ext:Parameter Name="Value" Value="Ext.encode(#{grdAssetOther}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
								                                                        </ExtraParams>
                                                                                    </Command>
                                                                                </DirectEvents>
                                                                            </ext:CommandColumn>
                                                                        </Columns>
                                                                    </ColumnModel>
                                                                    <DirectEvents>
                                                                        <CellDblClick OnEvent="btn_edit_assetOther_doubleclick" >
                                                                             <EventMask ShowMask="true" />
                                                                                <ExtraParams>
	                                                                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdAssetOther}.getRowsValues({selectedOnly : true}))"
			                                                                                Mode="Raw">
	                                                                                </ext:Parameter>
                                                                                </ExtraParams>
                                                                        </CellDblClick>
                                                                    </DirectEvents>
                                                                </ext:GridPanel>
                                                            </Items>
                                                        </ext:FormPanel>
                                                        <ext:FormPanel ID="AssetSoftWare" runat="server" Title="Software Asset" BodyPadding="8" Border="false">
                                                            <Items>
                                                                <ext:GridPanel runat="server" ID="grdAssetSoftware" Region="Center" Layout="FitLayout" Height="280" EnableColumnHide="false">
                                                                    <Store>
                                                                        <ext:Store runat="server" ID="Store_AssetsSoftware" PageSize="10"  OnReadData="store_AssetsSoftware_RefreshData" >
								                                            <Model>
									                                            <ext:Model ID="Model16" runat="server" IDProperty="f_pidSoftware" Name="data"  >
									                                                <Fields>
                                                                                        <ext:ModelField Name="f_pidSoftware" />
                                                                                        <ext:ModelField Name="employeeidSoftware" />
							                                                            <ext:ModelField Name="f_employeenameSoftware" />
										                                                <ext:ModelField Name="f_assetidSoftware" />
										                                                <ext:ModelField Name="f_categorySoftware" />
										                                                <ext:ModelField Name="f_assetnameSoftware" />
                                                                                        <ext:ModelField Name="f_serialnumberSoftware" />
                                                                                        <ext:ModelField Name="f_siteSoftware" />
                                                                                        <ext:ModelField Name="f_locationSoftware" />
                                                                                        <ext:ModelField Name="f_ponumberSoftware" />
                                                                                        <ext:ModelField Name="f_ResponsibilitySoftware" />
                                                                                        <ext:ModelField Name="f_descriptionSoftware" />                                                    
									                                                </Fields>
								                                                </ext:Model>
								                                            </Model>
							                                            </ext:Store>
                                                                    </Store>
                                                                    <ColumnModel ID="ColumnModel5"  runat="server"> 
						                                                <Columns>
                                                                            <ext:Column ID="Column35" runat="server"  Text="pid" Hidden="true"   width="40" DataIndex="f_pidSoftware">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_pidSoftware" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column36" runat="server" Hidden="true" Text="Employee ID" width="100" DataIndex="employeeidSoftware">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="employeeidSoftware" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column37" runat="server"  Text="Employee Name" Hidden="true" width="230" DataIndex="f_employeenameSoftware">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_employeenameSoftware" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column38" runat="server"  Text="Asset ID" width="120" DataIndex="f_assetidSoftware">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_assetidSoftware" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column39" runat="server"  Text="Asset Name" width="200" DataIndex="f_assetnameSoftware">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_assetnameSoftware" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column40" runat="server"  Text="Serial Numer" width="120" DataIndex="f_serialnumberSoftware">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_serialnumberSoftware" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column41" runat="server"  Text="Asset Category" width="120" DataIndex="f_categorySoftware">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_categorySoftware" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column42" runat="server"  Text="PO Number" width="120" DataIndex="f_ponumberSoftware">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_ponumberSoftware" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column44" runat="server"  Text="Site" width="100" DataIndex="f_siteSoftware">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_siteSoftware" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column45" runat="server"  Text="Location" width="100" DataIndex="f_locationSoftware">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_locationSoftware" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column31" runat="server"  Text="Responsibility" Hidden="true" width="40" DataIndex="f_ResponsibilitySoftware">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_ResponsibilitySoftware" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:Column ID="Column46" runat="server"  Text="Description" width="200" DataIndex="f_descriptionSoftware">
                                                                                    <Editor>
                                                                                        <ext:TextField runat="server" ID="f_descriptionSoftware" />
                                                                                    </Editor>
                                                                            </ext:Column>
                                                                            <ext:CommandColumn ID="cmd_delete_AssetSoftware" runat="server" Width="40"  Align="Center" Text="Delete">
                                                                                <Commands>
                                                                                    <ext:GridCommand Icon="Delete" CommandName="Delete"> 
                                                                                        <ToolTip Text="Delete" />
                                                                                    </ext:GridCommand>
                                                                                </Commands>
                                                                                <DirectEvents>
                                                                                    <Command OnEvent="btn_delete_AssetSoftware">
                                                                                        <Confirmation ConfirmRequest="true" Message="Are you sure delete records ?"  Title="Delete Records"  />
                                                                                        <EventMask ShowMask="true" Msg="Delete Data Please Wait..." />
                                                                                        <ExtraParams>
									                                                        <ext:Parameter Name="Value" Value="Ext.encode(#{grdAssetSoftware}.getRowsValues({selectedOnly : true}))"  Mode="Raw" />
								                                                        </ExtraParams>
                                                                                    </Command>
                                                                                </DirectEvents>
                                                                            </ext:CommandColumn>
                                                                        </Columns>
                                                                    </ColumnModel>
                                                                    <DirectEvents>
                                                                        <CellDblClick OnEvent="btn_edit_assetSoftware_doubleclick" >
                                                                             <EventMask ShowMask="true" />
                                                                                <ExtraParams>
	                                                                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdAssetSoftware}.getRowsValues({selectedOnly : true}))"
			                                                                                Mode="Raw">
	                                                                                </ext:Parameter>
                                                                                </ExtraParams>
                                                                        </CellDblClick>
                                                                    </DirectEvents>
                                                                </ext:GridPanel>
                                                            </Items>
                                                        </ext:FormPanel>
                                                    </Items>
                                                    <Buttons>
                                                        <ext:Button runat="server" ID="btn_AddAssets" Height="30" UI="Info" Html="<b>Add Detail</b>" >
                                                            <DirectEvents>
                                                                <Click OnEvent="btn_AddAssets_click" />
                                                            </DirectEvents>
                                                        </ext:Button>
                                                        <ext:Button runat="server" ID="btnCloseAssets" Height="30" UI="Danger"  Html="<b>Close</b>" >
                                                           <DirectEvents>
                                                               <Click OnEvent="btnClose_click">
                                                                   <EventMask ShowMask="true" Msg="Loading..." />
                                                               </Click>
                                                           </DirectEvents>
                                                       </ext:Button>
                                                    </Buttons>
                                                </ext:FormPanel>
                                            </Items>
                                        </ext:Panel>

                                        <%-- Dependent  --%>
                                        <ext:Panel ID="Tab5"  runat="server"   Title="Dependent"  BodyPadding="6"    Border="false" >
                                            <Items>
                                                <ext:FormPanel runat="server" ID="frmDependent"  BodyPadding="2" Border="false">
                                                    <Items>
                                                        <ext:GridPanel runat="server" ID="grdDepentdent"  region="Center"  Layout="FitLayout" Height="280" EnableColumnHide="false" >
                                                            <Store>
									                            <ext:Store runat="server" ID="store_depentdent" PageSize="10"  OnReadData="store_dependent_RefreshData" >
								                                    <Model>
									                                    <ext:Model ID="Model1" runat="server" IDProperty="f_pid_dep" Name="datadep"  >
									                                        <Fields>
                                                                                <ext:ModelField Name ="f_pid_dep" />
                                                                                <ext:ModelField Name="employeeid" />
							                                                    <ext:ModelField Name="f_dependentid" />
										                                        <ext:ModelField Name="f_dependentname" />
										                                        <ext:ModelField Name="f_relationship" />
										                                        <ext:ModelField Name="f_birthdate" />
                                                                                <ext:ModelField Name ="f_education" />
										                                        <ext:ModelField Name="f_sex" />
                                                                                <ext:ModelField Name="f_eligibleclass" />
                                                                                <ext:ModelField Name="f_remark" />                                                                                                        
									                                        </Fields>
								                                        </ext:Model>
								                                    </Model>
							                                    </ext:Store>
							                                </Store>
                                                            <ColumnModel ID="ColumnModel2"  runat="server"> 
						                                        <Columns>       
                                                                    <ext:Column ID="Column14" runat="server"  Text="pid" Hidden="true"   width="40" DataIndex="f_pid_dep">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="f_pid_dep" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column24" runat="server" Text="Employee ID" Hidden="true"  width="100" DataIndex="employeeid">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="f_employeeid_dep" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column15" runat="server" Text="Dependent ID" width="100" DataIndex="f_dependentid">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="f_dependentid_dep" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column25" runat="server" Text="Dependent Name" width="100" DataIndex="f_dependentname">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="f_dependentname_dep" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column16" runat="server"  Text="Relationship"  width="100" DataIndex="f_relationship">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="f_relationship_dep" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:DateColumn runat="server"  Text="Date Of Birth" Format="ddd, dd-MMM-yyyy" width="120" DataIndex="f_birthdate">
                                                                        <Editor>
                                                                            <ext:DateField runat="server" ID="f_birthdate_dep" Format="dd-MMM-yyyy" />
                                                                        </Editor>
                                                                    </ext:DateColumn>
                                                                    <ext:Column ID="Column18" runat="server"  Text="Formal Education" width="200" DataIndex="f_education">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="f_education_dep" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column19" runat="server"  Text="Sex" width="120" DataIndex="f_sex">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="f_sex_dep" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column20" runat="server"  Text="Eligible Class" width="120" DataIndex="f_eligibleclass">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="f_eligibleclass_dep" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column23" runat="server"  Text="Note" width="100" DataIndex="f_remark">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="f_remark_dep" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40"  Align="Center" Text="Delete">
                                                                        <Commands>
                                                                            <ext:GridCommand Icon="Delete" CommandName="Delete"> 
                                                                                <ToolTip Text="Delete" />
                                                                            </ext:GridCommand>
                                                                        </Commands>
                                                                        <DirectEvents>
                                                                            <Command OnEvent="btn_delete_Dependents">
                                                                                <Confirmation ConfirmRequest="true" Message="Are you sure delete records ?"  Title="Delete Records"  />
                                                                                <EventMask ShowMask="true" Msg="Delete Data Please Wait..." />
                                                                                <ExtraParams>
									                                                <ext:Parameter Name="Value" Value="Ext.encode(#{grdDepentdent}.getRowsValues({selectedOnly : true}))"  Mode="Raw"/>
								                                                </ExtraParams>
                                                                            </Command>
                                                                        </DirectEvents>
                                                                    </ext:CommandColumn>
                                                                </Columns>
                                                            </ColumnModel>
                                                            <DirectEvents>
                                                                <CellDblClick OnEvent="btn_edit_dependen_doubleclick" >
                                                                     <EventMask ShowMask="true" />
                                                                        <ExtraParams>
	                                                                        <ext:Parameter Name="Values" Value="Ext.encode(#{grdDepentdent}.getRowsValues({selectedOnly : true}))"
			                                                                        Mode="Raw">
	                                                                        </ext:Parameter>
                                                                        </ExtraParams>
                                                                </CellDblClick>
                                                            </DirectEvents>
                                                        </ext:GridPanel>
                                                    </Items>
                                                    <Buttons>
                                                        <ext:Button runat="server" ID="btn_AddDependents" Height="30" UI="Info" Html="<b>Add Detail</b>" >
                                                            <DirectEvents>
                                                                <Click OnEvent="btn_addDependents_click" />
                                                            </DirectEvents>
                                                        </ext:Button>
                                                        <ext:Button runat="server" ID="btn_CloseDependents" Height="30" UI="Danger"  Html="<b>Close</b>" >
                                                           <DirectEvents>
                                                               <Click OnEvent="btnClose_click">
                                                                   <EventMask ShowMask="true" Msg="Loading..." />
                                                               </Click>
                                                           </DirectEvents>
                                                       </ext:Button>
                                                    </Buttons>
                                                </ext:FormPanel>
                                            </Items>
                                        </ext:Panel>

                                        <%-- Emegency Contact --%>
                                        <ext:Panel ID="Tab6" runat="server" Title="Emergency Contact" BodyPadding="6" AutoScroll="true"  Border="false"  >
                                            <Items>
                                                <ext:FormPanel runat="server" ID="frmEmergencyContact"  BodyPadding="8" Border="false">
                                                    <Items>
                                                        <ext:TextField runat="server" ID="NameOfContact"  LabelWidth="120"   FieldLabel="Name Of Contact"   Width="400" /> 
                                                        <ext:TextField runat="server" ID="Relationship"  LabelWidth="120"   FieldLabel="Relationship"   Width="400"   /> 
                                                        <ext:TextField runat="server" ID="PhoneNumber"      LabelWidth="120"  FieldLabel="Phone Number" Width="400"   /> 
                                                        <ext:TextField runat="server" ID="MobileNumber" LabelWidth="120"   FieldLabel="Mobile Number" Width="400" /> 
                                                        <ext:TextField runat="server" ID="Email"   LabelWidth="120"   FieldLabel="Email" Width="400"  /> 
                                                    </Items>
                                                    <Buttons>
                                                        <ext:Button runat="server" ID="btn_save_emergency" Height="30" UI="Success" Html="<b>SAVE DATA</b>" >
                                                            <DirectEvents>
                                                                <Click OnEvent="btn_save_emergency_click">
                                                                    <EventMask ShowMask="true" Msg="Loading..." />
                                                                </Click>
                                                            </DirectEvents>
                                                        </ext:Button>
                                                        <ext:Button runat="server" ID="btn_close_emergency" Height="30" UI="Danger"  Html="<b>CLOSE</b>" >
                                                            <DirectEvents>
                                                                <Click OnEvent="btnClose_click">
                                                                    <EventMask ShowMask="true" Msg="Loading..." />
                                                                </Click>
                                                            </DirectEvents>
                                                        </ext:Button> 
                                                    </Buttons>
                                                </ext:FormPanel>
                                            </Items>
                                        </ext:Panel>
                   
                                        <%-- Passport / Visa --%>
                                        <ext:Panel ID="Tab7" runat="server" Title="Passport / Visa" BodyPadding="6" AutoScroll="true"  Border="false"  >
                                            <Items>
                                                <ext:FormPanel runat="server" ID="frmPassport"  BodyPadding="8" Border="false">
                                                    <Items>
                                                        <ext:TextField runat="server" ID="PassportNo"  LabelWidth="150"   FieldLabel="Passport No"   Width="400"  /> 
                                                        <ext:DateField runat="server" ID="PassportDateofIssue" LabelWidth="150" FieldLabel="Passport Date of Issue" Format="ddd, dd-MMM-yyyy" />
                                                        <ext:DateField runat="server" ID="PassportExpirationDate" LabelWidth="150" FieldLabel="Passport Expiration Date" Format="ddd, dd-MMM-yyyy" />
                                                        <ext:DateField runat="server" ID="PassportIssuingOffice" LabelWidth="150" FieldLabel="Passport Issuing Office" Format="ddd, dd-MMM-yyyy"/>
                                                        <ext:TextField runat="server" ID="PassportRegNo"  LabelWidth="150"   FieldLabel="Passport RegNo"   Width="400"  /> 
                                                        <ext:TextField runat="server" ID="VisaNo"  LabelWidth="150"   FieldLabel="Visa No"   Width="400"  /> 
                                                        <ext:TextField runat="server" ID="VisaType"  LabelWidth="150"   FieldLabel="Visa Type"   Width="400"  /> 
                                                        <ext:DateField runat="server" ID="VisaIssueDate" LabelWidth="150" FieldLabel="Visa Issue Date" Format="ddd, dd-MMM-yyyy" />
                                                        <ext:DateField runat="server" ID="VisaExpirationDate" LabelWidth="150" FieldLabel="Visa Expiration Date" Format="ddd, dd-MMM-yyyy"/>
                                                        <ext:DateField runat="server" ID="VisaIssuingOffice" LabelWidth="150" FieldLabel="Visa Issuing Office" Format="ddd, dd-MMM-yyyy"/>
                                                    </Items>
                                                    <Buttons>
                                                        <ext:Button runat="server" ID="btn_save_passport" Height="30" UI="Success" Html="<b>SAVE DATA</b>" >
                                                            <DirectEvents>
                                                                <Click OnEvent="btn_save_passport_click">
                                                                    <EventMask ShowMask="true" Msg="Loading..." />
                                                                 </Click>
                                                            </DirectEvents>
                                                        </ext:Button>
                                                        <ext:Button runat="server" ID="btnClose_Passport" Height="30" UI="Danger"  Html="<b>CLOSE</b>" >
                                                            <DirectEvents>
                                                                <Click OnEvent="btnClose_click">
                                                                    <EventMask ShowMask="true" Msg="Loading..." />
                                                                </Click>
                                                            </DirectEvents>
                                                        </ext:Button> 
                                                    </Buttons>
                                                </ext:FormPanel>
                                            </Items>
                                        </ext:Panel>

                                        <%-- Training --%>
                                        <ext:Panel ID="Panel4" runat="server" Title="Training" BodyPadding="6" AutoScroll="true" Border="false">
                                            <Items>
                                                <ext:FormPanel runat="server" ID="FormPanel3" BodyPadding="8" Border="false">
                                                    <Items>
                                                        <ext:GridPanel runat="server" ID="grdTraining" Region="Center" Layout="FitLayout" Height="280" EnableColumnHide="false">
                                                            <Store>
									                             <ext:Store runat="server" ID="Store_Training" PageSize="10" OnReadData="store_Training_RefreshData" >
								                                    <Model>
									                                    <ext:Model ID="Model2" runat="server" IDProperty="pidTraining" Name="datadep"  >
									                                    <Fields>
                                                                            <ext:ModelField Name="pidTraining" />
                                                                            <ext:ModelField Name="EmployeeIDTraining" />
							                                                <ext:ModelField Name="TrainingType" />
										                                    <ext:ModelField Name="TrainingName" />
										                                    <ext:ModelField Name="TrainingMandatory" />
										                                    <ext:ModelField Name="TrainingLocation" />
                                                                            <ext:ModelField Name="TrainingStartDate" />
										                                    <ext:ModelField Name="TrainingEndDate" />
                                                                            <ext:ModelField Name="TrainingCertificateNo" />
                                                                            <ext:ModelField Name="TrainingProvider" />
                                                                            <ext:ModelField Name="ExpiredDate" />
									                                    </Fields>
								                                    </ext:Model>
								                                    </Model>
							                                    </ext:Store>
							                                 </Store>
                                                            <ColumnModel ID="ColumnModel3"  runat="server">
                                                                <Columns>
                                                                    <ext:Column ID="Column2" runat="server" Text="pid" Hidden="true" width="40" DataIndex="pidTraining">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="pidTraining" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column3" runat="server" Text="EmployeeID" Hidden="true" width="40" DataIndex="EmployeeIDTraining">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="EmployeeIDTraining" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column4" runat="server" Text="Training Type" width="150" DataIndex="TrainingType">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="TrainingType" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column5" runat="server" Text="Training Name" width="250" DataIndex="TrainingName">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="TrainingName" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column6" runat="server" Text="Mandatory" width="150" DataIndex="TrainingMandatory">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="TrainingMandatory" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column7" runat="server" Text="Location" width="150" DataIndex="TrainingLocation">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="TrainingLocation" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:DateColumn ID="Column8" runat="server" Text="Start Date" width="100" DataIndex="TrainingStartDate"
                                                                                    Format="ddd, dd-MMM-yyyy">
                                                                        <Editor>
                                                                            <ext:DateField runat="server" ID="TrainingStartDate" />
                                                                        </Editor>
                                                                    </ext:DateColumn>
                                                                    <ext:DateColumn ID="Column9" runat="server" Text="End Date" width="100" DataIndex="TrainingEndDate"
                                                                                    Format="ddd, dd-MMM-yyyy">
                                                                        <Editor>
                                                                            <ext:DateField runat="server" ID="TrainingEndDate" />
                                                                        </Editor>
                                                                    </ext:DateColumn>
                                                                    <ext:Column ID="Column10" runat="server" Text="Certificate No." width="150" DataIndex="TrainingCertificateNo">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="TrainingCertificateNo" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:Column ID="Column11" runat="server" Text="Provider" width="150" DataIndex="TrainingProvider">
                                                                        <Editor>
                                                                            <ext:TextField runat="server" ID="TrainingProvider" />
                                                                        </Editor>
                                                                    </ext:Column>
                                                                    <ext:DateColumn ID="Column12" runat="server" Text="Expired Date" width="100" DataIndex="ExpiredDate" 
                                                                                    Format="ddd, dd-MMM-yyyy">
                                                                        <Editor>
                                                                            <ext:DateField runat="server" ID="ExpiredDate" />
                                                                        </Editor>
                                                                    </ext:DateColumn>
                                                                    <ext:CommandColumn ID="CommandColumn3" runat="server" Width="40"  Align="Center" Text="Delete">
                                                                        <Commands>
                                                                            <ext:GridCommand Icon="Delete" CommandName="Delete"> 
                                                                                        <ToolTip Text="Delete" />
                                                                            </ext:GridCommand>
                                                                        </Commands>
                                                                        <DirectEvents>
                                                                        <Command OnEvent="btn_delete_Training">
                                                                            <Confirmation ConfirmRequest="true" Message="Are you sure delete records ?"  Title="Delete Records"  />
                                                                            <EventMask ShowMask="true" Msg="Delete Data Please Wait..." />
                                                                                <ExtraParams>
									                                                <ext:Parameter Name="Value" Value="Ext.encode(#{grdTraining}.getRowsValues({selectedOnly : true}))"  Mode="Raw" />
								                                                </ExtraParams>
                                                                        </Command>
                                                                        </DirectEvents>
                                                                    </ext:CommandColumn>
                                                                </Columns>
                                                            </ColumnModel>
                                                            <DirectEvents>
                                                                <CellDblClick OnEvent="btn_edit_training_doubleclick" >
                                                                     <EventMask ShowMask="true" />
                                                                        <ExtraParams>
	                                                                        <ext:Parameter Name="Values" Value="Ext.encode(#{grdTraining}.getRowsValues({selectedOnly : true}))"
			                                                                        Mode="Raw">
	                                                                        </ext:Parameter>
                                                                        </ExtraParams>
                                                                </CellDblClick>
                                                            </DirectEvents>
                                                        </ext:GridPanel>
                                                    </Items>
                                                    <Buttons>
                                                        <ext:Button runat="server" ID="btn_addTraining" Height="30" UI="Info" Html="<b>Add Detail</b>" >
                                                            <DirectEvents>
                                                                <Click OnEvent="btn_addTraining_click" />
                                                            </DirectEvents>
                                                        </ext:Button>
                                                        <ext:Button runat="server" ID="btn_closeTraining" Height="30" UI="Danger"  Html="<b>Close</b>" >
                                                           <DirectEvents>
                                                               <Click OnEvent="btnClose_click">
                                                                   <EventMask ShowMask="true" Msg="Loading..." />
                                                               </Click>
                                                           </DirectEvents>
                                                       </ext:Button>
                                                    </Buttons>
                                                </ext:FormPanel>
                                            </Items>
                                        </ext:Panel>
                                    </Items>
                                </ext:TabPanel>
                            </Items>
                        </ext:Panel>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <%-- PopUp Windows Assets--%>
    <ext:Window runat="server" ID="WindowAssets" Hidden="true" ToFrontOnShow="true" Modal="true" FocusOnToFront="true" Height="285" Width="612" 
                        Resizable="false" Title="Input Assets">
        <Items>
            <ext:FormPanel ID="FormPanel1" runat="server" Width="600" Height="250" Frame="true" Border="true" BodyPadding="13" 
                                    DefaultAnchor="100%" UI="Primary">
                <Items>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%"  Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="pidAssetWind" Hidden="true" />
                            <ext:TextField runat="server" ID="EmployeeIDAssetWindTemp" Hidden="true" />
                            <ext:TextField runat="server" ID="EmployeeIDAssetWind" Disabled="true" FieldLabel="Employee ID" Width="200" LabelWidth="115" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%"  Layout="HBoxLayout">
                        <Items>
                            <ext:ComboBox ID="fFicNumberWind" runat="server" TypeAhead="false" ForceSelection="false" FieldLabel="Asset Name "
                                  Width="500"  HideBaseTrigger="true" MinChars="0" TriggerAction="Query" PageSize="10"  QueryMode="Local" LabelWidth="115"
                                DisplayField="f_assetname" ValueField="ID" EmptyText="Please Type Asset Name .." >
                                <Store>
				                    <ext:Store ID="Store_AssetName" runat="server" AutoLoad="true" OnReadData="Store_AssetName_RefreshData" >
							            <Model>
								            <ext:Model ID="Model17" runat="server" >
									            <Fields>
										            <ext:ModelField Name="ID"  />
										            <ext:ModelField Name="f_assetname"  />
									            </Fields>
								            </ext:Model>
							            </Model>
				                    </ext:Store>
                                </Store>
                                <ListConfig  LoadingText="Searching...">
                                    <ItemTpl runat="server">
                                        <Html>
                                            <div class="search-item">
							                    {ID}</br>
							                    <h3>{Name}</h3>
						                    </div>
                                        </Html>
                                    </ItemTpl>
                                </ListConfig>
                                <DirectEvents>
                                    <Select OnEvent="getDataAssetType_click" />
                                </DirectEvents>
                            </ext:ComboBox>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%"  Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="AssetTypeWindTemp" Hidden="true" />
                            <ext:TextField runat="server" ID="AssetTypeWind" Disabled="true" FieldLabel="Asset Type " LabelWidth="115" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:ComboBox runat="server" ID="AssetSiteWind" FieldLabel="Asset Site " LabelWidth="115" >
                                <Items>
                                    <ext:ListItem Text="JKTA" Value="JKTA" />
                                    <ext:ListItem Text="PAPUA" Value="PAPUA" />
                                </Items>
                            </ext:ComboBox>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:ComboBox runat="server" ID="AssetLocationWind" FieldLabel="Asset Location " LabelWidth="115" >
                                <Items>
                                    <ext:ListItem Text="Pondok Indah" Value="Pondok Indah" />
                                    <ext:ListItem Text="Timika" Value="Timika" />
                                    <ext:ListItem Text="Jakarta - Plaza 89" Value="Jakarta - Plaza 89" />
                                    <ext:ListItem Text="Grasberg - Mile 66" Value="Grasberg - Mile 66" />
                                    <ext:ListItem Text="TPRA" Value="TPRA" />
                                    <ext:ListItem Text="Grasberg - Timika" Value="Grasberg - Timika" />
                                    <ext:ListItem Text="Grasberg - Tembagapura" Value="Grasberg - Tembagapura" />
                                    <ext:ListItem Text="Grasberg" Value="Grasberg" />
                                    <ext:ListItem Text="Grasberg - Kuala Kencana" Value="Grasberg - Kuala Kencana" />
                                    <ext:ListItem Text="Grasberg - Site Pool" Value="Grasberg - Site Pool" />
                                    <ext:ListItem Text="Kuala Kencana " Value="Kuala Kencana " />
                                    <ext:ListItem Text="RidgeCamp " Value="RidgeCamp " />
                                    <ext:ListItem Text="Site Pool" Value="Site Pool" />
                                    <ext:ListItem Text="Jakarta - Pondok Indah Of" Value="Jakarta - Pondok Indah Of" />
                                    <ext:ListItem Text="Grasberg - Lowland" Value="Grasberg - Lowland" />
                                    <ext:ListItem Text="Grasberg - Lowland" Value="Grasberg - Lowland" />
                                    <ext:ListItem Text="Jakarta" Value="Jakarta" />
                                    <ext:ListItem Text="Mile 66 " Value="Mile 66 " />
                                </Items>
                            </ext:ComboBox>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:ComboBox runat="server" ID="AssetResponsibilityWind" FieldLabel="Asset Responsibility " LabelWidth="115" >
                                <Items>
                                    <ext:ListItem Text="Personal Asset" Value="Shared Asset" />
                                    <ext:ListItem Text="Shared Asset" Value="Shared Asset" />
                                </Items>
                            </ext:ComboBox>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="DescriptionWind" FieldLabel="Remarks " LabelWidth="115" Width="500" />
                        </Items>
                    </ext:FieldContainer>
                </Items>
                <Buttons>
                    <ext:Button runat="server" ID="btn_Save_Asset" UI="Success" Text="Save" >
                        <DirectEvents>
                            <Click OnEvent="btn_Save_Asset_click" >
                                <EventMask ShowMask="true" Msg="Save Data..." />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button runat="server" ID="btn_Close_Asset" UI="Default" Text="Close" >
                        <DirectEvents>
                            <Click OnEvent="btn_Close_Windows_click" />
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <%-- PopUp Windows Dependents --%>
    <ext:Window runat="server" ID="WindowDependents" Hidden="true" ToFrontOnShow="true" Modal="true" FocusOnToFront="true" Height="233" Width="613"
                        Title="Input Dependents" Resizable="false" >
        <Items>
            <ext:FormPanel ID="panelDependents" runat="server" Width="600" Height="200" Frame="true" Border="true" BodyPadding="13" 
                                    DefaultAnchor="100%" UI="Primary">
                <Items>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%"  Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="pidDependentsWind" Hidden="true" />
                            <ext:TextField runat="server" ID="EmployeeIDDependentsWind" Hidden="true" />
                            <ext:TextField runat="server" ID="DependentsIDWind" FieldLabel="ID " LabelWidth="110"/>
                            <ext:Label runat="server" Width="20" />
                            <ext:TextField runat="server" ID="DependentsNameWind" FieldLabel="Name " />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="DependentsRelationshipWind" FieldLabel="Relationship " LabelWidth="110" />
                            <ext:Label runat="server" Width="20" />
                            <ext:DateField runat="server" ID="DependentsBirthdayWind" FieldLabel="Birth Date " Format="ddd, dd-MMM-yyyy" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="DependentsEducationWind" FieldLabel="Education" LabelWidth="110" />
                            <ext:Label runat="server" Width="20" />
                            <ext:ComboBox runat="server" ID="DependentsGenderWind" FieldLabel="Gender" >
                                <Items>
                                    <ext:ListItem Text="Male" Value="M" />
                                    <ext:ListItem Text="Female" Value="F" />
                                </Items>
                            </ext:ComboBox>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="DependentsEligibleClassWind" FieldLabel="Eligible Class" LabelWidth="110" />
                            <ext:Label runat="server" Width="20" />
                            <ext:TextField runat="server" ID="DependentsRemarksWind" FieldLabel="Remarks" />
                        </Items>
                    </ext:FieldContainer>
                </Items>
                <Buttons>
                    <ext:Button runat="server" ID="btn_Save_Dependents" UI="Success" Text="Save" >
                        <DirectEvents>
                            <Click OnEvent="btn_Save_Dependents_click" >
                                <EventMask ShowMask="true" Msg="Save Data..." />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button runat="server" ID="btn_Close_Dependents" UI="Default" Text="Close" >
                        <DirectEvents>
                            <Click OnEvent="btn_Close_Windows_click" />
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>

    <%-- PopUp Windows Training--%>
    <ext:Window runat="server" ID="WindowTraining" Hidden="true" ToFrontOnShow="true" Modal="true" FocusOnToFront="true" Height="233" Width="613"
                        Title="Input Training" Resizable="false">
        <Items>
            <ext:FormPanel ID="panelTraining" runat="server" Width="600" Height="200" Frame="true" Border="true" BodyPadding="13" 
                                    DefaultAnchor="100%" UI="Primary">
                <Items>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%"  Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="pidTrainingWind" Hidden="true" />
                            <ext:TextField runat="server" ID="EmployeeIDTrainingWind" Hidden="true" />
                            <ext:ComboBox runat="server" ID="TrainingTypeWind" FieldLabel="Training Type " LabelWidth="110" >
                                <Items>
                                    <ext:ListItem Text="Test" Value="Test" />
                                    <ext:ListItem Text="Coba Lagi" Value="CobaLagi" />
                                    <ext:ListItem Text="Hanya Coba" Value="HanyaCoba" />
                                </Items>
                            </ext:ComboBox>
                            <ext:Label runat="server" Width="20" />
                            <ext:TextField runat="server" ID="TrainingNameWind" FieldLabel="Training Name " />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="TrainingMandatoryWind" FieldLabel="Mandatory " LabelWidth="110" />
                            <ext:Label runat="server" Width="20" />
                            <ext:TextField runat="server" ID="TrainingLocationWind" FieldLabel="Location "/>
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:DateField runat="server" ID="TrainingStartDateWind" FieldLabel="Start Date " LabelWidth="110" Format="ddd, dd-MMM-yyyy" />
                            <ext:Label runat="server" Width="20" />
                            <ext:DateField runat="server" ID="TrainingEndDateWind" FieldLabel="End Date " Format="ddd, dd-MMM-yyyy" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField runat="server" ID="TrainingCertificateWind" FieldLabel="Certificate Number " LabelWidth="110" />
                            <ext:Label runat="server" Width="20" />
                            <ext:TextField runat="server" ID="TrainingProviderWind" FieldLabel="Training Provider" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:DateField runat="server" ID="ExpiredDateWind" FieldLabel="Expired Date" LabelWidth="110" Format="ddd, dd-MMM-yyyy" />
                        </Items>
                    </ext:FieldContainer>
                </Items>
                <Buttons>
                    <ext:Button runat="server" ID="btn_Save_Training" UI="Success" Text="Save" >
                        <DirectEvents>
                            <Click OnEvent="btn_Save_Training_click" >
                                <EventMask ShowMask="true" Msg="Save Data..." />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button runat="server" ID="btn_Close_Training" UI="Default" Text="Close" >
                        <DirectEvents>
                            <Click OnEvent="btn_Close_Windows_click" />
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>
</asp:Content>
