﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="mailbagtracking_form.aspx.cs" Inherits="NewERDM.Admin.mailbag.mailbagtracking_form" %>
<asp:Content ID="Header" runat="server" ContentPlaceHolderID="head">
    
     <ext:XScript ID="XScript1" runat="server">
	    <script>
	        var addmaster = function () {
	            var grid = #{grdDetail};
	            grid.editingPlugin.cancelEdit();

	            // Create a record instance through the ModelManager
	            var r = Ext.ModelManager.create({
	                MailBagID       : '',
	                DatePickup      : '',
	                SendBy          : '',
	                Description     : '',
	                Address         : '',
	                Colly           : '',
	                Weight          : '',
	                ReceivedBy      : ''
	            }, 'data');
	            grid.store.insert(0,r)
	            grd.editingPlugin.startEdit(0,0)
	            //grid.store.insert(0, r);
	            //grid.editingPlugin.startEdit(0, 0);
	        };
	    </script>
    </ext:XScript>
    <style>
	  .my-scale.x-btn-default-Small-icon-text-left .x-btn-icon {
			width: 16px;
		}
	   .add32 {
			background-image : url(img/Word-icon.png) !important;
		}   
         .kanan{text-align:right;}
        .Fkanan
         {
            text-align:right;
            border:none;
            background-image: none;
        }
        .my-panel .x-panel-header {
			background-color: #dfe8f6 !important;
			 
		}
 
		.my-panel .x-panel-body {
			border: none;
			background-color: #dfe8f6 !important;
		}

         
         .search-item {
            font          : normal 11px tahoma, arial, helvetica, sans-serif;
            padding       : 3px 10px 3px 10px;
            border        : 1px solid #fff;
            border-bottom : 1px solid #eeeeee;
            white-space   : normal;
            color         : #555;
        }
        
        .search-item h3 {
            display     : block;
            font        : inherit;
            font-weight : bold;
            color       : #222;
            margin      :0px;
        }

        .search-item h3 span {
            float       : right;
            font-weight : normal;
            margin      : 0 0 5px 5px;
            width       : 100px;
            display     : block;
            clear       : none;
        } 
        
        p { width: 650px; }
        
        .ext-ie .x-form-text { position : static !important; }
 	</style>

</asp:Content>

<asp:Content ID="Detail" runat="server" ContentPlaceHolderID="MainContent">
<div class="row">  
    <div class="col-md-12">
        <ext:FormPanel runat="server" ID="frmverified" Title ="" BodyPadding="12" Header="false" Frame="false" Border="false" DisabledCls="true" >
            <Items>
                <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                    <Items>
                        <ext:TextField runat="server" ID="IDMailBag" FieldLabel="No " LabelWidth="80" Width="220"/>
                        <ext:TextField runat="server" ID="ShipTo" FieldLabel="Ship To " Margins="0 0 0 62" LabelWidth="50" Width="250" />
                    </Items>
                </ext:FieldContainer>
                <ext:FieldContainer runat="server"  AnchorHorizontal="100%"  Layout="HBoxLayout">
                    <Items>
                        <ext:DateField runat="server" ID="MailBagDate" FieldLabel="Date " Width="220" LabelWidth="80" Editable="false" Format="ddd, dd-MMM-yyyy" />
                        <ext:TextField runat="server" ID="remark" FieldLabel="Remark " Margins="0 0 0 62" LabelWidth="50" Width="400" />
                    </Items>
                </ext:FieldContainer>
                <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                    <Items>
                        <ext:ComboBox ID="cmd_typeMail" runat="server" FieldLabel="MailBag Type" LabelWidth="80" Width="255" Text="Internal">
						    <Items>
							    <ext:ListItem Text="Internal"  Value="1" />
								<ext:ListItem Text="External"  Value="2"/>
							</Items>
						</ext:ComboBox>
                        <ext:TextField ID="iduserby" runat="server" Hidden="true" />
                    </Items>
                </ext:FieldContainer>
                <ext:GridPanel ID="grdDetail" runat="server" TabIndex="7" region="Center" Layout="FitLayout" Height="350" Padding="8" EnableColumnHide="false">
                    <Store>
                        <ext:Store runat="server" ID="Store_Detail" PageSize="20" OnReadData="Store_Detail_refreshData">
                            <Model>
                                <ext:Model ID="Model1" runat="server" IDProperty="mailbagid" Name="data">
                                    <Fields>
                                        <ext:ModelField Name="MailBagID" />
                                        <ext:ModelField Name="DatePickup" />
                                        <ext:ModelField Name="SendBy" />
                                        <ext:ModelField Name="Description" />
                                        <ext:ModelField Name="Address" />
                                        <ext:ModelField Name="Colli" />
                                        <ext:ModelField Name="Weight" />
                                        <ext:ModelField Name="ReceivedBy" />
                                        <ext:ModelField Name="RemarksDetail" />
                                        <ext:ModelField Name="statusReceived" />
                                        <ext:ModelField Name="dateReceived" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:Column ID="Column1" runat="server" Text="ID" Hidden="true" width="80" DataIndex="MailBagID">
                                <Editor>
                                    <ext:TextField runat="server" ID="MailBagID" />
                                </Editor>
                            </ext:Column>
                            <ext:DateColumn ID="Column2" runat="server" Text="Date Pickup" width="110" DataIndex="DatePickup" Format="ddd, dd-MMM-yyyy">
                                <Editor>
                                    <ext:DateField runat="server" ID="DatePickup" />
                                </Editor>
                            </ext:DateColumn>
                            <ext:Column ID="Column3" runat="server"  Text="Send By" width="180" DataIndex="SendBy" AutoDataBind="true">
                                <Editor>
                                    <ext:ComboBox ID="SendBy" runat="server" TypeAhead="false" ForceSelection="false" Width="200" HideBaseTrigger="true" 
                                                MinChars="0" TriggerAction="Query" PageSize="10"  QueryMode="Local" DisplayField="Name" ValueField="ID">
                                        <Store>
				                            <ext:Store ID="Store_SendBy" runat="server" OnReadData="store_Sendby_RefreshData">
							                    <Model>
								                    <ext:Model ID="Model2" runat="server" IDProperty="Name">
									                    <Fields>
										                    <ext:ModelField Name="ID" />
										                    <ext:ModelField Name="Name" />
									                    </Fields>
								                    </ext:Model>
							                    </Model>
				                            </ext:Store>
                                        </Store>
                                        <ListConfig  LoadingText="Searching...">
                                            <ItemTpl runat="server">
                                                <Html>
                                                    <div class="search-item">
                                                        {ID}</br>
							                            <h3>{Name}</h3>
						                            </div>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
<%--                                        <DirectEvents>
                                            <Select OnEvent="EmailSendBy_Click">
                                                <ExtraParams>
						                            <ext:Parameter Name="Values" Value="Ext.encode(#{grdDetail}.getRowsValues({selectedOnly : false}))" Mode="Raw" />
						                        </ExtraParams>
                                            </Select>
                                        </DirectEvents>--%>
                                    </ext:ComboBox>
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column5" runat="server" Text="Description" width="230" DataIndex="Description" AutoDataBind="true">
                                <Editor>
                                    <ext:TextField runat="server" ID="Description" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column6" runat="server"  Text="Address / Destination" width="230" DataIndex="Address" AutoDataBind="true">
                                <Editor>
                                    <ext:TextField runat="server" ID="Address" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column7" runat="server"  Text="Colly" width="35" DataIndex="Colli" AutoDataBind="true">
                                <Editor>
                                    <ext:TextField runat="server" ID="Colli" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column8" runat="server"  Text="Weight" width="50" DataIndex="Weight" AutoDataBind="true">
                                <Editor>
                                    <ext:TextField runat="server" ID="Weight" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column9" runat="server"  Text="Received By" width="180" DataIndex="ReceivedBy" AutoDataBind="true">
                                <Editor>
                                    <ext:ComboBox ID="ReceivedBy" runat="server" TypeAhead="false" ForceSelection="false" Width="200" HideBaseTrigger="true" 
                                                MinChars="0" TriggerAction="Query" PageSize="10"  QueryMode="Local" DisplayField="Name" ValueField="ID">
                                        <Store>
				                            <ext:Store ID="Store_ReceivedBy" runat="server" OnReadData="store_Receivedby_RefreshData">
							                    <Model>
								                    <ext:Model ID="Model3" runat="server" IDProperty="Name">
									                    <Fields>
										                    <ext:ModelField Name="ID" />
										                    <ext:ModelField Name="Name" />
									                    </Fields>
								                    </ext:Model>
							                    </Model>
				                            </ext:Store>
                                        </Store>
                                        <ListConfig  LoadingText="Searching...">
                                            <ItemTpl runat="server">
                                                <Html>
                                                    <div class="search-item">
                                                        {ID}</br>
							                            <h3>{Name}</h3>
						                            </div>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
  <%--                                      <DirectEvents>
                                            <Select OnEvent="EmailReceivedBy_Click">
                                                <ExtraParams>
						                            <ext:Parameter Name="Values" Value="Ext.encode(#{grdDetail}.getRowsValues({selectedOnly : false}))" Mode="Raw" />
						                        </ExtraParams>
                                            </Select>
                                        </DirectEvents>--%>
                                    </ext:ComboBox>
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column10" runat="server" Text="Remark" DataIndex="RemarksDetail" Width="100" AutoDataBind="true" >
                                <Editor>
                                    <ext:TextField runat="server" ID="RemarksDetail" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column11" runat="server"  Text="Status" width="80" Hidden="true" DataIndex="statusReceived" AutoDataBind="true">
                                <Editor>
                                    <ext:TextField runat="server" ID="statusReceived" />
                                </Editor>
                            </ext:Column>
                            <ext:DateColumn ID="Column12" runat="server"  Text="Date Received" width="110" Hidden="true" DataIndex="dateReceived" AutoDataBind="true" Format="ddd, dd-MMM-yyyy">
                                <Editor>
                                    <ext:TextField runat="server" ID="dateReceived" />
                                </Editor>
                            </ext:DateColumn>
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:CellEditing runat="server" ClicksToEdit="1" />
                    </Plugins>
                </ext:GridPanel>
            </Items>
            <Buttons>
                <ext:Button ID="btn_Detail" runat="server" UI="Success" Text="Add Detail" Height="30">
                    <Listeners>
                        <Click Fn="addmaster" />
                    </Listeners>
                </ext:Button>
                <ext:Button runat="server" ID="btn_submit" UI="Primary" Height="30" Text="<b>Submit</b>">
                    <DirectEvents>
                        <Click OnEvent="btn_submit_click" >
                            <EventMask ShowMask="true" Msg="Save data please wait..." />
                             <ExtraParams>
						        <ext:Parameter Name="Values" Value="Ext.encode(#{grdDetail}.getRowsValues({selectedOnly : false}))" Mode="Raw" />
						    </ExtraParams>
                        </Click>
                    </DirectEvents>
                </ext:Button>
                <ext:Button runat="server" ID="btn_close"  UI="Danger" Height="30" Text="<b>Close</b>">
                    <DirectEvents>
                        <Click OnEvent="btn_close_click" >
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
                </ext:Button>
            </Buttons>
        </ext:FormPanel>
    </div>
</div>
</asp:Content>