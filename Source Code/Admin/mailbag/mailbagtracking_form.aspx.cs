﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using NewERDM.lib.BusinessClass;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using SpreadsheetLight;

namespace NewERDM.Admin.mailbag
{
    public partial class mailbagtracking_form : System.Web.UI.Page
    {
        #region field

        UserApplication userApp;
        UserApplicationBL usrApplicationBL = new UserApplicationBL();

        #endregion

        #region Construction

        protected void Store_Detail_refreshData(object sender, StoreReadDataEventArgs e)
        {
            getDetailMailBag();
        }
        protected void store_Sendby_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeSendby();
        }
        protected void store_Receivedby_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeReceivedby();
        }

        #endregion Construction
        
        #region Method

        protected void getEmployeeSendby()
        {
            DataTable dt = new DataTable();
            var objgetEmployeeSendby = new clsMailBag();
            dt = objgetEmployeeSendby.get_EmployeeSendby();
            
            Store_SendBy.DataSource = dt;
            Store_SendBy.DataBind();
        }
        //protected void getEmployeeSendby_update(string idEmployeeUpdate)
        //{
        //    DataTable dt = new DataTable();
        //    var objgetEmployeeSendby = new clsMailBag();
        //    dt = objgetEmployeeSendby.get_EmployeeSendby_Update(idEmployeeUpdate);

        //    if (dt.Rows.Count > 0)
        //    {
        //        EmailSend.Text = dt.Rows[0]["EmailSend"].ToString();

        //        getEmployeeSendby();
        //    }

        //    //getEmployeeSendby();

        //}
        protected void getEmployeeReceivedby()
        {
            DataTable dt = new DataTable();
            var objgetEmployeeSendby = new clsMailBag();
            dt = objgetEmployeeSendby.get_EmployeeReceivedby();

            Store_ReceivedBy.DataSource = dt;
            Store_ReceivedBy.DataBind();
        }
        protected void getMailBagDetail_Update(string strMailBagID)
        {
            DataTable dt = null;
            var objget = new clsMailBag();
            dt = objget.get_mailBag_Detail_Update(strMailBagID);

            if (dt.Rows.Count > 0)
            {
                this.IDMailBag.Text = dt.Rows[0]["MailBagID"].ToString();
                this.ShipTo.Text = dt.Rows[0]["ShipTo"].ToString();
                this.MailBagDate.Text = dt.Rows[0]["dateMailbag"].ToString();
                this.remark.Text = dt.Rows[0]["Remark"].ToString();
                this.cmd_typeMail.Text = dt.Rows[0]["MailbagType"].ToString();
                this.MailBagID.Text = dt.Rows[0]["MailBagID"].ToString();
                this.DatePickup.Text = dt.Rows[0]["DatePickup"].ToString();
                this.SendBy.Text = dt.Rows[0]["SendBy"].ToString();
                this.Description.Text = dt.Rows[0]["Description"].ToString();
                this.Address.Text = dt.Rows[0]["Address"].ToString();
                this.Colli.Text = dt.Rows[0]["Colli"].ToString();
                this.Weight.Text = dt.Rows[0]["Weight"].ToString();
                this.ReceivedBy.Text = dt.Rows[0]["ReceivedBy"].ToString();
                this.statusReceived.Text = dt.Rows[0]["statusReceived"].ToString();
                this.dateReceived.Text = dt.Rows[0]["dateReceived"].ToString();
            }
        }
        protected void getDetailMailBag()
        {
            DataTable dt = null;
            var objget = new clsMailBag();
            dt = objget.get_MailBag_detail(this.IDMailBag.Text.Trim());
            Store_Detail.DataSource = dt;
            Store_Detail.DataBind();

            getEmployeeSendby();
            getEmployeeReceivedby();
        }

        //protected void getEmployeeReceivedby_update(string idEmployeeReceivedUpdate)
        //{
        //    DataTable dt = new DataTable();
        //    var objgetEmployeeSendby = new clsMailBag();
        //    dt = objgetEmployeeSendby.get_EmployeeReceivedby_Update(idEmployeeReceivedUpdate);

        //    if (dt.Rows.Count > 0)
        //    {
        //        EmailSend.Text = dt.Rows[0]["EmailReceived"].ToString();
        //    }

        //    getEmployeeReceivedby();

        //}


        //protected void getMailBagEmailSendby()
        //{
        //    DataTable dt = new DataTable();
        //    var objget = new clsMailBag();
        //    dt = objget.get_MailSendByDetail();

        //    Store_SendBy.DataSource = dt;
        //    Store_SendBy.DataBind();

        //}
        
        //protected void getMailBagEmailSendby2(string strIDEmployeeSen)
        //{
        //    DataTable dt = new DataTable();
        //    var objget = new clsMailBag();
        //    dt = objget.get_MailSendByDetail2(strIDEmployeeSen);

        //    if(dt.Rows.Count >0)
        //    {
        //        EmailSend.Text = dt.Rows[0]["Email"].ToString();
        //    }
        //}

        //protected void getMailBagEmailReceivedby()
        //{
        //    DataTable dt = new DataTable();
        //    var objget = new clsMailBag();
        //    dt = objget.get_MailReceivedDetail();

        //    Store_ReceivedBy.DataSource = dt;
        //    Store_ReceivedBy.DataBind();
        //}

        //protected void getMailBagEmailReceivedby2(string strIDEmployeeRec)
        //{
        //    DataTable dt = new DataTable();
        //    var objget = new clsMailBag();
        //    dt = objget.get_MailReceivedDetail2(strIDEmployeeRec);

        //    if (dt.Rows.Count > 0)
        //    {
        //        EmailReceived.Text = dt.Rows[0]["Email"].ToString();
        //    }
        //}

        #endregion Method


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                userApp = (UserApplication)Session["ses_user"];

                this.MailBagDate.Value = DateTime.Today;

                if (userApp == null)
                {

                }
                else
                {
                    //this.userby.Text = String.Format("{0}", userApp.UserName);
                    //this.iduserby.Text = String.Format("{0}", userApp.UserID);

                    getEmployeeSendby();
                    getEmployeeReceivedby();

                    string queryString_pid = string.Empty;
                    queryString_pid = Request.QueryString["MailBagID"];
                    if (!string.IsNullOrEmpty(queryString_pid))
                    {
                        this.btn_submit.Visible = false;
                        this.btn_Detail.Visible = false;
                        this.Column10.Hidden = false;
                        this.Column11.Hidden = false;
                        getMailBagDetail_Update(queryString_pid);
                        getDetailMailBag();
                    }
                    //getMailBagEmailSendby();
                    //getMailBagEmailReceivedby();
                    //getMailBagEmailSendby2(this.EmailSend.Text);
                    //getMailBagEmailReceivedby2(this.EmailReceived.Text);
                }
            }
        }
        protected void btn_close_click(object sender, DirectEventArgs e)
        {
            this.grdDetail.ClearContent();
            X.Redirect(CommonUtility.ResolveUrl("Admin/mailbag/MailBagSearch_form.aspx"), "Loading ... ");
        }
        protected void btn_submit_click(object sender, DirectEventArgs e)
        {
            userApp = (UserApplication)Session["ses_user"];
            if (userApp == null)
            {

            }
            else
            {
                this.iduserby.Text = String.Format("{0}", userApp.UserID);

                if (string.IsNullOrEmpty(this.IDMailBag.Text))
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Warning",
                        Message = "MailBag ID can't be empty !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    return;
                }
                if (string.IsNullOrEmpty(this.ShipTo.Text))
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Warning",
                        Message = "Ship to can't be empty !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    return;
                }
                if (string.IsNullOrEmpty(this.MailBagDate.Text))
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Warning",
                        Message = "date can't be empty !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    return;
                }

                DataTable getIDMailBag = new DataTable();
                var objGetIDMailBag = new clsMailBag();
                getIDMailBag = objGetIDMailBag.get_doubleIDMail(this.IDMailBag.Text);

                if (getIDMailBag.Rows.Count > 0)
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Warning",
                        Message = "MailBagID already exist !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    return;

                }
                else
                {
                    string jsonValues1 = e.ExtraParams["Values"];
                    List<Dictionary<string, string>> records1 = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues1);

                    if (records1.Count == 0)
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "Please selected row !",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                        return;
                    }
                }

                string jsonValues = e.ExtraParams["Values"];
                List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);

                foreach (var record in records)
                {
                    if (string.IsNullOrEmpty(record["DatePickup"].ToString()))
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "Warning",
                            Message = "Date Pickup can't be empty !",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                        return;
                    }
                    if (string.IsNullOrEmpty(record["SendBy"].ToString()))
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "Warning",
                            Message = "Sender can't be empty !",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                        return;
                    }
                    if (string.IsNullOrEmpty(record["Description"].ToString()))
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "Warning",
                            Message = "Description can't be empty !",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                        return;
                    }
                    if (string.IsNullOrEmpty(record["Address"].ToString()))
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "Warning",
                            Message = "Address can't be empty !",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                        return;
                    }
                    if (string.IsNullOrEmpty(record["ReceivedBy"].ToString()))
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "Warning",
                            Message = "Received can't be empty !",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                        return;
                    }

                    if (string.IsNullOrEmpty(record["Colli"].ToString()))
                    {
                        Convert.ToDecimal(record["Colli"] ="0");
                    }
                    if (string.IsNullOrEmpty(record["Weight"].ToString()))
                    {
                        Convert.ToDecimal(record["Weight"] = "0");
                    }

                    DataTable getIDMailBag1 = new DataTable();
                    var objGetIDMailBag1 = new clsMailBag();
                    getIDMailBag1 = objGetIDMailBag1.get_doubleIDMail(this.IDMailBag.Text);

                    if (getIDMailBag1.Rows.Count == 0)
                    {
                        var objInsertMailBagHeader = new clsMailBag();
                        objInsertMailBagHeader.InsertMailBagHeader(this.IDMailBag.Text.Trim(), Convert.ToDateTime(this.MailBagDate.Text), this.cmd_typeMail.Text.Trim(),
                            this.remark.Text.Trim(), this.ShipTo.Text, this.iduserby.Text.Trim());
                    }
                    else
                    { }

                    var objInsertMailBagDetail = new clsMailBag();
                    objInsertMailBagDetail.InsertMailBagDetail(this.IDMailBag.Text.Trim(), Convert.ToDateTime(record["DatePickup"]), record["SendBy"], record["Description"], record["Address"],
                                                                   Convert.ToDecimal(record["Colli"]), Convert.ToDecimal(record["Weight"]), record["ReceivedBy"], record["RemarksDetail"],
                                                                   Convert.ToDateTime(record["DatePickup"]), "0");
                    //Query Email
                    string emailSender;
                    string emailReceived;

                    //string gabEmail;
                    DataTable getEmailAddress = new DataTable();
                    var objGetEmailAddress = new clsMailBag();
                    getEmailAddress = objGetEmailAddress.get_CheckEmail(record["SendBy"], record["ReceivedBy"]);
                    if (getEmailAddress.Rows.Count > 0)
                    {
                        emailSender = getEmailAddress.Rows[0]["SenderEmail"].ToString();
                        emailReceived = getEmailAddress.Rows[0]["ReceivedEmail"].ToString();

                        //gabEmail = emailSender + ";" + emailReceived;
                        //strUserAddresstoSend = strUserAddresstoSend + ";" + record["receivedby"];

                        // Kirim Mail Sender
                        Mailer.SendEmail(emailSender, "ERDM", "Mailbag tracking #" + this.IDMailBag.Text.Trim(), @"This email was generated by Eksplorasi Nusa Jaya - Mailbag tracking system, <br><br><br>
                                        We have sent your order goods through PTFI-Mailbag, please wait for about 2 days
                                        and please check to the administration office within the next two days so
                                        the goods that you ordered can be received by your own. <br> <br>
                                        please give tick marks on the option Accepted  or Not Accepted and click Submit button,
                                        so we can track and record your ordered by this system <br> <br> --------- <br> <br>
                                        kami telah mengirim barang pesanan anda melalui PTFI-Mailbag,
                                        mohon menunggu kurang lebih 2 hari dan mohon di cek ke kantor administrasi dalam
                                        2 hari mendatang agar barang yang anda pesan dapat diterima oleh anda sendiri. <br> <br>
                                        mohon berikan tanda tick pada option ‘Accept / Reject’ dan klik tombol ‘Submit’
                                        agar kami bisa melacak dan menyimpan data barang pesanan anda ke dalam sistim ini. <br> <br> Description Goods : " + record["Description"].ToString(), null, false);

                        // Kirim Mail Received
                        Mailer.SendEmail(emailReceived, "ERDM", "Mailbag tracking #" + this.IDMailBag.Text.Trim(), @"This email was generated by Eksplorasi Nusa Jaya - Mailbag tracking system, <br><br><br>
                                        We have sent your order goods through PTFI-Mailbag, please wait for about 2 days
                                        and please check to the administration office within the next two days so
                                        the goods that you ordered can be received by your own. <br> <br>
                                        please give tick marks on the option Accepted  or Not Accepted and click Submit button,
                                        so we can track and record your ordered by this system <br> <br> --------- <br> <br>
                                        kami telah mengirim barang pesanan anda melalui PTFI-Mailbag,
                                        mohon menunggu kurang lebih 2 hari dan mohon di cek ke kantor administrasi dalam
                                        2 hari mendatang agar barang yang anda pesan dapat diterima oleh anda sendiri. <br> <br>
                                        mohon berikan tanda tick pada option ‘Accept / Reject’ dan klik tombol ‘Submit’
                                        agar kami bisa melacak dan menyimpan data barang pesanan anda ke dalam sistim ini. <br> <br> Description Goods : " + record["Description"].ToString(), null, false);
                    }

                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Information",
                        Message = "Save Data Success",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.INFO
                    });
                }
                this.ShipTo.Text = string.Empty;
                this.remark.Text = string.Empty;
                this.IDMailBag.Text = string.Empty;
                this.MailBagDate.Value = DateTime.Today;

                this.grdDetail.ClearContent();
            }
        }

        //protected void EmailSendBy_Click(object sender, DirectEventArgs e)
        //{
        //    string jsonValues = e.ExtraParams["Values"];
        //    List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);
            
        //    foreach (var record in records)
        //    {

        //        DataTable dt = new DataTable();
        //        var objget = new clsMailBag();
        //        dt = objget.get_MailSendByDetail2(record["SendBy"]);
        //        Store_SendBy.DataSource = dt;

        //        if (dt.Rows.Count > 0)
        //        {
        //            record["EmailSend"] = dt.Rows[0]["Email"].ToString();
        //            //record["receivedbyID"] = dt.Rows[0]["ID"].ToString();
        //            //this.receivedemail.Text = dt.Rows[0]["Email"].ToString();
        //            //this.receivedbyID.Text = dt.Rows[0]["ID"].ToString();

        //            getMailBagEmailSendby2(record["SendBy"]);
        //        }
        //    }
        //}
        //protected void EmailReceivedBy_Click(object sender, DirectEventArgs e)
        //{
        //    string jsonValues = e.ExtraParams["Values"];
        //    List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);

        //    foreach (var record in records)
        //    {

        //        DataTable dt = new DataTable();
        //        var objget = new clsMailBag();
        //        dt = objget.get_MailReceivedDetail2(record["ReceivedBy"]);
        //        Store_SendBy.DataSource = dt;

        //        if (dt.Rows.Count > 0)
        //        {
        //            record["EmailReceived"] = dt.Rows[0]["Email"].ToString();
        //            //record["receivedbyID"] = dt.Rows[0]["ID"].ToString();
        //            //this.receivedemail.Text = dt.Rows[0]["Email"].ToString();
        //            //this.receivedbyID.Text = dt.Rows[0]["ID"].ToString();

        //            getMailBagEmailReceivedby2(record["ReceivedBy"]);
        //        }
        //    }
        //}
    }
}