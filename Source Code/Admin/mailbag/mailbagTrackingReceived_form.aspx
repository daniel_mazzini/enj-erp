﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="mailbagTrackingReceived_form.aspx.cs" Inherits="NewERDM.Admin.mailbag.mailbagTrackingReceived_form" %>
<asp:Content ID="Header" runat="server" ContentPlaceHolderID="head">

         <ext:XScript ID="XScript1" runat="server">
	    <script>
	        var addmaster = function () {
	            var grid = #{grdDetail};
	            grid.editingPlugin.cancelEdit();

	            // Create a record instance through the ModelManager
	            var r = Ext.ModelManager.create({
	                mailbagid       : '',
	                datepickup      : '',
	                description     : '',
	                address         : '',
	                colli           : '',
	                weight          : '',
	                receivedby      : ''
	                //receiveddate    : '',
	                //status          : ''
	            }, 'data');
	            grid.store.insert(0,r)
                grd.editingPlugin.startEdit(0,0)
	            //grid.store.insert(0, r);
	            //grid.editingPlugin.startEdit(0, 0);
	        };
	    </script>
    </ext:XScript>
        <style>
	  .my-scale.x-btn-default-Small-icon-text-left .x-btn-icon {
			width: 16px;
		}
	   .add32 {
			background-image : url(img/Word-icon.png) !important;
		}   
         .kanan{text-align:right;}
        .Fkanan
         {
            text-align:right;
            border:none;
            background-image: none;
        }
        .my-panel .x-panel-header {
			background-color: #dfe8f6 !important;
			 
		}
 
		.my-panel .x-panel-body {
			border: none;
			background-color: #dfe8f6 !important;
		}

         
         .search-item {
            font          : normal 11px tahoma, arial, helvetica, sans-serif;
            padding       : 3px 10px 3px 10px;
            border        : 1px solid #fff;
            border-bottom : 1px solid #eeeeee;
            white-space   : normal;
            color         : #555;
        }
        
        .search-item h3 {
            display     : block;
            font        : inherit;
            font-weight : bold;
            color       : #222;
            margin      :0px;
        }

        .search-item h3 span {
            float       : right;
            font-weight : normal;
            margin      : 0 0 5px 5px;
            width       : 100px;
            display     : block;
            clear       : none;
        } 
        
        p { width: 650px; }
        
        .ext-ie .x-form-text { position : static !important; }
 	</style>

</asp:Content>
<asp:Content ID="Detail" runat="server" ContentPlaceHolderID="MainContent">
    <div class="row">  
    <div class="col-md-12">
        <ext:FormPanel runat="server" ID="frmverified" Title ="" BodyPadding="12" Header="false" Frame="false" Border="false" DisabledCls="true" >
            <Items>
                <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                    <Items>
                        <ext:TextField runat="server" ID="ReceivedBy" width="350" FieldLabel="Received By " ReadOnly="true" Enabled="false"/>
                        <ext:TextField runat="server" ID="iduserby" Hidden="true" />
                        <ext:TextField runat="server" ID="IDEmployee" Hidden="true" />
                    </Items>
                </ext:FieldContainer>
                <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                    <Items>
                        <ext:DateField runat="server" ID="ReceivedDate" FieldLabel="Received Date " Width="220" Format="ddd, dd-MMM-yyyy"/>
                    </Items>
                </ext:FieldContainer>
                <ext:GridPanel ID="grdDetail" runat="server" TabIndex="7" region="Center" Layout="FitLayout" Height="350" Padding="8" EnableColumnHide="false">
                    <Store>
                        <ext:Store runat="server" ID="store_detail" PageSize="10" OnReadData="store_detail_refreshData" >
	                        <Model>
		                        <ext:Model ID="Model7" runat="server" IDProperty="mailbagid" Name="data" >
		                            <Fields>
                                        <ext:ModelField Name="pid" />
                                        <ext:ModelField Name="MailBagID" />
                                        <ext:ModelField Name="DatePickup" />
                                        <ext:ModelField Name="SenderBy" />
                                        <ext:ModelField Name="Description" />
                                        <ext:ModelField Name="Address" />
                                        <ext:ModelField Name="Colli" />
                                        <ext:ModelField Name="Weight" />
                                        <ext:ModelField Name="EmailPengirim" />
                                        <ext:ModelField Name="Check" />
		                            </Fields>
	                            </ext:Model>
	                        </Model>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1"  runat="server"> 
                        <Columns>                                   
                            <ext:Column ID="Column1" runat="server" Text="ID" Hidden="true" width="80" DataIndex="pid">
                                <Editor>
                                    <ext:TextField runat="server" ID="pid" />
                                </Editor>
                            </ext:Column>
                            <ext:Column runat="server" width="150" Text="MailBag ID"  DataIndex="MailBagID" >
                                <Editor>
                                    <ext:TextField ID="MailBagID" runat="server" />                                                
                                </Editor>
                            </ext:Column>
                            <ext:DateColumn ID="Column2" runat="server" Text="Date Pickup" Width="120" DataIndex="DatePickup" Format="ddd, dd-MMM-yyyy">
                                <Editor>
                                    <ext:DateField runat="server" ID="DatePickup" />
                                </Editor>
                            </ext:DateColumn>
                            <ext:Column runat="server" width="90" Text="Sender" DataIndex="SenderBy" >
                                <Editor>
                                    <ext:TextField ID="SenderBy" runat="server" />                                                
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column4" runat="server"  Text="Description" width="300" DataIndex="Description">
                                <Editor>
                                    <ext:TextField runat="server" ID="Description" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column5" runat="server"  Text="Address / Destination" width="300" DataIndex="Address">
                                <Editor>
                                    <ext:TextField runat="server" ID="Address" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column6" runat="server"  Text="Colly" width="70" DataIndex="Colli">
                                <Editor>
                                    <ext:TextField runat="server" ID="Colli" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column3" runat="server"  Text="Weight" width="70" DataIndex="Weight">
                                <Editor>
                                    <ext:TextField runat="server" ID="Weight" />
                                </Editor>
                            </ext:Column>                 
                            <ext:Column ID="Column7" runat="server" Text="ID" Hidden="true" width="80" DataIndex="EmailPengirim">
                                <Editor>
                                    <ext:TextField runat="server" ID="EmailPengirim" />
                                </Editor>
                            </ext:Column>
                            <ext:Column ID="Column" runat="server" Text="Received" DataIndex="Check">
                                <Editor>
                                    <ext:Checkbox runat="server" ID="CheckBox">
                                        <DirectEvents>
                                            <Change OnEvent="Check_Click">
                                                <EventMask ShowMask="true" Msg="Loading" />
                                                <ExtraParams>
						                            <ext:Parameter Name="Values" Value="Ext.encode(#{grdDetail}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
						                        </ExtraParams>
                                            </Change>
                                        </DirectEvents>
                                    </ext:Checkbox>
                                </Editor>
                            </ext:Column>
<%--                            <ext:CheckColumn ID="chkBox" runat="server">
                                <Editor>
                                    <ext:Checkbox runat="server" ID="chkBoxID" />
                                </Editor>
                            </ext:CheckColumn>--%>
                        </Columns>
                    </ColumnModel>
                    <Plugins>
                        <ext:CellEditing runat="server" ClicksToEdit="1" />
                    </Plugins>
                    <BottomBar>
				        <ext:PagingToolbar ID="PagingToolbar4" runat="server" StoreID="store_cari"    />
				    </BottomBar>
                </ext:GridPanel>
            </Items>
        </ext:FormPanel>
        </div>
        </div>
</asp:Content>