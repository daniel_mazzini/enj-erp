﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using NewERDM.lib.BusinessClass;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using SpreadsheetLight;

namespace NewERDM.Admin.mailbag
{
    public partial class MailBagSearch_form : System.Web.UI.Page
    {
        #region field

        //UserApplication userApp;
        UserApplicationBL usrApplicationBL = new UserApplicationBL();

        #endregion field

        #region Constructor

        protected void store_mailBag_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getDataMailBag();
        }

        #endregion Constructor

        #region Method

        protected void getDataMailBag()
        {
            DataTable dt = new DataTable();
            var objGet = new clsMailBag();
            dt = objGet.get_DataMailBag_Search(this.f_search.Text.Trim());
            store_MailBag.DataSource = dt;

            if(dt.Rows.Count > 0)
            {
                return;
            }
            else
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Information",
                    Message = "MailBag Number Not Found !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO
                });
            }
        }
        public void ExportToExcelSearch(DataTable dt)
        {
            //SLDocument sl = new SLDocument();

            //var objget = new clsItenary();
            //dt = objget.getItenary_ExportToExcel(this.Booking_Code.Text.Trim(), Convert.ToDateTime(dFrom.Value), Convert.ToDateTime(dTo.Value));
            //store_travel.DataSource = dt;

            //if (dt.Rows.Count > 0)
            //{
            //    //Header
            //    sl.SetCellValue(1, 1, "Nama");
            //    sl.SetCellValue(1, 2, "Date Of Flight");
            //    sl.SetCellValue(1, 3, "Flight Detail");
            //    sl.SetCellValue(1, 4, "Booking Code");
            //    sl.SetCellValue(1, 5, "Route From");
            //    sl.SetCellValue(1, 6, "Route To");
            //    sl.SetCellValue(1, 7, "Status");
            //    sl.SetCellValue(1, 8, "Time Limit");

            //    //Color Header
            //    sl.ApplyNamedCellStyle(1, 1, SLNamedCellStyleValues.Accent1);
            //    sl.ApplyNamedCellStyle(1, 2, SLNamedCellStyleValues.Accent1);
            //    sl.ApplyNamedCellStyle(1, 3, SLNamedCellStyleValues.Accent1);
            //    sl.ApplyNamedCellStyle(1, 4, SLNamedCellStyleValues.Accent1);
            //    sl.ApplyNamedCellStyle(1, 5, SLNamedCellStyleValues.Accent1);
            //    sl.ApplyNamedCellStyle(1, 6, SLNamedCellStyleValues.Accent1);
            //    sl.ApplyNamedCellStyle(1, 7, SLNamedCellStyleValues.Accent1);
            //    sl.ApplyNamedCellStyle(1, 8, SLNamedCellStyleValues.Accent1);


            //    //Counter
            //    int iRows = 0;

            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        iRows = 0 + i;

            //        sl.SetCellValue(2 + iRows, 1, dt.Rows[i][0].ToString());
            //        sl.SetCellValue(2 + iRows, 2, dt.Rows[i][1].ToString());
            //        sl.SetCellValue(2 + iRows, 3, dt.Rows[i][2].ToString());
            //        sl.SetCellValue(2 + iRows, 4, dt.Rows[i][3].ToString());
            //        sl.SetCellValue(2 + iRows, 5, dt.Rows[i][4].ToString());
            //        sl.SetCellValue(2 + iRows, 6, dt.Rows[i][5].ToString());
            //        sl.SetCellValue(2 + iRows, 7, dt.Rows[i][6].ToString());
            //        sl.SetCellValue(2 + iRows, 8, dt.Rows[i][7].ToString());
            //    }


            //    //Auto Fit Column
            //    sl.AutoFitColumn(1);
            //    sl.AutoFitColumn(2);
            //    sl.AutoFitColumn(3);
            //    sl.AutoFitColumn(4);
            //    sl.AutoFitColumn(5);
            //    sl.AutoFitColumn(6);
            //    sl.AutoFitColumn(7);
            //    sl.AutoFitColumn(8);


            //    Response.Clear();
            //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //    Response.AddHeader("Content-Disposition", "attachment; filename=TravelItineraryExcel.xlsx");
            //    sl.SaveAs(Response.OutputStream);
            //    Response.End();
            //}
        }

        #endregion Method

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                //getItenary();
            }
        }
        protected void btn_search_click(object sender, DirectEventArgs e)
        {
            getDataMailBag();
 
            store_MailBag.Reload();
        }
        protected void cmd_New_Click(object sender, DirectEventArgs e)
        {
            X.Redirect("mailbagtracking_form.aspx", "Loading Page ...");
        }
        protected void btn_edit_doubleclick(object sender, DirectEventArgs e)
        {
            String jsonSelectedRows = e.ExtraParams["Values"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);
            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {

                        X.Redirect("~/Admin/mailbag/mailbagtracking_form.aspx?MailBagID=" + row["MailBagID"]);

                    }
                    catch
                    {

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "ERROR",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.ERROR
                        });
                    }

                }
            }
        }
        protected void btn_delete_click(object sender, DirectEventArgs args)
        {
            String jsonSelectedRows = args.ExtraParams["Values"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        var objDelete = new clsMailBag();
                        objDelete.DeleteID(row["MailBagID"]);

                    }
                    catch
                    {
                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "CAN'T DELETE",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING
                        });
                    }

                }
            }
            this.store_MailBag.Reload();
        }
        protected void btn_close_click(object sender, DirectEventArgs e)
        {
            X.Redirect(CommonUtility.ResolveUrl("Dashboard_itenary.aspx"), "Loading ... ");
        }
        protected void btn_export_click(object sender, DirectEventArgs e)
        {
            this.wndExport.Show();
        }
        protected void ToExcel_Click(object sender, EventArgs e)
        {
            //ExportToExcelSearch((DataTable)ViewState["grdMailBag"]);
        }
        protected void btn_view_close_click(object sender, DirectEventArgs e)
        {
            this.wndExport.Close();
        }
    }
}