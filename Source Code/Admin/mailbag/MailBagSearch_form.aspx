﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MailBagSearch_form.aspx.cs" Inherits="NewERDM.Admin.mailbag.MailBagSearch_form" %>
<asp:Content ID="Header" runat="server" ContentPlaceHolderID="head">

    <ext:XScript ID="XScript1" runat="server">
        <script>

        </script>
    </ext:XScript>
    <style>
	  .my-scale.x-btn-default-Small-icon-text-left .x-btn-icon {
			width: 16px;
		}
	   .add32 {
			background-image : url(img/Word-icon.png) !important;
		}   
         .kanan{text-align:right;}
        .Fkanan
         {
            text-align:right;
            border:none;
            background-image: none;
        }
        .my-panel .x-panel-header {
			background-color: #dfe8f6 !important;
			 
		}
 
		.my-panel .x-panel-body {
			border: none;
			background-color: #dfe8f6 !important;
		}
     
         .search-item {
            font          : normal 11px tahoma, arial, helvetica, sans-serif;
            padding       : 3px 10px 3px 10px;
            border        : 1px solid #fff;
            border-bottom : 1px solid #eeeeee;
            white-space   : normal;
            color         : #555;
        }
        
        .search-item h3 {
            display     : block;
            font        : inherit;
            font-weight : bold;
            color       : #222;
            margin      :0px;
        }

        .search-item h3 span {
            float       : right;
            font-weight : normal;
            margin      : 0 0 5px 5px;
            width       : 100px;
            display     : block;
            clear       : none;
        } 
        
        p { width: 650px; }
        
        .ext-ie .x-form-text { position : static !important; }
    </style>
</asp:Content>

<asp:Content ID="Detail" runat="server" ContentPlaceHolderID="MainContent">
    <div class="row">
        <div class="col-md-12">
            <ext:FormPanel runat="server" ID="frmverified" Title ="" BodyPadding="12" Header="false" Frame="false" Border="false" DisabledCls="true">
                <Items>
                <ext:GridPanel ID="grdMailBag" runat="server" Border="false" Layout="FitLayout" Height="520" Region="Center" Margins="0 5 5 5" EnableColumnHide="false"  >
                    <Store>
                        <ext:Store ID="store_MailBag" PageSize="21" runat="server" OnReadData="store_mailBag_RefreshData" GroupField="Nama" >
                            <Model>
                                <ext:Model ID="Model13" runat="server" IDProperty="MailBagID">
                                    <Fields>
                                        <ext:ModelField Name="MailBagID" />
                                        <ext:ModelField Name="dateMailbag" />
                                        <ext:ModelField Name="MailbagType" />
                                        <ext:ModelField Name="ShipTo" />
                                        <ext:ModelField Name="Remark" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store>

                    <ColumnModel runat="server">
                        <Columns>
                            <ext:Column ID="Column1" runat="server" Width="200" Text="No Mailbag" Filterable="false" DataIndex="MailBagID" />
                            <ext:Column ID="Column2" runat="server" Width="100" Text="Date" Filterable="false" DataIndex="dateMailbag" />
                            <ext:Column ID="Column3" runat="server" Width="80" Text="Mailbag Type" Filterable="false" DataIndex="MailbagType"  />
                            <ext:Column ID="Column4" runat="server" Width="200" Text="Ship To" Filterable="false" DataIndex="ShipTo"/>
                            <ext:Column ID="Column5" runat="server" Width="400" Text="Remark" Filterable="false" DataIndex="Remark"/>
                        </Columns>
                    </ColumnModel>
                    <DirectEvents>
                        <CellDblClick OnEvent="btn_edit_doubleclick" >
                             <EventMask ShowMask="true" />
                                <ExtraParams>
	                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdMailBag}.getRowsValues({selectedOnly : true}))"
			                                Mode="Raw">
	                                </ext:Parameter>
                                </ExtraParams>
                        </CellDblClick>
                    </DirectEvents>
                    <TopBar>
                        <ext:Toolbar ID="Toolbar2" runat="server" Layout="ColumnLayout">
                            <Items>
                                <ext:TextField runat="server" FieldLabel="Filter" ID="f_search" EmptyText="Please type MailBag Number "  Width="310" LabelWidth="50"     />
                                <ext:Label ID="Label7" Text="&nbsp;" Width="5" runat="Server" />

                                 <ext:Panel runat="server" Frame="false" Border="false" Cls="my-panel">
                                 <Items>
                                    <ext:Button ID="cmd_Search" runat="server" Text="Search"  UI="Success" ToolTipType="Qtip" ToolTip="<b>Search</b><br>Search New Travel Itinerary Items">
                                    <DirectEvents>
                                            <Click OnEvent="btn_search_click" >
                                                <EventMask ShowMask="true" Msg="Searching.... Please wait.." />
                                            </Click>
                                    </DirectEvents>
                                    </ext:Button>
                                    <ext:Label ID="Label1" Text="&nbsp;&nbsp;" Width="2" runat="Server" />
                                    <ext:Button ID="cmd_New" runat="server" Text="New MailBag" UI="Success" ToolTipType="Qtip" ToolTip="<b>New</b><br>Input New MailBag Tracking">
                                    <DirectEvents>
                                            <Click OnEvent="cmd_New_Click" >
                                                <EventMask ShowMask="true" Msg="Searching.... Please wait.." />
                                            </Click>
                                    </DirectEvents>
                                    </ext:Button>
                                     <ext:Label ID="Label8" Text="&nbsp;&nbsp;" Width="2" runat="Server" />
                                     <ext:Button ID="Button1" runat ="server" Text="Delete" UI="Warning" ToolTipType="Qtip" ToolTip="<b>DELETE</b><br>Delete MailBag Tracking</br>">
                                            <DirectEvents>
                                            <Click OnEvent="btn_delete_click">
                                                <Confirmation ConfirmRequest="true" Message="Are you sure to delete this record ?" Title="Delete Records" />
                                                <EventMask ShowMask="true" Msg="Delete Data Please wait..." />
                                                <ExtraParams>
									               <ext:Parameter Name="Values" Value="Ext.encode(#{grdMailBag}.getRowsValues({selectedOnly : true}))"
											               Mode="Raw">
									               </ext:Parameter>
								               </ExtraParams>
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Label ID="Label9" Text="&nbsp;&nbsp;" Width="2" runat="Server" />
                                    <ext:Button ID="Button2" runat="server" Text="Export To Excel" UI="Primary" ToolTipType="Qtip" ToolTip="<b>Export To Excel</b>">
                                        <DirectEvents>
                                            <Click OnEvent="btn_export_click">
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>
                                    <ext:Label ID="Label10" Text="&nbsp;&nbsp;" Width="2" runat="Server" />
                                    <ext:Button ID="btn_Close" runat="server" Text="Close"  UI="Danger" ToolTipType="Qtip" ToolTip="<b>Close</b><br>Close This Module" Handler="this.up('window').close()" >
                                        <DirectEvents>
                                            <Click OnEvent="btn_close_click" >
                                                <EventMask ShowMask="true" />
                                            </Click>
                                        </DirectEvents>
                                        <Listeners>
                                            <Click StopEvent="true"/>
                                        </Listeners>
                                    </ext:Button>
                                 </Items>                                     
                                 </ext:Panel>
                                <ext:TextField ID="IDMailBag" runat="server" Hidden ="true" />
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
<%--                    <View>
                        <ext:GridView runat="server" StripeRows="true" MarkDirty="false" />
                    </View>
                    <Features>
                        <ext:GroupingSummary ID="GroupingSummary" runat="server" GroupHeaderTplString="{MailBagID}" HideGroupedHeader="true" 
                            EnableGroupingMenu="false"  StartCollapsed="false" />
                    </Features>--%>
                  <BottomBar>
				<ext:PagingToolbar ID="PagingToolbar3" runat="server" >
                   <Items>
                       <ext:Button runat="server" Text="<b>Clear Filters</b>" Icon="Erase" Handler="this.up('grid').filters.clearFilters();" />
                   </Items>
                </ext:PagingToolbar>
			</BottomBar>
                </ext:GridPanel>
                </Items>
            </ext:FormPanel>
           <ext:Window ID="wndExport" runat="server" Hidden="true" Icon="PageExcel" Title="Export To Excel" Height="120" Width="350" Modal="true" BodyPadding="2" Closable="false"   >
           <Items>
                <ext:FormPanel runat="server" ID="FormPanel1" Title =""  BodyPadding="12"  Height="70" Header="false"  Frame="false"  Border="false"  >
                    <LayoutConfig >
                        <ext:VBoxLayoutConfig  />
                    </LayoutConfig>  
                     <Items>
                        <ext:TextField runat="server" ID="MailBagID" FieldLabel="No Mailbag" />                       
                    </Items>
                </ext:FormPanel>
            </Items>
              <Buttons>
                <ext:Button runat="server" ID="btn_exportItenerary" Text="TO EXCEL" Click="ToExcel_Click" UI="Primary">
                    <DirectEvents>
                        <Click OnEvent="ToExcel_Click" IsUpload="true" >
                        </Click>
                    </DirectEvents>
                </ext:Button>
                        
				<ext:Button runat="server" ID="Button3" UI="Warning"  Text="<b>CLOSE</b>"  >
                    <DirectEvents>
                          <Click OnEvent="btn_view_close_click">
                            <EventMask ShowMask="true" />
                        </Click>
                    </DirectEvents>
				</ext:Button>
			</Buttons>
        </ext:Window>
        </div>
    </div>
</asp:Content>