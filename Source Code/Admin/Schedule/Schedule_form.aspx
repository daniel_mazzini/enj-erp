﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Schedule_form.aspx.cs" Inherits="NewERDM.Admin.Schedule.Schedule_form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <ext:XScript ID="XScript1" runat="server">
        <script runat="server">
                              
        </script>
    </ext:XScript>
        <style>
        .ext-color-4,
        .ext-ie .ext-color-4-ad,
        .ext-opera .ext-color-4-ad {
	        color: #7F0000;
        }
        .ext-cal-day-col .ext-color-4,
        .ext-dd-drag-proxy .ext-color-4,
        .ext-color-4-ad,
        .ext-color-4-ad .ext-cal-evm,
        .ext-color-4 .ext-cal-picker-icon,
        .ext-color-4-x dl,
        .ext-color-4-x .ext-cal-evb {
	        background: #7F0000;
        }
        .ext-color-4-x .ext-cal-evb,
        .ext-color-4-x dl {
            border-color: #7C3939;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      <div class="row">
         <div class="col-md-12" >
            <ext:FormPanel runat="server" ID="frmverified" Title ="Calendar"  BodyPadding="12" Layout="fit"  Frame="false"  Border="false" DisabledCls="true" Height="600">
                <Items>
                    <ext:FieldContainer runat="server" AnchorHorizontal="100%" Layout="HBoxLayout">
                        <Items>
                            <ext:TextField ID="EmployeeID" runat="server" Hidden="true" />
                            <ext:DateField ID="dDateFrom" runat="server" FieldLabel="Date From " Format="ddd, dd-MMM-yyyy" Width="80" />
                            <ext:DateField ID="dDateTo" runat="server" FieldLabel="Date To " Format="ddd, dd-MMM-yyyy" Width="80" />
                            <ext:ComboBox ID="typeSchedule" runat="server" FieldLabel="Type ">
                                <Items>
                                    <ext:ListItem Text="Training" Value="Training" />
                                    <ext:ListItem Text="Travel" Value="Travel" />
                                    <ext:ListItem Text="Cuti" Value="Cuti" />
                                    <ext:ListItem Text="Working" Value="Working" />
                                    <ext:ListItem Text="Off Day" Value="Off Day" />
                                    <ext:ListItem Text="Other" Value="Other" />
                                </Items>
                            </ext:ComboBox>
                            <ext:TextArea ID="desc" runat="server" FieldLabel="Description" />
                        </Items>
                    </ext:FieldContainer>
                </Items>
                <Buttons>
                    <ext:Button runat="server" ID="btn_save_job" Height="30" UI="Success" Html="<b>SAVE DATA</b>" >
<%--                        <DirectEvents>
                            <Click OnEvent="btn_save_click">
                                <EventMask ShowMask="true" Msg="Saving Data Please Wait..." />
                            </Click>
                        </DirectEvents>--%>
                    </ext:Button>
                    <ext:Button runat="server" ID="btn_close_job" Height="30" UI="Danger"  Html="<b>CLOSE</b>" >
<%--                        <DirectEvents>
                            <Click OnEvent="btnClose_click" >
                                <EventMask ShowMask="true" Msg="Loading..." />
                            </Click>
                        </DirectEvents>--%>
                    </ext:Button> 
                </Buttons>
             </ext:FormPanel>
         </div>
     </div>
</asp:Content>