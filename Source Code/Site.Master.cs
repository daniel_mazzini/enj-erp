﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SmartLibraries.DataAccessLayer;
using NewERDM.lib.BusinessClass;
using Ext.Net;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;
using NewERDM.lib.BusinessClass;

namespace NewERDM
{
    public partial class Site : System.Web.UI.MasterPage
    {
        #region field

        UserApplication userApp;

        #endregion
        string imageuser = "";


        protected void Page_Load(object sender, EventArgs e)
        {

             userApp = (UserApplication)Session["ses_user"];
             if (userApp == null)
             {
             //    Response.Redirect("default.aspx");
             }
             else
             {
                 lblUserID.Text = userApp.UserID;
                 imageuser = string.Format(CommonUtility.ResolveUrl("~/photo/{0}.jpg"), userApp.EmployeeID);
                 imgphoto1.Src = imageuser;
                 imgphoto2.Src = imageuser;

                 if (!X.IsAjaxRequest)
                 {
                     this.lbluserName.Html = "<font color='white'>" + userApp.UserName + " <font/>";
                     this.lbljabatan.Text = userApp.jabatan;
                     this.lblUserName1.Text = userApp.UserName;

                     if(userApp.GroupID == "USER")
                     {
                         //-------Header Menu
                         //this.headerAdministration.Visible = false;
                         this.headerLogistic.Visible = false;
                         this.headerSafety.Visible = false;
                         this.headerESS.Visible = false;
                         this.headerAccounting.Visible = false;
                         this.headerSetup.Visible = false;
                         this.headerContract.Visible = false;
                         this.headerHRD.Visible = false;
                         //this.headerLookup.Visible = false;
                         //-------Modul Administration
                         //this.subAdminItinerary.Visible = false;
                         this.searchItenery.Visible = false;
                         //this.DashboardSchedule.Visible = false;

                         this.subAdminCrown.Visible = false;

                         this.subAdminPettyCash.Visible = false;

                         //this.subAdminOfficeSupply.Visible = false;

                         this.subAdminLibrary.Visible = false;

                         this.mailBagInput.Visible = false;
                         //-------Modul Employee
                         this.profilUpdate.Visible = false;
                         this.TimeSheet.Visible = false;
                         //-------Lookup Master
                         //-----Lookup Employee Data
                         this.subLookupEmployee.Visible = false;
                         this.subLookupAsset.Visible = false;

                         this.profilUpdate.Visible = false;
                         this.employeeDataEthic.Visible = false;
                         this.employeeDataCity.Visible = false;
                         this.employeeDataState.Visible = false;
                         this.employeeDataCountry.Visible = false;
                         this.employeeDataEmployeeType.Visible = false;
                         this.employeeDataEmployeeClass.Visible = false;
                         this.employeeDataIndividualGrade.Visible = false;
                         this.employeeDataOrgGroup.Visible = false;
                         this.employeeDataSection.Visible = false;
                         this.employeeDataCostCenter.Visible = false;
                         this.employeeDataPayGroup.Visible = false;
                         //-----Lookup Administration
                         this.subLookupAdmin.Visible = false;

                         //this.adminAccomodation.Visible = false;
                         //this.adminAirline.Visible = false;
                         //this.adminFlightStatus.Visible = false;
                         //this.adminJustification.Visible = false;
                         //this.adminPaymentStatus.Visible = false;
                         //this.adminPOH.Visible = false;
                         //this.adminSponsor.Visible = false;
                         //this.adminTransportation.Visible = false;

                         //this.changePassword.Attributes
                         dashboarID.HRef = "Dashboard_User.aspx";
                     }
                     if(userApp.GroupID == "ADMIN")
                     {
                         //-------Header Menu
                         this.headerLogistic.Visible = false;
                         this.headerSafety.Visible = false;
                         this.headerESS.Visible = false;
                         this.headerAccounting.Visible = false;
                         this.headerSetup.Visible = false;
                         this.headerContract.Visible = false;
                         this.headerHRD.Visible = false;
                         //-------Modul Administration
                         this.subAdminCrown.Visible = false;

                         this.subAdminPettyCash.Visible = false;

                         //this.subAdminOfficeSupply.Visible = false;

                         this.subAdminLibrary.Visible = false;

                         this.mailBagInput.Visible = false;
                         //-------Modul HRD
                         this.profilUpdate.Visible = false;
                         this.TimeSheet.Visible = false;
                         //-------Master Lookup
                         //----Lookup Employee Data
                         this.subLookupEmployee.Visible = false;
                         this.subLookupAsset.Visible = false;

                         this.profilUpdate.Visible = false;
                         this.employeeDataEthic.Visible = false;
                         this.employeeDataCity.Visible = false;
                         this.employeeDataState.Visible = false;
                         this.employeeDataCountry.Visible = false;
                         this.employeeDataEmployeeType.Visible = false;
                         this.employeeDataEmployeeClass.Visible = false;
                         this.employeeDataIndividualGrade.Visible = false;
                         this.employeeDataOrgGroup.Visible = false;
                         this.employeeDataSection.Visible = false;
                         this.employeeDataCostCenter.Visible = false;
                         this.employeeDataPayGroup.Visible = false;

                         //X.Redirect("Dashboard_itenary.aspx", "Loading ... ");

                         dashboarID.HRef = "Dashboard_itenary.aspx";
                     }
                     if (userApp.GroupID == "SUPERADMIN" || userApp.GroupID == "DATABASE")
                     {
                         //X.Redirect("Dashboard_itenary.aspx", "Loading ... ");
                         dashboarID.HRef = "Dashboard_itenary.aspx";
                     }
                 }
             }
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            this.txtUserID.Text = lblUserID.Text;
            this.WindowChangePassword.Show();
        }
        protected void btn_logout_Click(object sender, DirectEventArgs e)
        {
            try
            {
                Session.RemoveAll();
                Response.Redirect("~/default.aspx");
            }
            catch (Exception ex)
            {
                X.Msg.Alert("Logout", ex.Message).Show();
            }
        }
        protected void btn_profile_Click(object sender, DirectEventArgs e) 
        {
            Response.Redirect(CommonUtility.ResolveUrl("~/admin/asset/profile.aspx"));
        }
        protected void btnSave_Click(object sender, DirectEventArgs e)
        {
            if(string.IsNullOrEmpty(txtUserID.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Userid can't be empty !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else if(string.IsNullOrEmpty(txtPasswordOld.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "Old password can't be empty !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else if(string.IsNullOrEmpty(txtPasswordNew.Text))
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "Warning",
                    Message = "New password can't be empty !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }
            else
            {
                DataTable dtPassOld = new DataTable();
                var objGetPassOld = new clsLogin();
                dtPassOld = objGetPassOld.get_PassOld(this.txtUserID.Text, this.txtPasswordOld.Text);

                if(dtPassOld.Rows.Count > 0)
                {
                    var objUpdatePass = new clsLogin();
                    objUpdatePass.updateNewPassword(this.txtUserID.Text, this.txtPasswordNew.Text);

                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Information",
                        Message = "Update password success !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });

                    this.WindowChangePassword.Hide();

                }
                else
                {
                    X.MessageBox.Show(new MessageBoxConfig
                    {
                        Title = "Warning",
                        Message = "Old password not match !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING
                    });
                    return;
                }
            }
        }
        protected void btnCancel_Click(object sender, DirectEventArgs e)
        {
            this.WindowChangePassword.Hide();
        }
    }
}