﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SmartLibraries.DataAccessLayer;
using NewERDM.lib.BusinessClass;
using Ext.Net;
using System.Text;
using NewERDM.lib.Helper;
using SpreadsheetLight;
using NewERDM.lib.ObjectModel;

namespace NewERDM
{
    public partial class Dashboard_User : System.Web.UI.Page
    {

        #region Field
        UserApplication userApp;
        #endregion Field

        #region Properties
        protected void Store_Notif_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getBookingStatus();
        }
        protected void Store_Schedule_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getSchedule();
        }
        protected void Store_Training_RefreshData(object sender, StoreReadDataEventArgs e)
        {
 
        }
        protected void Store_HistoricalInfo_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getEmployeeTraining();
        }
        protected void Store_LeaveBalance_RefreshData(object sender, StoreReadDataEventArgs e)
        {

        }
        protected void Store_InterOffice_RefreshData(object sender, StoreReadDataEventArgs e)
        {

        }
        protected void Store_Timesheet_RefreshData(object sender, StoreReadDataEventArgs e)
        {

        }
        protected void Store_EarningSlip_RefreshData(object sender, StoreReadDataEventArgs e)
        {

        }


        #endregion Properties

        #region Method
        protected void getBookingStatus()
        {
            DataTable dt = null;
            var objget = new clsDashboardUser();
            dt = objget.get_StatusBooking();
            Store_Notif.DataSource = dt;
            Store_Notif.DataBind();
        }
        protected void getSchedule()
        {
            DataTable dt = null;
            var objget = new clsDashboardUser();
            dt = objget.get_Schedule(lblUserName.Text);
            Store_Schedule.DataSource = dt;
            Store_Schedule.DataBind();
        }
        protected void getEmployeeTraining()
        {
            DataTable dt = null;
            var objget = new clsDashboardUser();
            dt = objget.get_EmployeeTraining(lblUserID.Text);
            Store_HistoricalInfo.DataSource = dt;
            Store_HistoricalInfo.DataBind();
        }


        #endregion Method

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!X.IsAjaxRequest)
            {
                userApp = (UserApplication)Session["ses_user"];

                lblUserName.Text = userApp.UserName;
                lblUserID.Text = userApp.EmployeeID;
                getBookingStatus();
                getSchedule();
                getEmployeeTraining();
                txtDateNow.Text = DateTime.Today.ToString("ddd, dd-MMM-yyyy").Trim();
            }
        }
        protected void Download_Click(object sender, DirectEventArgs e)
        {
            string jsonValues = e.ExtraParams["Values"];
            List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);

            //Validasi, jika user tidak melakukan click baris
            if (records.Count == 0)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Please selected row !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }



            String jsonSelectedRows = e.ExtraParams["Values"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        foreach (var record in records)
                        {
                            DataTable dtFilePDF = new DataTable();
                            var objGetFilePDF = new clsDashBoard();
                            dtFilePDF = objGetFilePDF.getUploadFilePDF(record["BookingCode"].ToString());

                            if (dtFilePDF.Rows.Count > 0)
                            {
                                this.upload.Text = dtFilePDF.Rows[0]["FilePDF"].ToString();

                                if (this.upload.Text == "")
                                {
                                    X.MessageBox.Show(new MessageBoxConfig
                                    {
                                        Title = "WARNING",
                                        Message = "File PDF Not Found !",
                                        Buttons = MessageBox.Button.OK,
                                        Icon = MessageBox.Icon.WARNING
                                    });

                                    return;
                                }
                                else
                                {
                                    getDownload(row["BookingCode"]);
                                }
                            }
                        }
                    }
                    catch
                    {

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "ERROR",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.ERROR
                        });
                    }

                }
            }
        }
        protected void getDownload(string idBooking)
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getDownloadFile(idBooking);
            Store_Notif.DataSource = dt;

            //SqlDataReader dr = dt.

            if (dt.Rows.Count > 0)
            {
                string pathUploadFile = dt.Rows[0]["pathUpload"].ToString();
                string nameFile = dt.Rows[0]["upload"].ToString();
                string pathFile = pathUploadFile + "\\" + nameFile;
                Response.ContentType = "Application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=" + dt.Rows[0]["upload"].ToString());
                //this.EnableViewState = false;
                Response.WriteFile(pathFile);
                Response.End();
            }
            this.grdToday.Refresh();
            this.grdSchedule.Refresh();
        }
    }
}