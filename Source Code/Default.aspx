﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="NewERDM.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="~/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
       <link href="plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
  
    <script type="text/javascript">
     
    </script>

  </head>
 <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
          <img src="images/login-logo_1.png" width="200" height="70" /><br />
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to ERDM Systems</p>
        <form  id="form1" runat="server">
              <ext:ResourceManager ID="ResourceManager1" runat="server"   />
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="User ID" runat="server" id="txtUsername"/>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
             <input type="password" class="form-control" placeholder="Password" runat="server" id="txtPassword"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">    
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>                        
            </div><!-- /.col -->
            <div class="col-xs-4">
                 <ext:Button ID="btn_login" runat="server" Html="<b>Sign In</b>"   Cls="btn btn-primary btn-block btn-flat"  >
                          <DirectEvents>
                                 <Click OnEvent="btn_login_Click">
                                        <EventMask ShowMask="true" /> 
                                 </Click>
                          </DirectEvents>
                    </ext:Button> 
            </div><!-- /.col -->
          </div>
        </form>

       
        <a href="#">I forgot my password</a><br>
   
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

   <script type="text/javascript" src="<%= ResolveUrl("plugins/jQuery/jQuery-2.1.3.min.js") %>"></script>
   <script type="text/javascript" src="<%= ResolveUrl("bootstrap/js/bootstrap.min.js") %>"></script>
   <script type="text/javascript" src="<%= ResolveUrl("plugins/iCheck/icheck.min.js") %> "></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });

      $("#txtPassword").keypress(function (event) {
          var keycode = (event.keyCode ? event.keyCode : event.which);
          if (keycode == 13) {
              //  $("#btn_login").click();
              // #{DeleteButton}.fireEvent("click");
              //  #{btn_login}.fireEvent("click");
              //goload();
              App.btn_login.fireEvent("click")
          }
      });
      function goload()
      {
       //  #{btn_login}.fireEvent("click");
      }


    </script>
  </body>
</html>