﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard_User.aspx.cs" Inherits="NewERDM.Dashboard_User" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <ext:Hidden ID="Hidden1" runat="server" />
    <div class="row">
        <section class="col-lg-4 connectedSortable" >
            <div class="box box-primary" style="width: 368px;">
                <div class="box-header with-border">
                    <div class="box-tools pull-right" >
                        <b><ext:Label runat="server" ID="txtDateNow" Width="200px" /></b>
                        <h3 class="box-title"><b class="text-red">Today Flight&nbsp;&nbsp;</b></h3>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <!--<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <ext:GridPanel id="grdToday" runat="server" Frame="false" Height="240px" Width="345px" Header="false" Border="false" Cls="x-grid-custom" HideHeaders="true"    >
                        <Store>
				            <ext:Store ID="Store_Notif" runat="server" PageSize="5" OnReadData="Store_Notif_RefreshData" >
							    <Model>
								    <ext:Model ID="Model1" runat="server"  IDProperty="BookingCode" >
									    <Fields>
                                            <ext:ModelField Name="Nama" />
                                            <ext:ModelField Name="BookingCode" />
                                            <ext:ModelField Name="DateOfFlight" />
										    <ext:ModelField Name="RouteFrom" />
										    <ext:ModelField Name="RouteTo" />
                                            <ext:ModelField Name="ETD" />
                                            <ext:ModelField Name="Status" />
                                        </Fields>
								    </ext:Model>
							    </Model>
				            </ext:Store>
                        </Store>                 
                        <ColumnModel ID="ColumnModel1" runat="server" >
				            <Columns>
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Flex="1" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for=".">
                                                <b>{Nama}</b>
                                                <br /><br />
                                                <img src="images/flown.png" />
				                                <font color="blue"><b>&nbsp;&nbsp;{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br /><br />
			                                </tpl>         
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                {RouteFrom}-{RouteTo}<br />
				                                <font color="blue"><b>{DateOfFlight}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>  
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="30px" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                <font color="green"><b>{Status}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
                                <ext:CommandColumn ID="DownloadPdf" runat="server" Width="40">
                                    <Commands>
                                        <ext:GridCommand Icon="Attach" CommandName="download"> 
                                            <ToolTip Text="download" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="Download_Click" IsUpload="true">
                                            <ExtraParams>
                                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
									        </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>  
                            </Columns>
			            </ColumnModel>
<%--                        <DirectEvents>
                            <CellDblClick OnEvent="btn_edit_doubleclick" >
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
                                </ExtraParams>
                            </CellDblClick>
                        </DirectEvents>--%>
                        <BottomBar >
                            <ext:PagingToolbar runat="server" ID="PagingToolbar1"  DisplayInfo="false" DisplayMsg="false" >
                                <Items>
                                    <ext:TextField runat="server" ID="upload" Hidden="true" />
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
			        </ext:GridPanel>
                </div>
            </div>
        </section>

        <section class="col-lg-8 contenteditable" >
            <div class="box box-primary" style="width: 380px;">
                <div class="box-header with-border">
                    <div class="box-tools pull-right" >
                        <b><ext:Label runat="server" ID="lblUserName" Hidden="true" /></b>
                        <b><ext:Label runat="server" ID="lblUserID" Hidden="true" /></b>
                        <h3 class="box-title"><b class="text-red">Schedule&nbsp;&nbsp;</b></h3>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <!--<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <ext:GridPanel id="grdSchedule" runat="server" Frame="false" Height="240px" Width="360px" Header="false" Border="false" Cls="x-grid-custom" HideHeaders="true"    >
                        <Store>
				            <ext:Store ID="Store_Schedule" runat="server" PageSize="3" OnReadData="Store_Schedule_RefreshData" >
							    <Model>
								    <ext:Model ID="Model2" runat="server" IDProperty="BookingCode" >
									    <Fields>
                                            <ext:ModelField Name="Nama" />
                                            <ext:ModelField Name="BookingCode" />
                                            <ext:ModelField Name="DateOfFlight" />
										    <ext:ModelField Name="RouteFrom" />
										    <ext:ModelField Name="RouteTo" />
                                            <ext:ModelField Name="ETD" />
                                            <ext:ModelField Name="Status" />
                                        </Fields>
								    </ext:Model>
							    </Model>
				            </ext:Store>
                        </Store>                 
                        <ColumnModel ID="ColumnModel2" runat="server" >
				            <Columns>
                                <ext:TemplateColumn runat="server" MenuDisabled="true" Header="Template" Flex="1" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for=".">
                                                <b>{Nama}</b>
                                                <br /><br />
                                                <img src="images/flown.png" />
				                                <font color="blue"><b>&nbsp;&nbsp;{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br /><br />
			                                </tpl>         
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                {RouteFrom}-{RouteTo}<br />
				                                <font color="blue"><b>{DateOfFlight}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>  
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="80px" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                <font color="green"><b>{Status}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
<%--                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>
                                        <ext:GridCommand Icon="Attach" CommandName="download"> 
                                            <ToolTip Text="download" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="Download_Click" IsUpload="true">
                                            <ExtraParams>
                                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
									        </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>--%>
                            </Columns>
			            </ColumnModel>
<%--                        <DirectEvents>
                            <CellDblClick OnEvent="btn_edit_doubleclick" >
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
                                </ExtraParams>
                            </CellDblClick>
                        </DirectEvents>--%>
                        <BottomBar >
                            <ext:PagingToolbar runat="server" ID="PagingToolbar2"  DisplayInfo="false" DisplayMsg="false" >
                                <Items>
                                    <%--<ext:TextField runat="server" ID="TextField1" Hidden="true" />--%>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
			        </ext:GridPanel>
                </div>
            </div>
        </section>
    </div>
    <div class='row'>
        <div class="col-md-4 connectedSortable">
            <div class="box box-primary" style="width: 368px;">
                <div class="box-header with-border">
                    <div class="box-tools pull-right" >
                        <h3 class="box-title"><b class="text-red">Training Info&nbsp;&nbsp;</b></h3>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <!--<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                    </div>
                </div><!-- /.box-header -->
                                <div class="box-body">
                    <ext:GridPanel id="grdTraining" runat="server" Frame="false" Height="240px" Width="345px" Header="false" Border="false" Cls="x-grid-custom" HideHeaders="true"    >
                        <Store>
				            <ext:Store ID="Store_Training" runat="server" PageSize="3" OnReadData="Store_Training_RefreshData" >
							    <Model>
								    <ext:Model ID="Model3" runat="server" IDProperty="BookingCode" >
									    <Fields>
                                            <ext:ModelField Name="Nama" />
                                            <ext:ModelField Name="BookingCode" />
                                            <ext:ModelField Name="DateOfFlight" />
										    <ext:ModelField Name="RouteFrom" />
										    <ext:ModelField Name="RouteTo" />
                                            <ext:ModelField Name="ETD" />
                                            <ext:ModelField Name="Status" />
                                        </Fields>
								    </ext:Model>
							    </Model>
				            </ext:Store>
                        </Store>                 
                        <ColumnModel ID="ColumnModel3" runat="server" >
				            <Columns>
                                <ext:TemplateColumn runat="server" MenuDisabled="true" Header="Template" Flex="1" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for=".">
                                                <b>{Nama}</b>
                                                <br /><br />
                                                <img src="images/flown.png" />
				                                <font color="blue"><b>&nbsp;&nbsp;{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br /><br />
			                                </tpl>         
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                {RouteFrom}-{RouteTo}<br />
				                                <font color="blue"><b>{DateOfFlight}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>  
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="80px" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                <font color="green"><b>{Status}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
<%--                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>
                                        <ext:GridCommand Icon="Attach" CommandName="download"> 
                                            <ToolTip Text="download" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="Download_Click" IsUpload="true">
                                            <ExtraParams>
                                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
									        </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>--%>
                            </Columns>
			            </ColumnModel>
<%--                        <DirectEvents>
                            <CellDblClick OnEvent="btn_edit_doubleclick" >
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
                                </ExtraParams>
                            </CellDblClick>
                        </DirectEvents>--%>
                        <BottomBar >
                            <ext:PagingToolbar runat="server" ID="PagingToolbar3"  DisplayInfo="false" DisplayMsg="false" >
                                <Items>
                                    <%--<ext:TextField runat="server" ID="TextField1" Hidden="true" />--%>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
			        </ext:GridPanel>
                </div>

            </div>
        </div>

        <div class="col-md-4 connectedSortable">
            <div class="box box-primary" style="width: 368px;">
                <div class="box-header with-border">
                    <div class="box-tools pull-right" >
                        <h3 class="box-title"><b class="text-red">Historical Training&nbsp;&nbsp;</b></h3>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <!--<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <ext:GridPanel id="grdHistoricalInfo" runat="server" Frame="false" Height="240px" Width="345px" Border="false" Cls="x-grid-custom" EnableColumnHide="false" >
                        <Store>
				            <ext:Store ID="Store_HistoricalInfo" runat="server" PageSize="3" OnReadData="Store_HistoricalInfo_RefreshData" >
							    <Model>
								    <ext:Model ID="Model4" runat="server" IDProperty="pid" >
									    <Fields>
                                            <ext:ModelField Name="pid" />
                                            <ext:ModelField Name="EmployeeID" />
                                            <ext:ModelField Name="TrainingType" />
                                            <ext:ModelField Name="TrainingName" />
                                            <ext:ModelField Name="Mandatory" />
										    <ext:ModelField Name="TrainingLocation" />
										    <ext:ModelField Name="TrainingStartDate" />
                                            <ext:ModelField Name="TrainingEndDate" />
                                            <ext:ModelField Name="TrainingCertificateNumber" />
                                            <ext:ModelField Name="TrainingProvider" />
                                        </Fields>
								    </ext:Model>
							    </Model>
				            </ext:Store>
                        </Store>                 
                        <ColumnModel ID="ColumnModel4" runat="server" >
				            <Columns>
                                <ext:Column ID="Column1" runat="server" Text="PID" DataIndex="pid" Hidden="true" />
                                <ext:Column ID="Column10" runat="server" Text="Employee ID" DataIndex="EmployeeID" Hidden="true"  />
                                <ext:Column ID="Column2" runat="server" Text="Training Type" DataIndex="TrainingType"  />
                                <ext:Column ID="Column3" runat="server" Text="Training Name" DataIndex="TrainingName" />
                                <ext:Column ID="Column4" runat="server" Text="Mandatory" DataIndex="Mandatory"  />
                                <ext:Column ID="Column5" runat="server" Text="Training Location" DataIndex="TrainingLocation" />
                                <ext:DateColumn ID="Column6" runat="server" Text="Training Start Date" DataIndex="TrainingStartDate" Format="ddd, dd-MMM-yyyy" Width="110" />
                                <ext:DateColumn ID="Column7" runat="server" Text="Training End Date" DataIndex="TrainingEndDate" Format="ddd, dd-MMM-yyyy" Width="110" />
                                <ext:Column ID="Column8" runat="server" Text="Certificate No." DataIndex="TrainingCertificateNumber" />
                                <ext:Column ID="Column9" runat="server" Text="Provider" DataIndex="TrainingProvider" Width="200" />
                            </Columns>
			            </ColumnModel>
<%--                        <DirectEvents>
                            <CellDblClick OnEvent="btn_edit_doubleclick" >
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
                                </ExtraParams>
                            </CellDblClick>
                        </DirectEvents>--%>
                        <BottomBar >
                            <ext:PagingToolbar runat="server" ID="PagingToolbar4"  DisplayInfo="false" DisplayMsg="false" >
                                <Items>
                                    <%--<ext:TextField runat="server" ID="TextField1" Hidden="true" />--%>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
			        </ext:GridPanel>
                </div>

            </div>
        </div>

        <div class="col-md-4 connectedSortable">
            <div class="box box-primary" style="width: 368px;">
                <div class="box-header with-border">
                    <div class="box-tools pull-right" >
                        <h3 class="box-title"><b class="text-red">Leave Balance&nbsp;&nbsp;</b></h3>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <!--<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <ext:GridPanel id="grdLeaveBalance" runat="server" Frame="false" Height="240px" Width="345px" Header="false" Border="false" Cls="x-grid-custom" HideHeaders="true"    >
                        <Store>
				            <ext:Store ID="Store_LeaveBalance" runat="server" PageSize="3" OnReadData="Store_LeaveBalance_RefreshData" >
							    <Model>
								    <ext:Model ID="Model5" runat="server" IDProperty="BookingCode" >
									    <Fields>
                                            <ext:ModelField Name="Nama" />
                                            <ext:ModelField Name="BookingCode" />
                                            <ext:ModelField Name="DateOfFlight" />
										    <ext:ModelField Name="RouteFrom" />
										    <ext:ModelField Name="RouteTo" />
                                            <ext:ModelField Name="ETD" />
                                            <ext:ModelField Name="Status" />
                                        </Fields>
								    </ext:Model>
							    </Model>
				            </ext:Store>
                        </Store>                 
                        <ColumnModel ID="ColumnModel5" runat="server" >
				            <Columns>
                                <ext:TemplateColumn runat="server" MenuDisabled="true" Header="Template" Flex="1" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for=".">
                                                <b>{Nama}</b>
                                                <br /><br />
                                                <img src="images/flown.png" />
				                                <font color="blue"><b>&nbsp;&nbsp;{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br /><br />
			                                </tpl>         
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                {RouteFrom}-{RouteTo}<br />
				                                <font color="blue"><b>{DateOfFlight}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>  
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="80px" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                <font color="green"><b>{Status}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
<%--                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>
                                        <ext:GridCommand Icon="Attach" CommandName="download"> 
                                            <ToolTip Text="download" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="Download_Click" IsUpload="true">
                                            <ExtraParams>
                                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
									        </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>--%>
                            </Columns>
			            </ColumnModel>
<%--                        <DirectEvents>
                            <CellDblClick OnEvent="btn_edit_doubleclick" >
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
                                </ExtraParams>
                            </CellDblClick>
                        </DirectEvents>--%>
                        <BottomBar >
                            <ext:PagingToolbar runat="server" ID="PagingToolbar5"  DisplayInfo="false" DisplayMsg="false" >
                                <Items>
                                    <%--<ext:TextField runat="server" ID="TextField1" Hidden="true" />--%>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
			        </ext:GridPanel>
                </div>

            </div>
        </div>
    </div>
    <div class='row'>
        <div class="col-md-4 connectedSortable">
            <div class="box box-primary" style="width: 368px;">
                <div class="box-header with-border">
                    <div class="box-tools pull-right" >
                        <h3 class="box-title"><b class="text-red">Interoffice Memo&nbsp;&nbsp;</b></h3>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <!--<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                    </div>
                </div><!-- /.box-header -->
                                <div class="box-body">
                    <ext:GridPanel id="grdInterOffice" runat="server" Frame="false" Height="240px" Width="345px" Header="false" Border="false" Cls="x-grid-custom" HideHeaders="true"    >
                        <Store>
				            <ext:Store ID="Store_InterOffice" runat="server" PageSize="3" OnReadData="Store_InterOffice_RefreshData" >
							    <Model>
								    <ext:Model ID="Model6" runat="server" IDProperty="BookingCode" >
									    <Fields>
                                            <ext:ModelField Name="Nama" />
                                            <ext:ModelField Name="BookingCode" />
                                            <ext:ModelField Name="DateOfFlight" />
										    <ext:ModelField Name="RouteFrom" />
										    <ext:ModelField Name="RouteTo" />
                                            <ext:ModelField Name="ETD" />
                                            <ext:ModelField Name="Status" />
                                        </Fields>
								    </ext:Model>
							    </Model>
				            </ext:Store>
                        </Store>                 
                        <ColumnModel ID="ColumnModel6" runat="server" >
				            <Columns>
                                <ext:TemplateColumn runat="server" MenuDisabled="true" Header="Template" Flex="1" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for=".">
                                                <b>{Nama}</b>
                                                <br /><br />
                                                <img src="images/flown.png" />
				                                <font color="blue"><b>&nbsp;&nbsp;{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br /><br />
			                                </tpl>         
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                {RouteFrom}-{RouteTo}<br />
				                                <font color="blue"><b>{DateOfFlight}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>  
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="80px" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                <font color="green"><b>{Status}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
<%--                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>
                                        <ext:GridCommand Icon="Attach" CommandName="download"> 
                                            <ToolTip Text="download" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="Download_Click" IsUpload="true">
                                            <ExtraParams>
                                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
									        </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>--%>
                            </Columns>
			            </ColumnModel>
<%--                        <DirectEvents>
                            <CellDblClick OnEvent="btn_edit_doubleclick" >
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
                                </ExtraParams>
                            </CellDblClick>
                        </DirectEvents>--%>
                        <BottomBar >
                            <ext:PagingToolbar runat="server" ID="PagingToolbar6"  DisplayInfo="false" DisplayMsg="false" >
                                <Items>
                                    <%--<ext:TextField runat="server" ID="TextField1" Hidden="true" />--%>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
			        </ext:GridPanel>
                </div>

            </div>
        </div>

        <div class="col-md-4 connectedSortable">
            <div class="box box-primary" style="width: 368px;">
                <div class="box-header with-border">
                    <div class="box-tools pull-right" >
                        <h3 class="box-title"><b class="text-red">Timesheet&nbsp;&nbsp;</b></h3>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <!--<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <ext:GridPanel id="grdTimesheet" runat="server" Frame="false" Height="240px" Width="345px" Header="false" Border="false" Cls="x-grid-custom" HideHeaders="true"    >
                        <Store>
				            <ext:Store ID="Store_Timesheet" runat="server" PageSize="3" OnReadData="Store_Timesheet_RefreshData" >
							    <Model>
								    <ext:Model ID="Model7" runat="server" IDProperty="BookingCode" >
									    <Fields>
                                            <ext:ModelField Name="Nama" />
                                            <ext:ModelField Name="BookingCode" />
                                            <ext:ModelField Name="DateOfFlight" />
										    <ext:ModelField Name="RouteFrom" />
										    <ext:ModelField Name="RouteTo" />
                                            <ext:ModelField Name="ETD" />
                                            <ext:ModelField Name="Status" />
                                        </Fields>
								    </ext:Model>
							    </Model>
				            </ext:Store>
                        </Store>                 
                        <ColumnModel ID="ColumnModel7" runat="server" >
				            <Columns>
                                <ext:TemplateColumn runat="server" MenuDisabled="true" Header="Template" Flex="1" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for=".">
                                                <b>{Nama}</b>
                                                <br /><br />
                                                <img src="images/flown.png" />
				                                <font color="blue"><b>&nbsp;&nbsp;{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br /><br />
			                                </tpl>         
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                {RouteFrom}-{RouteTo}<br />
				                                <font color="blue"><b>{DateOfFlight}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>  
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="80px" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                <font color="green"><b>{Status}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
<%--                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>
                                        <ext:GridCommand Icon="Attach" CommandName="download"> 
                                            <ToolTip Text="download" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="Download_Click" IsUpload="true">
                                            <ExtraParams>
                                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
									        </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>--%>
                            </Columns>
			            </ColumnModel>
<%--                        <DirectEvents>
                            <CellDblClick OnEvent="btn_edit_doubleclick" >
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
                                </ExtraParams>
                            </CellDblClick>
                        </DirectEvents>--%>
                        <BottomBar >
                            <ext:PagingToolbar runat="server" ID="PagingToolbar7"  DisplayInfo="false" DisplayMsg="false" >
                                <Items>
                                    <%--<ext:TextField runat="server" ID="TextField1" Hidden="true" />--%>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
			        </ext:GridPanel>
                </div>

            </div>
        </div>

        <div class="col-md-4 connectedSortable">
            <div class="box box-primary" style="width: 368px;">
                <div class="box-header with-border">
                    <div class="box-tools pull-right" >
                        <h3 class="box-title"><b class="text-red">Earning Slip&nbsp;&nbsp;</b></h3>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <!--<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <ext:GridPanel id="grdEarningSlip" runat="server" Frame="false" Height="240px" Width="345px" Header="false" Border="false" Cls="x-grid-custom" HideHeaders="true"    >
                        <Store>
				            <ext:Store ID="Store_EarningSlip" runat="server" PageSize="3" OnReadData="Store_EarningSlip_RefreshData" >
							    <Model>
								    <ext:Model ID="Model8" runat="server" IDProperty="BookingCode" >
									    <Fields>
                                            <ext:ModelField Name="Nama" />
                                            <ext:ModelField Name="BookingCode" />
                                            <ext:ModelField Name="DateOfFlight" />
										    <ext:ModelField Name="RouteFrom" />
										    <ext:ModelField Name="RouteTo" />
                                            <ext:ModelField Name="ETD" />
                                            <ext:ModelField Name="Status" />
                                        </Fields>
								    </ext:Model>
							    </Model>
				            </ext:Store>
                        </Store>                 
                        <ColumnModel ID="ColumnModel8" runat="server" >
				            <Columns>
                                <ext:TemplateColumn runat="server" MenuDisabled="true" Header="Template" Flex="1" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for=".">
                                                <b>{Nama}</b>
                                                <br /><br />
                                                <img src="images/flown.png" />
				                                <font color="blue"><b>&nbsp;&nbsp;{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br /><br />
			                                </tpl>         
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                {RouteFrom}-{RouteTo}<br />
				                                <font color="blue"><b>{DateOfFlight}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>  
                                <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="80px" >
	                                <Template runat="server">
		                                <Html>
			                                <tpl for="."><br />
				                                <font color="green"><b>{Status}</b></font><br />
			                                </tpl>
		                                </Html>
	                                </Template>
                                </ext:TemplateColumn>
<%--                                <ext:CommandColumn ID="CommandColumn1" runat="server" Width="40">
                                    <Commands>
                                        <ext:GridCommand Icon="Attach" CommandName="download"> 
                                            <ToolTip Text="download" />
                                        </ext:GridCommand>
                                    </Commands>
                                    <DirectEvents>
                                        <Command OnEvent="Download_Click" IsUpload="true">
                                            <ExtraParams>
                                                <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
									        </ExtraParams>
                                        </Command>
                                    </DirectEvents>
                                </ext:CommandColumn>--%>
                            </Columns>
			            </ColumnModel>
<%--                        <DirectEvents>
                            <CellDblClick OnEvent="btn_edit_doubleclick" >
                                <EventMask ShowMask="true" />
                                <ExtraParams>
                                    <ext:Parameter Name="Values" Value="Ext.encode(#{grdToday}.getRowsValues({selectedOnly : true}))" Mode="Raw" />
                                </ExtraParams>
                            </CellDblClick>
                        </DirectEvents>--%>
                        <BottomBar >
                            <ext:PagingToolbar runat="server" ID="PagingToolbar8"  DisplayInfo="false" DisplayMsg="false" >
                                <Items>
                                    <%--<ext:TextField runat="server" ID="TextField1" Hidden="true" />--%>
                                </Items>
                            </ext:PagingToolbar>
                        </BottomBar>
			        </ext:GridPanel>
                </div>

            </div>
        </div>
    </div>
</asp:Content>