﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SmartLibraries.DataAccessLayer;
using NewERDM.lib.BusinessClass;
using Ext.Net;
using System.Text;
using NewERDM.lib.Helper;
using SpreadsheetLight;


namespace NewERDM
{
    public partial class Dashboard_itenary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getBookingStatus_waitinglist();
            getBookingStatus();
            getBookingStatus_paidprocess();
            txtDateNow.Text = DateTime.Today.ToString("ddd, dd-MMM-yyyy").Trim();
        }
        protected void Store_waiting_list_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getBookingStatus_waitinglist();
        }
        protected void Store_Notif_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getBookingStatus();
        }
        protected void Store_paid_process_RefreshData(object sender, StoreReadDataEventArgs e)
        {
            getBookingStatus_paidprocess();
        }
        protected void getBookingStatus_waitinglist()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getStatusBooking_waitinglist();
            Store_waiting_list.DataSource = dt;
            Store_waiting_list.DataBind();
        }
        protected void getBookingStatus()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getStatusBooking();
            Store_Notif.DataSource = dt;
            Store_Notif.DataBind();
        }
        protected void getBookingStatus_paidprocess()
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getStatusBooking_paidprocess();
            Store_paid_process_rev.DataSource = dt;
            Store_paid_process_rev.DataBind();
        }
        protected void btnNew_click(object sender, DirectEventArgs e)
        {
            X.Redirect("Admin/Itenary/Itenary_form.aspx", "Loading Page ...");
        }
        protected void btn_edit_doubleclick(object sender, DirectEventArgs e)
        {
            String jsonSelectedRows = e.ExtraParams["Values"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);
            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        //     getBookingDetail_Update(row["BookingCode"]);

                        //    getDetailBooking();

                        //  redirect ke itenary entri

                        X.Redirect("~/Admin/Itenary/Itenary_form.aspx?bookingCode=" + row["BookingCode"]);


                    }
                    catch
                    {

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "ERROR",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.ERROR
                        });
                    }

                }
            }
        }
        protected void ToExcelWaiting(object sender, EventArgs e)
        {
            ExportToExcelWaiting((DataTable)ViewState["grdWaitingList"]);
        }
        protected void ToExcelToday(object sender, EventArgs e)
        {
            ExportToExcelToday((DataTable)ViewState["grdToday"]);
        }
        protected void ToExcelPayment(object sender, EventArgs e)
        {
            ExportToExcelPayment((DataTable)ViewState["grdPayment"]);
        }
        public void ExportToExcelWaiting(DataTable dt)
        {
            // "Response" refers to Page.Response, an HttpResponse class
            SLDocument sl = new SLDocument();
            var objget = new clsItenary();
            dt = objget.getExportDashboard_WaitingList();
            Store_waiting_list.DataSource = dt;

            if (dt.Rows.Count > 0)
            {
                //Header
                sl.SetCellValue(1, 1, "Nama");
                sl.SetCellValue(1, 2, "Date Of Flight");
                sl.SetCellValue(1, 3, "Flight Detail");
                sl.SetCellValue(1, 4, "Booking Code");
                sl.SetCellValue(1, 5, "Route From");
                sl.SetCellValue(1, 6, "Route To");
                sl.SetCellValue(1, 7, "Status");
                sl.SetCellValue(1, 8, "Time Limit");

                //Color Header
                sl.ApplyNamedCellStyle(1, 1, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 2, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 3, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 4, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 5, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 6, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 7, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 8, SLNamedCellStyleValues.Accent1);


                //Counter
                int iRows = 0;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    iRows = 0 + i;

                    sl.SetCellValue(2 + iRows, 1, dt.Rows[i][0].ToString());
                    sl.SetCellValue(2 + iRows, 2, dt.Rows[i][1].ToString());
                    sl.SetCellValue(2 + iRows, 3, dt.Rows[i][2].ToString());
                    sl.SetCellValue(2 + iRows, 4, dt.Rows[i][3].ToString());
                    sl.SetCellValue(2 + iRows, 5, dt.Rows[i][4].ToString());
                    sl.SetCellValue(2 + iRows, 6, dt.Rows[i][5].ToString());
                    sl.SetCellValue(2 + iRows, 7, dt.Rows[i][6].ToString());
                    sl.SetCellValue(2 + iRows, 8, dt.Rows[i][7].ToString());
                }


                //Auto Fit Column
                sl.AutoFitColumn(1);
                sl.AutoFitColumn(2);
                sl.AutoFitColumn(3);
                sl.AutoFitColumn(4);
                sl.AutoFitColumn(5);
                sl.AutoFitColumn(6);
                sl.AutoFitColumn(7);
                sl.AutoFitColumn(8);


                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=WaitingFlightExcel.xlsx");
                sl.SaveAs(Response.OutputStream);
                Response.End();
            }
        }
        public void ExportToExcelToday(DataTable dt)
        {
            SLDocument sl = new SLDocument();

            var objget = new clsItenary();
            dt = objget.getExportDashboard_TodayFlight();
            Store_Notif.DataSource = dt;

            if (dt.Rows.Count > 0)
            {
                //Header
                sl.SetCellValue(1, 1, "Nama");
                sl.SetCellValue(1, 2, "Date Of Flight");
                sl.SetCellValue(1, 3, "Flight Detail");
                sl.SetCellValue(1, 4, "Booking Code");
                sl.SetCellValue(1, 5, "Route From");
                sl.SetCellValue(1, 6, "Route To");
                sl.SetCellValue(1, 7, "Status");
                sl.SetCellValue(1, 8, "Time Limit");

                //Color Header
                sl.ApplyNamedCellStyle(1, 1, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 2, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 3, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 4, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 5, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 6, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 7, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 8, SLNamedCellStyleValues.Accent1);


                //Counter
                int iRows = 0;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    iRows = 0 + i;

                    sl.SetCellValue(2 + iRows, 1, dt.Rows[i][0].ToString());
                    sl.SetCellValue(2 + iRows, 2, dt.Rows[i][1].ToString());
                    sl.SetCellValue(2 + iRows, 3, dt.Rows[i][2].ToString());
                    sl.SetCellValue(2 + iRows, 4, dt.Rows[i][3].ToString());
                    sl.SetCellValue(2 + iRows, 5, dt.Rows[i][4].ToString());
                    sl.SetCellValue(2 + iRows, 6, dt.Rows[i][5].ToString());
                    sl.SetCellValue(2 + iRows, 7, dt.Rows[i][6].ToString());
                    sl.SetCellValue(2 + iRows, 8, dt.Rows[i][7].ToString());
                }


                //Auto Fit Column
                sl.AutoFitColumn(1);
                sl.AutoFitColumn(2);
                sl.AutoFitColumn(3);
                sl.AutoFitColumn(4);
                sl.AutoFitColumn(5);
                sl.AutoFitColumn(6);
                sl.AutoFitColumn(7);
                sl.AutoFitColumn(8);


                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=TodayFlightExcel.xlsx");
                sl.SaveAs(Response.OutputStream);
                Response.End();
            }
        }
        public void ExportToExcelPayment(DataTable dt)
        {
            SLDocument sl = new SLDocument();

            var objget = new clsItenary();
            dt = objget.getExportDashboard_Paidprosess();
            Store_paid_process_rev.DataSource = dt;

            if (dt.Rows.Count > 0)
            {
                //Header
                sl.SetCellValue(1, 1, "Nama");
                sl.SetCellValue(1, 2, "Date Of Flight");
                sl.SetCellValue(1, 3, "Flight Detail");
                sl.SetCellValue(1, 4, "Booking Code");
                sl.SetCellValue(1, 5, "Route From");
                sl.SetCellValue(1, 6, "Route To");
                sl.SetCellValue(1, 7, "Status");
                sl.SetCellValue(1, 8, "Time Limit");

                //Color Header
                sl.ApplyNamedCellStyle(1, 1, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 2, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 3, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 4, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 5, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 6, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 7, SLNamedCellStyleValues.Accent1);
                sl.ApplyNamedCellStyle(1, 8, SLNamedCellStyleValues.Accent1);


                //Counter
                int iRows = 0;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    iRows = 0 + i;

                    sl.SetCellValue(2 + iRows, 1, dt.Rows[i][0].ToString());
                    sl.SetCellValue(2 + iRows, 2, dt.Rows[i][1].ToString());
                    sl.SetCellValue(2 + iRows, 3, dt.Rows[i][2].ToString());
                    sl.SetCellValue(2 + iRows, 4, dt.Rows[i][3].ToString());
                    sl.SetCellValue(2 + iRows, 5, dt.Rows[i][4].ToString());
                    sl.SetCellValue(2 + iRows, 6, dt.Rows[i][5].ToString());
                    sl.SetCellValue(2 + iRows, 7, dt.Rows[i][6].ToString());
                    sl.SetCellValue(2 + iRows, 8, dt.Rows[i][7].ToString());
                }


                //Auto Fit Column
                sl.AutoFitColumn(1);
                sl.AutoFitColumn(2);
                sl.AutoFitColumn(3);
                sl.AutoFitColumn(4);
                sl.AutoFitColumn(5);
                sl.AutoFitColumn(6);
                sl.AutoFitColumn(7);
                sl.AutoFitColumn(8);


                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=PaymentFlightExcel.xlsx");
                sl.SaveAs(Response.OutputStream);
                Response.End();
            }
        }
        protected void Download_Click(object sender, DirectEventArgs e)
        {
            string jsonValues = e.ExtraParams["Values"];
            List<Dictionary<string, string>> records = JSON.Deserialize<List<Dictionary<string, string>>>(jsonValues);

            //Validasi, jika user tidak melakukan click baris
            if (records.Count == 0)
            {
                X.MessageBox.Show(new MessageBoxConfig
                {
                    Title = "WARNING",
                    Message = "Please selected row !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING
                });
                return;
            }



            String jsonSelectedRows = e.ExtraParams["Values"];
            StringBuilder sBuilderdelete;
            Dictionary<string, string>[] selectedRowData = JSON.Deserialize<Dictionary<string, string>[]>(jsonSelectedRows);

            if (selectedRowData.Length > 0)
            {
                sBuilderdelete = new StringBuilder();
                foreach (Dictionary<string, string> row in selectedRowData)
                {
                    try
                    {
                        foreach (var record in records)
                        {
                            DataTable dtFilePDF = new DataTable();
                            var objGetFilePDF = new clsDashBoard();
                            dtFilePDF = objGetFilePDF.getUploadFilePDF(record["BookingCode"].ToString());

                            if (dtFilePDF.Rows.Count > 0)
                            {
                                this.upload.Text = dtFilePDF.Rows[0]["FilePDF"].ToString();
                                
                                if(this.upload.Text == "")
                                {
                                    X.MessageBox.Show(new MessageBoxConfig
                                    {
                                        Title = "WARNING",
                                        Message = "File PDF Not Found !",
                                        Buttons = MessageBox.Button.OK,
                                        Icon = MessageBox.Icon.WARNING
                                    });

                                    return;
                                }
                                else
                                {
                                    getDownload(row["BookingCode"]);
                                }
                            }
                        }
                    }
                    catch
                    {

                        X.MessageBox.Show(new MessageBoxConfig
                        {
                            Title = "WARNING",
                            Message = "ERROR",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.ERROR
                        });
                    }

                }
            }
        }
        protected void getDownload(string idBooking)
        {
            DataTable dt = null;
            var objget = new clsItenary();
            dt = objget.getDownloadFile(idBooking);
            Store_Notif.DataSource = dt;

            //SqlDataReader dr = dt.

            if (dt.Rows.Count > 0)
            {
                string pathUploadFile = dt.Rows[0]["pathUpload"].ToString();
                string nameFile = dt.Rows[0]["upload"].ToString();
                string pathFile = pathUploadFile + "\\" + nameFile;
                Response.ContentType = "Application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=" + dt.Rows[0]["upload"].ToString());
                //this.EnableViewState = false;
                Response.WriteFile(pathFile);
                Response.End();
            }
            this.grdToday.Refresh();
            this.grdWaitingList.Refresh();
            this.grdPayment.Refresh();
            //Store_Notif.Reload();
            //Store_paid_process_rev.Reload();
            //Store_waiting_list.Reload();
            //Store_chart.Reload();
        }
        
    }
}