﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default_temp.aspx.cs" Inherits="NewERDM.Default_temp" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
    
    
    <!-- Morris chart -->
    <link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />



    <!-- Theme style -->
   <link href="plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />

    <link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    
    
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
  

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src=/js/html5shiv.js"></script>
        <script src="/js/respond.min.js"></script>
    <![endif]-->
    <style>
        .x-grid-custom  {
            background   : repeat-x scroll 0 0 #FFFFFF;
            border-color : #fffff;
            border-style : none;
            border:none;
        }
        
         
        }
    </style>
  </head>
 <body class="skin-blue">
   <form id="form1" runat="server">
   <ext:ResourceManager ID="ResourceManager1" runat="server"   />
  
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo"><b>ERDM</b></a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 4 messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- start message -->
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
                          </div>
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li><!-- end message -->
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="user image"/>
                          </div>
                          <h4>
                            AdminLTE Design Team
                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="user image"/>
                          </div>
                          <h4>
                            Developers
                            <small><i class="fa fa-clock-o"></i> Today</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="user image"/>
                          </div>
                          <h4>
                            Sales Department
                            <small><i class="fa fa-clock-o"></i> Yesterday</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="user image"/>
                          </div>
                          <h4>
                            Reviewers
                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-danger">10</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 10 notifications</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-red"></i> 5 new members joined
                        </a>
                      </li>

                      <li>
                        <a href="#">
                          <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li>
            
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                  <span class="hidden-xs">Admin</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                    <p>
                      Alexander Pierce - Web Developer
                      <small>Member since Nov. 2012</small>
                    </p>
                  </li>
                 
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="#" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Master Lookup</span>
              </a>
              <ul class="treeview-menu">
                 <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i>Accomodation</a></li>
                <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i>Airline</a></li>
                <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i>Booking Type</a></li>
                <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i>Flight Status</a></li>
                <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i>Justification</a></li>
                <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i>Payment Status</a></li>
                <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i>POH</a></li>
                <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i>Sponsor</a></li>
                <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i>Transportation</a></li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Settings</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i>My Profile</a></li>
                <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i>My Task</a></li>
                <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i>My Schedulle</a></li>
              </ul>
            </li>
             
          
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        

        <!-- Main content -->
        <section class="content">
        
        
     <div class='row'> 
           <section class="col-lg-4 connectedSortable">
              <div class="box box-warning">
                <div class="box-header with-border">
                   <div class="box-tools pull-right">
                    <h3 class="box-title"><b class="text-yellow">Waiting List Flight&nbsp;&nbsp;</b></h3>
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                          <ext:GridPanel id="grdWaitingList" runat="server" Frame="false"   Height="360px"   Header="false"  Border="false" Cls="x-grid-custom" HideHeaders="true"    >
                                    <Store>
				                        <ext:Store ID="Store_waiting_list" runat="server" PageSize="7" OnReadData="Store_waiting_list_RefreshData">
						                    <Model>
							                    <ext:Model ID="Model3" runat="server"  IDProperty="BookingCode"  OnReadData="Store_waiting_list_RefreshData" >
								                    <Fields>
                                                        <ext:ModelField Name="Nama" />
                                                        <ext:ModelField Name="BookingCode" />
                                                        <ext:ModelField Name="DateOfFlight" />
										                <ext:ModelField Name="RouteFrom"  />
										                <ext:ModelField Name="RouteTo"  />
                                                        <ext:ModelField Name="ETD"  />
                                                        <ext:ModelField Name="Status"  />
                                                    </Fields>
							                    </ext:Model>
						                    </Model>
				                        </ext:Store>
                                    </Store>                 
                                    <ColumnModel  ID="ColumnModel2"  runat="server" >
				                        <Columns>
                                        <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="30"   >
	                                         <Template runat="server">
		                                         <Html>
                                                     <img src="images/seat.png" />
			                                      </Html>
	                                         </Template>
                                         </ext:TemplateColumn>
                                        <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Flex="1"  >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
				                                         {Nama}<br />
				                                         <font color="blue"><b>{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br />

			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>
                                           <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
				                                         {RouteFrom}-{RouteTo}<br />
				                                         <font color="red"><b>{DateOfFlight}</b></font><br />

			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>  
                                        </Columns>
			                        </ColumnModel>
                    
                                                    
                                    <BottomBar >
                                        <ext:PagingToolbar runat="server" ID="paging1"  DisplayInfo="false" DisplayMsg="false" >
                                           <Items>
                                                <ext:Button runat="server" ID="btn_exp"  Html="<b>EXPORT</b>" Cls="btn btn-warning btn-flat" /> 
                                           </Items>
                                        </ext:PagingToolbar>
                                    </BottomBar>
                                    
				                </ext:GridPanel>
                </div><!-- /.box-body -->
               </div> 
              
            </section>
           <section class="col-lg-8 connectedSortable">
            <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Date range"><i class="fa fa-calendar"></i></button>
                    <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->

                  <i class="fa fa-map-marker"></i>
                  <h3 class="box-title">
                    Itenary
                  </h3>
                </div>
                <div class="box-body">
                  <div id="world-map" style="height: 250px; width: 100%;"></div>
                </div><!-- /.box-body-->
                <div class="box-footer no-border">
                  <div class="row">
                    <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                      <div id="sparkline-1"></div>
                      <div class="knob-label">Visitors</div>
                    </div><!-- ./col -->
                    <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                      <div id="sparkline-2"></div>
                      <div class="knob-label">Online</div>
                    </div><!-- ./col -->
                    <div class="col-xs-4 text-center">
                      <div id="sparkline-3"></div>
                      <div class="knob-label">Exists</div>
                    </div><!-- ./col -->
                  </div><!-- /.row -->
                </div>
              </div>
              <!-- /.box -->
</section>
     </div>    
     <div class='row'>         
           <div class="col-md-4 connectedSortable">
              <div class="box box-primary">
                <div class="box-header with-border">
                  
                  <div class="box-tools pull-right">
                      <b><ext:Label runat="server" ID="txtDateNow" Width="160px" /></b>
                    <h3 class="box-title"><b class="text-red">Today Flight&nbsp;&nbsp;</b></h3>
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                       <ext:GridPanel id="grdToday" runat="server" Frame="false"   Height="240px"   Header="false"  Border="false" Cls="x-grid-custom" HideHeaders="true"    >
                                    <Store>
				                          <ext:Store ID="Store_Notif" runat="server" PageSize="3" OnReadData="Store_Notif_RefreshData" >
							                    <Model>
								                    <ext:Model ID="Model1" runat="server"  IDProperty="BookingCode" >
									                    <Fields>
                                                            <ext:ModelField Name="Nama" />
                                                            <ext:ModelField Name="BookingCode" />
                                                            <ext:ModelField Name="DateOfFlight" />
										                    <ext:ModelField Name="RouteFrom"  />
										                    <ext:ModelField Name="RouteTo"  />
                                                            <ext:ModelField Name="ETD"  />
                                                            <ext:ModelField Name="Status"  />
                                                        </Fields>
								                    </ext:Model>
							                    </Model>
				                            </ext:Store>
                                    </Store>                 
                                    <ColumnModel  ID="ColumnModel1"  runat="server" >
				                        <Columns>
                                          <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template"  Width="180px" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
                                                         <b>{Nama}</b>
                                                         <br /><br />
                                                        <img src="images/flown.png" />
				                                         <font color="blue"><b>&nbsp;&nbsp;{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br /><br />
			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>
                                         <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for="."><br />
				                                         {RouteFrom}-{RouteTo}<br />
				                                         <font color="blue"><b>{DateOfFlight}</b></font><br />

			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>  
                                        <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="30px" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
				                                         <br />
				                                         <font color="green"><b>{Status}</b></font><br />

			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>  
                                        </Columns>
			                        </ColumnModel>
                    
                                                    
                                    <BottomBar >
                                        <ext:PagingToolbar runat="server" ID="PagingToolbar1"  DisplayInfo="false" DisplayMsg="false" >
                                           <Items>
                                        
                                           </Items>
                                        </ext:PagingToolbar>
                                               
                                    </BottomBar>
                                    
				                </ext:GridPanel>
                   <br /> 
                        <table style="align-content:flex-end">
                            <tr>
                                <td> <ext:Button runat="server" ID="btnNew" Cls="btn btn-success pull-right"  Html="<b>Add New</b>" /></td>
                                <td>&nbsp;</td>
                                <td>  <ext:Button runat="server" ID="btnExport2" Cls="btn btn-primary pull-right"  Html="<b>Export&nbsp;&nbsp;&nbsp;&nbsp;</b>" /></td>   
                            </tr>
                        </table>
                    
              
                      </div><!-- /.box-body -->
                
              </div><!-- /.box -->
            </div>
           <div class="col-md-4 connectedSortable">
              <div class="box box-success">
                <div class="box-header with-border">
                  <div class="box-tools pull-right">
                    <h3 class="box-title"><b class="text-red">Payment In Progress</b>&nbsp;&nbsp;</h3>
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <ext:GridPanel id="grdPayment" runat="server" Frame="false"   Height="290px"   Header="false"  Border="false" Cls="x-grid-custom" HideHeaders="true"    >
                                   <Store>
				                            <ext:Store ID="Store_paid_process_rev" runat="server" PageSize="4" OnReadData="Store_paid_process_RefreshData">
                                                <Model>
								                    <ext:Model ID="Model2" runat="server"  IDProperty="BookingCode" >
									                    <Fields>
                                                            <ext:ModelField Name="Nama" />
                                                            <ext:ModelField Name="BookingCode" />
                                                            <ext:ModelField Name="DateOfFlight" />
										                    <ext:ModelField Name="RouteFrom"  />
										                    <ext:ModelField Name="RouteTo"  />
                                                            <ext:ModelField Name="PaymentStatus"  />
                                                            <ext:ModelField Name="ETD"  />
                                                        </Fields>
								                    </ext:Model>
							                    </Model>
				                            </ext:Store>
                                        </Store>    
                                    <ColumnModel  ID="ColumnModel3"  runat="server" >
				                        <Columns>
                                          <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template"  Width="180px" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
                                                         <b>{Nama}</b>
                                                         <br /><br />
                                                        <img src="images/payment.png" />
				                                         <font color="blue"><b>&nbsp;&nbsp;{BookingCode}</b></font>&nbsp;&nbsp;{ETD}<br /><br />
			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>
                                         <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for="."><br />
                                                         {DateOfFlight}
				                                         <br />
				                                         <font color="blue"><b>{RouteFrom}-{RouteTo}</b></font><br />

			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>  
                                        <ext:TemplateColumn runat="server"  MenuDisabled="true" Header="Template" Width="30px" >
	                                         <Template runat="server">
		                                         <Html>
			                                         <tpl for=".">
				                                         <br />
				                                         <font color="green"><b>{Status}</b></font><br />

			                                         </tpl>
                                                     
		                                         </Html>
	                                         </Template>
                                         </ext:TemplateColumn>  
                                        </Columns>
			                        </ColumnModel>
                    
                                                    
                                    <BottomBar >
                                        <ext:PagingToolbar runat="server" ID="PagingToolbar2"  DisplayInfo="false" DisplayMsg="false" >
                                           <Items>
                                              <ext:Button runat="server" ID="Button1" Html="<b>EXPORT</b>" Cls="btn btn-danger" /> 
                                           </Items>
                                        </ext:PagingToolbar>
                                               
                                    </BottomBar>
                                    
				                </ext:GridPanel>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>


           <div class="col-md-4 connectedSortable">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title"><b>Flight Chart</b></h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />
                      <br />

                      <br />
                      <br />
                      <br />
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
                
              </div><!-- /.box -->
            </div>

     </div>
              
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.0
        </div>
        <strong>Copyright &copy; 2015 <a href="http://www.abhimantras.com/">Abhimantra Solusindo</a>.</strong> All rights reserved.
      </footer>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->
    <script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>

    <!-- jQuery UI 1.11.2 -->
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>

    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='plugins/fastclick/fastclick.min.js'></script>
    
       <!-- Morris.js charts -->
    <script src="js/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js" type="text/javascript"></script>
       
     <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>

 <script src="plugins/knob/jquery.knob.js" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="plugins/chartjs/Chart.min.js" type="text/javascript"></script>




        <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='plugins/fastclick/fastclick.min.js'></script>



   <script src="js/app.min.js" type="text/javascript"></script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="js/dashboard.js" type="text/javascript"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="js/demo.js" type="text/javascript"></script>

       </form>
  </body>
</html>