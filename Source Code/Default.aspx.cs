﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ext.Net;
using System.Data;
using NewERDM.lib.BusinessClass;
using NewERDM.lib.ObjectModel;
using NewERDM.lib.Helper;

namespace NewERDM
{
    public partial class Default : System.Web.UI.Page
    {

        #region Field

        UserApplicationBL usrApplicationBL = new UserApplicationBL();
        UserApplication usr;
        #endregion



        protected void Page_Load(object sender, EventArgs e)
        {
       
        }


        protected void btn_login_Click(Object sender, DirectEventArgs e)
        {
            try
            {
                // string  strPassword = CommonUtility.encryptText(this.txtPassword.Text);
                string strPassword = this.txtPassword.Value;
                usr = usrApplicationBL.getUserApplicationByUserNamePassword(this.txtUsername.Value.Trim(), strPassword);
                if (usr != null)
                {
                    Session["ses_user"] = usr;
                    if(usr.GroupID == "ADMIN")
                    {
                        X.Redirect("Dashboard_itenary.aspx", "Loading ... ");
                    }
                    else if (usr.GroupID == "USER")
                    {
                        X.Redirect("Dashboard_User.aspx", "Loading ... ");
                    }
                    else if (usr.GroupID == "SUPERADMIN" || usr.GroupID == "DATABASE")
                    {
                        //Masih Contoh untuk yang groupid SUPERADMIN dan DATABASE
                        X.Redirect("Dashboard_itenary.aspx", "Loading ... ");
                    }

                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Peringatan",
                        Message = "User Name Atau Password Salah !",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        AnimEl = this.btn_login.ClientID

                    });

                }

            }
            catch (Exception ex)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Title = "Peringatan",
                    Message = "Kesalahan Koneksi !",
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.ERROR,
                    AnimEl = this.btn_login.ClientID

                });
            }
        }
    }
}