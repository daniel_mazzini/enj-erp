﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using NewERDM.lib.BusinessClass;
using NewERDM.lib.ObjectModel;


namespace NewERDM.lib.Helper
{
    public class UserApplicationBL
    {
        public UserApplication getUserApplicationByUserNamePassword(String strUserName, string strPassword)
        {
            DataTable dt = null;
            UserApplication usr = null;
            try
            {

                dt = getLogin(strUserName, strPassword);
                if (dt.Rows.Count > 0)
                {
                    usr = new UserApplication();
                    usr.UserID = dt.Rows[0]["UserID"].ToString();
                    usr.UserName = dt.Rows[0]["UserName"].ToString();
                    usr.EmployeeID = dt.Rows[0]["EmployeeID"].ToString();
                    usr.jabatan = dt.Rows[0]["jabatan"].ToString();
                    usr.GroupID = dt.Rows[0]["GroupID"].ToString();
               }



                return usr;


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //public UserApplication getUserApplicationByUserName(String strUserName1)
        //{
        //    DataTable dt = null;
        //    UserApplication usr = null;
        //    try
        //    {

        //        dt = getLogin1(strUserName1);
        //        if (dt.Rows.Count > 0)
        //        {
        //            usr = new UserApplication();
        //            usr.UserID = dt.Rows[0]["UserID"].ToString();
        //            usr.UserName = dt.Rows[0]["UserName"].ToString();
        //        }



        //        return usr;


        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}
        DataTable getLogin(string strUserName, string strPassword)
        {
            DataTable r_dt = new DataTable();
            var objGet = new clsLogin();
            return r_dt = objGet.getLogin(strUserName, strPassword);
         }
        //DataTable getLogin1(string strUserName)
        //{
        //    DataTable r_dt = new DataTable();
        //    var objGet = new clsLogin();
        //    return r_dt = objGet.getLogin1(strUserName);
        //}
  
    }
}