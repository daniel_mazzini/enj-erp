﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewERDM.lib.ObjectModel
{
    public class UserApplication
    {
        #region Field
        
                private string _UserID;
                private string _UserName;
                private string _lastlogin;
                private string _GroupID;
                private string _jabatan;
                private string _deptcode;
                private string _costcenterDesc;
                private string _deptname;
                private string _costcenterCode;
                private string _lokasi;
                private string _isApprover;
                private string _LocCode;
                private string _EmployeeID; 

                private Boolean _isLogistic;
            
            
        
        #endregion

        #region Properties

        public string LocCode
                {
                    get { return _LocCode; }
                    set { _LocCode = value; }
                }

        public Boolean isLogistic 
        {
            get { return _isLogistic; }
            set { _isLogistic = value; }
        }

        public string costcenterCode
        {
            get { return _costcenterCode; }
            set { _costcenterCode = value; }
        }
        public string isApprover
        {
            get { return _isApprover; }
            set { _isApprover = value; }
        }
        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }

        public String lastlogin
        {
            get { return _lastlogin; }
            set { _lastlogin = value; }
        }
        public String UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        public string GroupID
        {
            get { return _GroupID; }
            set { _GroupID = value; }
        }
        public string lokasi
        {
            get { return _lokasi; }
            set { _lokasi = value; }
        }

        public string jabatan
        {
            get { return _jabatan; }
            set { _jabatan = value; }
        }
  
        public string deptcode
        {
            get { return _deptcode; }
            set { _deptcode = value; }
        }

        public string deptname
        {
            get { return _deptname; }
            set { _deptname = value; }
        }

        public string costcenterName
        {
            get { return _costcenterDesc; }
            set { _costcenterDesc = value; }
        }


        #endregion

    }
}