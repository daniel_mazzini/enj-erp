﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartLibraries.DataAccessLayer;
using System.Data;
using NewERDM.lib.Helper;


namespace NewERDM.lib.BusinessClass
{
    class clsItenary
    {

        public DataTable getDeptByID(string strCode)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("SELECT * FROM VW_GETMST_DEPARTMENT_ID WHERE Code ='{0}'", strCode.Trim());
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookupJustification()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='Justification'";
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookupaccomodation()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='Accomodation'";
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookuptransport()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='Transportation'";
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookupEmployee()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                // Query.Query = "SELECT rtrim(isnull(ID,'')) as 'ID', rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+ ' '+ rtrim(isnull(Last_Name,'')) as 'Name' FROM Employees Where Status='A' order by id";

                //New query
                //1.Dibuatkan table temporary untu menyimpan data employee
                Query.Query = "Select Distinct ISNULL(Name, '') As Name From EmployeeTemp Order By Name Asc";

                return (Query.GetDataTable());
            }
        }

        public DataTable getLookupReceivedBy()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                // Query.Query = "SELECT rtrim(isnull(ID,'')) as 'ID', rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+ ' '+ rtrim(isnull(Last_Name,'')) as 'Name' FROM Employees Where Status='A' order by id";

                //New query
                //1.Dibuatkan table temporary untu menyimpan data employee
                Query.Query = "Select Distinct ISNULL(Name, '') As Name From EmployeeTemp Order By Name Asc";

                return (Query.GetDataTable());
            }
        }

        public void InsertEmployeeTemp(string strName, string strBookingCode, string strUserModified)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Insert Into EmployeeTemp (Name, BookingCode, UserModified) Values ('{0}', '{1}', '{2}')", strName, strBookingCode, strUserModified);

                Query.ExecuteNonQuery();
            }
        }

        public void UpdateEmployeeTemp(string strBookingCode, string strName, string strUserModified)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Update EmployeeTemp Set Name='{1}', UserModified='{2}'  Where BookingCode ='{0}'", strBookingCode, strName, strUserModified);

                Query.ExecuteNonQuery();
            }
        }

        public DataTable getLookupEmployee_SR(string strSearch)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format("SELECT rtrim(isnull(ID,'')) as 'ID', rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Last_Name,'')) as 'Name', Email_Address FROM Employees Where Status='A'  and ID LIKE '%{0}%' OR  rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Last_Name,'')) LIKE '%{0}%' order by id", strSearch.Trim());
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookupEmployeeMailBag()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"SELECT rtrim(isnull(ID,'')) as 'ID', rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name',
                                             Email_Address as 'Email' FROM Employees Where Status='A'");
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookupEmployeeReceivedBy()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"SELECT rtrim(isnull(ID,'')) as 'ID', rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name', 
                                                Email_Address as 'Email' FROM Employees Where Status='A'");
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookupEmployeeSendBy()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"SELECT rtrim(isnull(ID,'')) as 'ID', rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name', 
                                                Email_Address as 'Email' FROM Employees Where Status='A'");
                return (Query.GetDataTable());
            }
        }

        public DataTable getListItenary(DateTime dFlightFirst, DateTime dFlightEnd)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("EXEC GETLISTITENARY @dBegin='{0}',  @dEnd='{1}'", dFlightFirst, dFlightEnd);
             //   Query.Query = "EXEC GETLISTITENARY ";

                return (Query.GetDataTable());
            }
        }


        public DataTable getPOH()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(CityCode) as 'Code', rtrim(CityCode) + '-' + rtrim(CityName) as 'Name' FROM LK_POH";
                return (Query.GetDataTable());
            }
        }

        public DataTable getFlightDetail()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(ETD) As 'Name' From BookingDetail";
                return (Query.GetDataTable());
            }
        }

        
        public DataTable getLookuppayment()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='PAYMENT'";
                return (Query.GetDataTable());
            }
        }
        public DataTable getLookupstatus()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='STATUS'";
                return (Query.GetDataTable());
            }
        }

        public DataTable getChartItenary()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "EXEC get_airline_chart";
                return (Query.GetDataTable());
            }
        }

        public DataTable getStatusBooking()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = @"SELECT B.BookingCode, B.Nama, A.RouteFrom, A.RouteTo, A.ETD, A.Status, convert(char, A.DateOfFlight,106) As 'DateOfFlight'
                                From  BookingDetail A
                                Inner Join Booking B ON A.BookingCode = B.BookingCode
                                Where  cast (A.DateOfFlight as Date) = cast (GETDATE() as DATE) And A.Status='OK'
                                Group By B.BookingCode, B.Nama, A.RouteFrom, A.RouteTo, A.ETD, A.Status, A.DateOfFlight 
                                Order By A.DateOfFlight Desc";
                return (Query.GetDataTable());
            }
        }
        public DataTable getExportDashboard_TodayFlight()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = @"Select A.Nama, convert(char,B.DateOfFlight,106) As 'Date Of Flight', 
                                B.ETD As 'Flight Detail', A.BookingCode As 'Booking Code',
                                B.RouteFrom As 'Route From', B.RouteTo As 'Route To', B.Status,
                                convert(char, B.TimeLimit,106) As 'Time Limit'
                                From Booking A 
                                Inner Join BookingDetail B On A.BookingCode = B.BookingCode
                                WHERE B.DateOfFlight = dateadd(dd, datediff(dd, 0, getdate()), 0) And B.Status='OK'
                                Group By A.BookingCode, A.Nama, B.RouteFrom, B.RouteTo, B.ETD, B.Status, B.DateOfFlight, B.TimeLimit
                                Order By B.DateOfFlight Desc";

                return (Query.GetDataTable());
            }

        }

        public DataTable getStatusBooking_paidprocess()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                //Query.Query = "SELECT BookingCode, RouteFrom, RouteTo, PaymentStatus From  BookingDetail Where DateOfFlight = dateadd(dd, datediff(dd, 0, getdate()), 0) And PaymentStatus='PROCESS'";
                Query.Query = @"SELECT B.BookingCode, B.Nama, A.RouteFrom, A.RouteTo, A.PaymentStatus, convert(char, A.DateOfFlight,106) As 'DateOfFlight',A.ETD  
                                From  BookingDetail A
                                Inner Join Booking B ON A.BookingCode = B.BookingCode
                                Where A.PaymentStatus='PROCESS'
                                Group By B.BookingCode, B.Nama, A.RouteFrom, A.RouteTo, A.PaymentStatus, A.DateOfFlight,A.ETD  
                                Order By A.dAteOfFlight DESC";

                return (Query.GetDataTable());
            }
        }
        public DataTable getExportDashboard_Paidprosess()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = @"Select A.Nama, convert(char, B.DateOfFlight,106) As 'Date Of Flight',
                                B.ETD As 'Flight Detail', A.BookingCode As 'Booking Code',
                                B.RouteFrom As 'Route From', B.RouteTo As 'Route To', B.Status,
                                convert(char, B.TimeLimit,106) As 'Time Limit'
                                From Booking A 
                                Inner Join BookingDetail B On A.BookingCode = B.BookingCode 
                                WHERE B.PaymentStatus='PROCESS'
                                Group By A.BookingCode, A.Nama, B.RouteFrom, B.RouteTo, B.ETD, B.Status, B.DateOfFlight, B.TimeLimit
                                Order By B.DateOfFlight DESC";

                return (Query.GetDataTable());
            }
        }

        public DataTable getStatusBooking_waitinglist()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                //Query.Query = "SELECT BookingCode, RouteFrom, RouteTo, ETD, Status from  BookingDetail Where DateOfFlight = dateadd(dd, datediff(dd, 0, getdate()), 0) and status='WAITINGLIST'";
                Query.Query = @"SELECT B.BookingCode, B.Nama, A.RouteFrom, A.RouteTo, A.ETD, A.Status, convert(char, A.DateOfFlight,106) As 'DateOfFlight'
                                From  BookingDetail A
                                Inner Join Booking B ON A.BookingCode = B.BookingCode
                                Where  year(cast (A.DateOfFlight as Date)) = year(cast (GETDATE() as DATE)) and substring(A.Status,1,7) ='Waiting'  
                                Group By B.BookingCode, B.Nama, A.RouteFrom, A.RouteTo, A.ETD, A.Status, A.DateOfFlight
                                Order By A.DateOfFlight DESC";

                return (Query.GetDataTable());
            }
        }
        public DataTable getExportDashboard_WaitingList()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = @"Select A.Nama, convert(char, B.DateOfFlight,106) As 'Date Of Flight', 
                                B.ETD As 'Flight Detail', A.BookingCode As 'Booking Code',
                                B.RouteFrom As 'Route From', B.RouteTo As 'Route To', B.Status,
                                convert(char, B.TimeLimit,106) As 'Time Limit'
                                From Booking A 
                                Inner Join BookingDetail B On A.BookingCode = B.BookingCode 
                                WHERE B.Status ='WaitingList' or B.Status = 'Waiting List'
                                Group By A.BookingCode, A.Nama, B.RouteFrom, B.RouteTo, B.ETD, B.Status, B.DateOfFlight, B.TimeLimit
                                Order By B.DateOfFlight DESC";

                return (Query.GetDataTable());
            }
        }
        public DataTable get_costcode_category()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT DISTINCT CATEGORY FROM lk_CostCode";
                return (Query.GetDataTable());
            }
        }
        public DataTable get_costcode_activity(string s_category)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format("SELECT DISTINCT ACTIVITY FROM lk_CostCode Where Category='{0}'", s_category);
                return (Query.GetDataTable());
            }
        }
        public DataTable get_costcode_costcode(string strCategory, string strActivity, string strCompany)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format("SELECT DISTINCT COSTCODE FROM lk_CostCode Where Category='{0}' and activity ='{1}' and Client ='{2}'", strCategory.Trim(), strActivity.Trim(), strCompany.Trim());
                return (Query.GetDataTable());
            }
        }
        public DataTable get_costcode_client(string strCategory , string strActivity)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format("SELECT DISTINCT CLIENT FROM lk_CostCode  Where Category='{0}' and Activity ='{1}'", strCategory.Trim(), strActivity.Trim());
                return (Query.GetDataTable());
            }
        }

        public void InsertBooking(string BookingCode, string Nama, string Justification, string Accomodation, string Transportation, string UserModified, string Remark,
            string upload, string pathUpload) 
        {
         string constr = CommonUtility.ConStr();
         using (ISmartQuery Query = new SmartOledbQuery(constr))
         {
             Query.Query = String.Format(@"EXEC INSBOOKING @BookingCode='{0}', @Nama='{1}', @Justification='{2}', @Accomodation='{3}', @tranportation='{4}',
                                         @usermodified='{5}', @remark='{6}', @upload='{7}', @nameFile='{8}'", 
                                        BookingCode.Trim(), Nama.Trim(), Justification.Trim(), Accomodation.Trim(), Transportation.Trim(), 
                                        UserModified.Trim(), Remark, upload, pathUpload); 

             Query.ExecuteNonQuery();
         }

        }

        public void InsertBookingDetail(string BookingCode, DateTime DateOfFlight, string RouteFrom, string RouteTo, string ETD, string AirlineCode, string Status, DateTime TimeLimit, string CancelCode, string CostCode, string PaymentStatus, string UserModified)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"EXEC InsertBookingDetail @bookingCode='{0}' ,@DateOfFlight ='{1}' , @RouteFrom='{2}' ,
                                    @RouteTo='{3}',@ETD='{4}', @AirlineCode='{5}', @status='{6}',@timelimit='{7}',@CancelCode='{8}', 
                                    @CostCode='{9}',@PaymentStatus='{10}', @UserModified='{11}'",
                                    BookingCode.Trim(), DateOfFlight, RouteFrom.Trim(), RouteTo.Trim(), ETD.Trim(), AirlineCode.Trim(), Status,
                                    TimeLimit, CancelCode.Trim(), CostCode.Trim(), PaymentStatus.Trim(), UserModified);
                Query.ExecuteNonQuery();
            }
        }

        public DataTable get_booking_detail(string strbooking_code)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                
                Query.Query = string.Format(@"Select A.pid, DATENAME(dw, A.DateOfFlight) + ', ' + cast(datepart(dd,DateOfFlight) as char(2)) + ' ' + datename(mm, A.DateOfFlight) + ' ' +
                                cast(datepart(yyyy,A.DateOfFlight) as char(4)) As 'f_dFligt', A.RouteFrom as 'f_routefrom', A.RouteTo as 'f_routeTo',   
                                A.ETD as 'f_flightdetail', A.AirlineCode As 'f_airline',
                                DATENAME(dw, A.TimeLimit) + ', ' + cast(datepart(dd,TimeLimit) as char(2)) + ' ' +  datename(mm, A.TimeLimit) + ' ' +
                                cast(datepart(yyyy,A.TimeLimit) as char(4)) As 'f_timelimit', 
                                A.CancelCode as 'f_CancelCode', A.CostCode as 'f_costcode', A.PaymentStatus as 'f_paymentStat', 
                                A.Status  as 'f_status', A.AirlineCode As 'f_alirlinecode', A.CancelCode As 'f_cancelcode', 
                                A.CostCode As 'f_costcode', B.Remark
                                FROM BOOKINGDETAIL A
                                Inner Join Booking B ON A.BookingCode = B.BookingCode
                                Where A.BookingCode = '{0}'", strbooking_code);

                return (Query.GetDataTable());
            }
        }



        public DataTable getLookupAirline()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='Airline'";
                return (Query.GetDataTable());
            }
        }
        public DataTable getListItenaryAll_search()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("EXEC GETLISTITENARYALL");
                //   Query.Query = "EXEC GETLISTITENARY ";

                return (Query.GetDataTable());
            }
        }
        public DataTable getListItenaryName_search(string strnama)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("EXEC GETLISTITENARYNAME_SEARCH @NAMA ='{0}'", strnama.Trim());
                //   Query.Query = "EXEC GETLISTITENARY ";

                return (Query.GetDataTable());
            }
        }
        public DataTable getListItenaryDateFrom_search(DateTime dFlightFirst, string strnama)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("EXEC GETLISTITENARYDATEFROM_SEARCH @dBegin='{0}', @NAMA ='{1}'", dFlightFirst, strnama.Trim());
                //   Query.Query = "EXEC GETLISTITENARY ";

                return (Query.GetDataTable());
            }
        }
        public DataTable getListItenaryDateTo_search(DateTime dFlightEnd, string strnama)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("EXEC GETLISTITENARYDATETO_SEARCH @dEnd='{0}', @NAMA ='{1}'", dFlightEnd, strnama.Trim());
                //   Query.Query = "EXEC GETLISTITENARY ";

                return (Query.GetDataTable());
            }
        }
        public DataTable getListItenary_search(DateTime dFlightFirst, DateTime dFlightEnd, string strnama)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("EXEC GETLISTITENARY_SEARCH @dBegin='{0}', @dEnd='{1}', @NAMA ='{2}'", dFlightFirst, dFlightEnd, strnama.Trim());
                //   Query.Query = "EXEC GETLISTITENARY ";

                return (Query.GetDataTable());
            }
        }
        public void DeleteBooking(string strBookingCode)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Delete From Booking Where BookingCode='{0}'", strBookingCode);

                Query.ExecuteNonQuery();
            }
        }

        public DataTable get_Booking_Detail_Update(string strBookingCode)
        {
            string constr = CommonUtility.ConStr();
            using(ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"SELECT B.BookingCode, B.Nama, B.Accomodation, B.Justification, B.Transportation,
                                A.DateOfFlight as 'f_dFligt', A.RouteFrom as 'f_routefrom', A.RouteTo as 'f_routeTo',   
                                A.ETD as 'f_flightdetail', A.TimeLimit as 'f_timelimit',  A.PaymentStatus as 'f_paymentStat', 
                                A.Status  as 'f_status', A.AirlineCode As 'f_alirlinecode', A.CancelCode As 'f_cancelcode', 
                                A.CostCode As 'f_costcode', B.Remark, B.pathUpload, B.upload as f_UploadPdf
                                FROM BOOKINGDETAIL A
                                Inner Join Booking B ON A.BookingCode = B.BookingCode
                                Where B.BookingCode ='{0}'", strBookingCode);

                return (Query.GetDataTable());
            }
        }
        public DataTable getDataLookup(string category, string strPid)
        {
            string constr = CommonUtility.ConStr();
            using(ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"SELECT pid, Category, Code, Name From LOOKUP
                                                Where Category='{0}' And pid='{1}'", category, strPid);

                return (Query.GetDataTable());
            }
        }



        public DataTable get_CheckBookingCode(string strCode)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select * From Booking Where BookingCode ='{0}'", strCode.Trim());

                return (Query.GetDataTable());
            }
        }

        public DataTable get_CheckEmployeeName(string strNameEmp)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select Name From employeeTemp Where Name='{0}'", strNameEmp.Trim());

                return (Query.GetDataTable());
            }
        }


        public DataTable get_CheckCostCode(string strCostCode, string strBookingCode)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select BookingCode, CostCode From BookingDetail Where BookingCode='{0}' And CostCode='{1}'", strBookingCode, strCostCode);

                return (Query.GetDataTable());
            }
        }


        public void UpdateBooking(string BookingCode, string Nama, string Justification, string Accomodation, string Transportation, string UserModified,
            string Remark, string upload, string pathFile)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"EXEC UPDBOOKING @BookingCode='{0}', @Nama='{1}', @Justification='{2}', @Accomodation='{3}', @tranportation='{4}', @usermodified='{5}', @remark='{6}', @upload='{7}',@namaFile='{8}'",
                                        BookingCode.Trim(), Nama.Trim(), Justification.Trim(), Accomodation.Trim(), Transportation.Trim(), UserModified.Trim(), Remark, upload, pathFile); 
                Query.ExecuteNonQuery();
            }

        }
        
        public void UpdateBookingDetail(int pid, DateTime DateOfFlight, string RouteFrom, string RouteTo, string ETD, string AirlineCode, string Status, DateTime TimeLimit, string CancelCode, string CostCode, string PaymentStatus, string UserModified)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"EXEC UpdateBookingDetail @pid={0},@DateOfFlight ='{1}', @RouteFrom='{2}' ,
                                    @RouteTo='{3}',@ETD='{4}', @AirlineCode='{5}', @status='{6}',@timelimit='{7}',@CancelCode='{8}', 
                                    @CostCode='{9}',@PaymentStatus='{10}', @UserModified='{11}'",
                                    pid, DateOfFlight.ToShortDateString(), RouteFrom.Trim(), RouteTo.Trim(), ETD.Trim(), AirlineCode.Trim(), Status.Trim(),
                                    TimeLimit.ToShortDateString(), CancelCode.Trim(), CostCode.Trim(), PaymentStatus.Trim(), UserModified.Trim());

                Query.ExecuteNonQuery();
            }

        }

        public DataTable get_CheckBookingCode_Detail(string strBookingCode, DateTime DateOfFlight, string strRouteFrom, string strRouteTo)
        {
            string constr = CommonUtility.ConStr();
            using(ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select Distinct BookingCode, DateOfFlight, RouteFrom, RouteTo From BookingDetail 
                                        Where BookingCode='{0}' And DateOfFlight='{1}' And RouteFrom='{2}' And RouteTo='{3}'",
                                        strBookingCode, DateOfFlight.ToShortDateString(), strRouteFrom, strRouteTo);

                return (Query.GetDataTable());
            }
        }

        public void DeleteBooking_Detail(int iPid)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Delete From BookingDetail Where pid={0}", iPid);

                Query.ExecuteNonQuery();
            }

        }

        public void UpdateBookingDetail_Row(int pid, DateTime DateOfFlight, string RouteFrom, string RouteTo, string ETD, string AirlineCode, string Status, DateTime TimeLimit, string CancelCode, string CostCode, string PaymentStatus)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"EXEC UpdateBookingDetail_Row @pid={0},@DateOfFlight ='{1}', @RouteFrom='{2}' ,
                                    @RouteTo='{3}',@ETD='{4}', @AirlineCode='{5}', @status='{6}',@timelimit='{7}',@CancelCode='{8}', 
                                    @CostCode='{9}',@PaymentStatus='{10}'",
                                    pid, DateOfFlight.ToShortDateString(), RouteFrom.Trim(), RouteTo.Trim(), ETD.Trim(), AirlineCode.Trim(), Status.Trim(),
                                    TimeLimit.ToShortDateString(), CancelCode.Trim(), CostCode.Trim(), PaymentStatus.Trim());

                Query.ExecuteNonQuery();
            }

        }

        public void InsertBookingDetail_Row(string BookingCode, DateTime DateOfFlight, string RouteFrom, string RouteTo, string ETD, string AirlineCode, string Status, DateTime TimeLimit, string CancelCode, string CostCode, string PaymentStatus)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"EXEC InsertBookingDetail_Row @bookingCode='{0}' ,@DateOfFlight ='{1}' , @RouteFrom='{2}' ,
                                    @RouteTo='{3}',@ETD='{4}', @AirlineCode='{5}', @status='{6}',@timelimit='{7}',@CancelCode='{8}', 
                                    @CostCode='{9}',@PaymentStatus='{10}'",
                                    BookingCode.Trim(), DateOfFlight.ToShortDateString(), RouteFrom.Trim(), RouteTo.Trim(), ETD.Trim(), AirlineCode.Trim(), Status.Trim(),
                                    TimeLimit.ToShortDateString(), CancelCode.Trim(), CostCode.Trim(), PaymentStatus.Trim());
                Query.ExecuteNonQuery();
            }

        }

        public DataTable getEmployeeBooking()
        {
            string constr = CommonUtility.ConStr();
            using(ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select Distinct Nama From Booking Order By Nama Asc";

                return (Query.GetDataTable());
            }
        }

        public DataTable getEmployeeBooking_search(string strNama)
        {
            string constr = CommonUtility.ConStr();
            using(ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select Nama From Booking Where Nama Like '%' + '{0}' + '%'", strNama.Trim());

                return (Query.GetDataTable());
            }
        }

        //public DataTable getItenary_ExportToExcel(DateTime dFrom, DateTime dTo, string strBookingCode)
        //{
        //    string constr = CommonUtility.ConStr();
        //    using (ISmartQuery Query = new SmartOledbQuery(constr))
        //    {
        //        Query.Query = string.Format("EXEC GETLISTITENARY_EXPORTTOEXCEL @dBegin='{0}', @dEnd='{1}', @BookingCode ='{2}'", dFrom, dTo, strBookingCode.Trim());
                
        //        return (Query.GetDataTable());
        //    }
        //}
        public DataTable getItenary_ExportToExcel(string strBookingCode, DateTime dFrom, DateTime dTo)
        {
            string constr = CommonUtility.ConStr();
            using(ISmartQuery Query = new SmartOledbQuery(constr))
            {
                string strQuery = "";   
                if(!string.IsNullOrEmpty(strBookingCode))
                {
                    strQuery = string.Format("A.BookingCode='{0}'", strBookingCode.Trim());
                }
                else if(string.IsNullOrEmpty(strBookingCode))
                {
                    strQuery = string.Format("B.DateOfFlight >='{0}' And B.DateOfFlight <='{1}'", dFrom, dTo);
                }
                else
                {
                    strQuery = string.Format("A.BookingCode='{0}' And B.DateOfFlight >='{1}' And B.DateOfFlight <='{2}'", strBookingCode, dFrom, dTo);
                }

                Query.Query = string.Format(@"Select A.Nama, convert(char, B.DateOfFlight,106) As 'Date Of Flight', B.ETD As 'Flight Detail',    
                                A.BookingCode As 'Booking Code', B.RouteFrom As 'Route From', B.RouteTo As 'Route To', B.Status,
                                convert(char, B.TimeLimit,106) As 'Time Limit'
                                From Booking A 
                                Inner Join BookingDetail B On A.BookingCode = B.BookingCode 
                                WHERE " + strQuery);
//                Query.Query = string.Format(@"Select A.BookingCode, convert(char,B.DateOfFlight,105) as 'DateOfFlight' ,  
//                                A.Nama,dbo.f_getLookup(A.Justification,'Justification') as 'Justification', 
//                                dbo.f_getLookup(A.Accomodation,'Accomodation') as 'Accomodation', 
//                                dbo.f_getLookup(A.Transportation,'Transportation') as 'Transportation' , A.Remark,
//                                dbo.f_getLookup(B.AirlineCode, 'Airline') As 'Airline', B.ETD As 'FlightDetail', B.PaymentStatus, B.Status, B.CancelCode, 
//                                B.CostCode, B.RouteFrom, B.RouteTo, CONVERT(char, B.TimeLimit, 105) As 'TimeLimit', A.UserModified 
//                                From Booking A 
//                                Inner Join BookingDetail B On A.BookingCode = B.BookingCode 
//                                WHERE " + strQuery);

                return (Query.GetDataTable());
            }
        }
        public DataTable getItenary_ExportToExcelDetail(string strBookingCode)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                string strQuery = string.Format("A.BookingCode='{0}'", strBookingCode.Trim());

                Query.Query = string.Format(@"Select A.Nama, convert(char, B.DateOfFlight,106) As 'Date Of Flight', B.ETD As 'Flight Detail',    
                                A.BookingCode As 'Booking Code', B.RouteFrom As 'Route From', B.RouteTo As 'Route To', B.Status,
                                convert(char, B.TimeLimit,106) As 'Time Limit'
                                From Booking A 
                                Inner Join BookingDetail B On A.BookingCode = B.BookingCode 
                                WHERE " + strQuery);

                return (Query.GetDataTable());
            }
        }
        public DataTable getDownloadFile(string strIdBooking)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"SELECT BookingCode, Upload, pathUpload From Booking Where BookingCode='{0}'", strIdBooking);

                return (Query.GetDataTable());
            }
        }
        public DataTable get_CheckDetail(string strBooking)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"SELECT BookingCode From bookingDetail Where BookingCode='{0}'", strBooking);

                return (Query.GetDataTable());
            }
        }
    }
}