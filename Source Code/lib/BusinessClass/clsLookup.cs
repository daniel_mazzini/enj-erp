﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SmartLibraries.DataAccessLayer;
using NewERDM.lib.Helper;


namespace NewERDM.lib.BusinessClass
{
    class clsLookup
    {
        public DataTable getAccomodation()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='Accomodation'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getAirLine()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='Airline'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getBookingType()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='BookingType'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getFlightStatus()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='FlightStatus'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getJustification()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='Justification'";

                return (Query.GetDataTable());
            }
        }
        public DataTable getPaymentStatus()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='PaymentStatus'";

                return (Query.GetDataTable());
            }
        }
        public DataTable getPOH()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='POH'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getSponsor()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category ='Sponsor'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getTransportation()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='Transportation'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getEthnicGroup()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='EthnicGroup'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getCity()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='City'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getState()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='State'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getCountry()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='Country'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getEmployeeType()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='EmployeeType'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getEmployeeClass()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='EmployeeClass'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getIndividualGrade()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='IndividualGrade'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getOrgGroup()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='OrgGroup'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getSection()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='Section'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getCostCenter()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='CostCenter'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getPayGroup()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From Lookup Where Category='PayGroup'";

                return (Query.GetDataTable());
            }
        }

        public void fSave(int mode, int pid, string category, string lookup, string description, string updateid)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("EXEC ERDM_LOOKUP @mode={0}, @pid={1}, @Category='{2}', @Lookup='{3}', @Description='{4}', @UpdateId='{5}'",
                                            mode, pid, category, lookup, description, updateid);

                Query.ExecuteNonQuery();
            }
        }

        public void fUpdate(int mode, int pid, string category, string lookup, string description, string updateid)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("EXEC ERDM_LOOKUP @mode={0}, @pid={1}, @Category='{2}', @Lookup='{3}', @Description='{4}', @UpdateId='{5}'",
                                            mode, pid, category, lookup, description, updateid);

                Query.ExecuteNonQuery();
            }
        }

        public void fDelete(int pid)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Delete From Lookup Where pid={0}", pid);

                Query.ExecuteNonQuery();
            }
        }

    }
}
