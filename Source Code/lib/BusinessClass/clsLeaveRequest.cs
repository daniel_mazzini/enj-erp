﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartLibraries.DataAccessLayer;
using System.Data;
using NewERDM.lib.Helper;

namespace NewERDM.lib.BusinessClass
{
    public class clsLeaveRequest
    {
        public DataTable get_cekLeaveTempTb(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select employeeID From tbLeaveTemp Where employeeID='{0}'", strEmployeeID);

                return (Query.GetDataTable());
            }
        }

        public void InsertTbLeaveTemp(string strEmployeeID, int iWorkingDayValue, int iDayExcludedValue, int iCobusFormValue, int iDayDeductedValue,
                                                        int iProjectedAccrualValue, int iProjectedBalanceValue)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Insert Into tbLeaveTemp(employeeID, TotalWorking, DayExcluded, Cobus, DayDeducted, ProjectAccrual, ProjectLeaveBalance)
                                            values ('{0}', '{1}', '{2}','{3}', '{4}', '{5}','{6}')", strEmployeeID, iWorkingDayValue, iDayExcludedValue, iCobusFormValue,
                                                       iDayDeductedValue, iProjectedAccrualValue, iProjectedBalanceValue);

                Query.ExecuteNonQuery();
            }
        }
        public void UpdateTbLeaveTemp(string strEmployeeID, int iWorkingDayValue, int iDayExcludedValue, int iCobusFormValue, int iDayDeductedValue,
                                                int iProjectedAccrualValue, int iProjectedBalanceValue)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Update tbLeaveTemp Set TotalWorking='{0}', DayExcluded='{1}', Cobus='{2}', DayDeducted='{3}', ProjectAccrual='{4}',
                                            ProjectLeaveBalance='{5}' Where employeeID='{6}'", iWorkingDayValue, iDayExcludedValue, iCobusFormValue,
                                                       iDayDeductedValue, iProjectedAccrualValue, iProjectedBalanceValue, strEmployeeID);

                Query.ExecuteNonQuery();
            }
        }
        public DataTable getLeaveCalculate(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Select pid, TotalWorking, DayExcluded, Cobus, DayDeducted, ProjectAccrual, ProjectLeaveBalance From tbLeaveTemp
                                            Where employeeID='{0}'", strEmployeeID);

                return (Query.GetDataTable());
            }
        }
        public DataTable getHiredEmployee(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {

            }
        }

        public DataTable get_displayCalculate(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Select pid, TotalWorking, DayExcluded, Cobus, DayDeducted, ProjectAccrual, ProjectLeaveBalance From tbLeaveTemp
                                            Where employeeID='{0}'", strEmployeeID);

                return (Query.GetDataTable());
            }
        }
        public void Delete_tbLeaveTemp(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Delete From tbLeaveTemp Where employeeID={0}", strEmployeeID);

                Query.ExecuteNonQuery();
            }
        }
        public void InsertDataLeaveRequest()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Insert Into EmployeeLeave()values()");

                Query.ExecuteNonQuery();
            }
        }
    }
}