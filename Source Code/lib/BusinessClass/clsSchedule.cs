﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartLibraries.DataAccessLayer;
using System.Data;
using NewERDM.lib.Helper;

namespace NewERDM.lib.BusinessClass
{
    public class clsSchedule
    {
        public void InsertSchedule(string EmployeeID, DateTime dDateFrom, DateTime dDateTo, string typeSchedule, string desc)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Insert Into EmployeeSchedule(EmployeeID,DateFrom,DateTo,Location,Description)Values('{0}','{1}','{2}','{3}','{4}')",
                                            EmployeeID, dDateFrom, dDateTo, typeSchedule, desc);

                Query.ExecuteNonQuery();
            }
        }
    }
}