﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartLibraries.DataAccessLayer;
using System.Data;
using NewERDM.lib.Helper;

namespace NewERDM.lib.BusinessClass
{
    public class clsDashboardUser
    {
        public DataTable get_StatusBooking()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = @"SELECT B.BookingCode, B.Nama, A.RouteFrom, A.RouteTo, A.ETD, A.Status, convert(char, A.DateOfFlight,106) As 'DateOfFlight'
                                From  BookingDetail A
                                Inner Join Booking B ON A.BookingCode = B.BookingCode
                                Where  cast (A.DateOfFlight as Date) = cast (GETDATE() as DATE) And A.Status='OK'
                                Group By B.BookingCode, B.Nama, A.RouteFrom, A.RouteTo, A.ETD, A.Status, A.DateOfFlight 
                                Order By A.DateOfFlight Desc";
                return (Query.GetDataTable());
            }
        }
        public DataTable get_Schedule(string strNameEmployee)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"SELECT B.BookingCode, B.Nama, A.RouteFrom, A.RouteTo, A.ETD, A.Status, convert(char, A.DateOfFlight,106) As 'DateOfFlight'
                                From  BookingDetail A
                                Inner Join Booking B ON A.BookingCode = B.BookingCode
                                Where cast (A.DateOfFlight as Date) >= cast (GETDATE() as DATE) And B.Nama='{0}'
                                Group By B.BookingCode, B.Nama, A.RouteFrom, A.RouteTo, A.ETD, A.Status, A.DateOfFlight 
                                Order By B.Nama Asc", strNameEmployee);
                return (Query.GetDataTable());
            }
        }
        public DataTable get_EmployeeTraining(string strIDEmployee)
        {
            string constr = CommonUtility.ConStr();
            using(ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Select pid, EmployeeID, TrainingType, TrainingName, Mandatory, TrainingLocation, TrainingStartDate,
                                            TrainingEndDate, TrainingCertificateNumber, TrainingProvider
                                            From EmployeeTraining Where EmployeeID='{0}'", strIDEmployee);
                return (Query.GetDataTable());
            }
        }
        public DataTable get_EmployeeEmail(string strIDEmployee)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select ID, Email_Address, Email_Password From Employees Where ID='{0}'", strIDEmployee);

                return (Query.GetDataTable());
            }
        }

    }
}