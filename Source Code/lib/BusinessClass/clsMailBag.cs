﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartLibraries.DataAccessLayer;
using System.Data;
using NewERDM.lib.Helper;

namespace NewERDM.lib.BusinessClass
{
    public class clsMailBag
    {
        public DataTable get_DataMailBag_Search(string strIDMailBag)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                if (!string.IsNullOrEmpty(strIDMailBag))
                {
                    Query.Query = string.Format(@"Select MailBagID, convert(char,Date,106) as 'dateMailbag', Type as 'MailbagType', Remark, ShipTo
                                                From MailBag Where MailBagID='{0}'", strIDMailBag.Trim());
                }
                else
                {
                    Query.Query = string.Format(@"Select MailBagID, convert(char,Date,106) as 'dateMailbag', Type as 'MailbagType', Remark, ShipTo
                                                From MailBag");
                }

                return (Query.GetDataTable());
            }
        }

        public void DeleteID(string strMailID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Delete From MailBag Where MailBagID='{0}'", strMailID);

                Query.ExecuteNonQuery();
            }
        }

        public DataTable get_mailBag_Detail_Update(string strMailBagID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select B.MailBagID, convert(char,A.Date,106) as 'dateMailbag',
                              A.Type as 'MailbagType', A.ShipTo, A.Remark, CONVERT(char, B.DatePickup,106) as 'DatePickup',
                              C.First_Name+' '+C.Middle_Name+' '+C.Last_Name as 'SendBy', B.Description, B.Address, B.Colli,
                              B.Weight, D.First_Name+' '+D.Middle_Name+' '+D.Last_Name as 'ReceivedBy',
                             Case when B.status = '0' Then
	                            'Not Received'
	                            else
	                            'Received'
                            end as statusReceived,
                            Case when B.status='0' Then
	                            null
                            else
	                            B.ReceivedDate
                            end as dateReceived From MailBag A
                            inner join MailBagDetail B ON B.MailBagID = A.MailBagID
                            inner join Employees C ON C.ID = B.SendBy
                            inner join employees D ON D.ID = B.ReceivedBy Where B.MailBagID like '%{0}%'", strMailBagID);

                return (Query.GetDataTable());
            }
        }

        public DataTable get_MailBag_detail(string strMailBagID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select B.MailBagID, convert(char,A.Date,106) as 'dateMailbag',
                              A.Type as 'MailbagType', A.ShipTo, A.Remark, CONVERT(char, B.DatePickup,106) as 'DatePickup',
                              C.First_Name+' '+C.Middle_Name+' '+C.Last_Name as 'SendBy', B.Description, B.Address, B.Colli,
                              B.Weight, D.First_Name+' '+D.Middle_Name+' '+D.Last_Name as 'ReceivedBy',
                             Case when B.status = '0' Then
	                            'Not Received'
	                            else
	                            'Received'
                            end as statusReceived,
                            Case when B.status='0' Then
	                            null
                            else
	                            B.ReceivedDate
                            end as dateReceived From MailBag A
                            inner join MailBagDetail B ON B.MailBagID = A.MailBagID
                            inner join Employees C ON C.ID = B.SendBy
                            inner join employees D ON D.ID = B.ReceivedBy Where B.MailBagID='{0}'", strMailBagID);

                return (Query.GetDataTable());
            }
        }

        public void InsertMailBagHeader(string idMailBag, DateTime dDate, string strTypeMailBag, string strRemark, string strShipTo, string userid)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format("Insert Into MailBag(MailBagID,Date,Type,Remark,ShipTo,UserBy) values ('{0}', '{1}', '{2}','{3}', '{4}', '{5}')",
                                            idMailBag, dDate, strTypeMailBag, strRemark, strShipTo, userid);
                
                Query.ExecuteNonQuery();
            }
        }
        public void InsertMailBagDetail(string idMailBag, DateTime dDatePickup, string strSender, string strDescription, string strAddress, decimal dColli, decimal dWeight, string strReceivedby,
                    string strRemarksDetail, DateTime dDateReceived, string strStatus)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Insert Into MailBagDetail(MailBagID,DatePickup,SendBy,Description,Address,Colli,Weight,ReceivedBy,RemarksDetail,ReceivedDate,status)
                                            values('{0}', '{1}', '{2}','{3}', '{4}', '{5}','{6}','{7}','{8}','{9}','{10}')", idMailBag, dDatePickup, strSender, strDescription,
                                            strAddress, dColli, dWeight, strReceivedby, strRemarksDetail, dDateReceived, strStatus);

                Query.ExecuteNonQuery();
            }
        }

        public DataTable get_EmployeeSendby()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Select rtrim(isnull(ID,'')) as 'ID',
                                rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name',
                                Email_Address as 'EmailSend' FROM Employees Where Status='A'");

                return (Query.GetDataTable());
            }
        }

//        public DataTable get_EmployeeSendby_Update(string idEmployeeSenderUpdate)
//        {
//            string constr = CommonUtility.ConStr();
//            using (ISmartQuery Query = new SmartOledbQuery(constr))
//            {
//                if (!string.IsNullOrEmpty(idEmployeeSenderUpdate))
//                {
//                    Query.Query = String.Format(@"Select rtrim(isnull(ID,'')) as 'ID',
//                                rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name',
//                                Email_Address as 'EmailSend' FROM Employees Where Status='A' and ID='{0}'", idEmployeeSenderUpdate);
//                }
//                else
//                {
//                    Query.Query = String.Format(@"Select rtrim(isnull(ID,'')) as 'ID',
//                                rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name',
//                                Email_Address as 'EmailSend' FROM Employees Where Status='A'");
//                }

//                return (Query.GetDataTable());
//            }
//        }

        public DataTable get_EmployeeReceivedby()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Select rtrim(isnull(ID,'')) as 'ID',
                                rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name',
                                Email_Address as 'EmailReceived' FROM Employees Where Status='A'");
                
                return (Query.GetDataTable());
            }
        }

//        public DataTable get_EmployeeReceivedby_Update(string idEmployeeReceivedUpdate )
//        {
//            string constr = CommonUtility.ConStr();
//            using (ISmartQuery Query = new SmartOledbQuery(constr))
//            {
//                if (!string.IsNullOrEmpty(idEmployeeReceivedUpdate))
//                {
//                    Query.Query = String.Format(@"Select rtrim(isnull(ID,'')) as 'ID',
//                                rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name',
//                                Email_Address as 'EmailReceived' FROM Employees Where Status='A' and ID='{0}'", idEmployeeReceivedUpdate);
//                }
//                else
//                {
//                    Query.Query = String.Format(@"Select rtrim(isnull(ID,'')) as 'ID',
//                                rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name',
//                                Email_Address as 'EmailReceived' FROM Employees Where Status='A'");
//                }

//                return (Query.GetDataTable());
//            }
//        }

//        public DataTable get_MailSendByDetail()
//        {
//            string constr = CommonUtility.ConStr();
//            using (ISmartQuery Query = new SmartOledbQuery(constr))
//            {
//                Query.Query = String.Format(@"Select * From (SELECT rtrim(isnull(ID,'')) as 'ID',
//                                            rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name',
//                                            Email_Address as 'Email' FROM Employees Where Status='A') as A ");

//               return (Query.GetDataTable());
//            }
//        }
//        public DataTable get_MailSendByDetail2(string strIDEmployee)
//        {
//            string constr = CommonUtility.ConStr();
//            using (ISmartQuery Query = new SmartOledbQuery(constr))
//            {
//                Query.Query = String.Format(@"SELECT ID, Email_Address as 'Email' FROM Employees Where ID='{0}'", strIDEmployee);

//                return (Query.GetDataTable());
//            }
//        }

//        public DataTable get_MailReceivedDetail()
//        {
//            string constr = CommonUtility.ConStr();
//            using (ISmartQuery Query = new SmartOledbQuery(constr))
//            {
//                Query.Query = String.Format(@"Select * From (SELECT rtrim(isnull(ID,'')) as 'ID',
//                                            rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name',
//                                            Email_Address as 'Email' FROM Employees Where Status='A') as A");

//                return (Query.GetDataTable());
//            }
//        }
//        public DataTable get_MailReceivedDetail2(string strIDEmployee)
//        {
//            string constr = CommonUtility.ConStr();
//            using (ISmartQuery Query = new SmartOledbQuery(constr))
//            {
//                Query.Query = String.Format(@"SELECT ID, Email_Address as 'Email' FROM Employees Where ID='{0}'", strIDEmployee);

//                return (Query.GetDataTable());
//            }
//        }

//        public DataTable getLookupEmployeeEmail(string strIDEmployee)
//        {
//            string constr = CommonUtility.ConStr();
//            using (ISmartQuery Query = new SmartOledbQuery(constr))
//            {
//                Query.Query = String.Format(@"SELECT ID, Email_Address as 'Email' FROM Employees Where ID='{0}'", strIDEmployee);

//                return (Query.GetDataTable());
//            }
//        }
//        public DataTable getLookupReceivedByEmail(string strIDEmployee)
//        {
//            string constr = CommonUtility.ConStr();
//            using (ISmartQuery Query = new SmartOledbQuery(constr))
//            {
//                Query.Query = String.Format(@"Select * From (SELECT rtrim(isnull(ID,'')) as 'ID',
//                                            rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name',
//                                            Email_Address as 'Email' FROM Employees) as A Where A.ID='{0}'", strIDEmployee);

//                return (Query.GetDataTable());
//            }
//        }
//        public DataTable getEmailReceivedBy(string strIDEmployee)
//        {
//            string constr = CommonUtility.ConStr();
//            using (ISmartQuery Query = new SmartOledbQuery(constr))
//            {
//                Query.Query = String.Format(@"Select * From (SELECT rtrim(isnull(ID,'')) as 'ID',
//                                            rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Middle_Name,''))+' '+ rtrim(isnull(Last_Name,'')) as 'Name',
//                                            Email_Address as 'Email' FROM Employees) as A Where A.ID='{0}'", strIDEmployee);

//                return (Query.GetDataTable());
//            }
//        }
        public DataTable get_doubleIDMail(string strIDMailBag)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select MailBagID From MailBag Where MailBagID='{0}'", strIDMailBag);

                return (Query.GetDataTable());
            }
        }
        public DataTable get_CheckEmail(string strIDSender, string strIDReceived)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select A.pid, A.SendBy, B.Email_Address as 'SenderEmail',
                                            A.ReceivedBy, C.Email_Address as 'ReceivedEmail'
                                            From MailBagDetail A
                                            Inner join Employees B ON A.SendBy = B.ID
                                            Inner Join Employees C ON A.ReceivedBy = C.ID
                                            Where A.SendBy='{0}' and A.ReceivedBy='{1}'", strIDSender, strIDReceived);

                return (Query.GetDataTable());
            }
        }
        public DataTable getDataEntriMailBag(string strIDEmployee)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"SELECT B.pid, B.MailBagID, convert(char, B.DatePickup, 106) as 'DatePickup',
                                C.First_Name+' '+C.Middle_Name+' '+C.Last_Name as SenderBy,
                                B.Description, B.Address, B.Colli, B.Weight, C.Email_Address as 'EmailPengirim'
                                FROM MailBag A
                                inner Join MailBagDetail B ON A.MailBagID= B.MailBagID
                                inner Join Employees C ON B.SendBy = C.ID
                                Where B.status=0 And B.ReceivedBy ='{0}'", strIDEmployee);

                return (Query.GetDataTable());
            }
        }
        public DataTable get_detailMail(string strIDEmp)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"SELECT B.pid, B.MailBagID, convert(char, B.DatePickup, 106) as 'DatePickup',
                                C.First_Name+' '+C.Middle_Name+' '+C.Last_Name as SenderBy,
                                B.Description, B.Address, B.Colli, B.Weight, C.Email_Address as 'EmailPengirim'
                                FROM MailBag A
                                inner Join MailBagDetail B ON A.MailBagID= B.MailBagID
                                inner Join Employees C ON B.SendBy = C.ID
                                Where B.status=0 And B.ReceivedBy ='{0}'", strIDEmp);

                return (Query.GetDataTable());
            }
        }
        public DataTable getDataIDEmployee(string strIDUser)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"SELECT UserID, employeeid FROM ADMIN_USER Where UserID ='{0}'", strIDUser);

                return (Query.GetDataTable());
            }
        }
        public void UpdateDetailMailBag(int pid, DateTime dateReceived)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Update MailBagDetail Set ReceivedDate='{1}', status=1 Where pid={0}", pid, dateReceived);
//                Query.Query = String.Format(@"EXEC UpdateBookingDetail @pid={0},@DateOfFlight ='{1}', @RouteFrom='{2}' ,
//                                    @RouteTo='{3}',@ETD='{4}', @AirlineCode='{5}', @status='{6}',@timelimit='{7}',@CancelCode='{8}', 
//                                    @CostCode='{9}',@PaymentStatus='{10}', @UserModified='{11}'",
//                                    pid, DateOfFlight.ToShortDateString(), RouteFrom.Trim(), RouteTo.Trim(), ETD.Trim(), AirlineCode.Trim(), Status.Trim(),
//                                    TimeLimit.ToShortDateString(), CancelCode.Trim(), CostCode.Trim(), PaymentStatus.Trim(), UserModified.Trim());

                Query.ExecuteNonQuery();
            }

        }

    }
}