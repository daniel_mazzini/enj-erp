﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartLibraries.DataAccessLayer;
using System.Data;
using NewERDM.lib.Helper;



namespace NewERDM.lib.BusinessClass
{
    class clsLogin
    {

        public DataTable getLogin(string strUserID, string strPassword)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("EXEC GETLOGIN @userID ='{0}', @sPassword='{1}'", strUserID.Trim(), strPassword.Trim());
                return (Query.GetDataTable());
            }
        }
        public DataTable get_PassOld(string strUserID, string strPassword)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select UserID, UserPwd From Admin_User Where UserID='{0}' And UserPwd='{1}'", strUserID, strPassword);
                return (Query.GetDataTable());
            }
        }
        public void updateNewPassword(string strUserID, string strPassword)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Update Admin_User Set UserPwd='{0}' Where UserID='{1}'", strPassword, strUserID);
 
                Query.ExecuteNonQuery();
            }
        }

        //public DataTable getLogin1(string strUserID)
        //{
        //    string constr = CommonUtility.ConStr();
        //    using (ISmartQuery Query = new SmartOledbQuery(constr))
        //    {
        //        Query.Query = string.Format("EXEC GETLOGIN @userID ='{0}'", strUserID.Trim());
        //        return (Query.GetDataTable());
        //    }
        //}

    }
}
