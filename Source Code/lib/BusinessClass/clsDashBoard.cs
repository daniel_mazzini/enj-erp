﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartLibraries.DataAccessLayer;
using System.Data;
using NewERDM.lib.Helper;

namespace NewERDM.lib.BusinessClass
{
    class clsDashBoard
    {
        public DataTable getItenary_ExportToExcelDashboard(string strBookingCode, DateTime dFrom, DateTime dTo, string strStatus)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                string strQuery = "";
                if (!string.IsNullOrEmpty(strBookingCode))
                {
                    strQuery = string.Format("B.Status='{0}' And A.BookingCode='{1}'", strStatus, strBookingCode.Trim());
                }
                else if (string.IsNullOrEmpty(strBookingCode))
                {
                    strQuery = string.Format("B.Status='{0}' And B.DateOfFlight >='{1}' And B.DateOfFlight <='{2}'", strStatus, dFrom, dTo);
                }
                else
                {
                    strQuery = string.Format("B.Status='{0}' And A.BookingCode='{1}' And B.DateOfFlight >='{2}' And B.DateOfFlight <='{3}'", strStatus, strBookingCode, dFrom, dTo);
                }

                Query.Query = string.Format(@"Select A.Nama, convert(char,B.DateOfFlight,105) as 'DateOfFlight', B.ETD As 'FlightDetail',    
                                A.BookingCode, B.RouteFrom, B.RouteTo, B.Status, CONVERT(char, B.TimeLimit, 105) As 'TimeLimit'
                                From Booking A 
                                Inner Join BookingDetail B On A.BookingCode = B.BookingCode 
                                WHERE " + strQuery);
//                Query.Query = string.Format(@"Select A.BookingCode, convert(char,B.DateOfFlight,105) as 'DateOfFlight' ,  
//                                A.Nama,dbo.f_getLookup(A.Justification,'Justification') as 'Justification', 
//                                dbo.f_getLookup(A.Accomodation,'Accomodation') as 'Accomodation', 
//                                dbo.f_getLookup(A.Transportation,'Transportation') as 'Transportation' , A.Remark,
//                                dbo.f_getLookup(B.AirlineCode, 'Airline') As 'Airline', B.ETD As 'FlightDetail', B.PaymentStatus, B.Status, B.CancelCode, 
//                                B.CostCode, B.RouteFrom, B.RouteTo, CONVERT(char, B.TimeLimit, 105) As 'TimeLimit', A.UserModified 
//                                From Booking A 
//                                Inner Join BookingDetail B On A.BookingCode = B.BookingCode 
//                                WHERE " + strQuery);

                return (Query.GetDataTable());
            }
        }
        public DataTable getLookupJustification()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='Justification'";
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookupaccomodation()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='Accomodation'";
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookuptransport()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='Transportation'";
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookupEmployee()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(isnull(ID,'')) as 'ID', rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Last_Name,'')) as 'Name' FROM Employees Where Status='A' order by id";
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookupEmployee_SR(string strSearch)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format("SELECT rtrim(isnull(ID,'')) as 'ID', rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Last_Name,'')) as 'Name' FROM Employees Where Status='A'  and ID LIKE '%{0}%' OR  rtrim(isnull(First_Name,''))+ ' '+ rtrim(isnull(Last_Name,'')) LIKE '%{0}%' order by id", strSearch.Trim());
                return (Query.GetDataTable());
            }
        }

        public DataTable getUploadFilePDF(string strIDBookingCode)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select BookingCode, upload as FilePDF From Booking Where BookingCode='{0}'", strIDBookingCode);

                return (Query.GetDataTable());
            }
        }


    }
}
