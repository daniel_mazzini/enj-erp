﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewERDM.lib.Helper;
using SmartLibraries.DataAccessLayer;
using System.Data;

namespace NewERDM.lib.BusinessClass
{
    class clsEmployee
    {
        public DataTable getCompany()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select Code, Name From Lookup Where Category='Company'";

                return (Query.GetDataTable());
            }
        }
        public DataTable getEmpSex()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select Code, Name From Lookup Where Category='EmployeeSex'";

                return (Query.GetDataTable());
            }
        }
        public DataTable getEmpRoster()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select Code, Name From Lookup Where Category='EmployeeRoster'";

                return (Query.GetDataTable());
            }
        }
        public DataTable getEmpType()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select Code, Name From Lookup Where Category='EmployeeType'";

                return (Query.GetDataTable());
            }
        }
        public DataTable getEmpStatus()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select Code, Name From Lookup Where Category='EmployeeStatus'";

                return (Query.GetDataTable());
            }
        }

        public DataTable getEmpGrade()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select Code, Name From Lookup Where Category='EmployeeGrade'");

                return (Query.GetDataTable());
            }
        }
        public DataTable getEmpExpatNational()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select Code, Name From Lookup Where Category='ExpatNational'");

                return (Query.GetDataTable());
            }
        }

        public DataTable getPOH()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select CityCode As Code, CityName As Name From lk_POH");

                return (Query.GetDataTable());
            }
        }


        //Insert Data Personal
        public void InsPersonalEmployee(string strIDEmployee, string strCompanyID, string strNetworkID, string strFirstName, string strMiddleName, string strLastName, string strGender, DateTime dBirthDay,
                                        string strPlaceBirth, string strMaritalSts, string strRegion, string strNationality, string strEthnic, string strAddress,
                                        string strCity, string strState, string strCountry, string strZip, string strHomePhone, string strWorkPhone, string strMobilePhone, string strFax,
                                        string strWebPage, string strPhonetoRadion, string strEmail1, string strEmail2, string strKTP, string strBPJS, string strNPWP)
        {
             string constr = CommonUtility.ConStr();
             using (ISmartQuery Query = new SmartOledbQuery(constr))
             {
                 if (dBirthDay == null)
                 {
                     Query.Query = string.Format(@"Insert Into employees(ID,CompanyID,NetworkID,Last_Name,First_Name,Middle_Name,Gender,DateOfBirth,PlaceOfBirth,
                                                  MaritalStatus,Religion,Nationality,EthnicGroup,Address,City,State_Province,Country_Region,ZIPPostal_Code,
                                                 Home_Phone,Business_Phone,Mobile_Phone,Fax_Number,Web_Page,PhoneToRadion,Email1,Email2,KTP,BPJS,NPWP)
                                                 Values('{0}','{1}','{2}',{3}','{4}','{5}','{6}',NULL,'{7}',{8}','{9}','{10}','{11}',
                                                '{12}','{13}',{14}','{15}','{16}','{17}','{18}','{19}',{20}','{21}','{22}','{23}','{24}',
                                                '{25}',{26}','{27}'", strIDEmployee, strCompanyID, strNetworkID, strLastName, strFirstName, strMiddleName, strGender,
                                                 strPlaceBirth, strMaritalSts, strRegion, strNationality, strEthnic, strAddress, strCity, strState, strCountry, strZip,
                                                 strHomePhone, strWorkPhone, strMobilePhone, strFax, strWebPage, strPhonetoRadion, strEmail1, strEmail2, strKTP, strBPJS, strNPWP);
                 }
                 else
                 {
                    Query.Query = string.Format(@"Insert Into employees(ID,CompanyID,NetworkID,Last_Name,First_Name,Middle_Name,Gender,DateOfBirth,PlaceOfBirth,
                                                  MaritalStatus,Religion,Nationality,EthnicGroup,Address,City,State_Province,Country_Region,ZIPPostal_Code,
                                                 Home_Phone,Business_Phone,Mobile_Phone,Fax_Number,Web_Page,PhoneToRadion,Email1,Email2,KTP,BPJS,NPWP)
                                                 Values('{0}','{1}','{2}',{3}','{4}','{5}','{6}','{7}','{8}',{9}','{10}','{11}','{12}',
                                                '{13}','{14}',{15}','{16}','{17}','{18}','{19}','{20}',{21}','{22}','{23}','{24}','{25}',
                                                '{26}',{27}','{28}'", strIDEmployee, strCompanyID, strNetworkID, strLastName, strFirstName, strMiddleName, strGender,
                                                 dBirthDay, strPlaceBirth, strMaritalSts, strRegion, strNationality, strEthnic, strAddress, strCity, strState,
                                                 strCountry, strZip, strHomePhone, strWorkPhone, strMobilePhone, strFax, strWebPage, strPhonetoRadion,
                                                 strEmail1, strEmail2, strKTP, strBPJS, strNPWP);
                 }

                 Query.ExecuteNonQuery();
             }
        }


        //Update Data Personal
        public void UpdPersonalEmployee(string strIDEmployee, string strCompanyID, string strNetworkID, string strFirstName, string strMiddleName, string strLastName, string strGender, DateTime dBirthDay,
                                string strPlaceBirth, string strMaritalSts, string strRegion, string strNationality, string strEthnic, string strAddress,
                                string strCity, string strState, string strCountry, string strZip, string strHomePhone, string strWorkPhone, string strMobilePhone, string strFax,
                                string strWebPage, string strPhonetoRadion, string strEmail1, string strEmail2, string strKTP, string strBPJS, string strNPWP)
        {
             string constr = CommonUtility.ConStr();
             using (ISmartQuery Query = new SmartOledbQuery(constr))
             {
                 if (Convert.ToString(dBirthDay) == "1/1/0001 12:00:00 AM")
                 {
                     Query.Query = string.Format(@"Update Employees Set CompanyID='{0}', NetworkID='{1}', Last_Name='{2}', First_Name='{3}', Middle_Name='{4}', Gender='{5}',
                                                 DateOfBirth=NULL, PlaceOfBirth='{6}', MaritalStatus='{7}', Religion='{8}', Nationality='{9}', EthnicGroup='{10}', Address='{11}',
                                                 City='{12}', State_Province='{13}', Country_Region='{14}', ZIPPostal_Code='{15}', Home_Phone='{16}', Business_Phone='{17}', 
                                                 Mobile_Phone='{18}', Fax_Number='{19}', Web_Page='{20}', PhoneToRadion='{21}', Email1='{22}', Email2='{23}', KTP='{24}',
                                                 BPJS='{25}', NPWP='{26}' Where ID='{27}'", strCompanyID, strNetworkID, strLastName, strFirstName, strMiddleName, strGender,
                                                 strPlaceBirth, strMaritalSts, strRegion, strNationality, strEthnic, strAddress, strCity, strState, strCountry, strZip,
                                                 strHomePhone, strWorkPhone, strMobilePhone, strFax, strWebPage, strPhonetoRadion, strEmail1, strEmail2, strKTP, strBPJS,
                                                 strNPWP, strIDEmployee);
                 }
                 else
                 {
                     Query.Query = string.Format(@"Update Employees Set CompanyID='{0}', NetworkID='{1}', Last_Name='{2}', First_Name='{3}', Middle_Name='{4}', Gender='{5}',
                                                 DateOfBirth='{6}', PlaceOfBirth='{7}', MaritalStatus='{8}', Religion='{9}', Nationality='{10}', EthnicGroup='{11}', Address='{12}',
                                                 City='{13}', State_Province='{14}', Country_Region='{15}', ZIPPostal_Code='{16}', Home_Phone='{17}', Business_Phone='{18}', 
                                                 Mobile_Phone='{19}', Fax_Number='{20}', Web_Page='{21}', PhoneToRadion='{22}', Email1='{23}', Email2='{24}', KTP='{25}',
                                                 BPJS='{26}', NPWP='{27}' Where ID='{28}'", strCompanyID, strNetworkID, strLastName, strFirstName, strMiddleName, strGender,
                                                 dBirthDay, strPlaceBirth, strMaritalSts, strRegion, strNationality, strEthnic, strAddress, strCity, strState, strCountry, strZip,
                                                 strHomePhone, strWorkPhone, strMobilePhone, strFax, strWebPage, strPhonetoRadion, strEmail1, strEmail2, strKTP, strBPJS,
                                                 strNPWP, strIDEmployee);
                 }

                 Query.ExecuteNonQuery();
             }
        }

        //Update Data Job Information
        public void UpdJobInformationEmployee(string strEmployeeID, string strEmployeeType, DateTime dOriginalHireDate, DateTime dRehireDate, DateTime dServiceDate,
                                              DateTime dLastPromotion, string strVendor, string strWorkAt, string strFunctionalDepartment, string strOrgGroup,
                                              string strSection, string strLocation, string strOffice, string strJobTitle, string strPositionTitle, string Supervisor,
                                              string strCostCenter, string strPayGroup, string strBenefitCode, string strUnionCode, string strTaxCode)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Update Employee Set Type='{0}', OrginalHireDate='{1}', RehireDate='{2}', ServiceDate='{3}', LastPromotion='{4}',
                                            Vendor='{5}', WorkAt='{6}', FunctionalDepartment='{7}', OrgGroup='{8}', Section='{9}', Location='{10}', Office='{11}',
                                            Job_Title='{12}', PositionTitle='{13}', Supervisior='{14}', CostCenter='{15}', PayGroup='{16}', BenefitCode='{17}',
                                            UnionCode='{18}', TaxCode='{19}' Where ID='{20}' ", strEmployeeType, dOriginalHireDate, dRehireDate, dServiceDate, dLastPromotion,
                                            strVendor, strWorkAt, strFunctionalDepartment, strOrgGroup, strSection, strLocation, strOffice, strJobTitle, strPositionTitle, 
                                            Supervisor, strCostCenter, strPayGroup, strBenefitCode, strUnionCode, strTaxCode);

                Query.ExecuteNonQuery();
            }
        }

        //Update Data Emergency
        public void UpdEmergencyEmployee(string strIDEmployee, string strNameOfContact, string strRelationship, string strPhoneNumber, string strMobileNumber, string strEmail)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Update Employees Set NameOfContact='{0}', Relationship='{1}', PhoneNumber='{2}', MobileNumber='{3}', Email='{4}'
                                            Where ID='{5}'", strNameOfContact, strRelationship, strPhoneNumber, strMobileNumber, strEmail, strIDEmployee);
                Query.ExecuteNonQuery();
            }
        }

        //Update Data PassportVisa
        public void UpdPassportVisaEmployee(String strIDEmployee, string strPassportNo, DateTime dPassportDateofIssue, DateTime dPassportExpirationDate, DateTime dPassportIssuingOffice,
                                            string strPassportRegNo, string VisaNo, string VisaType, DateTime VisaIssueDate, DateTime VisaExpirationDate,
                                            DateTime VisaIssuingOffice)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Update Employees Set PassportNo='{0}', PassportDateOfIssue='{1}', PassportExpirationIssue='{2}', PassportIssuingOffice='{3}',
                                            PassportRegNo='{4}', VisaNo='{5}', VisaType='{6}', VisaIssueDate='{7}', VisaExpirationDate='{8}', VisaIssuingOffice='{9}'
                                            Where ID='{10}'", strPassportNo, dPassportDateofIssue, dPassportExpirationDate, dPassportIssuingOffice,
                                            strPassportRegNo, VisaNo, VisaType, VisaIssueDate, VisaExpirationDate, VisaIssuingOffice, strIDEmployee);
                Query.ExecuteNonQuery();
            }
        }

        //Insert Data
        public void InsEmployee(string ID, string CompanyID, string NetworkID, string Last_Name, string First_Name, string Middle_Name, string Gender, string Email_Address,
            string Job_Title, string Business_Phone, string Home_Phone, string Mobile_Phone, string Fax_Number, string Address, string City, string State_Province,
            string ZIPPostal_Code, string Country_Region, string Web_Page, string POH, string Status, string Type, string Notes, string Grade, DateTime DateOfBirth, string PlaceOfBirth, string MaritalStatus,
            string Religion, string Nationality, string EthnicGroup, string WorkPhone, string PhoneToRadion, string Email1, string Email2, string KTP, string BPJS,
            string NPWP, string Class, DateTime OrginalHireDate, DateTime RehireDate, DateTime ServiceDate, DateTime LastPromotion, string Vendor, string WorkAt, string FunctionalDepartment,
            string OrgGroup, string Section, string Location, string Office, string PositionTitle, string Supervisior, string CostCenter, string PayGroup,
            string BenefitCode, string UnionCode, string TaxCode, string NameOfContact, string Relationship, string PhoneNumber, string MobileNumber, string Email,
            string PassportNo, DateTime PassportDateOfIssue, DateTime PassportExpirationIssue, DateTime PassportIssuingOffice, string PassportRegNo, string VisaNo,
            string VisaType, DateTime VisaIssueDate, DateTime VisaExpirationDate, DateTime VisaIssuingOffice)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"EXEC INSEMPLOYEES @ID='{0}', @CompanyID='{1}', @NetworkID='{2}', @Last_Name='{3}', @First_Name='{4}', 
                            @Middle_Name='{5}', @Gender='{6}', @Email_Address='{7}', @Job_Title='{8}', @Business_Phone='{9}', @Home_Phone='{10}', @Mobile_Phone='{11}', 
                            @Fax_Number='{12}', @Address='{13}', @City='{14}', @State_Province='{15}', @ZIPPostal_Code='{16}', @Country_Region='{17}', @Web_Page='{18}',
                            @POH='{19}', @Status='{20}', @Type='{21}', @Notes='{22}', @Grade='{23}', @DateOfBirth='{24}', @PlaceOfBirth='{25}', @MaritalStatus='{26}',
                            @Religion='{27}', @Nationality='{28}', @EthnicGroup='{29}', @WorkPhone='{30}', @PhoneToRadion='{31}', @Email1='{32}', @Email2='{33}',
                            @KTP='{34}', @BPJS='{35}', @NPWP='{36}', @Class='{37}', @OrginalHireDate='{38}', @RehireDate='{39}', @ServiceDate='{40}', @LastPromotion='{41}',
                            @Vendor='{42}', @WorkAt='{43}', @FunctionalDepartment='{44}', @OrgGroup='{45}', @Section='{46}', @Location='{47}', @Office='{48}', @PositionTitle='{49}',
                            @Supervisior='{50}', @CostCenter='{51}', @PayGroup='{52}', @BenefitCode='{53}', @UnionCode='{54}', @TaxCode='{55}', @NameOfContact='{56}',
                            @Relationship='{57}', @PhoneNumber='{58}', @MobileNumber='{59}', @Email='{60}', @PassportNo='{61}', @PassportDateOfIssue='{62}', @PassportExpirationIssue='{63}',
                            @PassportIssuingOffice='{64}', @PassportRegNo='{65}', @VisaNo='{66}', @VisaType='{67}', @VisaIssueDate='{68}', @VisaExpirationDate='{69}', @VisaIssuingOffice='{70}'",
                            ID, CompanyID, NetworkID, Last_Name, First_Name, Middle_Name, Gender, Email_Address,
                             Job_Title, Business_Phone, Home_Phone, Mobile_Phone, Fax_Number, Address, City, State_Province,
                             ZIPPostal_Code, Country_Region, Web_Page, POH, Status, Type, Notes, Grade, DateOfBirth, PlaceOfBirth, MaritalStatus,
                             Religion, Nationality, EthnicGroup, WorkPhone, PhoneToRadion, Email1, Email2, KTP, BPJS,
                             NPWP, Class, OrginalHireDate, RehireDate, ServiceDate, LastPromotion, Vendor, WorkAt, FunctionalDepartment,
                             OrgGroup, Section, Location, Office, PositionTitle, Supervisior, CostCenter, PayGroup,
                             BenefitCode, UnionCode, TaxCode, NameOfContact, Relationship, PhoneNumber, MobileNumber, Email,
                             PassportNo, PassportDateOfIssue, PassportExpirationIssue, PassportIssuingOffice, PassportRegNo, VisaNo,
                             VisaType, VisaIssueDate, VisaExpirationDate, VisaIssuingOffice);


                Query.ExecuteNonQuery();
            }
        }

        //Update Data
        public void UpdEmployee(string ID, string CompanyID, string NetworkID, string Last_Name, string First_Name, string Middle_Name, string Gender, string Email_Address,
            string Job_Title, string Business_Phone, string Home_Phone, string Mobile_Phone, string Fax_Number, string Address, string City, string State_Province,
            string ZIPPostal_Code, string Country_Region, string Web_Page, string POH, string Status, string Type, string Notes, string Grade, DateTime DateOfBirth, string PlaceOfBirth, string MaritalStatus,
            string Religion, string Nationality, string EthnicGroup, string WorkPhone, string PhoneToRadion, string Email1, string Email2, string KTP, string BPJS,
            string NPWP, string Class, DateTime OrginalHireDate, DateTime RehireDate, DateTime ServiceDate, DateTime LastPromotion, string Vendor, string WorkAt, string FunctionalDepartment,
            string OrgGroup, string Section, string Location, string Office, string PositionTitle, string Supervisior, string CostCenter, string PayGroup,
            string BenefitCode, string UnionCode, string TaxCode, string NameOfContact, string Relationship, string PhoneNumber, string MobileNumber, string Email,
            string PassportNo, DateTime PassportDateOfIssue, DateTime PassportExpirationIssue, DateTime PassportIssuingOffice, string PassportRegNo, string VisaNo,
            string VisaType, DateTime VisaIssueDate, DateTime VisaExpirationDate, DateTime VisaIssuingOffice)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
//                if(DateOfBirth == null)
//                {
//                    Query.Query = string.Format(@"EXEC UPDEMPLOYEES @ID='{0}', @CompanyID='{1}', @NetworkID='{2}', @Last_Name='{3}', @First_Name='{4}', 
//                            @Middle_Name='{5}', @Gender='{6}', @Email_Address='{7}', @Job_Title='{8}', @Business_Phone='{9}', @Home_Phone='{10}', @Mobile_Phone='{11}', 
//                            @Fax_Number='{12}', @Address='{13}', @City='{14}', @State_Province='{15}', @ZIPPostal_Code='{16}', @Country_Region='{17}', @Web_Page='{18}',
//                            @POH='{19}', @Status='{20}', @Type='{21}', @Notes='{22}', @Grade='{23}', @DateOfBirth=NULL'{24}', @PlaceOfBirth='{25}', @MaritalStatus='{26}',
//                            @Religion='{27}', @Nationality='{28}', @EthnicGroup='{29}', @WorkPhone='{30}', @PhoneToRadion='{31}', @Email1='{32}', @Email2='{33}',
//                            @KTP='{34}', @BPJS='{35}', @NPWP='{36}', @Class='{37}', @OrginalHireDate='{38}', @RehireDate='{39}', @ServiceDate='{40}', @LastPromotion='{41}',
//                            @Vendor='{42}', @WorkAt='{43}', @FunctionalDepartment='{44}', @OrgGroup='{45}', @Section='{46}', @Location='{47}', @Office='{48}', @PositionTitle='{49}',
//                            @Supervisior='{50}', @CostCenter='{51}', @PayGroup='{52}', @BenefitCode='{53}', @UnionCode='{54}', @TaxCode='{55}', @NameOfContact='{56}',
//                            @Relationship='{57}', @PhoneNumber='{58}', @MobileNumber='{59}', @Email='{60}', @PassportNo='{61}', @PassportDateOfIssue='{62}', @PassportExpirationIssue='{63}',
//                            @PassportIssuingOffice='{64}', @PassportRegNo='{65}', @VisaNo='{66}', @VisaType='{67}', @VisaIssueDate='{68}', @VisaExpirationDate='{69}', @VisaIssuingOffice='{70}'",
//                                ID, CompanyID, NetworkID, Last_Name, First_Name, Middle_Name, Gender, Email_Address,
//                                Job_Title, Business_Phone, Home_Phone, Mobile_Phone, Fax_Number, Address, City, State_Province,
//                                ZIPPostal_Code, Country_Region, Web_Page, POH, Status, Type, Notes, Grade, PlaceOfBirth, MaritalStatus,
//                                Religion, Nationality, EthnicGroup, WorkPhone, PhoneToRadion, Email1, Email2, KTP, BPJS,
//                                NPWP, Class, OrginalHireDate, RehireDate, ServiceDate, LastPromotion, Vendor, WorkAt, FunctionalDepartment,
//                                OrgGroup, Section, Location, Office, PositionTitle, Supervisior, CostCenter, PayGroup,
//                                BenefitCode, UnionCode, TaxCode, NameOfContact, Relationship, PhoneNumber, MobileNumber, Email,
//                                PassportNo, PassportDateOfIssue, PassportExpirationIssue, PassportIssuingOffice, PassportRegNo, VisaNo,
//                                VisaType, VisaIssueDate, VisaExpirationDate, VisaIssuingOffice);
//                }
//                else
//                {

//                }

                Query.Query = string.Format(@"EXEC UPDEMPLOYEES @ID='{0}', @CompanyID='{1}', @NetworkID='{2}', @Last_Name='{3}', @First_Name='{4}', 
                            @Middle_Name='{5}', @Gender='{6}', @Email_Address='{7}', @Job_Title='{8}', @Business_Phone='{9}', @Home_Phone='{10}', @Mobile_Phone='{11}', 
                            @Fax_Number='{12}', @Address='{13}', @City='{14}', @State_Province='{15}', @ZIPPostal_Code='{16}', @Country_Region='{17}', @Web_Page='{18}',
                            @POH='{19}', @Status='{20}', @Type='{21}', @Notes='{22}', @Grade='{23}', @DateOfBirth='{24}', @PlaceOfBirth='{25}', @MaritalStatus='{26}',
                            @Religion='{27}', @Nationality='{28}', @EthnicGroup='{29}', @WorkPhone='{30}', @PhoneToRadion='{31}', @Email1='{32}', @Email2='{33}',
                            @KTP='{34}', @BPJS='{35}', @NPWP='{36}', @Class='{37}', @OrginalHireDate='{38}', @RehireDate='{39}', @ServiceDate='{40}', @LastPromotion='{41}',
                            @Vendor='{42}', @WorkAt='{43}', @FunctionalDepartment='{44}', @OrgGroup='{45}', @Section='{46}', @Location='{47}', @Office='{48}', @PositionTitle='{49}',
                            @Supervisior='{50}', @CostCenter='{51}', @PayGroup='{52}', @BenefitCode='{53}', @UnionCode='{54}', @TaxCode='{55}', @NameOfContact='{56}',
                            @Relationship='{57}', @PhoneNumber='{58}', @MobileNumber='{59}', @Email='{60}', @PassportNo='{61}', @PassportDateOfIssue='{62}', @PassportExpirationIssue='{63}',
                            @PassportIssuingOffice='{64}', @PassportRegNo='{65}', @VisaNo='{66}', @VisaType='{67}', @VisaIssueDate='{68}', @VisaExpirationDate='{69}', @VisaIssuingOffice='{70}'",
                            ID, CompanyID, NetworkID, Last_Name, First_Name, Middle_Name, Gender, Email_Address,
                            Job_Title, Business_Phone, Home_Phone, Mobile_Phone, Fax_Number, Address, City, State_Province,
                            ZIPPostal_Code, Country_Region, Web_Page, POH, Status, Type, Notes, Grade, DateOfBirth, PlaceOfBirth, MaritalStatus,
                            Religion, Nationality, EthnicGroup, WorkPhone, PhoneToRadion, Email1, Email2, KTP, BPJS,
                            NPWP, Class, OrginalHireDate, RehireDate, ServiceDate, LastPromotion, Vendor, WorkAt, FunctionalDepartment,
                            OrgGroup, Section, Location, Office, PositionTitle, Supervisior, CostCenter, PayGroup,
                            BenefitCode, UnionCode, TaxCode, NameOfContact, Relationship, PhoneNumber, MobileNumber, Email,
                            PassportNo, PassportDateOfIssue, PassportExpirationIssue, PassportIssuingOffice, PassportRegNo, VisaNo,
                            VisaType, VisaIssueDate, VisaExpirationDate, VisaIssuingOffice);

                Query.ExecuteNonQuery();
            }
        }

        //Check ID yang double
        public DataTable getCheckEmployeeID(string strID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select ID From Employees Where ID='{0}'", strID.Trim());

                return (Query.GetDataTable());
            }
        }

        public DataTable getEmployeeID(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select * From Employees Where ID='{0}'", strEmployeeID.Trim());

                return (Query.GetDataTable());
            }
        }

        public DataTable getEmployee()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select * From Employees");

                return (Query.GetDataTable());
            }
        }

        public void DelEmployee(string strID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Delete From Employees Where ID='{0}'", strID.Trim());

                Query.ExecuteNonQuery();
            }
        }


        //Asset
        public DataTable getEmployeeAsset(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select * From employeeAsset Where EmployeeID='{0}'", strEmployeeID.Trim());

                return (Query.GetDataTable());
            }
        }
        public DataTable getEmployees()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select ID as employeeid, First_Name+ ' ' + Middle_Name + ' ' + Last_Name as employeename  From employees ";

                return (Query.GetDataTable());
            }
        }


        //Get data employee for update
        public DataTable getEmployeeID_Profile(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select ID As employeeid, First_Name+ ' ' + Middle_Name + ' ' + Last_Name as f_employeename,  
                                            Home_Phone As f_homephone, Mobile_Phone As f_mobile, Email_Address As f_email, Email_Password As f_emailPass,
                                            Address As f_address, Job_Title As f_jabatan
                                            From Employees
                                            Where ID='{0}'", strEmployeeID.Trim());

                return (Query.GetDataTable());
            }
        }

        public void UpdEmployeeProfile(string Position, string HomePhone, string MobilePhone, string Email, string EmailPass, string Address, string UserID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Update Employees Set Job_Title='{0}', Home_Phone='{1}', Mobile_Phone='{2}', Email_Address='{3}',
                                            Email_Password='{4}', Address='{5}' Where ID='{6}'",
                                        Position, HomePhone, MobilePhone, Email, EmailPass, Address, UserID);

                Query.ExecuteNonQuery();
            }
        }


        public void InsEmployeeAsset(string EmployeeID, string AssetID, string CategoryAsset, string AssetName, string AssetPhoto,
                   string AreaCode, string Site, string Location, string SerialNumber, string PONumber, string Description)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"EXEC INSEMPLOYEEASSET @EmployeeID='{0}', @AssetID='{1}', @CategoryAsset='{2}', @AssetName='{3}', @AssetPhoto='{4}',
                                            @AreaCode='{5}', @Site='{6}', @Location='{7}', @SerialNumber='{8}', @PONumber='{9}', @Description='{10}'",
                                            EmployeeID, AssetID, CategoryAsset, AssetName, AssetPhoto, AreaCode, Site, Location, SerialNumber, PONumber, Description);

                Query.ExecuteNonQuery();
            }
        }

        public void UpdEmployeeAsset(int pid, string EmployeeID, string AssetID, string CategoryAsset, string AssetName, string AssetPhoto,
                  string AreaCode, string Site, string Location, string SerialNumber, string PONumber, string Description)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"EXEC UPDEMPLOYEEASSET @pid={0}, @EmployeeID='{1}', @AssetID='{2}', @CategoryAsset='{3}', @AssetName='{4}', @AssetPhoto='{5}',
                                            @AreaCode='{6}', @Site='{7}', @Location='{8}', @SerialNumber='{9}', @PONumber='{10}', @Description='{11}'",
                                            pid, EmployeeID, AssetID, CategoryAsset, AssetName, AssetPhoto, AreaCode, Site, Location, SerialNumber, PONumber, Description);

                Query.ExecuteNonQuery();
            }
        }
        public DataTable getEmployeeAssetOther_Detail(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select A.pid As f_pid, A.EmployeeID As employeeid,
                                ISNULL(B.First_Name,'') + '' + ISNULL(B.Middle_Name, '') + '' + ISNULL(B.Last_Name, '') As f_employeename, 
                                A.AssetID As f_assetid, C.AssetType As f_category, C.AssetDetail As f_assetname,
                                C.SerialNumber As f_serialnumber, A.Site As f_site, A.Location As f_location,
                                C.PONumber As f_ponumber, A.Responsibility as f_Responsibility, A.Description As f_description
                                From EmployeeAsset A
                                Left Join Employees B ON A.EmployeeID = B.ID
                                Left join AssetItems C ON A.AssetID = C.FICNumber
                                Where B.ID='{0}' And C.iCategory='0'", strEmployeeID.Trim());

                return (Query.GetDataTable());
            }
        }
        public DataTable getEmployeeAssetOther_Update(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select A.pid As f_pid, A.EmployeeID As employeeid,
                                ISNULL(B.First_Name,'') + '' + ISNULL(B.Middle_Name, '') + '' + ISNULL(B.Last_Name, '') As f_employeename, 
                                A.AssetID As f_assetid, C.AssetType As f_category, C.AssetDetail As f_assetname,
                                C.SerialNumber As f_serialnumber, A.Site As f_site, A.Location As f_location,
                                C.PONumber As f_ponumber, A.Responsibility as f_Responsibility, A.Description As f_description
                                From EmployeeAsset A
                                Left Join Employees B ON A.EmployeeID = B.ID
                                Left join AssetItems C ON A.AssetID = C.FICNumber
                                Where B.ID='{0}' And C.iCategory='0'", strEmployeeID.Trim());

                return (Query.GetDataTable());
            }
        }
        public DataTable getCheckDoublePid(int ipid)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select *  From EmployeeAsset Where pid={0}", ipid);

                return (Query.GetDataTable());
            }
        }

        //Dependents
        public void InsEmployeeDependents(string EmployeeID, string DependentID, string DependentName, string DependentRelationship, DateTime BirthDate, string Education, string Sex, string EligibleClass, string Remark)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"EXEC INSEMPLOYEEDEPENDENTS @EmployeeID='{0}', @DependentID='{1}', @DependentName='{2}', @DependentRelationship='{3}', @BirthDate='{4}',
                                            @Education='{5}', @Sex='{6}', @EligibleClass='{7}', @Remark='{8}'",
                                            EmployeeID.Trim(), DependentID, DependentName, DependentRelationship, BirthDate, Education, Sex, EligibleClass, Remark);

                Query.ExecuteNonQuery();
            }
        }
        public void UpdEmployeeDependents(int pid, string EmployeeID, string DependentID, string DependentName, string DependentRelationship, DateTime BirthDate, string Education, string Sex, string EligibleClass, string Remark)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"EXEC UPDEMPLOYEEDEPENDENTS @pid={0}, @EmployeeID='{1}', @DependentID='{2}', @DependentName='{3}', @DependentRelationship='{4}', @BirthDate='{5}',
                                            @Education='{6}', @Sex='{7}', @EligibleClass='{8}', @Remark='{9}'",
                                            pid, EmployeeID.Trim(), DependentID, DependentName, DependentRelationship, BirthDate, Education, Sex, EligibleClass, Remark);

                Query.ExecuteNonQuery();
            }
        }
        public DataTable getCheckDoublePid_Dependents(int ipid)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select *  From EmployeeDependents Where pid={0}", ipid);

                return (Query.GetDataTable());
            }
        }
        public DataTable getEmployeeDependents_Detail(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select A.pid As f_pid_dep, A.EmployeeID As employeeid, A.DependentID As f_dependentid, A.DependentName As f_dependentname, 
                                            A.DependentRelationship As f_relationship, A.BirthDate As f_birthdate, A.Education As f_education, A.Sex As f_sex,
                                            A.EligibleClass As f_eligibleclass, A.Remark As f_remark
                                            From EmployeeDependents A
                                            Left Join Employees B ON A.EmployeeID = B.ID
                                            Where B.ID='{0}'", strEmployeeID.Trim());

                return (Query.GetDataTable());
            }
        }
        public DataTable getEmployeeDependents_Update(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select A.pid As f_pid_dep, A.EmployeeID As employeeid, A.DependentID As f_dependentid, A.DependentName As f_dependentname, 
                                            A.DependentRelationship As f_relationship, A.BirthDate As f_birthdate, A.Education As f_education, A.Sex As f_sex,
                                            A.EligibleClass As f_eligibleclass, A.Remark As f_remark
                                            From EmployeeDependents A
                                            Left Join Employees B ON A.EmployeeID = B.ID
                                            Where B.ID='{0}'", strEmployeeID.Trim());

                return (Query.GetDataTable());
            }
        }

        //Personal Data, Job Information, Emergency Contact, 

        public DataTable getEmployeesList(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select * From Employees Where ID='{0}'", strEmployeeID.Trim());

                return (Query.GetDataTable());
            }
        }
        
        public DataTable getLookupEthnicGroup()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='EthnicGroup'";
                return (Query.GetDataTable());
            }
        }
        public DataTable getLookupCity()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='City'";
                return (Query.GetDataTable());
            }
        }
        public DataTable getLookupState()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='State'";
                return (Query.GetDataTable());
            }
        }
        public DataTable getLookupCountry()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='Country'";
                return (Query.GetDataTable());
            }
        }
        public DataTable getLookupEmployeeType()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='EmployeeType'";
                return (Query.GetDataTable());
            }
        }
        public DataTable getLookupEmployeeClass()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='EmployeeClass'";
                return (Query.GetDataTable());
            }
        }
        public DataTable getLookupIndividualGrade()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='IndividualGrade'";
                return (Query.GetDataTable());
            }
        }
        public DataTable getLookupOrgGroup()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='OrgGroup'";
                return (Query.GetDataTable());
            }
        }
        public DataTable getLookupSection()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='Section'";
                return (Query.GetDataTable());
            }
        }
        public DataTable getLookupCostCenter()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='CostCenter'";
                return (Query.GetDataTable());
            }
        }
        public DataTable getLookupPayGroup()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='PayGroup'";
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookupAssetType()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "SELECT rtrim(Code) as 'Code', rtrim(Name) as 'Name' FROM LOOKUP WHERE rtrim(Category) ='AssetType'";
                return (Query.GetDataTable());
            }
        }

        public DataTable getLookupAssetName()
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select FICNumber as 'ID', AssetDetail as 'f_assetname' From AssetItems Where iUsed='0'";
                return (Query.GetDataTable());
            }
        }

        public DataTable getEmployeeTraining_Detail(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select A.pid As 'pidTraining', A.EmployeeID As 'EmployeeIDTraining', A.TrainingType As 'TrainingType',
                                            A.TrainingName As 'TrainingName', A.Mandatory As TrainingMandatory, A.TrainingLocation As TrainingLocation,
                                            A.TrainingStartDate As 'TrainingStartDate', A.TrainingEndDate As 'TrainingEndDate',
                                            A.TrainingCertificateNumber As 'TrainingCertificateNo', A.TrainingProvider As 'TrainingProvider',
                                            A.ExpiredDate As 'ExpiredDate'
                                            From EmployeeTraining A
                                            Left Join Employees B ON A.EmployeeID = B.ID
                                            Where B.ID='{0}'", strEmployeeID.Trim());

                return (Query.GetDataTable());
            }
        }
        public DataTable getEmployeeTraining_Update(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select A.pid As 'pidTraining', A.EmployeeID As 'EmployeeIDTraining', A.TrainingType As 'TrainingType',
                                            A.TrainingName As 'TrainingName', A.Mandatory As TrainingMandatory, A.TrainingLocation As TrainingLocation,
                                            A.TrainingStartDate As 'TrainingStartDate', A.TrainingEndDate As 'TrainingEndDate',
                                            A.TrainingCertificateNumber As 'TrainingCertificateNo', A.TrainingProvider As 'TrainingProvider',
                                            A.ExpiredDate As 'ExpiredDate'
                                            From EmployeeTraining A
                                            Left Join Employees B ON A.EmployeeID = B.ID
                                            Where B.ID='{0}'", strEmployeeID.Trim());

                return (Query.GetDataTable());
            }
        }

        public void InsTraining(string idEmployeeTraining, string TrainingType, string TrainingName, string TrainingMandatory, string TrainingLocation,
                                DateTime TrainingStartDate, DateTime TrainingEndDate, string TrainingCertificate, string TrainingProvider, DateTime ExpiredDate)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Insert Into EmployeeTraining(EmployeeID,TrainingType,TrainingName,Mandatory,TrainingLocation,
                                            TrainingStartDate,TrainingEndDate,TrainingCertificateNumber,TrainingProvider,ExpiredDate)
                                            Values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')", idEmployeeTraining, TrainingType,
                                            TrainingName, TrainingMandatory, TrainingLocation, TrainingStartDate, TrainingEndDate, TrainingCertificate,
                                            TrainingProvider, ExpiredDate);
                Query.ExecuteNonQuery();
            }
        }

        public void UpdTraining(int pidTraining, string idEmployeeTraining, string TrainingType, string TrainingName, string TrainingMandatory, string TrainingLocation,
                                DateTime TrainingStartDate, DateTime TrainingEndDate, string TrainingCertificate, string TrainingProvider, DateTime ExpiredDate)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {

                Query.Query = String.Format(@"Update EmployeeTraining Set EmployeeID='{0}', TrainingType='{1}', TrainingName='{2}', Mandatory='{3}',
                                            TrainingLocation='{4}', TrainingStartDate='{5}', TrainingEndDate='{6}', TrainingCertificateNumber='{7}',
                                            TrainingProvider='{8}', ExpiredDate='{9}' Where pid='{10}'",idEmployeeTraining, TrainingType,
                                            TrainingName, TrainingMandatory, TrainingLocation, TrainingStartDate, TrainingEndDate, TrainingCertificate,
                                            TrainingProvider, ExpiredDate, pidTraining);
                Query.ExecuteNonQuery();
            }
        }

        public DataTable getEmployeeAssetSoftware_Detail(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select A.pid As f_pidSoftware, A.EmployeeID As employeeidSoftware,
                                ISNULL(B.First_Name,'') + '' + ISNULL(B.Middle_Name, '') + '' + ISNULL(B.Last_Name, '') As f_employeenameSoftware, 
                                A.AssetID As f_assetidSoftware, C.AssetType As f_categorySoftware, C.AssetDetail As f_assetnameSoftware,
                                C.SerialNumber As f_serialnumberSoftware, A.Site As f_siteSoftware, A.Location As f_locationSoftware,
                                C.PONumber As f_ponumberSoftware, A.Responsibility as f_ResponsibilitySoftware, A.Description As f_descriptionSoftware
                                From EmployeeAsset A
                                Left Join Employees B ON A.EmployeeID = B.ID
                                Left join AssetItems C ON A.AssetID = C.FICNumber
                                Where B.ID='{0}' And C.iCategory='1'", strEmployeeID.Trim());
//                Query.Query = string.Format(@"Select A.pid As f_pidSoftware, A.EmployeeID As employeeidSoftware,
//                                ISNULL(B.First_Name,'') + '' + ISNULL(B.Middle_Name, '') + '' + ISNULL(B.Last_Name, '') As f_employeenameSoftware, 
//                                A.AssetID As f_assetidSoftware, A.CategoryAsset As f_categorySoftware, A.AssetName As f_assetnameSoftware,
//                                A.SerialNumber As f_serialnumberSoftware, A.AreaCode  As f_areacodeSoftware, A.Site As f_siteSoftware, A.Location As f_locationSoftware,
//                                A.PONumber As f_ponumberSoftware, A.Description As f_descriptionSoftware
//                                From EmployeeAsset A
//                                Left Join Employees B ON A.EmployeeID = B.ID
//                                Where B.ID='{0}' And iCategory='1'", strEmployeeID.Trim());

                return (Query.GetDataTable());
            }
        }
        public DataTable getEmployeeAssetSoftware_Update(string strEmployeeID)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select A.pid As f_pidSoftware, A.EmployeeID As employeeidSoftware,
                                ISNULL(B.First_Name,'') + '' + ISNULL(B.Middle_Name, '') + '' + ISNULL(B.Last_Name, '') As f_employeenameSoftware, 
                                A.AssetID As f_assetidSoftware, C.AssetType As f_categorySoftware, C.AssetDetail As f_assetnameSoftware,
                                C.SerialNumber As f_serialnumberSoftware, A.Site As f_siteSoftware, A.Location As f_locationSoftware,
                                C.PONumber As f_ponumberSoftware, A.Responsibility as f_ResponsibilitySoftware, A.Description As f_descriptionSoftware
                                From EmployeeAsset A
                                Left Join Employees B ON A.EmployeeID = B.ID
                                Left join AssetItems C ON A.AssetID = C.FICNumber
                                Where B.ID='{0}' And C.iCategory='1'", strEmployeeID.Trim());

                return (Query.GetDataTable());
            }
        }
        public DataTable getAssetType(string strFICNumber)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select FICNumber, AssetType From AssetItems Where FICNumber='{0}'", strFICNumber.Trim());

                return (Query.GetDataTable());
            }
        }

        public void UpdAssetItem(string FICNumber, string strAssetType, Int16 iUsed)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Int16 iCategory;

                if(strAssetType == "Software")
                {
                    iCategory = 1;
                }
                else
                {
                    iCategory = 0;
                }

                Query.Query = String.Format("Update AssetItems Set iCategory='{0}', iUsed='{1}' Where FICNumber='{2}'", iCategory, iUsed, FICNumber);

                Query.ExecuteNonQuery();
            }
        }

        public void InsAsset(string EmployeeIDAsset, string AssetID, string AssetSite, string AssetLocation, string AssetResponsibility, string strRemarkAsset)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = String.Format(@"Insert Into EmployeeAsset(EmployeeID,AssetID,Site,Location,Responsibility,Description)
                                            Values('{0}','{1}','{2}','{3}','{4}','{5}')", EmployeeIDAsset, AssetID, AssetSite, AssetLocation, AssetResponsibility, strRemarkAsset);

                Query.ExecuteNonQuery();
            }
        }
        public void UpdateAssetItem(string strFICNumber)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Update AssetItems Set iUsed='0' Where FICNumber='{0}'", strFICNumber);

                Query.ExecuteNonQuery();
            }
        }
        public void Delete_Assets(int iPid)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Delete From EmployeeAsset Where pid={0}", iPid);

                Query.ExecuteNonQuery();
            }
        }
        public void Delete_Dependents(int iPid)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Delete From EmployeeDependents Where pid={0}", iPid);

                Query.ExecuteNonQuery();
            }
        }
        public void Delete_Training(int iPid)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Delete From EmployeeTraining Where pid={0}", iPid);

                Query.ExecuteNonQuery();
            }
        }
        public DataTable get_AssetItems_Search(string strFICNumber)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                if (!string.IsNullOrEmpty(strFICNumber))
                {
                    Query.Query = string.Format(@"Select A.FICNumber AS 'FICNumber', A.FICName AS 'FICName', A.OLDName AS 'OLDName', A.ServiceTag AS 'ServiceTag',
                                                A.SerialNumber AS 'SerialNo', A.AssetType AS 'AssetType', A.BrandModel as 'BrandModel', A.AssetDetail AS 'AssetDetail',
                                                A.DeliveryNo AS 'DeliveryNo', Convert(char,A.DeliveryDate,106) as 'DeliveryDate', A.ItemCode AS 'ItemCode', 
                                                A.AccountingCode AS 'AccountingCode', A.CompanyAsset AS 'CompanyAsset', A.JoinDomainFMI AS 'JoinDomainFMI',
                                                A.JoinDomainTicket AS 'JoinDomainTicket', A.JoinDomainDate AS 'JoinDomainDate', A.JoinDomainBy AS 'JoinDomainBy',
                                                A.AssetStatus AS 'AssetStatus', A.DisposedTicket AS 'DisposedTicket', CONVERT(char,A.DisposedDate,106) AS 'DisposedDate',
                                                A.DisposedLetter AS 'DisposedLetter', A.DisposedBy AS 'DisposedBy', A.AssetRemark AS 'AssetRemarks',
                                                Case when A.iUsed='0' then
	                                                'No'
                                                else
	                                                'Yes'
                                                end as iUsed,
                                                C.First_Name+' '+C.Middle_Name+' '+C.Last_Name AS 'EmployeeName'
                                                From AssetItems A
                                                Left join EmployeeAsset B ON A.FICNumber = B.AssetID
                                                Left join Employees C ON B.EmployeeID = C.ID
                                                Where A.FICNumber like '%{0}%' OR A.FICNAME like '%{0}%'", strFICNumber.Trim());
                }
                else
                {
                    Query.Query = string.Format(@"Select A.FICNumber AS 'FICNumber', A.FICName AS 'FICName', A.OLDName AS 'OLDName', A.ServiceTag AS 'ServiceTag',
                                                A.SerialNumber AS 'SerialNo', A.AssetType AS 'AssetType', A.BrandModel as 'BrandModel', A.AssetDetail AS 'AssetDetail',
                                                A.DeliveryNo AS 'DeliveryNo', Convert(char,A.DeliveryDate,106) as 'DeliveryDate', A.ItemCode AS 'ItemCode', 
                                                A.AccountingCode AS 'AccountingCode', A.CompanyAsset AS 'CompanyAsset', A.JoinDomainFMI AS 'JoinDomainFMI',
                                                A.JoinDomainTicket AS 'JoinDomainTicket', A.JoinDomainDate AS 'JoinDomainDate', A.JoinDomainBy AS 'JoinDomainBy',
                                                A.AssetStatus AS 'AssetStatus', A.DisposedTicket AS 'DisposedTicket', CONVERT(char,A.DisposedDate,106) AS 'DisposedDate',
                                                A.DisposedLetter AS 'DisposedLetter', A.DisposedBy AS 'DisposedBy', A.AssetRemark AS 'AssetRemarks',
                                                Case when A.iUsed='0' then
	                                                'No'
                                                else
	                                                'Yes'
                                                end as iUsed,
                                                C.First_Name+' '+C.Middle_Name+' '+C.Last_Name AS 'EmployeeName'
                                                From AssetItems A
                                                Left join EmployeeAsset B ON A.FICNumber = B.AssetID
                                                Left join Employees C ON B.EmployeeID = C.ID");
                }

                return (Query.GetDataTable());
            }
        }
        public DataTable get_CheckFICNumber(string strCode)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Select * From AssetItems Where FICNumber ='{0}'", strCode.Trim());

                return (Query.GetDataTable());
            }
        }

        public void InsertItemsAset(string strFicNumber, string strFicName, string strOldName, string ServiceTag, string strSerialNo, string strPONumber, string strPOPath,
                            string strAssetType, string strBrandModel, string strAssetDetail, DateTime dDeliveryDate, string strDeliveryNo, string strItemCode,
                            string strAccCode, string strCompanyAsset, string strJoinDomainFMI, string strJoinDomainTicket, DateTime dJoinDomainDate, string strJoinDomainBy,
                            string strAssetStatus, string strDisposedTicket, DateTime dDisposedDate, string strDisposedLetter, string strDisposedBy, string strPhoto,
                            string strPhotoPath, string strAssetRemark, string strUserID, Int16 iCategory, Int16 iUsed)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                if (Convert.ToString(dDeliveryDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) == "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Insert Into AssetItems(FICNumber, FICName, OLDName, ServiceTag, SerialNumber, PONumber, POPath, AssetType,
                          BrandModel, AssetDetail, DeliveryDate, DeliveryNo, ItemCode, AccountingCode, CompanyAsset, JoinDomainFMI, JoinDomainTicket, JoinDomainDate,
                          JoinDomainBy, AssetStatus, DisposedTicket, DisposedDate, DisposedLetter, DisposedBy, Photo, PhotoPath, AssetRemark, ModifiedBy, iCategory, iUsed)
                          values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',NULL,'{10}','{11}','{12}','{13}','{14}','{15}',NULL,'{16}','{17}','{18}',NULL,
                          '{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}')", strFicNumber, strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, strJoinDomainBy, strAssetStatus, strDisposedTicket, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed);
                }
                else if (Convert.ToString(dDeliveryDate) != "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) == "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Insert Into AssetItems(FICNumber, FICName, OLDName, ServiceTag, SerialNumber, PONumber, POPath, AssetType,
                          BrandModel, AssetDetail, DeliveryDate, DeliveryNo, ItemCode, AccountingCode, CompanyAsset, JoinDomainFMI, JoinDomainTicket, JoinDomainDate,
                          JoinDomainBy, AssetStatus, DisposedTicket, DisposedDate, DisposedLetter, DisposedBy, Photo, PhotoPath, AssetRemark, ModifiedBy, iCategory, iUsed)
                          values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}',NULL,'{17}','{18}','{19}',NULL,
                          '{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}')", strFicNumber, strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, dDeliveryDate, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, strJoinDomainBy, strAssetStatus, strDisposedTicket, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed);
                }
                else if (Convert.ToString(dDeliveryDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) != "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) == "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Insert Into AssetItems(FICNumber, FICName, OLDName, ServiceTag, SerialNumber, PONumber, POPath, AssetType,
                          BrandModel, AssetDetail, DeliveryDate, DeliveryNo, ItemCode, AccountingCode, CompanyAsset, JoinDomainFMI, JoinDomainTicket, JoinDomainDate,
                          JoinDomainBy, AssetStatus, DisposedTicket, DisposedDate, DisposedLetter, DisposedBy, Photo, PhotoPath, AssetRemark, ModifiedBy, iCategory, iUsed)
                          values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',NULL,'{10}','{11}','{12}','{13}','{14}','{15}',NULL,'{16}','{17}','{18}','{19}',
                          '{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}')", strFicNumber, strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, strJoinDomainBy, strAssetStatus, strDisposedTicket, dDisposedDate, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed);
                }
                else if (Convert.ToString(dDeliveryDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) != "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Insert Into AssetItems(FICNumber, FICName, OLDName, ServiceTag, SerialNumber, PONumber, POPath, AssetType,
                          BrandModel, AssetDetail, DeliveryDate, DeliveryNo, ItemCode, AccountingCode, CompanyAsset, JoinDomainFMI, JoinDomainTicket, JoinDomainDate,
                          JoinDomainBy, AssetStatus, DisposedTicket, DisposedDate, DisposedLetter, DisposedBy, Photo, PhotoPath, AssetRemark, ModifiedBy, iCategory, iUsed)
                          values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',NULL,'{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}',NULL,
                          '{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}')", strFicNumber, strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, dJoinDomainDate, strJoinDomainBy, strAssetStatus, strDisposedTicket, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed);
                }
                else if (Convert.ToString(dDeliveryDate) != "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) != "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Insert Into AssetItems(FICNumber, FICName, OLDName, ServiceTag, SerialNumber, PONumber, POPath, AssetType,
                          BrandModel, AssetDetail, DeliveryDate, DeliveryNo, ItemCode, AccountingCode, CompanyAsset, JoinDomainFMI, JoinDomainTicket, JoinDomainDate,
                          JoinDomainBy, AssetStatus, DisposedTicket, DisposedDate, DisposedLetter, DisposedBy, Photo, PhotoPath, AssetRemark, ModifiedBy, iCategory, iUsed)
                          values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}',NULL,
                          '{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}')", strFicNumber, strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, dDeliveryDate, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, dJoinDomainDate, strJoinDomainBy, strAssetStatus, strDisposedTicket, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed);
                }
                else if (Convert.ToString(dDeliveryDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) != "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) != "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Insert Into AssetItems(FICNumber, FICName, OLDName, ServiceTag, SerialNumber, PONumber, POPath, AssetType,
                          BrandModel, AssetDetail, DeliveryDate, DeliveryNo, ItemCode, AccountingCode, CompanyAsset, JoinDomainFMI, JoinDomainTicket, JoinDomainDate,
                          JoinDomainBy, AssetStatus, DisposedTicket, DisposedDate, DisposedLetter, DisposedBy, Photo, PhotoPath, AssetRemark, ModifiedBy, iCategory, iUsed)
                          values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',NULL,'{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}',
                          '{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}')", strFicNumber, strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, dJoinDomainDate, strJoinDomainBy, strAssetStatus, strDisposedTicket, dDisposedDate, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed);
                }
                else if(Convert.ToString(dDeliveryDate) != "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) != "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) == "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Insert Into AssetItems(FICNumber, FICName, OLDName, ServiceTag, SerialNumber, PONumber, POPath, AssetType,
                          BrandModel, AssetDetail, DeliveryDate, DeliveryNo, ItemCode, AccountingCode, CompanyAsset, JoinDomainFMI, JoinDomainTicket, JoinDomainDate,
                          JoinDomainBy, AssetStatus, DisposedTicket, DisposedDate, DisposedLetter, DisposedBy, Photo, PhotoPath, AssetRemark, ModifiedBy, iCategory, iUsed)
                          values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}',NULL,'{17}','{18}','{19}',
                          '{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}')", strFicNumber, strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, dDeliveryDate, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, strJoinDomainBy, strAssetStatus, strDisposedTicket, dDisposedDate, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed);
                }
                else
                {
                    Query.Query = String.Format(@"Insert Into AssetItems(FICNumber, FICName, OLDName, ServiceTag, SerialNumber, PONumber, POPath, AssetType,
                          BrandModel, AssetDetail, DeliveryDate, DeliveryNo, ItemCode, AccountingCode, CompanyAsset, JoinDomainFMI, JoinDomainTicket, JoinDomainDate,
                          JoinDomainBy, AssetStatus, DisposedTicket, DisposedDate, DisposedLetter, DisposedBy, Photo, PhotoPath, AssetRemark, ModifiedBy, iCategory, iUsed)
                          values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}',
                          '{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}')", strFicNumber, strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                                strAssetType, strBrandModel, strAssetDetail, dDeliveryDate, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI, 
                                strJoinDomainTicket, dJoinDomainDate, strJoinDomainBy, strAssetStatus, strDisposedTicket, dDisposedDate, strDisposedLetter, strDisposedBy,
                                strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed);
                }

                Query.ExecuteNonQuery();
            }
        }

        public void UpdateItemsAset(string strFicNumber, string strFicName, string strOldName, string ServiceTag, string strSerialNo, string strPONumber, string strPOPath,
                    string strAssetType, string strBrandModel, string strAssetDetail, DateTime dDeliveryDate, string strDeliveryNo, string strItemCode,
                    string strAccCode, string strCompanyAsset, string strJoinDomainFMI, string strJoinDomainTicket, DateTime dJoinDomainDate, string strJoinDomainBy,
                    string strAssetStatus, string strDisposedTicket, DateTime dDisposedDate, string strDisposedLetter, string strDisposedBy, string strPhoto,
                    string strPhotoPath, string strAssetRemark, string strUserID, Int16 iCategory, Int16 iUsed)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                if (Convert.ToString(dDeliveryDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) == "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Update AssetItems Set FICName='{0}', OLDName='{1}', ServiceTag='{2}', SerialNumber='{3}', PONumber='{4}', POPath='{5}',
                            AssetType='{6}', BrandModel='{7}', AssetDetail='{8}', DeliveryDate=NULL, DeliveryNo='{9}', ItemCode='{10}', AccountingCode='{11}',
                            CompanyAsset='{12}', JoinDomainFMI='{13}', JoinDomainTicket='{14}', JoinDomainDate=NULL, JoinDomainBy='{15}', AssetStatus='{16}', 
                            DisposedTicket='{17}', DisposedDate=NULL, DisposedLetter='{18}', DisposedBy='{19}', Photo='{20}', PhotoPath='{21}', AssetRemark='{22}',
                            ModifiedBy='{23}', iCategory='{24}', iUsed='{25}' Where FICNumber='{26}'",strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, strJoinDomainBy, strAssetStatus, strDisposedTicket, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed, strFicNumber);
                }
                else if (Convert.ToString(dDeliveryDate) != "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) == "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Update AssetItems Set FICName='{0}', OLDName='{1}', ServiceTag='{2}', SerialNumber='{3}', PONumber='{4}', POPath='{5}',
                            AssetType='{6}', BrandModel='{7}', AssetDetail='{8}', DeliveryDate='{9}', DeliveryNo='{10}', ItemCode='{11}', AccountingCode='{12}',
                            CompanyAsset='{13}', JoinDomainFMI='{14}', JoinDomainTicket='{15}', JoinDomainDate=NULL, JoinDomainBy='{16}', AssetStatus='{17}', 
                            DisposedTicket='{18}', DisposedDate=NULL, DisposedLetter='{19}', DisposedBy='{20}', Photo='{21}', PhotoPath='{22}', AssetRemark='{23}',
                            ModifiedBy='{24}', iCategory='{25}', iUsed='{26}' Where FICNumber='{27}'", strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, dDeliveryDate, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, strJoinDomainBy, strAssetStatus, strDisposedTicket, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed, strFicNumber);
                }
                else if (Convert.ToString(dDeliveryDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) != "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) == "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Update AssetItems Set FICName='{0}', OLDName='{1}', ServiceTag='{2}', SerialNumber='{3}', PONumber='{4}', POPath='{5}',
                            AssetType='{6}', BrandModel='{7}', AssetDetail='{8}', DeliveryDate=NULL, DeliveryNo='{9}', ItemCode='{10}', AccountingCode='{11}',
                            CompanyAsset='{12}', JoinDomainFMI='{13}', JoinDomainTicket='{14}', JoinDomainDate=NULL, JoinDomainBy='{15}', AssetStatus='{16}', 
                            DisposedTicket='{17}', DisposedDate='{18}', DisposedLetter='{19}', DisposedBy='{20}', Photo='{21}', PhotoPath='{22}', AssetRemark='{23}',
                            ModifiedBy='{24}', iCategory='{25}', iUsed='{26}' Where FICNumber='{27}'", strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, strJoinDomainBy, strAssetStatus, strDisposedTicket, dDisposedDate, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed, strFicNumber);
                }
                else if (Convert.ToString(dDeliveryDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) != "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Update AssetItems Set FICName='{0}', OLDName='{1}', ServiceTag='{2}', SerialNumber='{3}', PONumber='{4}', POPath='{5}',
                            AssetType='{6}', BrandModel='{7}', AssetDetail='{8}', DeliveryDate=NULL, DeliveryNo='{9}', ItemCode='{10}', AccountingCode='{11}',
                            CompanyAsset='{12}', JoinDomainFMI='{13}', JoinDomainTicket='{14}', JoinDomainDate='{15}', JoinDomainBy='{16}', AssetStatus='{17}', 
                            DisposedTicket='{18}', DisposedDate=NULL, DisposedLetter='{19}', DisposedBy='{20}', Photo='{21}', PhotoPath='{22}', AssetRemark='{23}',
                            ModifiedBy='{24}', iCategory='{25}', iUsed='{26}' Where FICNumber='{27}'", strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, dJoinDomainDate, strJoinDomainBy, strAssetStatus, strDisposedTicket, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed, strFicNumber);
                }
                else if (Convert.ToString(dDeliveryDate) != "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) != "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Update AssetItems Set FICName='{0}', OLDName='{1}', ServiceTag='{2}', SerialNumber='{3}', PONumber='{4}', POPath='{5}',
                            AssetType='{6}', BrandModel='{7}', AssetDetail='{8}', DeliveryDate='{9}', DeliveryNo='{10}', ItemCode='{11}', AccountingCode='{12}',
                            CompanyAsset='{13}', JoinDomainFMI='{14}', JoinDomainTicket='{15}', JoinDomainDate='{16}', JoinDomainBy='{17}', AssetStatus='{18}', 
                            DisposedTicket='{19}', DisposedDate=NULL, DisposedLetter='{20}', DisposedBy='{21}', Photo='{22}', PhotoPath='{23}', AssetRemark='{24}',
                            ModifiedBy='{25}', iCategory='{26}', iUsed='{27}' Where FICNumber='{28}'", strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, dDeliveryDate, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, dJoinDomainDate, strJoinDomainBy, strAssetStatus, strDisposedTicket, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed, strFicNumber);
                }
                else if (Convert.ToString(dDeliveryDate) == "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) != "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) != "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Update AssetItems Set FICName='{0}', OLDName='{1}', ServiceTag='{2}', SerialNumber='{3}', PONumber='{4}', POPath='{5}',
                            AssetType='{6}', BrandModel='{7}', AssetDetail='{8}', DeliveryDate=NULL, DeliveryNo='{9}', ItemCode='{10}', AccountingCode='{11}',
                            CompanyAsset='{12}', JoinDomainFMI='{13}', JoinDomainTicket='{14}', JoinDomainDate='{15}', JoinDomainBy='{16}', AssetStatus='{17}', 
                            DisposedTicket='{18}', DisposedDate='{19}', DisposedLetter='{20}', DisposedBy='{21}', Photo='{22}', PhotoPath='{23}', AssetRemark='{24}',
                            ModifiedBy='{25}', iCategory='{26}', iUsed='{27}' Where FICNumber='{28}'", strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, dJoinDomainDate, strJoinDomainBy, strAssetStatus, strDisposedTicket, dDisposedDate, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed, strFicNumber);
                }
                else if (Convert.ToString(dDeliveryDate) != "1/1/0001 12:00:00 AM" && Convert.ToString(dDisposedDate) != "1/1/0001 12:00:00 AM" && Convert.ToString(dJoinDomainDate) == "1/1/0001 12:00:00 AM")
                {
                    Query.Query = String.Format(@"Update AssetItems Set FICName='{0}', OLDName='{1}', ServiceTag='{2}', SerialNumber='{3}', PONumber='{4}', POPath='{5}',
                            AssetType='{6}', BrandModel='{7}', AssetDetail='{8}', DeliveryDate='{9}', DeliveryNo='{10}', ItemCode='{11}', AccountingCode='{12}',
                            CompanyAsset='{13}', JoinDomainFMI='{14}', JoinDomainTicket='{15}', JoinDomainDate=NULL, JoinDomainBy='{16}', AssetStatus='{17}', 
                            DisposedTicket='{18}', DisposedDate='{19}', DisposedLetter='{20}', DisposedBy='{21}', Photo='{22}', PhotoPath='{23}', AssetRemark='{24}',
                            ModifiedBy='{25}', iCategory='{26}', iUsed='{27}' Where FICNumber='{28}'", strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, dDeliveryDate, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, strJoinDomainBy, strAssetStatus, strDisposedTicket, dDisposedDate, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed, strFicNumber);
                }
                else
                {
                    Query.Query = String.Format(@"Update AssetItems Set FICName='{0}', OLDName='{1}', ServiceTag='{2}', SerialNumber='{3}', PONumber='{4}', POPath='{5}',
                            AssetType='{6}', BrandModel='{7}', AssetDetail='{8}', DeliveryDate='{9}', DeliveryNo='{10}', ItemCode='{11}', AccountingCode='{12}',
                            CompanyAsset='{13}', JoinDomainFMI='{14}', JoinDomainTicket='{15}', JoinDomainDate='{16}', JoinDomainBy='{17}', AssetStatus='{18}', 
                            DisposedTicket='{19}', DisposedDate='{20}', DisposedLetter='{21}', DisposedBy='{22}', Photo='{23}', PhotoPath='{24}', AssetRemark='{25}',
                            ModifiedBy='{26}', iCategory='{27}', iUsed='{28}' Where FICNumber='{29}'", strFicName, strOldName, ServiceTag, strSerialNo, strPONumber, strPOPath,
                        strAssetType, strBrandModel, strAssetDetail, dDeliveryDate, strDeliveryNo, strItemCode, strAccCode, strCompanyAsset, strJoinDomainFMI,
                        strJoinDomainTicket, dJoinDomainDate, strJoinDomainBy, strAssetStatus, strDisposedTicket, dDisposedDate, strDisposedLetter, strDisposedBy,
                        strPhoto, strPhotoPath, strAssetRemark, strUserID, iCategory, iUsed, strFicNumber);
                }

                Query.ExecuteNonQuery();
            }
        }

        public DataTable get_Asset_Detail_Update(string strFICNumber)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"Select A.FICNumber, A.FICName, A.OLDName, A.ServiceTag, A.SerialNumber, A.PONumber, A.POPath, B.Code as AssetType,
                                A.BrandModel, A.AssetDetail, A.DeliveryDate, A.DeliveryNo, A.ItemCode, A.AccountingCode, A.CompanyAsset,
                                A.JoinDomainFMI, A.JoinDomainTicket, A.JoinDomainBy, A.AssetStatus, A.DisposedTicket, A.DisposedDate, A.DisposedLetter,
                                A.DisposedBy, A.Photo, A.PhotoPath, A.AssetRemark
                                 From AssetItems A
                                Left Join Lookup B ON B.Code = A.AssetType
                                Where A.FICNumber='{0}'", strFICNumber);

                return (Query.GetDataTable());
            }
        }

        public DataTable getAssetItems_ExportToExcel(string strFICNumber)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                if (!string.IsNullOrEmpty(strFICNumber))
                {
                    Query.Query = string.Format(@"Select A.FICNumber AS 'FIC Number', A.FICName AS 'FIC Name', A.OLDName AS 'Old Name', A.ServiceTag AS 'Service Tag',
                                                A.SerialNumber AS 'Serial No.', A.AssetType AS 'Asset Type', A.BrandModel as 'Brand Model', A.AssetDetail AS 'Asset Detail',
                                                A.DeliveryNo AS 'Delivery No.', Convert(char,A.DeliveryDate,106) as 'Delivery Date', A.ItemCode AS 'Item Code', 
                                                A.AccountingCode AS 'Acc. Code', A.CompanyAsset AS 'Company Asset', A.JoinDomainFMI AS 'Join Domain FMI',
                                                A.JoinDomainTicket AS 'Join Domain Ticket', A.JoinDomainDate AS 'Join Domain Date', A.JoinDomainBy AS 'Join Domain By',
                                                A.AssetStatus AS 'Asset Status', A.DisposedTicket AS 'DisposedT icket', CONVERT(char,A.DisposedDate,106) AS 'Disposed Date',
                                                A.DisposedLetter AS 'Disposed Letter', A.DisposedBy AS 'Disposed By', A.AssetRemark AS 'Remark',
                                                Case when A.iUsed='0' then
	                                                'No'
                                                else
	                                                'Yes'
                                                end as Used,
                                                C.First_Name+' '+C.Middle_Name+' '+C.Last_Name AS 'Employee Name'
                                                From AssetItems A
                                                Left join EmployeeAsset B ON A.FICNumber = B.AssetID
                                                Left join Employees C ON B.EmployeeID = C.ID
                                                Where A.FICNumber='{0}'", strFICNumber.Trim());
                }
                else
                {
                    Query.Query = string.Format(@"Select A.FICNumber AS 'FIC Number', A.FICName AS 'FIC Name', A.OLDName AS 'Old Name', A.ServiceTag AS 'Service Tag',
                                                A.SerialNumber AS 'Serial No.', A.AssetType AS 'Asset Type', A.BrandModel as 'Brand Model', A.AssetDetail AS 'Asset Detail',
                                                A.DeliveryNo AS 'Delivery No.', Convert(char,A.DeliveryDate,106) as 'Delivery Date', A.ItemCode AS 'Item Code', 
                                                A.AccountingCode AS 'Acc. Code', A.CompanyAsset AS 'Company Asset', A.JoinDomainFMI AS 'Join Domain FMI',
                                                A.JoinDomainTicket AS 'Join Domain Ticket', A.JoinDomainDate AS 'Join Domain Date', A.JoinDomainBy AS 'Join Domain By',
                                                A.AssetStatus AS 'Asset Status', A.DisposedTicket AS 'DisposedT icket', CONVERT(char,A.DisposedDate,106) AS 'Disposed Date',
                                                A.DisposedLetter AS 'Disposed Letter', A.DisposedBy AS 'Disposed By', A.AssetRemark AS 'Remark',
                                                Case when A.iUsed='0' then
	                                                'No'
                                                else
	                                                'Yes'
                                                end as Used,
                                                C.First_Name+' '+C.Middle_Name+' '+C.Last_Name AS 'Employee Name'
                                                From AssetItems A
                                                Left join EmployeeAsset B ON A.FICNumber = B.AssetID
                                                Left join Employees C ON B.EmployeeID = C.ID");
                }

                return (Query.GetDataTable());
            }
        }

        public void DeleteAssetItems(string strFICNumber)
        {
            string constr = CommonUtility.ConStr();
            using (ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Delete From AssetItems Where FICNumber='{0}'", strFICNumber);

                Query.ExecuteNonQuery();
            }
        }
    }
}
