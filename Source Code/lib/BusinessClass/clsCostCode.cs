﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartLibraries.DataAccessLayer;
using System.Data;
using NewERDM.lib.Helper;

namespace NewERDM.lib.BusinessClass
{
    class clsCostCode
    {
        public DataTable getCostCode()
        {
            string constr = CommonUtility.ConStr();
            using(ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = "Select * From LK_COSTCODE";
                return (Query.GetDataTable());
            }
        }


        public void fSave(int mode, int pid, string costcode, string category, string location, string client, string activity, string description)
        {
            string constr = CommonUtility.ConStr();
            using(ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"EXEC ERDM_COSTCODE @mode={0}, @pid={1}, @CostCode='{2}', @Category='{3}', 
                            @Location='{4}', @Client='{5}', @Activity='{6}', @Description='{7}'",
                             mode, pid, costcode, category, location, client, activity, description);

                Query.ExecuteNonQuery();
            }
        }
        public void fUpdate(int mode, int pid, string costcode, string category, string location, string client, string activity, string description)
        {
            string constr = CommonUtility.ConStr();
            using(ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format(@"EXEC ERDM_COSTCODE @mode={0}, @pid={1}, @CostCode='{2}', @Category='{3}', 
                            @Location='{4}', @Client='{5}', @Activity='{6}', @Description='{7}'",
                             mode, pid, costcode, category, location, client, activity, description);

                Query.ExecuteNonQuery();
            
            }
        }

        public void fDelete(int pid)
        {
            string constr = CommonUtility.ConStr();
            using(ISmartQuery Query = new SmartOledbQuery(constr))
            {
                Query.Query = string.Format("Delete From LK_COSTCODE Where pid='{0}'", pid);

                Query.ExecuteNonQuery();
            }
        }

    }
}
