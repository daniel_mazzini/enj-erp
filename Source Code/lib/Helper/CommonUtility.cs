﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Threading;
using System.Text;
using System.IO;
using Ext.Net;
using System.Web;


namespace NewERDM.lib.Helper
{
    public class CommonUtility
    {
        public static String encryptText(String strText)
        {

            byte[] _encryted;
            String strEncryptedText = "";
            try
            {
                _encryted = Encoding.Unicode.GetBytes(strText);
                strEncryptedText = Convert.ToBase64String(_encryted);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return strEncryptedText;
        }
        public static String decryptText(String strText)
        {
            byte[] _decryted;
            String strDecryptedText = "";
            try
            {

                _decryted = Convert.FromBase64String(strText);
                strDecryptedText = Encoding.Unicode.GetString(_decryted, 0, _decryted.Length);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return strDecryptedText;
        }
    
        public static string Terbilang(int x)
        {
            string[] bilangan = {"", "satu", "dua", "tiga", "empat", "lima",
			"enam", "tujuh", "delapan", "sembilan", "sepuluh",
			"sebelas"};
            string temp = "";

            if (x < 12)
            {
                temp = " " + bilangan[x];
            }
            else if (x < 20)
            {
                temp = Terbilang(x - 10) + " belas";
            }
            else if (x < 100)
            {
                temp = String.Format("{0} puluh{1}", Terbilang(x / 10), Terbilang(x % 10));
            }
            else if (x < 200)
            {
                temp = " seratus" + Terbilang(x - 100);
            }
            else if (x < 1000)
            {
                temp = String.Format("{0} ratus{1}", Terbilang(x / 100), Terbilang(x % 100));
            }
            else if (x < 2000)
            {
                temp = " seribu" + Terbilang(x - 1000);
            }
            else if (x < 1000000)
            {
                temp = String.Format("{0} ribu{1}", Terbilang(x / 1000), Terbilang(x % 1000));
            }
            else if (x < 1000000000)
            {
                temp = String.Format("{0} juta{1}", Terbilang(x / 1000000), Terbilang(x % 1000000));
            }

            return temp;
        }
        public static string sTanggal(DateTime Tanggal)
        {

            int bulan = Tanggal.Month;
            string strTanggal;

            string sNamaBulan = "";
            switch (bulan)
            {
                case 1:
                    sNamaBulan = "Januari";
                    break;
                case 2:
                    sNamaBulan = "Februari";
                    break;
                case 3:
                    sNamaBulan = "Maret";
                    break;
                case 4:
                    sNamaBulan = "April";
                    break;
                case 5:
                    sNamaBulan = "Mei";
                    break;
                case 6:
                    sNamaBulan = "Juni";
                    break;
                case 7:
                    sNamaBulan = "Juli";
                    break;
                case 8:
                    sNamaBulan = "Agustus";
                    break;
                case 9:
                    sNamaBulan = "September";
                    break;
                case 10:
                    sNamaBulan = "Oktober";
                    break;
                case 11:
                    sNamaBulan = "Nopember";
                    break;
                case 12:
                    sNamaBulan = "Desember";
                    break;
            }

            strTanggal = String.Format("{0} {1} {2}", Tanggal.Day, sNamaBulan, Tanggal.Year);

            return strTanggal;
        }
        public static string UpperFirst(string text)
        {
            return char.ToUpper(text[0]) +
                ((text.Length > 1) ? text.Substring(1).ToLower() : string.Empty);
        }
        public static string UppercaseFirstEach(string s)
        {
            char[] a = s.ToLower().ToCharArray();

            for (int i = 0; i < a.Count(); i++)
            {
                a[i] = i == 0 || a[i - 1] == ' ' ? char.ToUpper(a[i]) : a[i];

            }

            return new string(a);
        }
        public static Boolean MoveAndRename(string sFilename, string sFilenameDest, string sCode)
        {
            if (sFilename == "" || sFilenameDest == "") { return true; }

            string strFilenamedest = sFilenameDest.Replace("/", "");


            Boolean isSukses;
            string strpathweb = HttpContext.Current.Server.MapPath("~");
            try
            {
                if (sCode == "TOR")
                {
                    if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\tor\\{1}", strpathweb, strFilenamedest))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\tor\\{1}", strpathweb, strFilenamedest));}
                    File.Move(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFilename), String.Format("{0}\\app\\epurchase\\Requestor\\Document\\tor\\{1}", strpathweb, strFilenamedest));
                }
                else if (sCode == "RAB") 
                {
                    if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\rab\\{1}", strpathweb, strFilenamedest))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\rab\\{1}", strpathweb, strFilenamedest)); }
                    File.Move(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFilename), String.Format("{0}\\app\\epurchase\\Requestor\\Document\\rab\\{1}", strpathweb, strFilenamedest));
                }
                else if (sCode == "RABWP")
                {
                    if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\rab\\{1}", strpathweb, strFilenamedest))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\rab\\{1}", strpathweb, strFilenamedest)); }
                    File.Move(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFilename), String.Format("{0}\\app\\epurchase\\Requestor\\Document\\rab\\{1}", strpathweb, strFilenamedest));
                }
                else if (sCode == "OE")
                {
                    if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\oe\\{1}", strpathweb, strFilenamedest))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\oe\\{1}", strpathweb, strFilenamedest)); }
                    File.Move(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFilename), String.Format("{0}\\app\\epurchase\\Requestor\\Document\\oe\\{1}", strpathweb, strFilenamedest));
                }
                else if (sCode == "DWR")
                {
                    if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\drawing\\{1}", strpathweb, strFilenamedest))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\drawing\\{1}", strpathweb, strFilenamedest)); }
                    File.Move(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFilename), String.Format("{0}\\app\\epurchase\\Requestor\\Document\\drawing\\{1}", strpathweb, strFilenamedest));
                }
                else if (sCode == "CT")
                {
                    if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\closetender\\{1}", strpathweb, strFilenamedest))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\closetender\\{1}", strpathweb, strFilenamedest)); }
                    File.Move(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFilename), String.Format("{0}\\app\\epurchase\\Requestor\\Document\\closetender\\{1}", strpathweb, strFilenamedest));
                }
                else if (sCode == "DA")
                {
                    if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\directaward\\{1}", strpathweb, strFilenamedest))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\directaward\\{1}", strpathweb, strFilenamedest)); }
                    File.Move(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFilename), String.Format("{0}\\app\\epurchase\\Requestor\\Document\\directaward\\{1}", strpathweb, strFilenamedest));
                }
             

                isSukses = true;

            }
            catch 
            {
                isSukses= false;
            }
            if (sFilenameDest == "") { return true;}

            return isSukses;
        }
        public static  void PrintMessageInTaskBar(string sMessage, string sCaption)
        {
            Notification.Show(new NotificationConfig
            {
                Title = String.Format("<b>{0}</b>", sCaption),
                HideDelay = 5000,
                Html = String.Format("<p align='Center'>{0}</p>", sMessage)
            });
        }
        public static string ReturnstrFileUploadMoveName( string sCode, string sNoPurchaseRequest, string sFileCompare) 
        {
             string sFilename = "";
             if (sCode == "TOR") 
             {
                 if (sFileCompare != "")
                 {
                     sFilename = sNoPurchaseRequest + "TOR.pdf";
                 }
             }
             else if (sCode == "RAB") 
             {
                 if (sFileCompare != "")
                 {
                     sFilename = sNoPurchaseRequest + "RAB.pdf";
                 }
             }
             else if (sCode == "RABWP")
             {
                 if (sFileCompare != "")
                 {
                     sFilename = sNoPurchaseRequest + "RABWP.pdf";
                 }
             }
             else if (sCode == "OE")
             {
                 if (sFileCompare != "")
                 {
                     sFilename = sNoPurchaseRequest + "OE.pdf";
                 }
             }
             else if (sCode == "DWR")
             {
                 if (sFileCompare != "")
                 {
                     sFilename = sNoPurchaseRequest + "DWR.pdf";
                 }
             }
             else if (sCode == "CT")
             {
                 if (sFileCompare != "")
                 {
                     sFilename = sNoPurchaseRequest + "CT.pdf";
                 }
             }
             else if (sCode == "DA")
             {
                 if (sFileCompare != "")
                 {
                     sFilename = sNoPurchaseRequest + "DA.pdf";
                 }
             }

            return sFilename;
        }
        public static void PrintMessageInTopLeft(string sMessage, string sCaption)
        {
            Notification.Show(new NotificationConfig
            {
                Title = String.Format("<b>{0}</b>", sCaption),
                AlignCfg = new NotificationAlignConfig
                {
                    ElementAnchor = AnchorPoint.TopLeft,
                    TargetAnchor = AnchorPoint.TopLeft,
                    OffsetX = -20,
                    OffsetY = 20
                },
                HideDelay = 5000,
                Html = String.Format("<p align='Center'>{0}</p>", sMessage)
            });
        }
        public static void deleteTemporaryAttachmentFile(string sFileTOR, string sFileRAB, string sFileRABWP, string sFileDrawing, string sFileOE, string sFileCloseTender, string strDA)
        {
            string strpathweb = HttpContext.Current.Server.MapPath("~");
            if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFileTOR))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFileTOR)); }
            if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFileRAB))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFileRAB)); }
            if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFileRABWP))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFileRABWP)); }
            if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFileDrawing))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFileDrawing)); }
            if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFileOE))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFileOE)); }
            if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFileCloseTender))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, sFileCloseTender));}
            if (File.Exists(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, strDA))) { File.Delete(String.Format("{0}\\app\\epurchase\\Requestor\\Document\\temp\\{1}", strpathweb, strDA)); }
        }
        public static void  KLfile(string strFile)
       {
         if(File.Exists(strFile))
         {
            File.Delete(strFile);
         }
       
       }


        public static String ConStr()
        {
            var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ERDM"].ToString();
            return connectionString;

        }
        public static string ResolveUrl(string relativeUrl)
        {
            if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

            if (relativeUrl.Length == 0 || relativeUrl[0] == '/' || relativeUrl[0] == '\\')
                return relativeUrl;

            int idxOfScheme = relativeUrl.IndexOf(@"://", StringComparison.Ordinal);
            if (idxOfScheme != -1)
            {
                int idxOfQM = relativeUrl.IndexOf('?');
                if (idxOfQM == -1 || idxOfQM > idxOfScheme) return relativeUrl;
            }

            StringBuilder sbUrl = new StringBuilder();
            sbUrl.Append(HttpRuntime.AppDomainAppVirtualPath);
            if (sbUrl.Length == 0 || sbUrl[sbUrl.Length - 1] != '/') sbUrl.Append('/');

            // found question mark already? query string, do not touch!
            bool foundQM = false;
            bool foundSlash; // the latest char was a slash?
            if (relativeUrl.Length > 1
                && relativeUrl[0] == '~'
                && (relativeUrl[1] == '/' || relativeUrl[1] == '\\'))
            {
                relativeUrl = relativeUrl.Substring(2);
                foundSlash = true;
            }
            else foundSlash = false;
            foreach (char c in relativeUrl)
            {
                if (!foundQM)
                {
                    if (c == '?') foundQM = true;
                    else
                    {
                        if (c == '/' || c == '\\')
                        {
                            if (foundSlash) continue;
                            else
                            {
                                sbUrl.Append('/');
                                foundSlash = true;
                                continue;
                            }
                        }
                        else if (foundSlash) foundSlash = false;
                    }
                }
                sbUrl.Append(c);
            }

            return sbUrl.ToString();
        }

        public static string strNull(string strField) 
        {
            if (!string.IsNullOrEmpty(strField))
            {
                return strField;
            }
            else 
            {
                return string.Empty;
            }
        }

    }
}