﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Configuration;

namespace NewERDM.lib.Helper
{
    public class Mailer
    {
      public static bool SendEmail(string EmailTo, string EmailToDisplayName, string EmailSubject, string EmailContents, string[] EmailAttachment,bool SSL)
      {
        bool Status = false;
        try
        {
            //Step 1: Populate the Email Info into class.
            EmailInfo email = new EmailInfo() { EmailTo = new string[] { EmailTo }, EmailToDisplayName = new string[] { EmailToDisplayName }, EmailSubject = EmailSubject, EmailContents = EmailContents, EmailAttachments = EmailAttachment, SSL = SSL };
            Status = Mailer.SendMail(email);
            //Step 2: .
        }
        catch (Exception ex)
        {
            //SLFailSafe.HandleFault(ex);
        }
        finally
        {
            //TODO: Close all open files.
            //TODO: Close all open database connections.
            //TODO: Clean all unwanted data.
        }
        return Status;
      }

      public static bool SendEmail(string[] EmailTo, string[] EmailToDisplayName, string EmailSubject, string EmailContents, string[] EmailAttachment, bool SSL)
        {
            bool Status = false;
            try
            {
                //Step 1: Populate the Email Info into class.
                EmailInfo email = new EmailInfo() { EmailTo = EmailTo, EmailToDisplayName = EmailToDisplayName, EmailSubject = EmailSubject, EmailContents = EmailContents, EmailAttachments = EmailAttachment, SSL = SSL };
                Status = Mailer.SendMail(email);
                //Step 2: .
            }
            catch (Exception ex)
            {
                //SLFailSafe.HandleFault(ex);
            }
            finally
            {
                //TODO: Close all open files.
                //TODO: Close all open database connections.
                //TODO: Clean all unwanted data.
            }
            return Status;
        }

      public static bool SendEmail(string EmailFrom, string[] EmailTo, string[] EmailToDisplayName, string EmailSubject, string EmailContents, string[] EmailAttachment,bool SSL)
        {
            bool Status = false;
            try
            {
                //Step 1: Populate the Email Info into class.
                EmailInfo email = new EmailInfo() { EmailFrom = EmailFrom, EmailTo = EmailTo, EmailToDisplayName = EmailToDisplayName, EmailSubject = EmailSubject, EmailContents = EmailContents, EmailAttachments = EmailAttachment, SSL = SSL };
                Status = Mailer.SendMail(email);
                //Step 2: .
            }
            catch (Exception ex)
            {
                //SLFailSafe.HandleFault(ex);
            }
            finally
            {
                //TODO: Close all open files.
                //TODO: Close all open database connections.
                //TODO: Clean all unwanted data.
            }
            return Status;
        }

      static bool SendMail(EmailInfo EmailDetails)
        {
            bool Status = false;
            try
            {
                //Step 1: Create Mail client, Mail message, Mail addresses.
                SmtpClient MailClient = new SmtpClient();
                MailMessage Email = new MailMessage();
                MailAddress FromMailAddress = new MailAddress(EmailDetails.EmailFrom, EmailDetails.EmailDisplayName);
                MailAddress ReplyToMailAddress = new MailAddress(EmailDetails.EmailFrom, EmailDetails.EmailDisplayName);
                MailAddress[] ToMailAddress = new MailAddress[EmailDetails.EmailTo.Length];

                //Step 2: Set the Mail client credentials and enable SSL.
                MailClient.Host = ConfigurationManager.AppSettings["EmailHost"];
                MailClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["EmailPort"]);
                MailClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailUsername"], ConfigurationManager.AppSettings["EmailPassword"]);
                MailClient.EnableSsl = false;

                //Step 3: Set all Email options.
                Email.From = FromMailAddress;
                Email.ReplyTo = ReplyToMailAddress;
                Email.Subject = EmailDetails.EmailSubject;
                Email.Body = EmailDetails.EmailContents;
                Email.IsBodyHtml = true;
                for (int i = 0; i < ToMailAddress.Length; i++)
                {
                    string EmailDisplayName = string.IsNullOrEmpty(EmailDetails.EmailToDisplayName[i]) ? "Recepient " + i.ToString() : EmailDetails.EmailToDisplayName[i];
                    ToMailAddress[i] = new MailAddress(EmailDetails.EmailTo[i], EmailDisplayName);
                    Email.To.Add(ToMailAddress[i]);
                }

                //Step 4: Set all the attachments.
                if (EmailDetails.EmailAttachments!=null)
                {
                    Attachment EmailAttachment = null;
                    for (int i = 0; i < EmailDetails.EmailAttachments.Length; i++)
                    {
                        EmailAttachment = new Attachment(EmailDetails.EmailAttachments[i]);
                        Email.Attachments.Add(EmailAttachment);
                    }
                }

                //Step 5: Send Email.
                MailClient.Send(Email);
                Status = true;
            }
            catch (Exception ex)
            {
                //SLFailSafe.HandleFault(ex);
            }
            finally
            {
                //TODO: Close all open files.
                //TODO: Close all open database connections.
                //TODO: Clean all unwanted data.
            }


            return Status;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EmailInfo
    {
        public string EmailFrom = ConfigurationManager.AppSettings["EmailFrom"];
        public string EmailDisplayName = "ERDM SYSTEMS";
        public string ReplyEmail = "erdm.admin@enj.co.id";
        public string ReplyEmailDisplayName = "Reply To";
        public string[] EmailTo;
        public string[] EmailToDisplayName;
        public string EmailSubject;
        public string EmailContents;
        public string[] EmailAttachments;
        public bool SSL = true;
        public bool TLS = false;
    }
}